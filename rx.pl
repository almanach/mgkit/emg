/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: rx.pl 1623 2011-12-12 15:55:52Z clerger $
 * Copyright (C) 2004, 2009, 2011, 2012 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  rx.pl -- DyALog Regular Expressions API
 *
 * ----------------------------------------------------------------
 * Description
 *   Inspired frm Van Noord package rx
 *   http://odur.let.rug.nl/~vannoord/prolog-rx/
 * ----------------------------------------------------------------
 */

:-module(rx).

:-features(rx, [pattern,extended_flag,newline_flag,icase_flag,nosub_flag,compiled] ).
:-features(rx!match, [string, substrings, rx, noteol, notbol, subs_flag]).


:-std_prolog rx!tokenize/3.
:-std_prolog rx!tokenize/4.
:-std_prolog rx!choice_tokenize/3.
:-std_prolog rx!bounded_tokenize/4.
:-std_prolog rx!bounded_tokenize/5.

:-op(  700, xfx, [?=]). % for default value
:-xcompiler(( X ?= V :- (X=V xor true))). %% Setting a default value

:-std_prolog rx!comp/1.

rx!comp( rx{ pattern => Pattern,
	     extended_flag => Extended_Flag,
	     newline_flag => NewLine_Flag,
	     icase_flag => ICase_Flag,
	     nosub_flag => NoSub_Flag,
	     compiled => RX } ) :-
	Extended_Flag ?= 0,
	NewLine_Flag ?= 0,
	ICase_Flag ?= 0,
	NoSub_Flag ?= 0,
	'$interface'( regcomp_pl( Pattern:string,
				  Extended_Flag:int,
				  NewLine_Flag:int,
				  ICase_Flag:int,
				  NoSub_Flag:int ),
		      [ return( RX:ptr ) ] )
	.

:-std_prolog rx!match{}.

rx!match{ string => String,
	  rx => RX::rx{ compiled => RE},
	  noteol => NotEol_Flag,
	  notbol => NotBol_Flag,
	  subs_flag => Subs_Flag,
	  substrings => SubStrings
	} :-
	NotEol_Flag ?= 0,
	NotBol_Flag ?= 0,
	Subs_Flag ?= 1,
	( var(RE) ->
	    rx!comp( RX )
	;   
	    true
	),
	'$interface'( regexec_pl( String: string,
				  NotBol_Flag: int,
				  NotEol_Flag: int,
				  RE: ptr,
				  Subs_Flag: int,
				  SubStrings: term ),
		      [ return(true:bool) ] )
	.

rx!free(rx{ compiled => RE}) :- '$interface'( regfree_pl( RE: ptr ), [ ] ).

rx!tokenize(String,Delims,SubStrings) :-
	rx!tokenize(String,Delims,[],SubStrings).

rx!bounded_tokenize(String,Delims,SubStrings,Bound) :-
	rx!bounded_tokenize(String,Delims,[],SubStrings,Bound).

rx!tokenize(String,Delims,Last,SubStrings) :-
	'$interface'( 'DyALog_strtok'( String: string,
				       Delims: string,
				       Last:term,
				       SubStrings: term ), [] ).

rx!bounded_tokenize(String,Delims,Last,SubStrings,Bound) :-
	'$interface'( 'DyALog_bounded_strtok'( String: string,
					       Delims: string,
					       Last:term,
					       SubStrings: term,
					       Bound: int), [] ).


rx!choice_tokenize(String,Delims,Comp):-
	'$interface'( 'DyALog_choice_strtok'( String: string,
					      Delims: string,
					      Comp: -string ),
		      [choice_size(1)])
	.
