/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2011, 2012 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  disamb.pl -- Disamboguisation
 *
 * ----------------------------------------------------------------
 * Description
 * Disambiguation of EMG output, integrated with the parser
 *  The possible converted formats are EASy, Passage, and CONLL
 *
 *  The disambiguation process is a 1-best algorithm, summing
 *       - edge costs (independant of derivations)
 *       - and (regional) node costs (attached to derivations)
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.

:-require 'format.pl'.
:-require 'forest.pl'.

:-op(  700, xfx, [?=]). % for default value

:-xcompiler
X ?= V :- (X=V xor true). %% Setting a default value

:-op(  800, xfx, [::=]).

:-import{ module=> xml,
	  file => 'libdyalogxml.pl',
	  preds => [
		    event_handler/5,
		    event_process/3,
		    event_process/4,
		    event_ctx/2,
		    event_super_handler/2,
		    read_event/3,
		    attribute_handler/3,
		    attribute_dyalog_handler/4
		   ]
         }.


:-import{ module=> sqlite,
	  file => 'libdyalogsqlite.pl'
	}.

%% To undo potential presence of -parse in compile flags
%% important to be in this mode to parse the list of options to easyforest
%% :-parse_mode(list).

:-require('rx.pl').

:-features(ctx,[class,node,constraint,current]).

:-features(cluster, [id,left,right,lex,token] ).
:-features(pcluster, [id,left,right,lex,token,fids] ).
:-features(node, [id,cat,cluster,tree,lemma,form,xcat,deriv,w,lemmaid] ).
:-features(edge, [ id,
		   type,
		   source,
		   target,
		   label,
		   deriv
		 ]
	  ).
:-features(edgederiv,[eid,target_op,target_node,source_op,span,reroot,derivs]).
:-features(op,[id,cat,deriv,span,top,bot]).
:-features(hypertag,[id,deriv,ht]).

:-features(f,[cid,id,lex,rank]).
:-features(groupe, [id,type,content,left,right] ).
:-features(relation, [id,type,arg1,arg2,arg3] ).

:-features(dstruct,[node,w,span,deriv,children,oid,constraint]).
:-features(deriv,[node,span,op,ht]).

:-extensional
	x/1,
	dlist/2,
	cluster{},
	node{},
	edge{},
	edgederiv{},
	
	f{},
	groupe{},
	op{},
	hypertag{},
	sentence/1,
	used/1,
	opt/1,
	extra_edge/2,
	sourceop/3,
	targetop/3,
	span/3,
%	span/2,
	edge2sop/2,
	edge2top/2,
	edge2ops/3,
	cat_abstract/2,
	node2op/2,
	node_op2deriv/3,
	extra_elem_cost/2,
	extra_node_cost/2,
	extra_chunk_cost/4,

	op2node/2,

	rule_no_cost/1
	
	.

:-xcompiler
span(NId,Span) :-
	node2op(NId,OId),
	op{ id => OId, span => Span }
	.

%%:-rec_prolog relation{}.
:-light_tabular relation{}.
:-mode(relation{},[+|-]).

:-subset( nominal, cat[nc,cln,pro,np,pri,prel,ncpred,xpro,ce,ilimp,caimp] ).
:-subset( xnominal, cat[nc,cln,pro,np,pri,prel,ncpred,adj,xpro,ce,ilimp,caimp] ).

:-subset( terminal, cat[~ [-,'CS','N','N2','PP','S','V']] ).

:-finite_set(pos,[left,right,xleft,xright,in,out,xin,xout]).

:-finite_set(const,['GN','GA','GP','NV','GR','PV']).

:-finite_set(rel,[ 'SUJ-V',	% sujet-verbe
		   'AUX-V',	% auxiliaire-verbe
		   'COD-V',	% cod-verbe
		   'CPL-V',	% complement-verbe
		   'MOD-V',	% modifier verbe
		   'COMP',	% complementeur: csu->NV, prep->(GN,NV)
		   'ATB-SO',	% attribut sujet/objet  
		   'MOD-N',	% modifier nom
		   'MOD-A',	% modifier adjectif
		   'MOD-R',	% modifier adverbe
		   'MOD-P',	% modifier preposition
		   'COORD',	% coordination
		   'APPOS',	% apposition
		   'JUXT'	% juxtaposition
		   ]).

:-finite_set(label,[ subject,impsubj,
		     object,arg,preparg,scomp,comp,xcomp,
		     'Infl','V1','V',v,'vmod',
		     cla,cld,clg,cll,clr,clneg,
		     void,coo,coo2,coord2,coord3,coord,
		     det,det1, incise,en,
		     nc,np,number,number2,ncpred,
		     'CS','Modifier',
		     'N','N2','N2app','Nc2','Np2',
		     'PP',prep,csu,
		     prel,pri,pro,'Root','Punct',
		     'S','S2','SRel','N2Rel','SubS',varg,
		     start,wh,starter,advneg,aux,ce,
		     predet_ante,predet_post,
		     'Monsieur',adjP, person_mod, time_mod, audience, reference,
		     'S_incise', pas, adj,quoted_S,quoted_N2,quoted_PP,quantity,
		     supermod, 'ExtraWPunct','ExtraSPunct', position
		     ]).

:-finite_set(quoted,[double_quoted,chevron_quote,simple_quoted,plus_quoted,quoted_as_N2,
		     quoted_sentence_as_post_mod,
		     quoted_sentence_as_ante_mod
		    ]).

:-finite_set(edge_kind,[adj,subst,lexical,virtual,epsilon]).

:-finite_set(fkind,[scomp,prepscomp,vcomp,prepvcomp,obj,whcomp,prepobj,acomp,vcompcaus,sadv,subj,
		    prepwhcomp]).
:-finite_set(args,[arg0,arg1,arg2]).

:-finite_set(unknown,['uw','_Uw']).
:-finite_set(true_time,[arto,artf,arti]).
:-finite_set(date,['_DATE_arto','_DATE_artf','_DATE_year']).

:-finite_set(entities,['_PERSON',
		       '_PERSON_m',
		       '_PERSON_f',
		       '_PRODUCT',
		       '_ORGANIZATION',
		       '_COMPANY',
		       '_NP',
		       '_NP_WITH_INITIALS',
		       '_LOCATION',
		       '_NUMBER',
		       '_NUM',
		       '_ROMNUM'
		      ]).

:-subset(number,entities['_NUMBER','_NUM','_ROMNUM']).

:-finite_set(parse_mode,[full,corrected,robust]).

:-finite_set(de_form,[de,du,des,'de la','de l''']).

:-xcompiler
xml!wrapper(Handler,Ctx,Name,Attr,G) :-
	event_process(Handler,start_element{ name => Name, attributes => Attr },Ctx),
	G,
	event_process(Handler,end_element{ name => Name },Ctx)
	.

:-xcompiler
xml!text(Handler,Ctx,Text) :-
	event_process(Handler,characters{ value => Text }, Ctx )
	.

:-xcompiler
once(G) :- (G xor fail).

:-extensional source2edge/2.

:-xcompiler
source2edge(Edge::edge{ source => node{ id => NId} }) :- source2edge(NId,Edge).

:-extensional target2edge/2.

:-xcompiler
target2edge(Edge::edge{ target => node{ id => NId }}) :- target2edge(NId,Edge).

:-extensional sourcecluster2edge/2.

:-xcompiler
sourcecluster2edge(Edge::edge{ source => node{ cluster => C} }) :- sourcecluster2edge(C,Edge).

:-extensional targetcluster2edge/2.

:-xcompiler
targetcluster2edge(Edge::edge{ target => node{ cluster => C} }) :- targetcluster2edge(C,Edge).

:-xcompiler
precedes( N1::node{},
	  N2::node{}
	) :-
	N1=node{ cluster=> cluster{ right => Right}},
	N2=node{ cluster => cluster{ left => Left}},
	Right =< Left
	.

%:-op(700,xfy, [:>,<:]).
:-op(700,xfy, [>>,<<]).

/*
  We can write complex query expression with chain using a compact notation

  Exp := Node >> ForwardPathExp
       | Node << BackwardPathExp

  XExp := Node
       |  Exp

  ForwardPathExp := EdgeExp >> XExp
                  | ( ForwardPathExp & ForwardPathExp)
                  | ( ForwardPathExp ; ForwardPathExp)
                  | ( ForwardPathExp xor ForwardPathExp)
                  | ( \+ ForwardPathExp )

  BackwardPathExp := EdgeExp >> XExp
                  | ( BackwardPathExp & BackwardPathExp )
                  | ( BackwardPathExp ; BackwardPathExp )
                  | ( BackwardPathExp xor BackwardPathExp)
                  | ( \+ BackwardPathExp )

  EdgeExp :=  edge{}
           |  edge_kind
           |  edge_kind @ edge_label
           |  ind( EdgeExp )
           |  (EdgeExp @*)
           |  { EdgeExp ; EdgeExp ... }

  Node := node{}
  
*/

:-toplevel_clause
chain(In,Out) :-
    ( In = (A >> (E & Rest)) ->
	chain(A >> E,XE),
	chain(A >> Rest,XRest),
	Out = (XE,XRest)
    ;	In = (A >> (E ; Rest)) ->
	chain(A >> E,XE),
	chain(A >> Rest,XRest),
	Out = (XE ; XRest)
    ;	In = (A >> (E xor Rest)) ->
	chain(A >> E,XE),
	chain(A >> Rest,XRest),
	Out = (XE xor XRest)
    ;   In = (A >> (\+ E)) ->
	chain(A >> E,XE),
	Out = (\+ XE)
    ;  In = (A << (E & Rest)) ->
	chain(A << E,XE),
	chain(A << Rest,XRest),
	Out = (XE,XRest)
    ;	In = (A << (E ; Rest)) ->
	chain(A << E,XE),
	chain(A << Rest,XRest),
	Out = (XE ; XRest)
    ;	In = (A << (E xor Rest)) ->
	chain(A << E,XE),
	chain(A << Rest,XRest),
	Out = (XE xor XRest)
    ;	In = (A << (\+ E)) ->
	chain(A << E,XE),
	Out = (\+ XE)
    ;
	( In = (A >> E >> In2) 
	  ->
%	  format('xhere E=~w\n',[E]),
	    (
%	     \+ var(E),
	     E = '$head' 
	    ->
%	     format('*** chain expansion of head G=~w\n',[E]),
	     G = (get_head(A,B)),
	     true
	    ; 
%	     \+ var(E),
		E = {E1;Rest} ->
		chain( A >> E1 >> B, G1),
		chain( A >> { Rest } >> B, G2),
		G = (G1;G2)
	    ;	% \+ var(E),
		E = {E1} ->
		chain( A >> E1 >> B, G)
	    ;	% \+ var(E),
		E = ind(E1) ->
		chain( A >> adj >> node{} >> E1 >> B, G )
	    ;	% \+ var(E),
		E = (E1 @*) ->
		chain( _A::node{} >> E1 >> _B::node{}, G1),
		G = ( @*{ goal => G1,
			  collect_first => [A],
			  collect_last => [B],
			  collect_loop => [_A],
			  collect_next => [_B]
			}
		    )
	    ;
		(   E=edge{ id => EId,
			    type => Type,
			    label => Label
			  }
		xor E = Type @ Label
		xor E = Type
		),
		G=source2edge(
			      edge{ source => A::node{},
				    target => B::node{},
				    id => EId,
				    type => Type,
				    label => Label
				  }
			     )
	    )
	; In = (A << E << In2) ->
	    (	% \+ var(E),
		E = {E1;Rest} ->
		chain( A << E1 << B, G1),
		chain( A << {Rest} << B, G2),
		G = (G1;G2)
	    ;	% \+ var(E),
		E = {E1} ->
		chain( A << E1 << B, G)
	    ;	% \+ var(E),
		E = ind(E1) ->
		chain( A << E1 << node{} << adj << B, G )
	    ;	% \+ var(E),
		E = (E1 @*) ->
		chain( _A::node{} << E1 << _B::node{}, G1),
		G = ( @*{ goal => G1,
			  collect_first => [A],
			  collect_last => [B],
			  collect_loop => [_A],
			  collect_next => [_B]
			}
		    )
	    ;
		(   E=edge{ id => EId,
			    type => Type,
			    label => Label
			  }
		xor E= Type @ Label 
		xor E= Type
		),
		G=target2edge(
			      edge{ target => A,
				    source => B,
				    id => EId,
				    type => Type,
				    label => Label
				  }
			     )
	    )
	;
	  format('*** Chain Expansion problem: chain(~w)\n',[In]),
	  fail
	),
	( In2 = B ->
	    Out = G
	;   In2 = (B >> _) ->
	    chain(In2,Out2),
	    Out = (G,Out2)
	;   In2 = (B << _),
	    chain(In2,Out2),
	    Out = (G,Out2)
	)
        )
.

:-toplevel_clause
term_expand(chain(In),Out) :-
	chain(In,Out),
%	format('term expansion ~w\n',[Out]),
	true
.

:-extensional agglutinate/3.

agglutinate('�','le__det','au').
agglutinate('�','les__det','aux').
agglutinate('�','le','au').
agglutinate('�','les','aux').
agglutinate('�','lequel','auquel').
agglutinate('�','lesquels','auxquels').
agglutinate('�','lesquelles','auxquelles').
agglutinate('de','le__det','du').
agglutinate('de','le','du').
agglutinate('de','lequel','duquel').
agglutinate('de','lesquels','desquels').
agglutinate('de','lequelles','desquelles').
agglutinate('de','les__det','des').
agglutinate('en','les__det','�s').
agglutinate('de','les','des').
agglutinate('en','les','�s').
agglutinate('�','ledit','audit').
agglutinate('�','lesdits','auxdits').
agglutinate('�','lesdites','audites').
agglutinate('de','ledit','dudit').
agglutinate('de','lesdits','dudits').
agglutinate('de','lesdites','auxdites').

%% Special case of _SENT_BOUND introduced by sxpipe
:-finite_set(sbound,['*','-','_','+']).
agglutinate(X::sbound[],'_SENT_BOUND',X).

:-light_tabular part_of_agglutinate/2.
:-mode(part_of_agglutinate/2,+(-,-)).

part_of_agglutinate(Form,Token) :- agglutinate(Form,_,Token).
part_of_agglutinate(Form,Token) :- agglutinate(_,Form,Token).

:-finite_set(quepro,['que?','queComp?']).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parse command line options

:-std_prolog parse_options/1.

parse_options(Options) :-
        ( Options == [] -> true
	;
	  ( Options=['-e',Id|Rest] -> record( sentence(Id) )
	  ; Options = ['-verbose'|Rest] -> record_without_doublon( opt(verbose) )
	  ; Options=  ['-restrictions',File|Rest] ->
	    record_without_doublon( opt(restrictions(File)) )
	  ;   Options = ['-nodis'|Rest] ->
%%	    record_without_doublon(opt(depxml)),
	    record_without_doublon(opt(nodis))
	  ;   Options = ['-depxml'|Rest ] ->
	    record_without_doublon(opt(depxml)) 
	  ;   Options = ['-xmldep'|Rest] ->
	    record_without_doublon(opt(depxml))
	  ;   Options = ['-nodis'|Rest] ->
	    record_without_doublon(opt(nodis))
	  ;   Options = ['-passage'|Rest] ->
	    record_without_doublon(opt(passage))
	  ;   Options = ['-conll'|Rest] ->
	    record_without_doublon(opt(conll))
	  ;   Options = ['-easy'|Rest] ->
	    record_without_doublon(opt(easy))
	  ;   Options = ['-dstats'|Rest] ->
	    record_without_doublon(opt(dstats))
	  ;   Options = ['-conll_verbose'|Rest] ->
	    record_without_doublon(opt(conll_verbose))
	  ;   Options = ['-weights'|Rest] ->
	    record_without_doublon(opt(weights))
	  ;   Options = ['-stop',Stop|Rest] ->
	    record_without_doublon(opt(stop(Stop)))
	  ;   Options = ['-cost'|Rest] ->
	    record_without_doublon(opt(cost))
%	  ;   Options = ['-lpreader'|Rest] ->
%	    record_without_doublon(opt(lpreader))
	  ; Options = ['-multi'|Rest] ->
	    record_without_doublon(opt(multi))
	  ;   Options = ['-nocompound'|Rest] ->
	    record_without_doublon(opt(nocompound))
	  ;   Options = ['-eid',EId,XW|Rest] ->
	    atom_number(XW,W),
	    record_without_doublon(extra_elem_cost(edge{ id => EId},W))
	  ;   Options = ['-edge',XSLemma,XSCat,XLabel,XTLemma,XTCat,XW|Rest] ->
	    atom_number(XW,W),
	    ( XSLemma == '_' xor SLemma = XSLemma ),
	    ( XSCat == '_' xor SCat = XSCat ),
	    ( XTLemma == '_' xor TLemma = XTLemma ),
	    ( XTCat == '_' xor TCat = XTCat ),
	    ( XLabel == '_' xor Label = XLabel),
	    record( extra_elem_cost( edge{ source => node{ lemma => SLemma, cat => SCat },
					   target => node{ lemma => TLemma, cat => TCat },
					   label => Label
					 },
				     W
				   ))
	  ;   Options = ['-xedge',XSource,XTarget,XW|Rest] ->
	    atom_number(XW,W),
	    atom_number(XSource,Source),
	    atom_number(XTarget,Target),
	    record( extra_elem_cost( edge{ source => node{ cluster => cluster{ left => Source }},
					   target => node{ cluster => cluster{ left => Target }}
					 },
				     W
				   ))
	  ;   Options = ['-node',XLemma,XCat,XLeft,XRight,XW|Rest] ->
	    atom_number(XW,W),
	    (	  XLemma == '_' xor Lemma = XLemma ),
	    (	  XCat == '_' xor Cat = XCat ),
	    (	  XLeft == '_' xor atom_number(XLeft,Left) ),
	    (	  XRight == '_' xor atom_number(XRight,Right) ),
	    record( extra_node_cost( node{ lemma => Lemma, cat => Cat, cluster => cluster{ left => Left, right => Right} },
				     W
				   ))
	  ;   Options = ['-chunk',XChunkType,XLeft,XRight,XW|Rest] ->
	    atom_number(XW,W),
	    (	  XChunkType == '_' xor ChunkType = XChunkType ),
	    (	  XLeft == '_' xor atom_number(XLeft,Left) ),
	    (	  XRight == '_' xor atom_number(XRight,Right) ),
	    record( extra_chunk_cost( Left,
				      Right,
				      ChunkType,
				      W
				    ))
	  ; Options = ['-transform'|Rest] -> record_without_doublon(opt(transform)) % edge rewriting
	  ; Options = ['-res',File|Rest] -> read_dbfile(File)
	  ; Options = [ParserOption|Rest] ->
	    true
%%	    domain(ParserOption,['-robust','-no_lctag','-nocorrect','-stats','-disamb']) -> true
	  ;  fail
	  ),
	  parse_options(Rest)
        )
        .

:-std_prolog show_time/1.

show_time(Step) :-
	( (Step==latency xor opt(multi)) ->
	  utime(Time),
	  ( recorded(utime(M)) ->
	    mutable_read(M,OldTime),
	    Delta is Time - OldTime,
	    fast_mutable(M,Time)
	  ;
	    mutable(M,Time),
	    record(utime(M)),
	    Delta = Time
	  ),
	  format('<~w_time> ~wms\n',[Step,Delta])
	;
	  true
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Disamb

:-xcompiler
process_options :-	
	argv(Options),
	%%	parse_options(Options),
	( recorded(parsed_options)
	xor
	parse_options(Options),
	  record( parsed_options)
	),
	persistent!add(parsed_options),
	%% the following avoid reparsing options at each loop iteration
	%% using persistent facts
	persistent!add_fact(opt(_)),
	persistent!add_fact(use_feature_cost),
	persistent!add_fact(feature_cost_table(_)),
	persistent!add_fact(feature_cost_prepare(_,_)),
	persistent!add_fact(use_cluster_feature),
	persistent!add_fact(cluster_feature_table(_)),
	persistent!add_fact(cluster_feature_prepare(_)),
	every(( recorded(opt(restrictions(File))),
		verbose('open restriction database ~w\n',[File]),
		sqlite!open(File,DB),
		record_without_doublon( opt(restrictions,DB) ),
		prepare_restriction(_)
	      )),
	true
	.

:-xcompiler
persistent!add_fact(Fact) :-
	every(( recorded(Fact),
		persistent!add(Fact),
		true
	      ))
	.

:-xcompiler
loop_reset :-
	'$interface'('Easyforest_Reset_DStruct', [return(none)]),
	true
	.

disamb :-
	wait,
	%% deal with disamb specific options
	%%
	loop_reset,
	process_options,
	show_time(parsing),
	(   sentence(_) xor (recorded('S'(SId)) xor SId=1), record( sentence(SId) ) ),
	%% extract the derivation forest and convert it to a dependency forest
	disamb_reader,
	abolish(answer_extract_deriv/3),
	abolish(info_derivs/2),
	abolish(op2node/2),
	erase_relation(deriv_process(_,_,_)),
	show_time(extract),
	\+ opt(stop(reader)),
	(\+ opt(verbose) xor
	    every(( C::cluster{}, format('Cluster ~E\n',[C]) )),
	    every(( N::node{}, format('Node ~E\n',[N]) )),
	    every(( E::edge{}, format('Edge ~E\n',[E]) ))
	),
	( opt(dstats) -> dstats_emit, show_time(dstats) ; true ),
	( opt(nodis) -> depxml_emit(nodis), show_time(nodis) ; true ),
	easy_disamb_info,
	( opt(nodis)
	xor
	every(( E::edge{ id => EId, source => Source },
		( node!empty(Source),
		  edge{ id => _EId, type => adj, target => Source },
		  Constraints = _EId
		;
		  Constraints = []
		),
%		format('process ~w\n',[EId]),
		edge_cost(EId,_,_,Constraints),
%		format('done process ~w\n',[EId]),
		true
	      )),
	 \+ opt(stop(cost)),
	 wait((convert))
	),
	show_time(disamb),
	\+ opt(stop(dis)),
	record(disambiguated),
	(  opt(depxml), depxml_emit(disamb), show_time(depxml)
	; opt(conll), conll_emit, show_time(conll)
	; opt(passage), passage_emit, show_time(passage), abolish(emitted/1)
	; opt(easy), easy_emit, show_time(easy), abolish(emitted/1)
	),
	fail
	.

:-extensional disamb_node/6.

disamb_node(T (Left,_),T,Token,Lex,Lemma,LemmaId).
disamb_node(T (Left,_) * _ (_,_),T,Token,Lex,Lemma,LemmaId).
disamb_node(verbose!anchor(Token,Left,Right,_,T,(_ : [Lemma,Lex,LemmaId]),_),T,Token,Lex,Lemma,LemmaId).
disamb_node(verbose!coanchor(Token,Left,Right,T,[Lemma,Lex,LemmaId]),T,Token,Lex,Lemma,LemmaId).
disamb_node(verbose!lexical([Token],Left,Right,_Cat,[Lemma,Lex,LemmaId]),'_',Token,Lex,Lemma,LemmaId).
disamb_node(verbose!epsilon(Token,Left,Right,[Lemma,Lex,LemmaId]),'_',Token,Lex,Lemma,LemmaId).
disamb_node(pseudo(Left,_,T),T,'','','','').

:-light_tabular live_op/1.
:-mode(live_op/1,+(+)).

live_op(TOP) :-
	( recorded(root_op(TOP))
	;
	  recorded( top2sop(TOP,SOP) ),
	  live_op(SOP)
	),
%	format('live op ~w\n',[SOP]),
	true
	.

:-xcompiler
disamb_reader :-
	%% find all roots in the shared derivation forest
	%% and recursively extract all derivations
	%% cluster are created during the process
	%% all nodes, op, edges and hypertags are registered but not yet created (waiting to know all derivs)
	every((
	       recorded( O::'*ANSWER*'{}, Addr),
	       forest_root(Addr)
	      )),
	%% create all nodes, with attached derivs
	every((  ( recorded(node2object(NId,Add,CId)),
		   recorded(_O,Add),
		   ( tab_item_term(_O,Term) xor Term=_O)
		 ;
		   recorded(pseudonode2object(NId,Term,CId))
		 ),
		 ( recorded(node2xcat(NId,XCat)) xor XCat = ''),
		 ( recorded(node2tree(NId,Tree)) xor Tree = lexical),
		 disamb_node(Term,T,Token,Lex,Lemma,LemmaId),
		 term2cat(T,Cat),
		 ( Tree == lexical ->
		   Derivs = [],
		   recorded( terminal_op(NId,OId) ),
		   live_op(OId)
		 ;
		   mutable(MDeriv,[],true),
		   every(( node_op2deriv(NId,OId,DId),
			   live_op(OId),
			   mutable_list_extend(MDeriv,DId)
			 )),
		   mutable_read(MDeriv,Derivs),
		   Derivs = [_|_]
		 ),
		 C::cluster{ id => CId },
		 record_without_doublon(live_cluster(CId)),
		 Token ?= '',
		 Lemma ?= '',
		 LemmaId ?= Lemma,
		 record( N::node{ id => NId,
				  cat => Cat,
				  cluster => C,
				  tree => Tree,
				  lemma => Lemma,
				  form => Token,
				  xcat => XCat,
				  lemmaid => LemmaId,
				  w => [],
				  deriv => Derivs
				}
		       ),
%%		 format('node ~w\n',[N]),
		 true
	      )),
	every(( C::cluster{ id => CId },
		\+ recorded( live_cluster(CId) ),
		erase(C)
	      )),
	%% create all edges, with attached derivs
%	format('process edges\n',[]),
	every(( '$answers'( register_edge(SId,TId,Type,Label,EId,MDerivs) ),
%		format('process edge ~w\n',[EId]),
		mutable_read(MDerivs,Derivs),
		Source::node{ id => SId, cluster => SourceCluster },
		Target::node{ id => TId, cluster => TargetCluster },
		once(( recorded(edge2sop(EId,SOP)), live_op(SOP) )),
		record( E::edge{ id => EId,
				 type => Type,
				 label => Label,
				 source => Source,
				 target => Target,
				 deriv => Derivs
			       }
		      ),
		record( source2edge(SId,E) ),
		%%	record( sourcecluster2edge(SourceCluster,Edge) ),
		record( target2edge(TId,E) ),
		record( targetcluster2edge(TargetCluster,E) ),
%		format('Edge ~w\n',[E]),
		true
	      )),
	%% create all op, with attached derivs
%	format('process ops\n',[]),
	every(( '$answers'( register_op(Add,OId,MDerivs) ),
		live_op(OId),
%		format('process op ~w\n',[OId]),
		mutable_read(MDerivs,Derivs),
		recorded(_O,Add),
		( tab_item_term(_O,Term) xor Term=_O),
		( Term = Top (Left,Right) -> Bot = [], Span = [Left,Right]
		; Term = Top (Left,Right) * Bot (Left2,Right2) -> Span = [Left,Right,Left2,Right2]
		; Term = verbose!anchor(Token,Left,Right,_,Top,(_ : [Lemma,Lex,LemmaId]),_) -> Bot = [], Span = [Left,Right]
		; Term = verbose!coanchor(Token,Left,Right,Top,[Lemma,Lex,LemmaId]) -> Tree = lexical, Bot = [], Span = [Left,Right]
		; Term = verbose!lexical([Token],Left,Right,_,[Lemma,Lex,LemmaId]) ->
		  Tree = lexical, Span = [Left,Right], Top = [], Bot = []
		; Term = verbose!epsilon(Token,Left,Right,[Lemma,Lex,LemmaId]) -> Top = [], Bot = [], Span = [Left,Right]
		),
		term2cat(Top,Cat),
		( Span = [gen_pos(XL1,XL2),gen_pos(XR1,XR2)] ->
		  XSpan = [XL1,XR1]
		; Span = [gen_pos(XLeft,_),gen_pos(XRight,_),gen_pos(XLeft2,_),gen_pos(XRight2,_)] ->
		  XSpan = [XLeft,XRight,XLeft2,XRight2]
		; Span = XSpan
		),
		record(Op::op{ id => OId,
			       cat => Cat,
			       deriv => Derivs,
			       span => XSpan,
			       top => Top,
			       bot => Bot
			      }
		      ),
%		format('Op ~w\n',[Op]),
		true
	      )),
	%% create all hypertags, with attached derivs
%	format('process hypertags\n',[]),
	every(( '$answers'( register_ht(Add,HId,MDerivs) ),
		mutable_read(MDerivs,Derivs),
		recorded( _O, Add ),
		( tab_item_term(_O,HTAG::verbose!struct(_,_HT)) xor HTAG=_O),
		record( HT::hypertag{ id => HId,
				      deriv => Derivs,
				      ht => _HT
				    }
		      ),
%		format('Hypertag ~w\n',[HT]),
		true
	      )),
	%% set mode
	( recorded(robust) ->
	  record( mode(robust) )
	; recorded( added_correction ) ->
	  record( mode(corrected) )
	;
	  record( mode(full) )
	)
 .

:-std_prolog forest_follow_indirect/2.

forest_follow_indirect(Forest,XForest) :-
	( Forest = indirect(_Forest) -> forest_follow_indirect(_Forest,XForest)
	; Forest = _ : indirect(_Forest) -> forest_follow_indirect(_Forest,XForest)
	; Forest = XForest
	),
%	format('deref forest ~w => ~w\n',[Forest,XForest]),
	true
	.

:-xcompiler
forest!true_forest(Add,Forest) :-
%	format('process add=~w\n',[Add]),
	forest!forest(Add,_Forest),
%	format('=>got ~w\n',[_Forest]),
	forest_follow_indirect(_Forest,Forest)
	.

:-std_prolog forest_root/1.

forest_root(Add) :-
	\+ Add == 0,
%	format('entering forest ~w\n',[Add]),
	forest!true_forest(Add,Forest),
%	format('forest ~w => ~w\n',[Add,Forest]),
	( domain(Forest,[void,call,init]) ->
	  fail
	; Forest = and(Add_X,Y) ->
	  (   Y = LY:Add_Y xor Y=Add_Y, LY=[] ),
	  (  % format('add_x=~w\n',[Add_X]),
	     forest_root(Add_X)
	  ;  % format('tmp1 ~w\n',[Add_Y]),
	     \+ Add_Y == 0,
	     recorded(O,Add_Y),
	     % format('tmp add_y=~w => ~w\n',[Add_Y,O]),
	     ( tab_item_term(O,T:: K(Left,Right) ) ->
%	       format('potential root ~w add=~w\n',[T,Add_Y]),
	       deriv_process(Add_Y,T,[Left,Right]),
	       every(( '$answers'(register_op(Add_Y,OId,_)),
	%	       format('Root ~w => ~w\n',[T,OId]),
		       record_without_doublon(root_op(OId))
		     )),
	       true
	     ;
%	       format('add_y=~w O=~w\n',[Add_Y,O]),
	       forest_root(Add_Y)
	     )
	  )
	;
	  fail
	)
	.

:-xcompiler
mutable_min(M,V) :-
	'$interface'('DyALog_Mutable_Min'(M:ptr,V:int),[return(none)])
	.

:-xcompiler
mutable_max(M,V) :-
	'$interface'('DyALog_Mutable_Max'(M:ptr,V:int),[return(none)])
	.


:-xcompiler
mutable_check_min(M,V) :-
	'$interface'('DyALog_Mutable_Check_Min'(M:ptr,V:int),[])
	.

:-xcompiler
mutable_check_max(M,V) :-
	'$interface'('DyALog_Mutable_Check_Max'(M:ptr,V:int),[])
	.


:-extensional extract_deriv_handler/3.

extract_deriv_handler(K (Left,Right), op, edge(subst,Label,[Left,Right],Info,Add_Y,_)).
extract_deriv_handler(K1 (Left1,Right1) * K2 (Left2,Right2), op, edge(adj,Label,[Left1,Right1,Left2,Right2],Info,Add_Y,_)).
extract_deriv_handler(Term::verbose!anchor(_,Left,Right,_,_,_,_), node, edge(anchor,Label,[Left,Right],Info,Add_Y,_)).
extract_deriv_handler(Term::verbose!coanchor(_,Left,Right,_,_), single_node, edge(lexical,Label,[Left,Right],Info,Add_Y,_)).
extract_deriv_handler(Term::verbose!lexical(_,Left,Right,_,_), single_node, edge(lexical,Label,[Left,Right],Info,Add_Y,_)).
extract_deriv_handler(Term::verbose!epsilon(_,Left,Right,_), single_node, edge(lexical,Label,[Left,Right],Info,Add_Y,_)).
extract_deriv_handler(Term::verbose!struct(_,_), default, edge(hypertag,Label,[],Add_Y,Add_Y,_)).


:-rec_prolog extract_deriv_register/4.

extract_deriv_register(op,Add_Y,Term,Info) :- register_op(Add_Y,Info,_).
extract_deriv_register(node,Add_Y,Term,Info) :- register_node(Add_Y,Term,Info).
extract_deriv_register(single_node,Add_Y,Term,Info) :- register_single_node(Add_Y,Term,NId,Info).
extract_deriv_register(default,Add_Y,Term,Add_Y).

:-extensional answer_extract_deriv/3.

:-std_prolog extract_deriv/3.

extract_deriv(Add,NAdj,Edges) :-
%	format('try forest add=~w\n',[Add]),
	( Add == 0 ->
	  NAdj=0, Edges=[]
	; recorded(info_derivs(Add,_)) ->
%	  format('extract deriv add=~w info_deriv\n',[Add]),
	  answer_extract_deriv(Add,NAdj,Edges)
	; recorded(call_extract_deriv(Add)) ->
%	  format('extract deriv add=~w call_extract_deriv\n',[Add]),
	  format('looping on ~w\n',[Add]),
	  fail
	;
	  record( call_extract_deriv(Add) ), % protect against looping
	  mutable(MAdjRestr,10,true),
	  mutable(MDerivs,700,true),
	  every(( % format('here1 add=~w\n',[Add]),
		  forest!true_forest(Add,_Forest::and(Add_X,Y)),
%		  format('here1.1 add=~w forest=~w\n',[Add,_Forest]),
	  	  ( once(extract_deriv(Add_X,_,_)),
		    recorded(info_derivs(Add_X,XMaxRestr))
		  ;
		    (Y = _ : Add_Y xor Y = Add_Y),
		    forest!forest(Add_Y,indirect(YForest)),
		    once(extract_deriv(Add_Y,_,_)),
		    recorded(info_derivs(Add_Y,XMaxRestr))
		  ),
%	  	  XMaxRestr > 0,
%	  	  mutable_min(MAdjRestr,XMaxRestr),
		  true
	  	)),
	  every(
		(
%		 format('here2 add=~w\n',[Add]),
		  forest!true_forest(Add,Forest),
		  ( Forest = and(Add_X,Y) ->
		    (   Y=Label:Add_Y xor Y=Add_Y, Label = void ),
				%	    format('FOREST ~w\n',[Add_Y]),
		    Add_Y \== 0,
%		    format('here2.1 add_y=~w\n',[Add_Y]),
		    recorded(O,Add_Y),
%		    format('here2.2 ~w\n',[O]),
		    ( tab_item_term(O,Term) xor Term=O ),
%%		    format('handling forest add=~w add_y=~w forest=~w => term=~w\n',[Add,Add_Y,Forest,Term]),
		    ( extract_deriv_handler(Term,_Action,_Edge::edge(Type,Label,Span,Info,Add_Y,NOcc)),
		      Label \== duplicate
		    ->
		      extract_deriv_register(_Action,Add_Y,Term,Info),
		      every(( Type=edge_kind[subst,adj],
			      deriv_process(Add_Y,Term,Span)
			    )),
		      _Edges = [_Edge|_Edges2],
		      inlined_extract_deriv(Add_X,_NAdj2,_Edges2),
		      ( Type == adj ->
			( domain(edge(adj,Label,_,_,_,NOcc2),_Edges2) ->
			  NOcc is NOcc2 + 1
			;
			  NOcc = 1
			),
			(NOcc > _NAdj2 -> _NAdj = NOcc ; _NAdj = _NAdj2)
		      ;
			NOcc = 0,
			_NAdj = _NAdj2
		      ),
		%      format('extract deriv add=~w nadj=~w edges=~w\n',[Add,_NAdj,_Edges]),
		      mutable_check_min(MAdjRestr,_NAdj),
		      mutable_add(MDerivs,-1),
		      ( %% add the derivation
		%	_Edges = [_Edge|_Edges2],
			true
		      ;
			%% and remove a derivation if needed (maybe the very last one, but not necessarly)
%			fail,
			mutable_read(MDerivs,0),
			mutable_check_min(MAdjRestr,1),
			remove_some_derivs(Add,MAdjRestr,MDerivs),
			fail
		      )
		    ; forest!forest(Add_Y,_YForest),
		      ( _YForest = indirect(YForest) ; _YForest = _ : indirect(YForest))
		    ->
%		      format('indirect YForest ~w=~w\n',[Add_Y,YForest]),
		      extract_deriv(Add_X,_NAdjX,_EdgesX),
		      extract_deriv(Add_Y,_NAdjY,_EdgesY),
		      _NAdj is _NAdjX+_NAdjY,
		      append(_EdgesX,_EdgesY,_Edges)
		    ; Type = other,
%		      format('other type edge add_x=~w\n',[Add_X]),
		      extract_deriv(Add_X,_NAdj,_Edges)
		    )
		  ;
		    %% format('strange forest ~w ~w\n',[Add,Forest]),
		    _Edges = [],
		    _NAdj = 0
		  ),
		  __Edges ::= _Edges,
		  record_without_doublon(answer_extract_deriv(Add,_NAdj,__Edges))
		)),
	  mutable_read(MAdjRestr,FinalAdjRestr),
	  record( info_derivs(Add,FinalAdjRestr) ),
	  erase( call_extract_deriv(Add) ),
	  answer_extract_deriv(Add,NAdj,Edges)
	)
	.

:-xcompiler
inlined_extract_deriv(Add,NAdj,Edges) :-
	( Add == 0 -> NAdj = 0, Edges = [] ; answer_extract_deriv(Add,NAdj,Edges) )
	.

:-xcompiler
display_deriv_number :-
	value_counter(deriv,V),
	format('#derivs=~w\n',[V])
	.

:-xcompiler
remove_some_derivs(Add,MAdjRestr,MDerivs) :-
	%% repeat until true
	repeat((
		mutable_read(MAdjRestr,AdjRestr),
		( AdjRestr > 1 -> 
		  mutable_add(MAdjRestr,-1),
		  ( recorded(answer_extract_deriv(Add,AdjRestr,_)) ->
		    every(( recorded(answer_extract_deriv(Add,AdjRestr,_Edges),_Add),
	%%		    format('Remove deriv ~w nadj=~w edges=~w\n',[Add,AdjRestr,_Edges]),
			    delete_address(_Add),
			    mutable_add(MDerivs,1)
			  ))
		  ;
		    fail
		  )
		;
		  true
		)
	       ))
	.

:-light_tabular deriv_process/3.

deriv_process(Add,Term,Span::[Left|_]) :-
	(
%	 format('extract deriv add=~w\n',[Add]),
	  extract_deriv(Add,_,_Edges),
	  Edges ::= _Edges,
	  %%	format('add ~w: span=~w term=~w deriv=~w\n',[Add,Span,Term,Edges]),
	  update_counter(deriv,DId),
%	  format('deriv ~w add=~w term=~w => ~w\n',[DId,Add,Term,Edges]),
	  %%	format('register deriv ~w add=~w term=~w\n',[DId,Add,Term]),
	  ( domain(edge(anchor,_,_,NId,_,_),Edges)
	  xor
	  %%	  format('need empty node ~w\n',[Term]),
	  ( Term = Top (Left,_) xor Term = (Top (Left,_) * _ (_,_)) ),
	    domain(edge(hypertag,_,_,HTAdd,HTAdd,_),Edges),
	    term2cat(Top,XCat),
	    register_pseudo_node( pseudo(Left,HTAdd,XCat),NId)
	  xor
	  fail
	  ),
%	  format('here1 ~w\n',[DId]),
	  register_op(Add,SOP,M_SOP_Derivs),
	  mutable_list_extend(M_SOP_Derivs,DId),
	  register_deriv(DId,NId),
	  deriv_set_op(DId,SOP,Span),
	  record_without_doublon(op2node(SOP,NId)),
	  term2cat(Term,XCat),
	  record_without_doublon(node2xcat(NId,XCat)),
%	  format('here2 ~w\n',[DId]),
	  every(( domain(_E::edge(_Type,_Label,_Span,_TOP,_Add,_),Edges),
		  %%		format('process xedge ~w\n',[_E]),
		  domain(_Type,[lexical,epsilon,subst,adj]),
		  op2node(_TOP,_NId),
		  %%		format('ready to register edge top=~w nid=~w\n',[_Top,_NId]),
		  (_Type == adj,
		   domain(edge(lexical,_Label,_,_TOP2,_Add2,_),Edges) ->
		   %% rerooting, using _NId2 as root instead of true one NId, but keeping info about the original source NId
		   op2node(_TOP2,_NId2),
		   register_edge(_NId2,_NId,_Type,_Label,_EId,M_EId_Derivs),
		   record_without_doublon( reroot_source(_EId,_Span,_NId2,NId) )
		  ;
		   register_edge(NId,_NId,_Type,_Label,_EId,M_EId_Derivs)
		  ),
		  %%		format('register deriv edge EId=~w DId=~w TId=~w _TOP=~w\n',[_EId,DId,_NId,_TOP]),
		  mutable_list_extend(M_EId_Derivs,DId),
		  deriv_add_edge(DId,info(_EId,SOP,_TOP,_NId,_Span)),
		  %%		record_without_doublon( edge2sop_and_top(EId,SOP,_TOP) ),
		  record_without_doublon( edge2sop(_EId,SOP) ),
		  record_without_doublon( edge2top(_EId,_TOP) ),
				%		record_without_doublon( sop2top(SOP,_TOP) ),
		  record_without_doublon( top2sop(_TOP,SOP) ),
		true
		)),
%	  format('here3 ~w\n',[DId]),
	  every(( domain(edge(hypertag,_,_,HTAdd,HTAdd,_),Edges),
		 recorded(_O,HTAdd),
		 (tab_item_term(_O,HTAG::verbose!struct(XTree,HT)) xor _O = HTAG),
		 rx!tokenize(XTree,' ',Tree),
		 record(node2tree(NId,Tree)),
		 HT = ht{},
		 register_ht(HTAdd,HId,M_HId_Derivs),
		 deriv_set_ht(DId,HId),
		 mutable_list_extend(M_HId_Derivs,DId)
	       )),
%	  format('did=~w anchor cat=~w xcat=~w span=~w lex=~w truelex=~w lemma=~w\n',[DId,Cat,XCat,SpanAnchor,Lex,TrueLex,Lemma]),
	  true
	;
	  %% at the end we can remove info related to extract_deriv
	  erase(answer_extract_deriv(Add,_,_)),
	  fail
	)
	.

:-xcompiler
term2cat(T1,Cat) :-
	( T1 = T (_,_)
	xor T1 = T (_,_) * _ (_,_)
	xor T1 == [], T = '_'
	xor T1 = T
	),
	T =.. [F|_],
	functor2cat(F,Cat)
	.

:-light_tabular functor2cat/2.
:-mode(functor2cat/2,+(+,-)).

functor2cat(F,Cat) :-
	atom_module(F,A,B),
	( A = [] -> Cat = B ; Cat = A )
	.

:-light_tabular register_pseudo_node/2.
:-mode(register_pseudo_node/2,+(+,-)).

register_pseudo_node(Term::pseudo(Left,_,_),NId) :-
	update_counter(node,NId),
%%	format('register node ~w ~w\n',[NId,Add]),
	(Left = gen_pos(XLeft,_) xor Left = XLeft),
	Right = XLeft,
	%% ( Left == Right -> format('registering empty node id=~w left=~w term=~w\n',[NId,Left,Term]) ; true ),
	Lex = '',
	Token = '',
	( recorded( C::cluster{ id => CId,
				left => XLeft,
				right => Right,
				lex => Lex,
				token => Token
			      } )
	xor update_counter(cluster,CId),
%	  format('Register empty Cluster ~w Term=~w\n',[C,Term]),
	  record(C)
	),
	record( pseudonode2object(NId,Term,CId) ),
	true
	.

:-light_tabular register_node/3.
:-mode(register_node/3,+(+,+,-)).

register_node(Add,Term,NId) :-
	update_counter(node,NId),
%%	format('register node ~w ~w\n',[NId,Add]),
	( Term = T (Left,_) , Type = subst
	xor Term = T (Left,_) * _ (_,_), Type = adj
	xor Term = verbose!anchor(Token,Left,Right,_,_,(_ : [Lemma,Lex,LemmaId]),_), Type=anchor
	xor Term = verbose!coanchor(Token,Left,Right,_,[Lemma,Lex,LemmaId]), Type = lexical
	xor Term = verbose!lexical([Token],Left,Right,_,[Lemma,Lex,LemmaId]), Type=lexical 
	xor Term = verbose!epsilon(Token,Left,Right,[Lemma,Lex,LemmaId]), Type=lexical
	),
	Right ?= Left,
	%% ( Left == Right -> format('registering empty node id=~w left=~w term=~w\n',[NId,Left,Term]) ; true ),
	Lex ?= '',
	Token ?= '',
	cluster_lex2token(Lex,XToken),
	( Left = gen_pos(XL1,XL2),
	  Right = gen_pos(XR1,XR2) ->
	  ( XL1 == XR1, Type == lexical -> 
	    XLeft = XL2,
	    XRight = XR2
	  ;
	    XLeft = XL1,
	    XRight = XR1
	  )
	;
	  Left = XLeft,
	  Right = XRight
	),
	( recorded( C::cluster{ id => CId,
				left => XLeft,
				right => XRight,
				lex => Lex,
				token => XToken
			      } )
	xor update_counter(cluster,CId),
%	  format('Register Cluster ~w Term=~w\n',[C,Term]),
	  record(C)
	),
	record( node2object(NId,Add,CId) ),
	true
	.

:-light_tabular cluster_lex2token/2.
:-mode(cluster_lex2token/2,+(+,-)).

cluster_lex2token(Lex,Token) :-
	lex_tokenize(Lex,LLex),
	cluster_lex2token_aux(LLex,LTokens),
	(LTokens=[Token] xor name_builder('~L',[['~w',' '],LTokens],Token))
	.

:-std_prolog cluster_lex2token_aux/2.

cluster_lex2token_aux(LLex,LTokens) :-
	(LLex=[_Lex|LLex2] ->
	 LTokens = [_Token|LTokens2],
	 cluster_lex2token_aux(LLex2,LTokens2),
	 ( rx!tokenize(_Lex,'|',[_,__Lex]) xor __Lex = _Lex),
	 '$interface'(lowercase(__Lex:string),[return(_Token:string)])
	;
	 LTokens = []
	)
	.

:-light_tabular register_single_node/4.
:-mode(register_single_node/4,+(+,+,-,-)).

register_single_node(Add,Term,NId,OId) :-
	register_node(Add,Term,NId),
%	format('register single node add=~w nid=~w term=~w\n',[Add,OId,Term]),
	register_op(Add,OId,_),
	record( terminal_op(NId,OId) ),
	record_without_doublon(op2node(OId,NId))
	.

:-light_tabular register_op/3.
:-mode(register_op/3,+(+,-,-)).

register_op(Add,OId,MDerivs) :-
	update_counter(op,OId),
%%	format('register op ~w ~w\n',[OId,Add]),
	record( op2object(OId,Add) ),
	mutable(MDerivs,[]),
%	format('registered op ~w\n',[OId]),
	true
	.

:-light_tabular register_ht/3.
:-mode(register_ht/3,+(+,-,-)).

register_ht(Add,HId,MDerivs) :-
	update_counter(ht,HId),
	record( ht2object(HId,Add) ),
	mutable(MDerivs,[]),
%	format('registered ht ~w\n',[HId]),
	true
	.

:-light_tabular register_edge/6.
:-mode(register_edge/6,+(+,+,+,+,-,-)).

register_edge(SId,TId,Type,Label,EId,MDerivs) :-
	update_counter(edge,EId),
	mutable(MDerivs,[]),
%	format('registered edge ~w\n',[EId]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

:-std_prolog keep_only_best/3.

keep_only_best(CIds,EIds,DIds) :-
%	format('only best ~w\n',[EIds]),
	every(( domain(_EId,EIds),
		record(keep_edge(_EId)))),
	every(( _E::edge{ id => _EId,
			  source => node{ id => SourceId},
			  target => node{ id => TargetId}
			},
		\+ recorded( keep_edge(_EId) ),
		( '$answers'(edge_cost(_EId,_W,_Vector,_Cst))
		xor _W=none,
		  _Vector=[]
		),
		verbose('Erase w=~w edge ~E\n',[_W,_E]),
		erase( _E ),
		erase( source2edge(SourceId,_E) ),
		erase( target2edge(TargetId,_E) ),
		record( erased(_E) )
	      )),
	%% keep nodes related to existing edges or to cluster not covered by the clusters in CIds
	every(( domain(D,DIds),
		record_without_doublon( keep_deriv(D) ) )),
	every(( _N::node{ id => _NId,
			  cluster => _C::cluster{ id => _CId, lex => _Lex }
			},
		( edge{ source => _N} -> true
		; edge{ target => _N } -> true
		; _Lex \== '',
		  ( domain(__CId,CIds)
		  ; recorded( keep_cluster(__CId))),
		  cluster_overlap(_CId,__CId),
		  %%		  __C::cluster{ id => __CId },
		  ( _CId \== __CId
		  xor edge{ source => node{ cluster => cluster{ id => _CId } } }
		  xor edge{ target => node{ cluster => cluster{ id =>_CId } } }
		  )
		  ->
		  verbose('Erase node ~E overlap with ~w\n',[_N,__CId]),
		  erase( _N ),
		  erase( source2edge(_NId,_) ),
		  erase( target2edge(_NId,_) )
		; _Lex == '' ->
		  verbose('Erase node ~E emptylex\n',[_N]),
		  erase( _N ),
		  erase( source2edge(_NId,_) ),
		  erase( target2edge(_NId,_) )
		;
		  %% keep a non empty node
		  %% which is neither an edge source nor target
		  record_without_doublon( keep_cluster(_CId) ),
		  true
		)
	      )),
	every(( _C::cluster{ lex => Lex},
%		Lex \== '',
		\+ node{ cluster => _C },
		verbose('Erase cluster ~E\n',[_C]),
		erase(_C),
		erase( sourcecluster2edge(_C) ),
		erase( targetcluster2edge(_C) )
	      )),
	every(( _O::op{ deriv => ODerivs },
		\+ alive_deriv(ODerivs,_),
		verbose('Erase op ~w\n',[_O]),
		erase(_O))),
	every(( _HT::hypertag{ deriv => HDerivs },
		\+ alive_deriv(HDerivs,_),
		verbose('Erase ht ~w\n',[_HT]),
		erase(_HT))),
	(   \+ opt(verbose)
	xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
	    every(( _N::node{}, format('Node ~E\n',[_N]) )),
	    every(( _E::edge{ id => _EId },
		    '$answers'(edge_cost(_EId,_W,_Vector,_Cst)),
		    format('Edge ~w: ~E\n',[_W,_E]) ))
	)
	.

convert :-
	%% Add a virtual root node
	record( CRoot::cluster{ id => root,
				lex => '',
				token => '',
				left => 0,
				right => 0 }
	      ),
	mutable(MRDerivs,[],true),
	every(( N::node{ id => NId, cat => Cat },
		(\+ N = NRoot),
%		format('potential root nid ~w\n',[NId]),
		( node2op(NId,OId),
		  recorded(root_op(OId)) -> true
		; fail,
		  recorded(mode(robust)) ->
		  Cat = cat[v]
		;
		  fail,
		  span_max(Span),
		  span(NId,Span)
		),
%		format('root nid ~w\n',[NId]),
		_NId ::= root(NId),
		mutable_list_extend(MRDerivs,_NId)
	      )),
	mutable_read(MRDerivs,RDerivs),
	record( NRoot::node{ id => root,
			     cluster => CRoot,
			     cat => [],
			     tree => [],
			     deriv => RDerivs,
			     xcat => []
			   }
	      ),
	every(( domain(root(NId),RDerivs),
		NN::node{ id => NId },
		( recorded( mode(robust) )
		xor span_max(Span),
		  span(NId,Span)
		),
		verbose('Add root edge ~E\n',[NN]),
		record_without_doublon( edge{ id => root(NId),
					      source => NRoot,
					      target => NN,
					      label => root,
					      type => virtual,
					      deriv => [root(NId)]
					    } )
	      )),
	every( (( recorded( mode(robust) ) ->
		  recorded(edge{ id => root(NId) }),
		  span(NId,Span),
		  Span=[_,_]
		;
		  span_max(Span)
		),
%%		format('try with span ~w\n',[Span]),
		best_parse(root,[],Span,[],_),
		true
	       )
	     ),
	\+ opt(stop(best)),
	every(( \+ recorded( mode(robust) ),
		span_max(Span),
		'$answers'(best_parse(root,[],Span,[],DStruct))
		->
		verbose('Found root best parse ~w\n', [DStruct]),
%%		format('Found root best parse ~w\n', [DStruct]),
		dstruct2lists(DStruct,CIds,EIds,DIds),
		verbose('Found root best parse ~w: cids=~w eids=~w dids=~w\n', [DStruct,CIds,EIds,DIds]),
		(\+ opt(verbose)
		xor every(( domain(_EId,EIds),
			    _E::edge{ id => _EId },
			    '$answers'(edge_cost(_EId,_W,_Vector,_Cst)),
			    format('\t~w: ~E ~w\n',[_W,_E,_Vector]) ))),
		keep_only_best(CIds,EIds,DIds),
		record(has_best_parse)
	      ;	  find_best_parse_coverage ->
		(   \+ opt(verbose)
		xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
		    every(( _N::node{}, format('Node ~E\n',[_N]) )),
		    every(( _E::edge{ id => _EId },
			    '$answers'(edge_cost(_EId,_W,_Vector,_Cst)),
			    format('Edge ~w: ~E ~w\n',[_W,_E,_Vector]) ))
		)
	      ;	  
		  verbose('Not found best parse\n', [])
	      )),
%%	erase_relation( best_parse(_,_,_,_,_) ),
	every(( (opt(depxml) xor opt(nodis)),
		every(( opt(transform),
			edge_transform
		      )),
		every(( opt(transform),
			edge_transform
		      )),
		true
	      ; (opt(passage) xor opt(easy)),
		every(( word(0) )),
		verbose('Done word\n',[]),
		( %% fail,
		  opt(passage) ->
		  split_some_clusters
		;
		  true
		),
		every(( % fail,
			domain(Type,['NV','GN','GA','GR','PV','GP']),
			verbose('Try build group ~w\n',[Type]),
			build_group(Type),
			verbose('Done build group ~w\n',[Type]),
			true
		      )),
		verbose('Done group\n',[]),
		every(( extract_relation( relation{ type => rel[] } ))),
		true
	      )),
	true
	.

:-std_prolog split_some_clusters/0.

split_some_clusters :-
	every((
	       try_split_on_adj_mod( N::node{ id => NId,
					      cluster => cluster{ id => CId,
								  left => CLeft,
								  right => CRight,
								  token => CToken,
								  lex => CLex
								}
					    },
				     F::f{ cid => CId,
					   id => FId1,
					   lex => Lex
					 },
				     Lemma,
				     Pos
				   ),
	       node2live_deriv(NId,DId),
	       gensym(_PCId), PCId = gensym(_PCId),
	       record( PC::cluster{ id => PCId,
				    left => Pos,
				    right => Pos,
				    lex => CLex,
				    token => CToken
				  }
		     ),
	       erase(F),
	       record( f{ cid => PCId, id => FId1, lex => Lex, rank => R } ),
%%	       record( redirect(PCId,CId) ),
	       record( splitted(PCId,FId1) ),
	       gensym(_PNId), PNId=gensym(_PNId),
	       record( PN::node{ id => PNId,
				 lemma => Lemma,
				 form => Lex,
				 cat => adj,
				 deriv => [],
				 tree => [],
				 xcat => 'adjP',
				 cluster => PC,
				 lemmaid => Lemma,
				 w => []
			       } ),
	       gensym(_PEId), PEId = gensym(_PEId),
	       record( PE::edge{ id => PEId,
				 source => N,
				 target => PN,
				 type => adj,
				 label => 'N2',
				 deriv => [DId]
			       }
		     ),
	       verbose('added pseudo node N=~w\n\tPC=~w\n\tPN=~w\n\tPE=~w\n\n',[N,PC,PN,PE]),
%%	       format('added pseudo node N=~w\n\tPC=~w\n\tPN=~w\n\tPE=~w\n\n',[N,PC,PN,PE]),
	       true
	      )),
	verbose('done splitting\n',[]),
	true
	.

:-rec_prolog try_split_on_adj_mod/4.

try_split_on_adj_mod(N,F,Lemma,Pos) :-
	N::node{ cat => nc,
		 lemma => date[],
		 cluster => cluster{ id => CId, left => CLeft, right => CRight }
	       },
	F::f{ cid => CId, lex => Lex, rank => R },
	date_modifier(Lemma,Forms),
	domain(Lex,Forms),
	( S is R-1,
	  Pos = CRight
	;
	  S is R+1,
	  Pos = CLeft
	),
	f{ cid => CId, rank => S }
	.

:-extensional date_modifier/2.

date_modifier(prochain,[prochain,prochains,prochaine,prochaines]).
date_modifier(suivant,[suivant,suivants,suivante,suivantes]).
date_modifier(premier,[premier,premiers,premi�re,premi�res]).
date_modifier(dernier,[dernier,derniers,derni�re,derni�res]).


%% The modifier is detached only if being the last one
try_split_on_adj_mod(N,F,Lemma,CRight) :-
	N::node{ cat => np,
		 lemma => entities['_ORGANIZATION','_COMPANY'],
		 cluster => cluster{ id => CId,
				     left => CLeft,
				     right => CRight,
				     token => CToken,
				     lex => CLex
				   }
	       },
	F::f{ cid => CId, id => FId1, lex => Lex, rank => R },
	organization_modifier(Lemma,Forms),
	domain(Lex,Forms),
	S is R-1,
	f{ cid => _CId, id => FId2, rank => S },
	( _CId = CId xor recorded( redirect(CId,_CId) ) ),
	Next is R+1,
	(   \+ f{cid => CId, rank=>Next}),
	    true
	.

:-extensional organization_modifier/2.

organization_modifier(national,[national,nationale,nationaux,nationales]).
organization_modifier(europ�en,[europ�en,europ�enne,europ�ens,europ�ennes]).
organization_modifier(fran�ais,[fran�ais,fran�aise,fran�aises]).



:-light_tabular dstruct2lists/4.
:-mode(dstruct2lists/4,+(+,-,-,-)).

dstruct2lists( DStruct::dstruct{ node => NId,
                                 deriv => DId,
                                 span => Span,
                                 children => Children },
               [CId|_CIds],
               EIds,
               DIds
             ) :-
        verbose('dstruct ~w\n',[DStruct]),
        node{ id => NId, cluster => cluster{ id => CId } },
        dstruct2lists_aux(Children,_CIds,EIds,_DIds),
        ( DId == [] ->
          DIds = _DIds
        ; DIds = [DId|_DIds]
        ),
        true
        .

:-light_tabular edge_best_parse_constraint/2.

edge_best_parse_constraint(EId,Constraints) :-
	( edge{ target => N::node{},
		type => adj,
		id => EId
	      },
	  node!empty(N) -> Constraints = EId ; Constraints = []
	).

:-rec_prolog dstruct2lists_aux/4.

dstruct2lists_aux([],[],[],[]).
dstruct2lists_aux([dinfo(OId1,EId1,NId1,Constraints)|L2],CIds,[EId1|EIds],DIds) :-
        dstruct2lists_aux(L2,CIds2,EIds2,DIds2),
	edge_best_parse_constraint(EId1,Constraints1),
        '$answers'(best_parse(NId1,OId1,Span1,Constraints1,DStruct1)),
        dstruct2lists(DStruct1,CIds1,EIds1,DIds1),
        append(CIds1,CIds2,CIds),
        append(EIds1,EIds2,EIds),
        append(DIds1,DIds2,DIds),
        true
        .

:-light_tabular span_max/1.

span_max(Span::[0,Max]) :- recorded('N'(Max)).

:-std_prolog find_best_parse_coverage/0.

find_best_parse_coverage :-
	verbose('Try find best coverage\n',[]),
	every((
	       edge{ source => Root::node{ id =>NId,
					   xcat => XCat::cat['S','N2','PP',comp,unknown,'CS'],
					   cat => Cat
					 %% cat => cat[v,nc,np,]
					 } },
	       '$answers'(best_parse(NId,_,Span::[Left,Right],[],
				     DStruct::dstruct{ w => W,
						       deriv => _Deriv,
						       children => _Children
						     })),
	       \+ (Span = [_L,_L] ),
	       verbose('best coverage considering root ~E xcat=~w w=~w cids=~w eids=~w\n',[Root,XCat,W,CIds,EIds]),
	       mutable(_WM,W,true),
	       every(( robust_re_weight(NId,_Children,_Deriv,Span,_W1),
		       mutable_add(_WM,_W1)
		     )),
	       mutable_read(_WM,W1),
	       %% no need to add best parse covering empty span !
	       Length is Right - Left,
	       %%	       Length > 1,
	       ( Length == 1 -> Length3 = 1
	       ; Length < 4 ->  Length3 = 2
	       ; Length < 10 -> Length3 = 3
	       ; Length3 = 4
	       ),
	       verbose('Insert parse length=~w o=~w\n',[Length,O]),
	       '$interface'('Easyforest_Add_DStruct'(Length3:int,W1:int,DStruct:term),[return(none)])
	      )),
	verbose('Sorted partial best parses ~L\n',[['\n\t~w',''],L]),
	(\+ opt(stop(partial)) xor exit(1)),
	best_parse_traversal(L,CIds,EIds,DIds),
	verbose('Second try: found best parse by gluing ~w ~w\n',[CIds,EIds]),
	(\+ opt(stop(second_try)) xor exit(1)),
	keep_only_best(CIds,EIds,DIds),
	true
	.
	      
:-std_prolog best_parse_traversal/4.

best_parse_traversal( L,
		      CIds,
		      EIds,
		      DIds
		    ) :-
	mutable(MDs,[],true),
	%% try to find large and good segment to cover the input sentence
	%% segments are tried by decreasing length and decreasing weight
	%% first, we get a list of compatible DStruct
	every(( '$interface'('Easyforest_Get_DStruct'(DStruct1:term),[choice_size(2)]),
		DStruct1 = dstruct{ span => Span::[Left,Right] },
		mutable_read(MDs,_Ds),
		\+ ( domain(dstruct{ span => _Span::[_Left,_Right] }, _Ds),
		     ( _Left =< Left, Left < _Right
		     xor _Left < Right, Right =< _Right
		     xor Left =< _Left, _Left < Right
		     xor Left < _Right, _Right =< Right
		     )
		   ),
		fast_mutable(MDs,[DStruct1|_Ds])
	      )),
	mutable_read(MDs,Ds),
	mutable(MCIds,[],true),
	mutable(MEIds,[],true),
	mutable(MDIds,[],true),
	%% then, we get the lists of corresponding clusters, edges and derivs
	every(( 
		domain(DStruct1,Ds),
		dstruct2lists(DStruct1,CIds1,EIds1,DIds1),
		mutable_read(MCIds,_CIds),
		mutable_read(MEIds,_EIds),
		mutable_read(MDIds,_DIds),
		append(CIds1,_CIds,NewCIds),
		append(EIds1,_EIds,NewEIds),
		append(DIds1,_DIds,NewDIds),
		verbose('Best parse recovery: agglutinate length=~w weight=~w node=~w\n',[Length,W,NId1]),
		mutable(MCIds,NewCIds),
		mutable(MEIds,NewEIds),
		mutable(MDIds,NewDIds)
	      )),
	mutable_read(MCIds,CIds),
	mutable_read(MEIds,EIds),
	mutable_read(MDIds,DIds)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

:-xcompiler
emit_multi(Stage) :- (opt(multi) -> format('------------ ~w -----------\n',[Stage]) ; true).

:-std_prolog easy_emit/0.

easy_emit :-
	emit_multi('EASY'),
	sentence(SId),
	recorded(mode(Mode)),
	( recorded(has_best_parse) ->
	  Best = yes
	;
	  Best = no
	),
	event_ctx(Ctx,0),
	Handler=default([]),
	xevent_process(Handler,start_document,Ctx,Handler),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	xevent_process(Handler,xmldecl,Ctx,Handler),
	xml!wrapper(Handler,
		    Ctx,
		    'DOCUMENT',
		    [id='emg',
		     xmlns!xlink= 'http://www.w3.org/1999/xlink'],
		    xml!wrapper(Handler,
				Ctx,
				'E', [ id:SId, mode:Mode, best: Best ],
				%% Constituants and relations
				xevent_process(Handler,[constituants,relations],Ctx,Handler)
			       )
		   ),
	xevent_process(Handler,end_document,Ctx,Handler),
	format('\n',[]),
	true
	.

:-std_prolog passage_emit/0.

passage_emit :-
	emit_multi('PASSAGE'),
	sentence(SId),
	recorded(mode(Mode)),
	( recorded(has_best_parse) ->
	  Best = yes
	;
	  Best = no
	),
	event_ctx(Ctx,0),
	Handler=passage([]),
	xevent_process(Handler,start_document,Ctx,Handler),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	xevent_process(Handler,xmldecl,Ctx,Handler),
	xml!wrapper(Handler,
		    Ctx,
		    'Document',
		    [id='emg',
		     xmlns!xlink= 'http://www.w3.org/1999/xlink'],
		    xml!wrapper(Handler,
				Ctx,
				'Sentence', [ id:SId, mode:Mode, best:Best ],
				%% Constituants and relations
				( xevent_process(Handler,
						 ['TWG',relations,entities],
						 Ctx,Handler),
				  every( ( '$answers'(edge_cost(EId,W,Ws,_Cst)),
					   ( recorded(keep_edge(EId)) ->
					     Kept = yes,
					     E::edge{ id => EId,
						      label => Label,
						      source => node{ cat => SCat,
								      id => SNId,
								      cluster => cluster{ id => SCId,
											  left => SLeft
											}
								    },
						      target => node{ cat => TCat,
								      id => TNId,
								      cluster => cluster{ id => TCId,
											  right => TLeft
											}
								    }
						    }
					   ;
					     Kept = no,
					     recorded( erased(E) )
					   ),
					   ( SLeft < TLeft ->
					     Dir = right
					   ;
					     Dir = left
					   ),

					   source2wid(SNId,SCId,SWId),
					   target2wid(TNId,TCId,TWId),

					   event_process(Handler,
							 element{ name => 'cost',
								  attributes => [eid:EId,
										 kept:Kept,
										 w:W,
										 ws:Ws,
										 info: [Dir,SCat,Label,TCat,SWId,TWId],
										 source: SCId,
										 target: TCId
										]
								},
							 Ctx),
					   true
					 ))
				  )
			       )
		   ),
	xevent_process(Handler,end_document,Ctx,Handler),
	format('\n',[]),
	true
	.

:-finite_set(conll_fullcat,['NC',
			    'DET',
			    'P',
			    'PONCT',
			    'ADJ',
			    'V',
			    'ADV',
			    'NPP',
			    'VPP',
			    'P+D',
			    'CC',
			    'VINF',
			    'CLS',
			    'PROREL',
			    'CS',
			    'CLR',
			    'PRO',
			    'VPR',
			    'CLO',
			    'ET',
			    'VS',
			    'PREF',
			    'ADVWH',
			    'PROWH',
			    'P+PRO',
			    'VIMP',
			    'I',
			    'ADJWH',
			    'DETWH'
			   ]).

:-subset(conll_det,conll_fullcat['DET','DETWH']).
:-subset(conll_adv,conll_fullcat['ADV','ADVWH']).
:-subset(conll_cl,conll_fullcat['CLR','CLO','CLS']).
:-subset(conll_notponct,conll_fullcat[~ ['PONCT']]).
	
:-finite_set(conll_cat,['N',
			'V',
			'C',
			'P',
			'D',
			'PONCT',
			'A',
			'CL',
			'I',
			'ET',
			'ADV',
			'PRO'
		       ]
	    ).


:-std_prolog conll_emit/0.

conll_emit :-
	emit_multi('CONLL'),
	conll_ids(0,1,[]),
	conll_collect_node_info(1),
	every(( conll_head )),
	sentence(SId),
	recorded(mode(Mode)),
	( recorded(has_best_parse) ->
	  Best = yes
	;
	  Best = no
	),
	format('## sentence=~w mode=~w best=~w\n',[SId,Mode,Best]),
	conll_emit(1),
	%% emit cost info
	every(( recorded(opt(cost)),
		'$answers'(edge_cost(EId,W,Ws,_Cst)),
%%		format('processing ~w cst=~w\n',[EId,_Cst]),
		( recorded(keep_edge(EId)) ->
		  Kept = yes,
		  E::edge{ id => EId,
			   label => Label,
			   type => Type::edge_kind[~ virtual],
			   source => Source::node{ cat => SCat,
						   lemma => SLemma,
						   form => SForm,
						   xcat => SXCat,
						   id => SNId,
						   tree => XSTree,
						   cluster => cluster{ id => SCId,
								       left => SLeft,
								       right => SRight,
								       token => SForm2,
								       lex => SLex
								     }
						 },
			   target => Target::node{ cat => TCat,
						   id => TNId,
						   lemma => TLemma,
						   form => TForm,
						   xcat => TXCat,
						   tree => XTTree,
						   cluster => cluster{ id => TCId,
								       left => TLeft,
								       right => TRight,
								       token => TForm2,
								       lex => TLex
								      }
						 }
			 },
%		  format('kept ~w\n',[E]),
		  true
		;
		  Kept = no,
		  recorded(erased(E)),
%		  format('unkept ~w\n',[E]),
		  true
		),
		(node!empty(Source) -> SEmpty = 1 ; SEmpty = 0),
		( node!empty(Target) ->
		  TEmpty = 1,
		  %% Test: only keep cost info for edges with non empty targets
		  ( IEdge::edge{ source => Target,
				 target => ITarget::node{ id => ITNId,
							  cluster => cluster{ id => ITCId,
									      left => ITLeft,
									      right => ITRight }},
				 type => subst
			       },
		    \+ node!empty( ITarget ),
		    node2conll(ITNId,ITPos) -> true
		  ; recorded( erased( IEdge ) ),
		    \+ node!empty( ITarget ),
		    ( node2conll(ITNId,ITPos) -> true
		    ; node{ id => _ITNId, cluster => cluster{ id => ITCId } },
		      node2conll(_ITNId,ITPos) -> true
		    ; node{ id => _ITNId, cluster => cluster{ left => ITLeft } },
		      node2conll(_ITNId,ITPos) -> true
		    ; node{ id => _ITNId, cluster => cluster{ right => ITRight } },
		      node2conll(_ITNId,ITPos) -> true
		    ;
		      ITPos = 0
		    ) -> true
		  ;
		    fail
		  )
		;
		  TEmpty = 0,
		  ITPos = 0
		),
		(XTTree = [TTree|_] xor XTTree = TTree),
		(XSTree = [STree|_] xor XSTree = STree),
		TXCat ?= none,
		SXCat ?= none,
		edge_rank(EId,Rank,Dir),
		( node2conll(TNId,TPos) -> true
		; node{ id => _TNId, cluster => cluster{ id => TCId } },
		  node2conll(_TNId,TPos) -> true
		; node{ id => _TNId, cluster => cluster{ left => TLeft } },
		  node2conll(_TNId,TPos) -> true
		; node{ id => _TNId, cluster => cluster{ right => TRight } },
		  node2conll(_TNId,TPos) -> true
		;
		  TPos = 0
		),
		( node2conll(SNId,SPos) -> true
		; node{ id => _SNId, cluster => cluster{ id => SCId } },
		  node2conll(_SNId,SPos) -> true
		; node{ id => _SNId, cluster => cluster{ left => SLeft } },
		  node2conll(_SNId,SPos) -> true
		; node{ id => _SNId, cluster => cluster{ right => SRight } },
		  node2conll(_SNId,SPos) -> true
		;
		  SPos = 0
		),
		next_and_prev_forms(SLeft,SRight,PSForm,NSForm),
		prev2_form(SLeft,PPSForm),
		next_and_prev_forms(TLeft,TRight,PTForm,NTForm),
		prev2_form(TLeft,PPTForm),
		( node_vmode(TNId,TVMode) xor TVMode = none ),
		( node_vmode(SNId,SVMode) xor SVMode = none ),
		( capitalized_cluster(SLex) -> SCap=1 ; SCap = 0 ),
		( capitalized_cluster(TLex) -> TCap=1 ; TCap = 0 ),
		( node_subcat(SNId,SSubcat) xor SSubcat = 'none'),
		( node_subcat(TNId,TSubcat) xor TSubcat = 'none'),

		form2cluster_feature(TForm2,TForm,TCluster),
		form2cluster_feature(SForm2,SForm,SCluster),
		
		suffix3(SForm,SSuff),
		suffix3(TForm,TSuff),

		(_Cst = [] ->
		 SFeatures = []
		; ( ParentEdge::edge{ id => _Cst, source => node{ id => SSNId,
								  lemma => SSLemma,
								  form => SSForm,
								  cat => SSCat,
								  cluster => cluster{ id => SSCId,
										      left => SSLeft,
										      right => SSRight,
										     token => SSForm2,
										      lex => SSLex
										    }
								}}
		  xor recorded( erased( ParentEdge ) )
		  ),
		 ( node2conll(SSNId,SSPos) -> true
		    ; node{ id => _SSNId, cluster => cluster{ id => SSCId } },
		      node2conll(_SSNId,SSPos) -> true
		    ; node{ id => _SSNId, cluster => cluster{ left => SSLeft } },
		      node2conll(_SSNId,SSPos) -> true
		    ; node{ id => _SSNId, cluster => cluster{ right => SSRight } },
		      node2conll(_SSNId,SSPos) -> true
		    ;
		      SSPos = 0
		 ),
		 form2cluster_feature(SSForm2,SSForm,SSCluster),
		 (node_vmode(SSNId,SSVMode) xor SSVMode = none),
		 suffix3(SSForm,SSSuff),
		 edge_abstract_rank(_Cst,SRank,SDir),
		 edge_delta(SSLeft,TLeft,SDir,SDelta),
		 SFeatures = [sspos:SSPos,
			      sscat:SSCat,
			      sscluster:SSCluster,
			      ssvmode:SSVMode,
			      sdelta:SDelta,
			      srank:SRank,
			      sdir:SDir,
			      sslemma: SSLemma
			     ]
		),

		%% Brother features
		(Kept == yes ->
		 ( previous_brother_edge(E,
					 Dir,
					 edge{ id => BEId,
					       label => BLabel,
					       type => BType,
					       target => node{ id => BNId,
							       form => BForm,
							       xcat => BXCat,
							       cat => BCat,
							       lemma => BLemma,
							       cluster => cluster{ id => BCId,
										   left => BLeft,
										   right => BRight,
										   token => BForm2,
										   lex => BLex
										 }
							     }
					     }
					) ->
		   ( node2conll(BNId,BPos) -> true
		   ; node{ id => _BNId, cluster => cluster{ id => BCId } },
		     node2conll(_BNId,BPos) -> true
		   ; node{ id => _BNId, cluster => cluster{ left => BLeft } },
		     node2conll(_BNId,BPos) -> true
		   ; node{ id => _BNId, cluster => cluster{ right => BRight } },
		     node2conll(_BNId,BPos) -> true
		   ;
		      BPos = 0
		   ),

		   BFeatures = [ btype: BType,
				 blabel: BLabel,
				 bform: BForm,
				 bcat: BCat,
				 blemma: BLemma,
				 bpos: BPos
			       ]

		 ;
		   BFeatures = [btype: none]
		 )
		;
		 BFeatures = []
		),
		
		mutable(MMax,-100000,true),
		edge_best_parse_constraint(EId,Constraints),
		every(( '$answers'( best_parse(TNId,_,_,Constraints,dstruct{ w => _W } ) ),
			mutable_read(MMax,_WOld),
			_W > _WOld,
			mutable(MMax,_W)
		      )),		
		mutable_read(MMax,WMax),
		format('## cost tpos=~w spos=~w kept=~w label=~w type=~w dir=~w tcat=~w tlemma=~w tform=~w scat=~w slemma=~w sform=~w txcat=~w sxcat=~w ttree=~w stree=~w rank=~w tvmode=~w svmode=~w tcap=~w scap=~w psform=~w nsform=~w ptform=~w ntform=~w ppsform=~w pptform=~w tsuff=~w ssuff=~w  tsubcat=~w ssubcat=~w tcluster=~w scluster=~w itpos=~w ~U ~U tw=~w ~U\n',
		       [TPos,
			SPos,
			Kept,
			Label,
			Type,
			Dir,
			TCat,
			TLemma,
			TForm,
			SCat,
			SLemma,
			SForm,
			TXCat,
			SXCat,
			TTree,
			STree,
			Rank,
			TVMode,
			SVMode,
			TCap,
			SCap,
			PSForm,
			NSForm,
			PTForm,
			NTForm,
			PPSForm,
			PPTForm,
			TSuff,
			SSuff,
			TSubcat,
			SSubcat,
			TCluster,
			SCluster,
			ITPos,
			['~w=~w',' '],SFeatures,
			['~w=~w',' '],BFeatures,
			WMax,
			['<~w>=~w',' '],Ws
			])
	      )),
	true
	.

:-std_prolog previous_brother_edge/3.

previous_brother_edge(edge{ source => Source::node{ cluster => cluster{ left => SLeft, right => SRight}},
			    target => Target::node{ cluster => cluster{ left => Left, right => Right }} },
		      Dir,
		      BEdge::edge{ source => Source,
				    target => node{ cluster => cluster{ left => BLeft, right => BRight }}
				 }
		     ) :-
	BEdge,
	( Dir = right ->
	  SRight =< BLeft,
	  BRight =< Left,
	  \+ ( edge{ source => Source,
		     target => node{ cluster => cluster{ left => _BLeft, right => _BRight }}
		   },
	       _BRight =< Left,
	       BRight =< _BLeft
	     )
	;
	  BRight =< SLeft,
	  Right =< BLeft,
	  \+ ( edge{ source => Source,
		     target => node{ cluster => cluster{ left => _BLeft, right => _BRight }}
		   },
	       Right =< _BLeft,
	       _BRight =< BLeft
	     ) 
	)
	.

:-std_prolog next_and_prev_forms/4.

next_and_prev_forms(Left,Right,PForm,NForm) :-
	(prev_form(Left,PForm,_) xor PForm = '****'),
	(next_form(Right,NForm) xor NFOrm = '****')
	.

:-std_prolog prev2_form/2.

prev2_form(Left,PPForm) :-
	( prev_form(Left,_,Left2),
	  prev_form(Left2,PPForm,_)
	xor PPForm = '****'
	).
	
:-light_tabular prev_form/3, next_form/2.
:-mode(prev_form/3,+(-,-,-)).
:-mode(next_form/2,+(-,-)).

prev_form(Left,PForm,_Left) :-
	node{ form => PForm, cluster => cluster{ right => Left, left => _Left } },
	_Left < Left.
prev_form(0,'^^^',-1).
prev_form(-1,'^^^^^^',-2).


next_form(N,'$$$') :- recorded('N'(N)).
next_form(Right,NForm) :-
	node{ form => NForm, cluster => cluster{ left => Right, right => _Right } },
	Right < _Right.

:-std_prolog conll_ids/3, conll_emit/1.
:-extensional conll2node/2, node2conll/2.

conll_ids(Left,Pos,Prev) :-
	( N::node{ id => NId,
		   lemma => Lemma,
		   cat => Cat,
		   cluster => cluster{ left => Left, right => Right, lex => Lex}
		 },
	  Right > Left ->
	  ( Lemma = '_SENT_BOUND' ->
	    %% skip sentence bounds
	    XPos is Pos
	  ; Prev = node{ lemma => PLemma, cat => PCat, cluster => cluster{ lex => Lex } },
	    lex_tokenize(Lex,[_]),
	      %% some strange case where sxpipe has merged two CONLL tokens into a single SxPipe tokens
	      %% corresponding to 2 FRMG words (a-t-on)
	      \+ (
		     PCat = cat[v,aux], Cat = cat[cln] % a-t-il a-t-on
		 )
	      ->
	    XPos is Pos,
	    U is Pos-1,
	      record( conll_compound(U) ),
	    record( conll_forward(Prev,N) ),
	    record( conll_backward(N,Prev) ),
	    record( conll_redirect(NId,U) )
	  ; Prev = node{ cluster => cluster{ lex => PrevLex }, lemma => '_NUMBER' },
	    lex_tokenize(PrevLex,PrevLLex::[_,_|_]),
	    domain(Lex,PrevLLex) ->
	    %% specific case for some numbers such as 20 millions !
	    XPos is Pos,
	    U is Pos-1,
	      record( conll_compound(U) ),
	    record( conll_forward(Prev,N) ),
	    record( conll_backward(N,Prev) ),
	    record( conll_redirect(NId,U) )
	  ;   lex_tokenize(Lex,[PrevLex,_|_]),
	      Prev =  node{ lemma => PLemma, cat => PCat, cluster => cluster{ lex => PrevLex }},
	      node{ id => NId2, cluster => cluster{ lex => Lex } },
	      NId \== NId2 ->
	      %% cases like "m�me_s'il"  with m�me_s = 1 CONLL tok for 1 Sxipe word (m�me)
	      %% and m�me_s'il = 2 CONLL tok for 2 sxpipe word (si + il)
	      XPos is Pos,
	      U is Pos-1,
	      record( conll_compound(U) ),
	      record( conll_forward(Prev,N) ),
	      record( conll_backward(N,Prev) ),
	      record( conll_redirect(NId,U) ),
	      record( conll_mixed_case(NId) )
	  ;   %% this case should be controlled by am option
	    %% do we wish to follow the token segmentation or the wordform segmentatiom
	    lex_tokenize(Lex,LLex::[_,_|_]),
	    \+ ( node{ id => _NId, cluster => cluster{ lex => Lex }},
		 _NId \== NId ) ->
	    lex2conll_forms(LLex,CLexL),
	    record( conll_compounds(NId,CLexL) ),
	    %% one single node for multiple tokens
	      mutable(MPos,Pos,true),
	      mutable(MRank,1,true),
	      ( 
		conll_expand_sxpipe_compound(CLexL,Expansion) ->
		record( conll_expansion(NId,Expansion) ),
		conll_register_expansion(NId,LLex,Expansion,MPos,MRank)
	      ;
		every(( domain(_Lex,LLex),
			mutable_read(MPos,_Pos),
			mutable_read(MRank,_Rank),
			%%		      format('here0 ~w ~w ~w\n',[_Pos,_Lex,_Rank]),
			_XPos is _Pos + 1,
			mutable(MPos,_XPos),
			_XRank is _Rank+1,
			mutable(MRank,_XRank),
			record( conll2node(_Pos,NId) ),
			record( conll_lex(_Pos,_Lex) ),
			record( conll_rank(_Pos,_Rank) ),
			%%		      format('## seg ~w lex=~w lemma=~w\n',[_Pos,_Lex,Lemma]),
			( ( (Lemma = entities[~ ['_NUMBER']] ; Lemma = date[]),			  
			    \+ node2conll(NId,_),
			    conll_form(_Lex,_Form),
			    \+ is_number(_Form),
			    \+ domain(_Form,[le,la,les,de,du,des,'Le','La','Les','De','Du','Des']),
			    \+ domain(_Form,['.',',','-','(',')','_',':','[',']','<','>'])
			  )
			->
			  record( node2conll(NId,_Pos) )
			;
			true
			)
		      ))
	      ),
	      mutable_read(MPos,XPos),
	      LastPos is XPos - 1,
	      ( node2conll(NId,_) xor record( node2conll(NId,LastPos) )),
	      record( conll_last_rank(LastPos) ),
	    record( conll_last_rank(NId,LastPos) )
	  ;
%%	    format('here1 ~w ~w\n',[Pos,Lex]),
	      (	  Prev = node{ id => PNId, cluster => cluster{ lex => Lex }},
		  recorded( conll_mixed_case(PNId) ),
		  lex_tokenize( Lex,[_,Lex2]) ->
		  record( conll_lex(Pos,Lex2) )
	      ;	  
		  true
	      ),
	    record( conll2node(Pos,NId) ),
	    record( node2conll(NId,Pos) ),
	      XPos is Pos + 1
	  ),
	  conll_ids(Right,XPos,N)
	;
	  true
	)
	.

:-std_prolog conll_expand_sxpipe_compound/2.
:-extensional conll_simple_expansion/2.
:-rec_prolog conll_complex_expansion/2.

conll_expand_sxpipe_compound(CLex,Expansion) :-
	( conll_simple_expansion(CLex,Expansion) -> true
	; conll_complex_expansion(CLex,Expansion) -> true
	; fail
	)
	.

:-finite_set(million,['millions','milliards','million','milliard','centaine','centaines','millier','milliers',dizaine,'dizaine']).
:-finite_set(month,[janvier,f�vrier,mars,avril,mai,juin,juillet,ao�t,septembre,octobre,novembre,d�cembre]).
:-finite_set(day,['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']).

conll_complex_expansion([N,million[]],[det:1,head]) :- is_number(N).
conll_complex_expansion([N,month[]],[mod:1,head]) :- is_number(N).
conll_complex_expansion([month[],N],[head,mod: -1]) :- is_number(N).
conll_complex_expansion([day[],N,month[]],[head,mod: -1, mod: -2]) :- is_number(N).
%conll_complex_expansion([N1,'-',N2],[head,ponct: -1,mod: -2]) :- is_number(N1), is_number(N2).
conll_complex_expansion([month[],Next],[head, mod: -1]) :- domain(Next,[prochain,suivant,dernier]).
conll_complex_expansion([Prep1,Noun,Prep2],[head,obj: -1,dep: -1]) :-
	domain(Prep1,[au,�,en]),
	domain(Prep2,[de,des,du,'d'''])
	.
conll_complex_expansion([Prep1,Noun,Csu],[head,obj: -1,dep: -1]) :-
	domain(Prep1,[au,�,en]),
	domain(Csu,[que,'qu'''])
	.

/*
conll_simple_expansion(['l''',un],[det: 1, head]).
conll_simple_expansion(['les',autres],[det: 1, head]).
conll_simple_expansion(['mais',aussi],[head, mod: -1]).
conll_simple_expansion(['m�me',si],[obj:1,head]).
conll_simple_expansion(['comme',si],[head,obj: -1]).
conll_simple_expansion([�,terme],[head, obj: -1]).
conll_simple_expansion([pour,autant],[head, obj: -1]).
conll_simple_expansion([en,vigueur],[head, obj: -1]).
conll_simple_expansion([en,place],[head, obj: -1]).
conll_simple_expansion([depuis,longtemps],[head, obj: -1]).
conll_simple_expansion([de,plus],[head, obj: -1]).
conll_simple_expansion([de,moins],[head, obj: -1]).
conll_simple_expansion([de,loin],[head, obj: -1]).
conll_simple_expansion([au,total],[head, obj: -1]).
conll_simple_expansion(['afrique',du,'sud'],[head, dep: -1, obj: -1]).
*/

:-std_prolog conll_register_expansion/5.

conll_register_expansion(NId,LLex,Expansion,MPos,MRank) :-
	( LLex = [_Lex|LLex2],
	  Expansion = [Exp|Expansion2] ->
	  mutable_read(MPos,_Pos),
	  mutable_read(MRank,_Rank),
	  _XPos is _Pos+1,
	  mutable(MPos,_XPos),
	  _XRank is _Rank + 1,
	  mutable(MRank,_XRank),
	  record( conll2node(_Pos,NId) ),
	  record( conll_lex(_Pos,_Lex) ),
	  record( conll_rank(_Pos,_Rank) ),
	  ( Exp = head ->
	    record( node2conll(NId,_Pos))
	  ; Exp = (Type : Delta),
	    Head is _Pos + Delta,
	    record( conll_expansion_edge(_Pos,Type,Head) ),
	    true
	  ),
	  conll_register_expansion(NId,LLex2,Expansion2,MPos,MRank)
	;
	  true
	)
	.


:-light_tabular conll_rep/2.
:-mode(conll_rep/2,+(+,-)).

conll_rep(NId,CId) :-
	( node2conll(NId,CId)
	xor recorded( conll_redirect(NId,CId) )
	)
	.


:-std_prolog conll_collect_node_info/1.
:-extensional conll_node_info/6.

conll_collect_node_info(Pos) :-
	( conll2node(Pos,NId) ->
	    N::node{ id => NId,
		     cat => Cat,
		     lemma => Lemma,
		     form => Form,
		     cluster => cluster{ lex => _Lex, token => Token },
		     lemmaid => Lemmaid
		 },
	    (	node2op(NId,OId),
		op{ id => OId }
	    ->	true
	    ;	
		OId = '_'
	  ),
	    XPos is Pos + 1,
	    MSTag = '_',	% to refine
	    (	recorded( conll_lex(Pos,Lex) ), Compound = 1 xor Lex = _Lex, Compound = 0 ),
	    conll_form(Lex,CLex),	  
	    conll_lemma(Lemma,Token,CLemma),
	  ( domain(CLex,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','/']) ->
	  CCat1 = 'PONCT',
	    FullCat1 = 'PONCT'
	  ; Compound = 1, Lemma = entities[],
	    domain(CLex,[de,'d'''])
	    ->
	    CCat1 = 'P',
	    FullCat1 = 'P'
	  ;
	    Compound = 1, Lemma = entities[],
	    domain(CLex,[le,la,'Le','La',les,'Les','l''','L'''])
	    ->
	    CCat1 = 'D',
	    FullCat1 = 'DET'
	  ;
	    ( conll_cat(Cat,CCat1,Lemma,N,CLex) xor CCat1 = Cat ),
	    (	conll_fullcat(Cat,CCat1,OId,N,FullCat1,CLex) xor FullCat1 = CCat1)
	  ),
	  ( recorded(conll_forward(N,_)) ->
		%% multitoken
		conll_collect_cats(N,CCat1,FullCat1,CCat,FullCat)
	    ;	
		CCat = CCat1,
		FullCat = FullCat1
	    ),
	    record( conll_node_info(Pos,CLex,CLemma,CCat,FullCat,MSTag) ),
	    conll_collect_node_info(XPos)
	;   
	    true
	)
	.

:-xcompiler
node2conllinfo(node{ id => NId},Lex,Lemma,Cat,FullCat,MSTag) :-
	conll_rep(NId,Pos),
	conll_node_info(Pos,Lex,Lemma,Cat,FullCat,MSTag)
	.

:-xcompiler
node2conllcat(N::node{},Cat) :-
	node2conllinfo(N,_,_,Cat,_,_)
	.

:-xcompiler
node2conllfullcat(N::node{},FullCat) :-
	node2conllinfo(N,_,_,_,FullCat,_)
	.


conll_emit(Pos) :-
	( conll2node(Pos,NId) ->
	    conll_node_info(Pos,CLex,CLemma,CCat,FullCat,MSTag),
	    N::node{ id => NId, lemma => Lemma, cat=> Cat, lemma => Lemma, form => Form },
	    recorded( node2conll(NId,_Pos) ),
	    ( _Pos == Pos ->
	      ( conll_edge(Pos,_Head,Label,Name,_) ->
		( conll2node(_Head,_HNId),
		  recorded( conll_last_rank(_HNId,_LastHead) ),
		  _Head < _LastHead,
		  recorded(conll_lex(_LastHead,_LastLex)),
		  conll_form(_LastLex,_LastForm),
		  domain(_LastForm,[de,du,des,'d''','que','qu''']) ->
		  %% if the head correspond to a complex prep or complex csu, we take its last pos as the target head
		  Head = _LastHead
		;
		  Head = _Head
		)
	      ; Head = 0,  Label = root,
		Name = 'R_root',
		( recorded(mode(robust)) ->
		  true
		; recorded( conll_root(RootPos) ) ->
		  ( edge{ target => N,
			  label => _Label,
			  type => __Type::edge_kind[~ virtual],
			  source => node{ cat => _SCat, lemma => _SLemma }
			},
		    _Type = __Type, _XLemma = Lemma, _XCat = Cat
		  xor
		  conll2node(RootPos,RootNId),
		    edge{ target => node{ id => RootNId, lemma => _XLemma, cat => _XCat },
			  label => _Label,
			  type => __Type::edge_kind[~ virtual],
			  source => node{ cat => _SCat, lemma => _SLemma }
			},
		    _Type = __Type
		  xor
		  _Label = none,
		    _SCat = none,
		    _SLemma = none,
		    _Type = none,
		    _XLemma = Lemma, _XCat = Cat
		  ),
		  format('## *** multiple root ~w and ~w cat=~w lemma=~w uplabel=~w uptype=~w upcat=~w uplemma=~w\n',
			 [RootPos,Pos,_XCat,_XLemma,_Label,_Type,_SCat,_SLemma])
		;
		  record( conll_root(Pos) )
		)
	      )
	    ;	
	      ( recorded( conll_expansion_edge(Pos,Label,_Head) ) ->
		true
	      ;
		( ( Lemma = '_NUMBER'
		  ; domain(CLex,[le,la,les,de,du,des,'Le','La','Les','De','Du','Des','l''','L'''])),
		  recorded( conll_rank(Pos,1) )
		) ->
		_Head = _Pos, % use mod dependencies inside a splitted node (rough approx !)
		Label = det,
		XCCat = 'D',
		XFullCat = 'DET'
	      ; CCat = 'P' ->
		_Head = _Pos, % use mod dependencies inside a splitted node (rough approx !)
		Label = dep
	      % ; CCat = 'D' ->
	      % 	  Label = det
	      ;
		_Head = _Pos, % use mod dependencies inside a splitted node (rough approx !)
		Label = mod
	      ),
	      conll_edge_register(Pos,_Head,Label,'R_compounds',[]),
	      ( conll_edge(Pos,Head,Label,Name,_) xor
	      %% this case should not arise !
	      Head=0 )
	    ),
	    PHead = '_',
	    PLabel = '_',
	    XCCat ?= CCat,
	    XFullCat ?= FullCat,
	    %% emitting
	    format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\n',
		   [Pos,CLex,CLemma,XCCat,XFullCat,MSTag,Head,Label,PHead,PLabel,Name]),
	    XPos is Pos + 1,
	    conll_emit(XPos)
	;   
	    true
	)
	.

:-light_tabular lex2conll_forms/2.
:-node(lex2conll_forms/2,+(+,-)).

lex2conll_forms(LLex,CLexL) :- lex2conll_forms_aux(LLex,CLexL).

:-std_prolog lex2conll_forms_aux/2.

lex2conll_forms_aux(LLex,CLexL) :-
	(LLex = [Lex|LLex2] ->
	 conll_form(Lex,_CLex),
	 '$interface'(lowercase(_CLex:string),[return(CLex:string)]),
	 lex2conll_forms_aux(LLex2,CLexL2),
	 CLexL=[CLex|CLexL2]
	;
	 CLexL=[]
	)
	.

:-light_tabular conll_form/2.
:-mode(conll_form/2,+(+,-)).

conll_form(Lex,CLex) :-
	lex_tokenize(Lex,L),
	conll_form_aux(L,CL),
	(CL=[CLex] xor name_builder('~L',[['~w','_'],CL],CLex))
	.

:-std_prolog conll_form_aux/2.

conll_form_aux(L,CL) :-
	( L = [SLex|XL] -> 
	  ( rx!tokenize(SLex,'|',[_,CSLex]) xor CSLex = SLex ),
	  conll_form_aux(XL,XCL),
	  CL = [CSLex|XCL]
	;
	  CL = []
	).

:-std_prolog conll_lemma/3.

conll_lemma(Lemma,Form,CLemma) :-
	( Lemma = cat[cln,ilimp,cla,clg,clr,clg,cll,cld] ->
	  CLemma = Form
	; domain(Lemma,[ '_META_TEXTUAL_PONCT',
			 '_Uv',
			 '_NUMBER',
			 '_PERSON_m',
			 '_PERSON_f',
			 '_PERSON',
			 '_EPSILON',
			 '_DATE_artf',
			 '_DATE_arto',
			 '_LOCATION',
			 '_DATE_year',
			 '_COMPANY',
			 '_ORGANIZATION',
			 '_ETR',
			 '_PRODUCT',
			 '_ADRESSE',
			 '_ROMNUM',
			 '_NUM',
			 '_META_TEXTUAL_GN',
			 '_NP_WITH_INITIALS',
			 '_HEURE',
			 '_TEL'
		       ]) ->
	  rx!tokenize(Form,' ',L),
	  (L = [CLemma] xor name_builder('~L',[['~w','_'],L],CLemma))
	;
	  rx!tokenize(Lemma,' ',L),
	  (L = [CLemma] xor name_builder('~L',[['~w','_'],L],CLemma))
	)
	.


:-std_prolog conll_is_adj/2.

conll_is_adj(E::edge{ type => Type, source => N }, A) :-
	( Type = adj ->
	  A = N
	;   
	  node!empty(N),
	  _E::edge{ target => N },
	  conll_is_adj(_E,A)
	)
	.

:-std_prolog conll_ancestor/3.

conll_ancestor(N::node{ id => NId},A::node{ id => _NId },Head) :-
	%%	format('call conll ancestor ~w\n',[N]),
	( conll_rep(NId,Head) ->
	  A = N
	; edge{ target => N, source => _N, type => adj } ->
	  conll_ancestor(_N,A,Head)
	; edge{ source => N, target => A, type => subst },
	  conll_rep(_NId,Head) ->
	  A = _N
	; edge{ source => N, target => A, type => lexical },
	  conll_rep(_NId,Head) ->
	  A = _N
	;
	  fail
	)
	.

:-light_tabular conll_modal_climbing/2.
:-mode(conll_modal_climbing/2,+(+,-)).

conll_modal_climbing(V,Anchor) :-
	( chain( V::node{} >> (adj @ 'V') >> Modal::node{}) ->
	    conll_modal_climbing(Modal,Anchor)
	;   
	    V=Anchor
	)
	.

:-light_tabular conll_aux2v/2.
:-mode(conll_aux2v/2,+(+,-)).

conll_aux2v(N::node{},V::node{ cat => v}) :-
	( edge{ target => N,
		source => _N::node{ cat => cat[v,aux] },
		label => 'Infl',
		type => adj
	      } ->
	    conll_aux2v(_N,V)
	;   
	    N=V
	).
	      
:-light_tabular conll_intro_or_self/2.
:-mode(conll_intro_or_self/2,+(+,-)).

conll_intro_or_self(N::node{},Intro::node{}) :-
	( chain( N << subst << _N::node{ cat => cat[prep,csu] } ) ->
	    Intro = Prep
	;
	    Intro = N
	)
	.

:-light_tabular conll_main_verb/2.
:-mode(conll_main_verb/2,+(+,-)).

%% climb aux till main verb
conll_main_verb(N::node{},V::node{}) :-
	( chain( N << (adj @ 'Infl') << _N::node{} ) ->
	    conll_main_verb(_N,V)
	;   
	    V = N
	)
	.

:-features(conll!relation,[dep,head,type,name,reroot]).

:-std_prolog conll_head/0.
       
conll_head :-
	%% find a relation
	( conll!relation{ type => Type, dep => _D, head => _H, name => Name, reroot => Reroot}
	; E::edge{ source => _H::node{}, target => _D::node{} },
	  conll_simple_relation(E,Type,Name)
	),
	Reroot ?= [],
	conll_up_till_non_empty(_H,H::node{ id => HNId }),
	conll_down_till_non_empty(_D,D::node{ id => DNId}),
%%	format('pre register name=~w _H=~w H=~w _D=~w D=~w\n',[Name,_H,H,_D,D]),
	conll_rep(DNId,DPos),
	conll_rep(HNId,HPos),
	DPos \== HPos,
	conll_edge_register(DPos,HPos,Type,Name,Reroot)
	.

:-std_prolog conll_down_till_non_empty/2.

conll_down_till_non_empty(N::node{},M::node{}) :-
	( node!empty(N) ->
	    chain( N >> subst @ Label >> N2::node{} ),
	    Label \== start,
	    Label \== end,
	    conll_down_till_non_empty(N2,M)
	;   
	    M=N
	)
	.

:-light_tabular conll_up_till_non_empty/2.
:-mode(light_tabular/2,+(+,-)).

conll_up_till_non_empty(N::node{ id => NId },M::node{}) :-
	( node!empty(N) ->
	  ( chain( N << edge_kind[adj,subst] << N1::node{ id => NId1 } ),
	    NId \== NId1
	  ->
	    conll_up_till_non_empty(N1,M)
	  ; chain( N >> subst @ start >> node{} ) ->
	    chain( N >> subst @ Label >> A1::node{} ),
	    Label \== start,
	    Label \== end,
	    ( node!empty(A1) ->
	      chain( A1 >> subst >> M )
	    ;
	      A1 = M
	    )
	  ; chain( N >> subst >> M::node{} ),
	    \+ node!empty(M)
	  )
	;
	  M = N
	)
	.

:-xcompiler
conll_verbose(Msg,Args) :-
	( recorded( opt(conll_verbose) ) ->
	    format(Msg,Args)
	;
	    true
	)
.	

:-std_prolog conll_edge_register/5.
:-extensional conll_edge/5.

conll_edge_register(DPos,HPos,Type,Name,Reroot) :-
	( recorded( Call::call_conll_edge_register(DPos,HPos,Type,Name,Reroot) ) ->
	  format('## *** loop in edge register ~w ~w ~w ~w ~w\n',[DPos,HPos,Type,Name,Reroot]),
	  fail
	;
	  conll_verbose('## +++ edge register ~w ~w ~w ~w ~w\n',[DPos,HPos,Type,Name,Reroot]),
	  record( Call ),
	  true
	),
	( conll_edge(HPos,_HPos,_Type,_Name,_Reroot),
	  domain(redirect,_Reroot) ->
	  conll_edge_register(DPos,_HPos,Type,Name,Reroot)
	; domain(mirror,Reroot),
	  conll_edge(HPos,_HPos,_Type,_Name,_Reroot) ->
	  conll_edge_register(DPos,_HPos,_Type,_Name,_Reroot)
	; E::conll_edge(DPos,_HPos,_Type,_Name,_Reroot) ->
	  %% potential race between two governor
	  %% we try to solve the problem using some heuristic rules !!
	    conll_verbose('## *** potential head race at pos=~w between (~w,~w,<~w>,~w) and (~w,~w,<~w>,~w)\n',
			  [DPos,HPos,Type,Name,Reroot,_HPos,_Type,_Name,_Reroot]),
	  ( Type = _Type, Type = ponct ->
	      %% don't solve conflict for ponct-ponct races
	      true
	  ;   
	      recorded( conll_edge_shift(DPos,_HPos2,NewDPos,_HighPos) ),
	      conll_is_connected(_HPos2,_HPos),
	      \+ _HPos == _HighPos ->
	      %% explicit request for edge shift ! (eg: clefted construction)
	      conll_verbose('## +++ use shift info ~w ~w ~w\n',[DPos,_HPos2,NewDPos]),
	      erase(E),
	      conll_edge_register(NewDPos,_HPos,_Type,_Name,_Reroot)
	  ;
	      recorded( conll_edge_shift(DPos,HPos2,NewDPos,HighPos) ),
	      conll_is_connected(HPos2,HPos),
	      \+ HPos == HighPos ->
	      conll_verbose('## +++ use shift info ~w ~w ~w\n',[DPos,HPos2,NewDPos]),
	      conll_edge_register(NewDPos,HPos,Type,Name,Reroot)
	  ;   ( domain(redirect,_Reroot)
	      ;	  HPos < _HPos, _HPos < DPos
	      ;	  _HPos < DPos, DPos < HPos
	      ),
	      \+ domain( dep_frozen, Reroot ) ->
	      conll_edge_register(_HPos,HPos,Type,Name,Reroot)
	  ;   ( domain(redirect,Reroot)
	      ;	  _HPos < HPos, HPos < DPos
	      ;	  HPos < DPos, DPos < _HPos
	      ),
	      \+ domain( dep_frozen, _Reroot) ->  
	      (	  \+ conll_is_connected(HPos,DPos)
	      xor
	      format('### *** acyclicity break for (~w,~w,<~w>)\n',[DPos,HPos,Type,Name]),
		  fail
	      ),
	      erase(E),
	      record( conll_edge(DPos,HPos,Type,Name,Reroot) ),
	      conll_edge_register(HPos,_HPos,_Type,_Name,_Reroot)
	  ; ( HPos == _HPos, _Type = Type ) -> true
	  ;
	    %% true race condition
	    %% warn for tracing these cases and do nothing
	    format('## *** head race at pos=~w between (~w,~w,<~w>) and (~w,~w,<~w>)\n',
		   [DPos,HPos,Type,Name,_HPos,_Type,_Name]),
	    true
	  )
	; domain(up,Reroot),
	  conll_edge(HPos,HPos2,_,_,_) ->
	  %% try to move up when possible
	  conll_edge_register(DPos,HPos2,Type,Name,Reroot)
	;
	  DPos \== HPos,
	    ( \+ conll_is_connected(HPos,DPos) -> true
	    %% cyclicity may arise from CONLL compound terms, that may play both head and dep roles for DEPXML
	    %% in that case, we favor the head role
	    ;	recorded(conll_compound(DPos)) ->
		%% keep old dep, don't add new one !
		fail
	    ;	E4::conll_edge(HPos,DPos,_,_,_), recorded(conll_compound(HPos)) ->
		%% erase the old one, add new one
		conll_verbose('## +++ erase ~w\n',[E4]),
		erase(E4)
	    ;	
		format('### *** acyclicity break for (~w,~w,<~w>)\n',[DPos,HPos,Type,Name]),
		fail
	    ),
	  record( conll_edge(DPos,HPos,Type,Name,Reroot) ),
	  %% move up any waiting edge or if edge enforce redirection
	  every(( E1::conll_edge(_DPos,DPos,_Type,_Name,_Reroot),
		  ( domain(up,_Reroot) xor domain(redirect,Reroot) ),
		  \+ domain(head_frozen,_Reroot),
		  erase(E1),
		  every( erase( call_conll_edge_register(_DPos,HPos,_Type,_Name,_Reroot) ) ),
		  conll_edge_register(_DPos,HPos,_Type,_Name,_Reroot)
		)),
	  %% mirror for enum: the head of the first coord becomes the head of all coordinated
	  every(( E1::conll_edge(_DPos,DPos,_Type,_Name,_Reroot),
		  domain(mirror,_Reroot),
		  erase(E1),
		  every( erase( call_conll_edge_register(_DPos,HPos,_Type,_Name,_Reroot) ) ),
		  conll_edge_register(_DPos,HPos,Type,Name,Reroot)
		))
	)
	.

:-std_prolog conll_is_connected/2.

conll_is_connected(Pos1,Pos2) :-
	( Pos1 == Pos2 ->
	  true
	; conll_edge(Pos1,Pos3,_,_,_),
	  conll_is_connected(Pos3,Pos2)
	)
	.

:-light_tabular conll_apply_reroot/4.
:-mode(conll_apply_reroot/4,+(+,+,+,-)).

:-rec_prolog
	conll!relation{},
	conll_simple_relation/3.

%% conll!relation{ dep => DepNode, head => HeadNode, type => Type }

%% obj      object (including sentential obj)
%% obj1     ? (error?)
%% mod      most modidiers
%% det      determiners
%% ponct    (almost) all punctuations [check coma as coord]
%% dep
%% suj     subject
%% root    entry point (no governor)
%% dep_coord  components of a coordination
%% coord    coordinations 
%% ats  attribut subject
%% ato  attribut object
%% mod_rel  relative sentenc
%% a_obj    preparg introduced by �
%% de_obj   preparg introduced by de
%% p_obj    other prepargs
%% aff
%% aux_tps     auxiliaries for past tense
%% aux_pass    auxiliaries for passive
%% aux_caus    auxiliaries for causative
%% arg
%% comp    PP modifiers on S
%% missinghead

conll!relation{ type => Type, dep => S, head => T , name => 'R_modal'} :- % *** reroot
	chain( T::node{ cat => cat[~ coo], lemma => Lemma } << (adj @ 'V') << S::node{}),
	( Lemma = sembler -> Type = ats ; Type = obj )
	.

conll!relation{ type => det, dep => T, head => S, name => 'R_det'} :-
	chain( S::node{} >> (_ @ label[det,det1,number]) >> T::node{} ),
	node2conllfullcat(T,conll_fullcat['DET','DETWH']).

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_det_range' } :-
	%% eg: 2 � 3 pommes
	%% not clear what is the correct annotation for FTB
	%% we find:
	%%       - all det dep to noun (selected here)
	%%       - D1 det to governor of N, A: dep of D1, D2 obj of A or det of N
	%%       - use of arg deprel for A
	%% also seem different in cases such as "de X � Y N"
	chain( N::node{}
	     >> subst @ det >> D1::node{ cat => number }
	     >> ( lexical @ prep >> A::node{ cat => prep, lemma => � }
		& lexical @ number2 >> D2::node{ cat => number }
		)
	     ),
	( Type = det, Dep = D1, Head = N
	; Type = det, Dep = A, Head = N
	; Type = det, Dep = D2, Head = N
	)
	.

%% predet de
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_predet_de' } :-
	chain( N::node{} >> subst @ det >> PDet::node{ cat => predet } >> lexical @ de >> De::node{} ),
	( Type = dep, Dep = De, Head = PDet
	;   Type = obj, Dep = N, Head = De
	)
	.

%% adv quantity modifier (pr�s de la moiti� de X)
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_adv_qmod' } :-
	chain( N::node{}  >> adj @ 'N2' >> Adv::node{ cat => adv, tree => Tree } >> lexical @ prep >> Prep::node{} ),
	domain(quantity_adv_N2,Tree),
	( Type = dep, Dep = Prep, Head = Adv
	; Type = obj, Dep = N, Head = Prep
	)
	.
	


%% partitive
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_partitive' } :-
	chain( Pro::node{ cat => pro } >> lexical @ prep >> De::node{ cat => prep, lemma => de } ),
	Type = obj,
	Head = De,
	Dep = Pro
	.

%% predet2 'beaucoup de nos '
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_predet2' } :-
	chain( Predet::node{ cat => predet }
	     << adj @ det << Det::node{ cat => cat[det,number] }
	     << subst @ det << N::node{} ),
	chain( Predet >> lexical @ de >> De::node{ cat => prep } ),
	( chain( Predet >> lexical @ entre >> Entre::node{ cat => prep }) ->
	  ( Type = dep, Dep = De, Head = Predet
	  ;  Type = obj, Dep = Entre, Head = De
	  ; Type = obj, Dep = N, Head = Entre
	  )
	;
	  ( Type = dep, Dep = De, Head = Predet
	  ; Type = obj, Dep = N, Head = De
	  )
	)
	.

conll!relation{ type => suj, dep => T, head => V, name => 'R_subject'} :-
	edge{ source => _V::node{},
	      label => Label::label[subject,impsubj],
	      target => T::node{}
	    },
	%% deep causative subj are not used by CONLL
	\+ chain( _V >> lexical @ causative_prep >> node{}),
	\+ ( chain( _V << subst @ xcomp << K::node{ id => KId, lemma => faire } ),
	       node2live_ht(KId,HTId),
	       check_arg_feature(HTId,arg1,kind,vcompcaus)
	   ),
	%% no deep subject (in concurrence with impsub)
	( Label = impsubj xor \+ chain( _V >> (_ @ impsubj) >> node{})),
	%% climb to non aux verb
	conll_aux2v(_V,V1::node{ cat => Cat1 }),
	%% adjectives may not be subj head
	( Cat1 = adj -> chain( V1 >> adj @ 'Infl' >> V2::node{ cat => v}) ; V1=V2 ),
	%% use first modal verb if any
	conll_modal_climbing(V2,V)
	.

%% deep causative subj are not used by CONLL
%% => prep as mod of verb and deep subj obj of prep
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_causative' } :-
	( chain( Faire::node{ cat => v, lemma => faire, id => NId }
	       >> subst @ xcomp
	       >> V::node{ cat => v }
	       >> subst @ subject >> DeepSubj::node{}
	       ),
	  node2live_ht(NId,HTId),
	  check_arg_feature(HTId,arg1,kind,vcompcaus)
	;
	  chain( Faire
	       << adj @ 'S'
	       << V::node{ cat => v }
	       >> subst @ subject >> DeepSubj::node{}
	       )
	),
	(   chain( V >> lexical @ causative_prep >>  P::node{ cat => prep }) ->
	    (	Type = mod, Dep = P, Head = V
	    ;	Type = obj, Dep = DeepSubj, Head = P
	    )
	;
	    Type = obj, Dep =DeepSubj, Head = V
	)
	.

%% adj with impsubj become ats and its deep subj become an obj of verb
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_adj_head' } :-
	chain( Adj::node{ cat => cat[adj]} >> adj @ 'Infl' >> V::node{}),
	( Type = ats, Dep = Adj, Head = V
	;   ( chain( Adj >> lexical @ impsubj >> S1::node{} ) ->
		(   
		    chain( Adj >> subst @ subject >> S2::node{}),
		    Type = obj, Dep = S2, Head = V
		;   conll_modal_climbing(V,V1),
		    Type = suj, Dep = S1, Head = V1
		)
	    ;	chain( Adj >> _ @ subject >> S1::node{}),
		conll_modal_climbing(V,V1),
		Type = suj, Dep = S1, Head = V1
	    )
	)
	.

%% sentence with adj as head and a deep subj but no verb
conll!relation{ type => dep, dep => Subj, head => Adj, name => 'R_adj_head_short' } :-
	chain( Adj::node{ cat => adj } >> subst @ subject >> Subj::node{} ),
	\+ chain( Adj >> adj @ 'Infl' >> node{} )
	.

%% v with impsubj and deep subj
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_v_impsubj' } :-
	chain( S1::node{}
	     << lexical @ impsubj << V::node{ cat => cat[v]}
	     >> subst @ subject >> S2::node{} ),
	( Type = obj, Dep = S2, Head = V
	; conll_modal_climbing(V,V1),
	  Type = suj, Dep = S1, Head = V1
	)
	.

%% object
conll!relation{ type => obj, dep => Dep, head => Head, name => 'R_object'} :-
	edge{ label => object, target => T::node{} , source => Head::node{} },
	( node!empty(T) ->
	  %% il veut de quoi manger
	  ( chain( T >> lexical @ prep >> De::node{ cat => prep, lemma => de } ) ->
	    Dep = De
	  ;
	    Dep = T
	  )
	;
	  Dep = T
	).

conll_simple_relation(edge{ label => label[clr],
			    source => V::node{ id => NId, lemma => Lemma }
			  },L,'R_CLR') :-
	node2live_ht(NId,HTId),
	( 
	  check_ht_feature(HTId,refl,(+)) ->
	    L = aff
	;   conll_se_moyen(Lemma) ->
	    L = aff
	;   chain( V >> (lexical @ impsubj) >> node{} ) ->
	    L = aff
	;
	  check_xarg_feature(HTId,args[arg1,arg2],Fun::function[],_,clr),
	  (	
		Fun = function[obj,att],
		conll_se_obj(Lemma)
	  ->
		L = obj
	  ;	Fun = function[obj�],
		conll_se_aobj(Lemma) ->
		L = a_obj
	  ;
		L=aff
	  )
	)
	.

conll_simple_relation(edge{ label => label[clg],
			    source => node{ id => NId }
			  },L,'R_CLG') :-
	node2live_ht(NId,HTId),
	(   check_xarg_feature(HTId,args[arg1,arg2],Fun::function[],_,clg) ->
	    (	
		Fun=function[obj,att] ->
		L = obj
	    ;	Fun=function[objde] ->
		L = de_obj
	    ;	Fun=function[loc,dloc,obl,obl2]->
		L=p_obj
	    ;	
		L = aff
	    )
	;
	    L = aff
	)
	.

conll_simple_relation(edge{ label => label[cll] },aff,'R_CLL').

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_coord' } :-
	%% reused from Passage conversion
	E::edge{ type => adj,
		 target => LastCoord::node{ cat => coo },
		 label => StartLabel,
		 source => Start::node{}
	       },
	( StartLabel = xcomp ->
	  ( edge{ source => Start,
		  target => XStart,
		  label => label[csu,prep],
		  type => lexical
		}
	  xor 
	  edge{ source => Start,
		target => XStart,
		label => xcomp,
		type => edge_kind[~ [adj]]
	      }
	  )
	; node!empty(Start), chain( Start >> lexical @ en >> XStart::node{}) ->
	  true
	; node!empty(Start), chain( Start >> subst @ start >> node{} ) ->
	  conll_down_till_non_empty(Start,XStart)
	;
	  XStart = Start
	),
	( _N2 = Start
	; edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	\+ node!empty(Coord),
	( node!empty(_N2) ->
	  ( edge{ source => _N2,
		  target => N2,
		  type => edge_kind[subst,lexical]
		},
	    N2=node{ cat => cat[v,prep,nc,np] }
	  xor _N2 = Start, N2=XStart
	  )
	; _N2 = Start ->
	  N2 = XStart
	;
	  N2 = _N2
	),
	( node!first_main_verb(N2,XN2)
	xor get_head(N2,XN2)
	),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	get_head(N3,XN3),
	( Type = coord, Dep = Coord, Head = XStart
	;
	  XN3 = node{ cluster => cluster{ left => XN3_Left } },
	  ( chain( Coord >> lexical @ en >> En::node{ cluster => cluster{ right => En_Right } } ),
	    En_Right =< XN3_Left,
	    \+ ( chain( Coord::node{} >> subst @  label[coord2,coord3]
		      >> N4::node{ cluster => cluster{ left => N4_Left, right => N4_Right}}
		      ),
		 En_Right =< N4_Left,
		 N4_Right =< XN3_Left
	       )
	  ) ->
	  ( Type = dep_coord, Dep = En, Head = Coord
	  ; Type = obj, Dep = XN3, Head = En
	  )
	;
	  Type = dep_coord, Head = Coord,
	  ( XN3 = node{ cat => v, cluster => cluster{ left => XN3_Left, right => XN3_Left }} ->
	    %% ellipsis on verb
	    edge{ source => XN3, target => N4 },
	    get_head(N4,XN4),
	    Dep = XN4
	  ;
	    Dep = XN3
	  )
	),
	true
	.

%% coma before last coord is a ponct
conll_simple_relation( edge{ type => lexical,
			     source => node{ cat => coo, cluster => cluster{ left => L} },
			     target => node{ lemma => ',', cluster => cluster{ right => L} }
			   },
		       ponct, 'R_coma_before_coo'
		     ).

%% coord on xcomp arg need rerooting to prep and csu
conll!relation{ type => obj, dep => Dep, head => Head, name => 'R_xcomp_intro_in_coord' } :-
	edge{ source => COO::node{ cat => coo },
	      type => lexical,
	      label => label[prep,csu],
	      target => Head::node{ cluster => cluster{ right => Right } }
	    },
	edge{  source => COO,
	       type => subst,
	       label => xcomp,
	       target => Dep::node{ cluster => cluster{ left => Left } }
	    },
	\+ ( edge{ source => COO,
		   target => node{ cluster => cluster{ left => _Left, right => _Right } }
		 },
	     Right =< _Left, _Right =< Left
	   )
	.
	
%% pb with head: need to understand the rules
conll_simple_relation( edge{ label => ni, type => lexical },coord, 'R_ni'). 

%% X ou non
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_ou_non' } :-
	chain( N::node{} >> adj >> node{}
	     >> ( lexical @ coo >> Ou::node{ cat => coo }
		& lexical @ advneg >> Non::node{}
		)
	     ),
	( Type = coord, Dep = Ou, Head = N
	; Type = dep_coord, Dep = Non, Head = Ou
	)
	.

%% a starter coord become the head of a sentence
%% reroot
conll!relation{ type => dep_coord, head => COO, dep => V, name => 'R_starter_coord' } :-
	E::edge{ label => starter,
		 type => lexical,
		 target => COO::node{ cat  => coo}
	       },
	conll_is_adj(E,V)
	.

conll!relation{ type => dep_coord, head => COO, dep => N, name => 'R_short_starter_coord' } :-
	edge{ type => lexical,
	      label => coo,
	      source => _N::node{},
	      target => COO::node{ cat => coo }
	    },
	chain( _N >> subst @ start >> node{ cat => start } ),
	( node!empty(_N) ->
	    chain( _N >> subst @ Label >> N::node{} ),
	    \+ Label = start
	;   
	    N = _N
	)
	.

conll!relation{ type => mirror, dep => Dep, head => Head, name => 'R_enum_mirror', reroot => [mirror]} :-
	chain( Head::node{} >> adj >> node{} >> edge_kind[subst,lexical] @ coord >> Dep::node{} )
	.

conll!relation{ type => L, dep => Dep, head => V, name => 'R_att' } :-
	edge{ label => comp,
	      type => edge_kind[subst,lexical],
	      source => V::node{ cat => v, id => NId},
	      target => _Dep::node{}
	    },
	node2live_ht(NId,HTId),
	( check_ht_feature(HTId,ctrsubj,suj) ->
	    L = ats
	;
	    L = ato
	),
	( node!empty(_Dep) ->
	    chain( _Dep >> subst >> Dep::node{} )
	;
	    Dep = _Dep
	)
	.

conll_simple_relation( edge{ type => edge_kind[subst,lexical],
			     label => Label,
			     source => node{ cat => prep } },
		       obj, 'R_prep_obj' ) :-
	Label \== skip.

conll!relation{ type => mod, dep => T, head => V, name => 'R_adv_mod' } :-
	E::edge{ type => edge_kind[adj],
		 label => Label,
		 source => _V::node{ cat => SCat },
		 target => T::node{ cat => TCat::cat[adv,advneg,que_restr,adj,adjPref,advPref] }
	       },
	\+ domain(Label,['S','S2',vmod,det]),
	(   Label = adj,
	    TCat = cat[adv,advneg],
	    SCat = 'N2',
	    chain( _V >> subst @ 'SubS' >> V::node{})
	->  
	    true
	;   Label = 'V1', TCat=advneg, SCat = cat[v,aux], chain( _V << adj @ 'Infl' << node{ cat => adj }) ->
	    V = _V
	;
	    conll_main_verb(_V,V)
	)
	.

conll_simple_relation( edge{ type => lexical, label => advneg, source => node{ cat => cat[~ coo]} },mod,'R_ante_advneg').

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_quantity_on_adv' } :-
	%% eg: un an plus tard
	%% FTB6 not clear on coherent annotation !
	chain( Quant::node{ cat => nominal[] }
	     << adj @ quantity << Adv::node{ cat => adv }
	     << adj << H::node{}
	     ),
	Type = dep,
	Dep = Quant,
	Head = H
	.

conll!relation{ type => mod, dep => Dep, head => Head, name => 'R_adj_on_det' } :-
	chain( Dep::node{ cat => adj } << adj @ det << node{ cat => det } << subst << Head::node{} )
	.

conll_simple_relation(edge{ label => 'Np2', target => node{ cat => cat[~ coo]}},mod, 'R_Np2').
conll_simple_relation(edge{ label => 'N2',  type => adj, target => node{ cat => cat[np] }},mod, 'R_np').
conll_simple_relation(edge{ label => 'adjP',
			    type => adj,
			    target => node{ cat => cat[~ [prep,coo]] }},mod,'R_adjP').

conll_simple_relation(edge{ label => advneg,
			    type => edge_kind[adj,lexical],
			    target => node{ cat => que_restr} },mod,'R_advneg_que').

conll_simple_relation(edge{ label => nc, type => adj, target => node{ cat => cat[~ coo]}},mod,'R_nc_mod').
conll_simple_relation(edge{ label => clneg, source => node{ cat => cat[v,aux] }},mod,'R_clneg').

conll!relation{ type => mod, dep => CLNEG, head => V, name => 'R_clneg_on_adj' } :-
	chain( CLNEG::node{ cat => clneg }
	     << lexical @ clneg
	     << node{ cat => adj }
	     >> adj @ 'Infl' >> V::node{ cat => v }
	     ).

/*
conll!relation{ type => ponct, dep => T, head => A, name => 'R_incise' } :-
	E::edge{ source => node{ cat => incise }, target => T::node{}},
	node2conllfullcat(T,'PONCT'),
	conll_is_adj(E,A).
*/

%% follow internal reroot for punctuation
conll!relation{ type => ponct, dep => T, head => A, name => 'R_ponct', reroot => Reroot } :-
	E::edge{ source => S::node{ cat => SCat, cluster => cluster{ left => Left }, tree => Tree },
		 target => T::node{ cluster => cluster{ right => Right }}
	       },
	node2conllfullcat(T,'PONCT'),
	\+ (SCat == coo, Right < Left ),
	( \+ node!empty(S) ->
	    A=S
	; domain(quoted[],Tree) ->
	    (conll_down_till_non_empty(S,A) xor chain( S << adj << A) )
	;   SCat = incise, chain(S << adj << node{} << adj << A::node{} ) ->
	    true
	;   
	    A  = S,
	    Reroot = [up]
	),
	true
	.

conll!relation{ type => Type, dep => T, head => A, name => 'R_PP_mod' } :-
	E::edge{ label => L::label['N2','adjP', 'PP'],
		 source => N::node{},
		 target => T::node{ cat => prep, lemma => TLemma } },
	conll_is_adj(E,_A),
	(conll_down_till_non_empty(_A,A) xor _A = A),
	node2conllfullcat(T,FullCat),
	%% conll_fullcat['P','P+D']
	( FullCat = 'CC' ->
	  fail
	; A = node{ cat => coo } ->
	  Type = dep_coord
	; FullCat = 'ADV' -> Type = mod
	; L = 'PP' ->
	  ( TLemma = de,
	      A = node{ cat => v, lemma => Lemma },
	      domain(Lemma,[augmenter,r�duire,doter,targuer,contraindre,ralentir,
			    cro�tre,progresser,chuter,manquer,retarder,d�border,
			    convenir,�lever,amputer,soucier,accompagner,b�n�ficier,
			    disposer,offusquer,menacer,priver,disposer,perdre,
			    rivaliser,inqui�ter,marquer,accoucher,�vincer,reculer,passer,tirer,
			    traiter,�quiper,diminuer,baisser,d�passer,d�missionner,
			    importer,accro�tre,d�valuer,obliger,contenter,rallonger,
			    t�moigner,�carter,qualifier,occuper,accommoder,s�parer,sortir,
			    �tonner,conna�tre,inculper,rapprocher,d�faire,appr�cier,composer,servir,provenir,
			    majorer,grimper,sur�valuer,dater,souffrir,revaloriser,prot�ger,pr�c�der,obtenir,
			    inspirer,gargariser,enorgueillir,�loigner,exiger,dispenser,discuter,d�tourner,d�tacher,
			    attendre,assortir,d�pendre
			   ]) ->
	      %% the info about these verbs should take place in Lefff
	      Type = de_obj
	  ; TLemma = �,
	      A = node{ cat => v, lemma => Lemma },
	      domain(Lemma,[destiner,r�pondre,estimer,proc�der,participer,�valuer,limiter,situer,affecter,adapter,
			    tarder,suffir,servir,reverser,parvenir,opposer,octroyer,�tablir]) ->
	      Type = a_obj
	  ;
	      A = node{ cat => v, lemma => Lemma },
	      domain(Lemma:TLemma,[traduire:par,porter:sur,inscrire:dans,d�boucher:sur,aligner:sur,
				   expliquer:par,choisir:entre,
				   peser:sur,solder:par,caract�riser:par,
				   tomber:dans,
				   lutter:contre,
				   tourner:vers,
				   red�ployer:vers,
				   prononcer:sur,
				   tirer:sur,
				   recentrer:sur,
				   replier:sur,
				   pencher:sur,
				   consister:en,
				   protester:contre,
				   engager:dans,
				   protester:contre,
				   �lever:contre,
				   investir:dans,
				   r�investir:dans,
				   retomber:dans
				  ]) ->
	      Type = p_obj
	  ; A = node{ cat => cat[nc,adj,np] } ->
	    Type = dep		% may arise in short sentence
	  ;   
	    Type = mod
	  )
	; Type = dep
	).

%% comparative
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_comparative' } :-
	edge{ type => adj,
	      label => supermod,
	      source => Adv::node{ cat => adv },
	      target => Mod1::node{}
	    },
	chain( Adv << adj << X::node{ cat => XCat } ),
	( XCat = v -> XType = mod ; XType = dep ),
	( node!empty(Mod1) ->
	    chain( Mod1 >> edge_kind[subst,lexical] @ 'Modifier' >>  Mod2::node{}),
	    (	chain( Mod1 >> lexical @ que >> Que::node{}) ->
		Mod = Mod2
	    ;	Mod2 = node{ cat => adj, lemma => possible } ->
		QueAlt=false, Que = Mod2
	    ;	Que  = Mod2,
		chain( Que >> subst @ 'S' >> Mod::node{} )
	    ),
	    (	Type = XType, Dep = Que, Head = X
	    ;	QueAlt=true, Type = obj, Dep = Mod, Head = Que
	    )
	;
	    Mod = Mod1,
	    Type = XType,
	    Dep = Mod,
	    Head = X
	)
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_comparative_m�me' } :-
	chain( N::node{}
	     >> adj >> Adj::node{ cat => adj }
	     >> ( lexical @ que  >> Que::node{}
		& subst @ 'Comparative' >> Mod::node{}
		)
	     ),
	( XCat = v -> XType = mod ; XType = dep ),
	( Type = XType,
	    Dep = Que,
	    Head = N
	; Type = obj,
	    Dep = Mod,
	    Head = Que
	)
	.

%% *** CONLL guideline: we climb till main verb (as found in FTB6 but in contradiction with guidelines !)
conll!relation{ type => Type, dep => T, head => V, name => 'R_aux' } :-
	E::edge{ label => 'Infl',
		 type => adj,
		 source => _V::node{ cat => SCat, id => NId },
		 target => T::node{ lemma => TLemma }
	       },
	conll_main_verb(_V,V::node{ cat => v}),
	( TLemma = avoir ->
	  Type = aux_tps
	; SCat = aux ->
	  Type = aux_tps
	; node2live_ht(NId,HTId),
	  ( check_ht_feature(HTId,diathesis,passive) ->
	    Type = aux_pass
	  ;
	    Type = aux_tps
	  )
	).

%% relative with antecedent
conll!relation{ type => mod_rel, dep => T, head => S::node{ }, name => 'R_SRel' } :-
	E::edge{ label => label['SRel','N2Rel'], source => _S::node{ cat => Cat::cat['N2',ce] }, target => T},
	( Cat = 'N2', conll_is_adj(E,S)
	xor Cat=ce, S=_S
	).

conll_simple_relation(edge{ label => ncpred, type => lexical},obj,'R_ncpred').
conll_simple_relation(edge{ label => csu,
			    type =>lexical,
			    source => cat[~coo]
			  }, obj, 'R_csu_arg'). % to check

conll!relation{ type => L, dep => T, head => Head, name => 'R_sentential_arg' } :-
	E::edge{ label => xcomp,
		 type => subst,
		 target => T::node{ },
		 source => S::node{ cat => v, id => NId, lemma => Lemma }
	       },
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,Arg::args[arg1,arg2],
			   Fun,
			   FKind::fkind[scomp,prepscomp,vcomp,prepvcomp,whcomp,prepwhcomp,vcompcaus],
			   _
			  ),
%%	check_arg_feature(HTId,Arg,extracted,Extracted),
%%	format('here fun=~w fkind=~w ~w\n',[Fun,FKind,E]),
	( Fun = obj ->
	  L = obj,
	  ( FKind = scomp ->
	    edge{ source => S, type => lexical, label => csu, target => Head::node{} }
	  ; FKind = whcomp,
	    edge{ source => S, type => lexical, label => siwh, target => Head } ->
	    true
	  ; FKind = prepvcomp ->
	    edge{ source => S, type => lexical, label => prep, target => Head }
	  ; FKind = vcompcaus -> % causative
	      \+ (
		     chain( S >> _ @ object >> node{} )
		 ;
		     chain( T >> _ @ subject >> node{} ) 
		 ),
	      Head = S
	  ;   
	      Head = S
	  )
	; Fun = att ->
	    %%	  Head = S,
	    ( check_ht_feature(HTId,ctrsubj,obj) ->
		_L = ato
	    ;	
		_L = ats
	    ),
	    ( FKind = scomp ->
		edge{ source => S, type => lexical, label => csu, target => Head::node{} },
		L = obj
	    ;	FKind = prepvcomp ->
		edge{ source => S, type => lexical, label => prep, target => Head },
		L = obj
	    ;	
		Head = S,
		L = _L
	    )
	; Fun = function[obj�,objde] ->
	    edge{ source => S, type => lexical, target => _S::node{ cat => cat[prep,que] } },
	    Head = _S,
	    L = obj
	;   
	    Head = S,
	    L = obj
	)
	.

conll!relation{ type => dep, dep => T, head => S, name => 'R_que_arg' } :-
	edge{ label => csu,
	      type => lexical,
	      target => T::node{ cat => que },
	      source => V::node{ cat => cat[~ v], id => NId }},
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],_,fkind[prepscomp],_),
	edge{ source => V, target => S, label => xcomp, type => subst }
	.

conll!relation{ type => obj, dep => T, head => S, name => 'R_ce_que'} :-
	edge{ label => ce,
	      type => lexical,
	      target => T::node{ lemma => ce },
	      source => V::node{ cat => cat[v,adj], id => NId }},
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],_,fkind[prepscomp],_),
	edge{ source => V, target => S, label => prep, type => lexical }
	.

conll!relation{ type => Type, dep => T, head => V, name => 'R_que_scomp'} :-
	edge{ label => csu,
	      type => lexical,
	      target => T::node{ lemma => que },
	      source => V::node{ cat => v, id => NId }},
	node2live_ht(NId,HTId),
	check_xarg_feature( HTId,Arg::args[arg1,arg2],
			    F::function[obj,objde,att],
			    fkind[scomp],
			    _
			  ),
%%	format('que scomp arg=~w f=~w\n',[Arg,F]),
	( F = obj -> Type = obj
	; F = att -> Type = ats
	; Type = de_obj
	)
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_ce_est_que' } :-
	chain( Ce::node{ cat => cln, lemma => ce }
	     << lexical @ subject << Est::node{ cat => aux, lemma => �tre }
	     >> lexical @ csu >> Que::node{}
	     ),
	chain( Est << adj << V::node{} ),
	( Type = suj, Dep = Ce, Head = Est
	; Type = ats, Dep = Que, Head = Est
	; Type = obj, Dep = V, Head = Que
	)
	.

conll!relation{ type =>Type, dep => Dep, head => Head, name => 'R_dep_sentential' } :-
	chain( N::node{ cat => cat[adj,nc,adv] } >>
	     ( subst @ xcomp >> SubS::node{}
	     & lexical @ label[prep,csu] >> Intro::node{}
	     )),
	( Type = dep, Dep = Intro, Head = N
	; Type = obj, Dep = SubS, Head = Intro
	)
	.

%% causative as aux_caus
conll!relation{ type => aux_caus, dep => Faire, head => V, name => 'R_aux_caus', reroot => [redirect,dep_frozen] } :-
	( chain( Faire::node{ lemma => faire, cat => v, id => NId } >> subst @ xcomp >> V::node{} )
	;
	  chain( Faire << adj @ 'S' << V ),
	  Adj = yes
	),
	(   chain( Faire >> _ @ object >> node{} )
	;   chain( V >> _ @ subject >> node{} ),
	    (Adj = yes xor
	    node2live_ht(NId,HTId),
	    check_arg_feature(HTId,arg1,kind,vcompcaus)
	    )
	)
	.

conll!relation{ type => obj, dep => T, head => V, name => 'R_siwh'} :-
	edge{ label => siwh,
	      type => lexical,
	      target => T::node{ lemma => TLemma },
	      source => V::node{ cat => cat[v], id => NId }},
	domain(TLemma,[si,comme]),
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],obj,fkind[whcomp],_)
	.

conll!relation{ type => L, dep => T, head => V, name => 'R_v_preparg' } :-
	edge{ label => label[prep,preparg],
	      type => lexical,
	      target => T::node{ lemma => TLemma, cat => cat[prep,prel,pri] },
	      source => V::node{ cat => cat[v], id => NId}
	    },
	node2live_ht(NId,HTId),
	check_xarg_feature( HTId,args[arg1,arg2],
			    Fun::function[obj�,objde,obl,obl2,obj,att],
			    fkind[prepscomp,prepvcomp,prepwhcomp],
			    _
			  ),
	( Fun = obj -> L=obj
	; TLemma = 'de' -> L='de_obj'
	; TLemma = '�' -> L = 'a_obj'
	; L = 'p_obj'
	)
	.

conll!relation{ type => dep, dep => Dep, head => Head, name => 'R_preparg_alt' } :-
	chain( Head::node{ cat => cat[pres,adv] } >> subst @ preparg >> Dep::node{ cat => prep } )
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_obj_by_xcomp_adj' } :-
	edge{ target => V1::node{ cat => v, lemma => Lemma },
	      source => V2::node{ cat => v },
	      type => adj,
	      label => 'S'
	    },
	\+ ( Lemma = faire, chain( V2 >> lexical @ causative_prep >> node{} )),
	( chain( V1 >> lexical @ label[csu,prep] >> Intro::node{} ) ->
	  ( fail, Type = obj, Dep = Intro, Head = V1 % dealt with R_v_preparg
	  ; Type = obj, Dep = V2, Head = Intro
	  )
	; Type = obj, Dep = V2, Head = V1
	)
	.

conll!relation{ type => Type, dep => T, head => A, name => 'R_vmod'} :-
	E::edge{ target => _T::node{ cat => _TCat::cat[~ coo], tree => Tree, cluster => cluster{ left => TLeft } },
		 source => node{ cat => SCat },
		 label => _Label::label[vmod,audience,'SubS',reference,person_mod,time_mod,'S','S2',np,
				'S_incise', position
				] },
	
	( node!empty(_T) ->
	  edge{ source => _T,
		target => T::node{ cat => TCat::cat[~ coo] },
		type => edge_kind[~ adj],
		label => Label
	      },
	  Label = label[audience,'SubS',reference,person_mod,np,'S_incise',position]
	; T = _T,
	  Label = _Label,
	  node2conllfullcat(T,conll_notponct[~ ['CC']])
	),
	conll_is_adj(E,_A::node{ cat => _ACat, cluster => cluster{ left => _ALeft }}),
	%%	format('tree ~w\n',[Tree]),
	\+ domain(cleft_verb, Tree),
	\+ ( _Label = 'S', TCat = 'v' ), % xcomp with extraction done by adj
	\+ ( _Label = label[vmod,'S','S2'], chain( _T >> subst @ 'PP' >> node{} )),
	( _ACat = v,
	  ( TLeft < _ALeft xor TCat = cat[csu] xor Label = label['SubS',audience,time_mod,position])
	->
	  conll_modal_climbing(_A,A)
	;
	  A = _A
	),
	( fail, SCat == 'N2' ->
	  Type = dep		% those cases in FTB6 seem to be annotation errors !
	;
	  Type = mod
	),
	true
	.

conll_simple_relation( edge{ label => label[predet_ante,predet_post,pas], type => lexical },mod,'R_det_mod').

%conll_simple_relation( edge{ label => supermod, type => adj },mod,'R_supermod').

%% comparative

%% head inversion for Monsieur
%% rerooting
conll!relation{ type => mod, dep => S, head => T, name => 'R_Monsieur', reroot => [redirect] } :-
	E::edge{ label => 'Monsieur', type => lexical, target => T, source => S}
	.

%% preparg -> <x>_obj x in {a,de,p [par,sur,pour,avec ...]}
conll!relation{ type => L, dep => Dep, head => Head, name => 'R_x_obj' } :-
	E::edge{ label => label[preparg],
		 type => edge_kind[subst,lexical],
		 target => T::node{ cat => TCat, lemma => TLemma, cluster => cluster{ left => Left, lex => Lex} },
		 source => S::node{ cat => cat[v,adj]}
	       },
	( TCat = cld ->
	  L = 'a_obj', Dep = T, Head = S
	; TCat = prep ->
	  Dep = T, Head = S,
	  ( TLemma = 'de' -> L='de_obj'
	  ; TLemma = '�' -> L = 'a_obj'
	  ; L = 'p_obj'
	  )
	; TCat = prel ->
	  ( TLemma = dont ->
	    L='de_obj', Dep = T, Head = S
	  ; TLemma = lequel,
	    node{ cat => prep, lemma => TTLemma, cluster => cluster{ right => Left, lex => Lex } } ->
	    ( TTLemma = '�' -> L = 'a_obj'
	    ; TTLemma = 'de' -> L = 'de_obj'
	    ; L = 'p_obj'
	    ),
	    Dep = T, Head = S
	  ; chain( S >> lexical @ prep
		 >> Prep::node{ cat => prep,
				lemma => TTLemma,
				cluster => cluster{ right => PRight } }
		 ),
	    PRight =< Left
	    ->
	    ( Dep = Prep, Head = S,
	      ( TTLemma = 'de' -> L='de_obj'
	      ; TTLemma = '�' -> L = 'a_obj'
	      ; L = 'p_obj'
	      )
	    ; L = obj, Dep = T, Head = Prep
	    )
	  ;
	    L = p_obj, Dep = T, Head = S
	  )
	;
	  fail
	)
	.

conll!relation{ type => mirror, dep => Dep, head => Head, name => 'R_etc_enum', reroot => [mirror] } :-
	chain( Dep::node{ form => 'etc.' } << lexical @ void << Head::node{} )
	.
%%conll_simple_relation(edge{ label => 'PP', type => adj},,comp).

conll!relation{ type => mod, dep => T, head => S, name => 'R_N2app'} :-
	E::edge{ label => 'N2app', target => T},
        conll_is_adj(E,S).

conll_simple_relation(edge{ label => skip, type => epsilon },ponct,'R_skip_ponct').

conll!relation{ type => ponct, dep => T, head => A, name => 'R_ponct' } :-
	edge{ source => node{ cat => unknown}, target => T::node{ id => NId}},
	node2conllfullcat(T,'PONCT'),
	%% robust mode: need to find some plausible governor !
	conll_rep(NId,TPos),
	( TPos > 1 ->
	    Pos is TPos -1
	;   Pos is TPos + 1
	),
	conll2node(Pos,ANId),
	A::node{ id => ANId }
	.

%%conll!relation(edge{ label => void, type => lexical},'PONCT',_,ponct).

conll_simple_relation(edge{ label => 'Nc2', type => lexical},mod,'R_Nc2').

%% xcomp before verb are considered as principals => Rerooting
conll!relation{ type => mod, dep => V, head => N, name => 'R_xcomp_incise' } :-
	E::edge{ source => V::node{ cat => v, cluster => cluster{ left => VLeft} },
		 target => _N::node{ cluster => cluster{ left => _NLeft }, tree => Tree},
		 type => adj,
		 label => 'S'
	       },
	_NLeft < VLeft,
	domain(quoted_sentence_as_ante_mod,Tree),
	conll_down_till_non_empty(_N,N)
	.

%% same for quoted sentences in ante position
conll!relation{ type => mod, dep => V, head => N, name => 'R_quoted_incise' } :-
	E::edge{ source => V::node{ cat => v, cluster => cluster{ left => VLeft} },
		 target => _N::node{ cluster => cluster{ left => _NLeft }},
		 type => subst,
		 label => xcomp
	       },
	_NLeft < VLeft,
	conll_down_till_non_empty(_N,N)
	.

%% embedded verb in CS
conll_simple_relation( edge{ label => 'S',
			     type => subst,
			     source => A::node{ cat => cat[~ prep] }
			   },obj,'R_CS') :- \+ node!empty(A).

%% juxtaposed sentences
conll!relation{ type => mod, dep => T, head => A, name => 'R_juxt_S'} :-
	E::edge{ source => N::node{},
		 target => T::node{},
		 label => Label,
		 type => subst
	       },
	node!empty(N),
	\+ Label = coord,
	conll_up_till_sep_S(N,_A::node{ cat => Cat} ),
	( node!empty(_A) ->
	  chain( _A >> (subst @ start) >> node{}),
	  chain( _A >> subst >> A1::node{} >> subst >> A2::node{}),
	  ( node!empty(A1) -> A = A2 ; A = A1 )
	;   
	  A = _A
	)
	.

%% prep mod
conll_simple_relation( edge{ type => adj, target => node{ cat => cat[~ coo]}, label => prep },mod,'R_prep_mod').

conll_simple_relation( edge{ type => adj, target => node{ cat => cat[~ coo]}, label => csu },mod,'R_csu_mod').

%% csu on N2
conll_simple_relation( edge{ type => adj,
			     target => node{ cat => csu},
			     source => node{ cat => nominal[] }
			   },mod,'R_csu_on_N2').

%% en intro particiale
conll!relation{ type => obj, dep => Dep, head => Head, name => 'R_en_participiale' } :-
	chain( Head::node{ cat => prep, lemma => en} << lexical @ en <<
	     node{} >> subst @ 'SubS' >> Dep::node{} )
	.

%% extracted modifiers
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_extracted_modifier' } :-
	chain( Rel::node{ lemma => Lemma } << lexical @ label[prel,pri] << S::node{} << adj << V::node{} ),
	( chain(S >> lexical @ prep >> Prep::node{}) ->
	  ( Type = mod, Dep = Prep, Head = V
	  ; Type = obj, Dep = Rel, Head = Prep
	  )
	; Lemma = dont ->
	  Type = de_obj, Dep = Rel, Head = V
	;
	  Type = mod, Dep = Rel, Head = V
	)
	.

conll!relation{ type => mod, dep => Prep, head => V, name => 'R_wh_modifier' } :-
	chain( Prep::node{ cat => prep } << subst @ wh << node{} << adj << V::node{} )
	.

%% quels que S, S
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_quel_que' } :-
	chain( VSub::node{} << subst @ 'SRel' << S::node{} >> subst @ 'N2' >> Quel::node{} ),
	node!empty(S),
	chain( S << adj << V::node{} ),
	( Type = mod, Dep = Quel, Head = V
	; Type = mod_rel, Dep = VSub, Head = Quel
	)
	.

%% CS
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_CS_subs' } :-
	chain( SSub::node{} << subst @ 'SubS' << CSU::node{ cat => csu } << adj << S::node{} ),
	Type = obj, Dep = SSub, Head = CSU
	.

%% N2 countable mod: 20 euros la tonne
conll_simple_relation( edge{ type => adj,
			     label =>  'N2',
			     source => node{ cat => cat[~ coo] },
			     target => node{ cat => cat[nc] }
			   },
		       mod,
		       'R_N2_countable_mod'
		     ).

%% Clefted constructions
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_cleft1', reroot => Reroot } :-
	chain( Etre::node{ lemma => �tre, cat => aux, id => EtreNId } << adj << V::node{ id => VNId } 
	     >> lexical @ 'CleftQue' >> Que::node{ id => QueNId, cluster => cluster{ left => Left }}
	     ),
	chain( V
	     >> edge_kind[subst,lexical] @ Label
	     >> N::node{ id => NId, lemma => Lemma, cluster => cluster{ right => Right } } ),
	chain( Etre >> lexical @ subject >> Subj::node{} ),
	Right =< Left,
	conll_rep(QueNId,QuePos),
	conll_rep(NId,NPos),
	conll_rep(VNId,VPos),
	conll_rep(EtreNId,EtrePos),
	record( Shift::conll_edge_shift(NPos,VPos,QuePos,EtrePos) ),
	%% format( '## register edge shift ~w\n',[Shift]),
	(   Type = ats, Dep = N, Head = Etre
	;   Type = mod_rel, Dep = V, Head = Etre
	;   Type = suj, Dep = Subj, Head = Etre
	)
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_cleft2', reroot => Reroot } :-
	chain( Etre::node{ lemma => �tre, cat => aux, id => EtreNId }
	     << adj << V::node{ id => VNId } 
	     >> adj >> N1::node{}
	     >> lexical @ 'CleftQue' >> Que::node{ id => QueNId, cluster => cluster{ left => Left }}
	     ),
	( node!empty(N1) ->
	    chain( N1 >> subst >> N::node{ id => NId, lemma => Lemma, cluster => cluster{ right => Right } })
	;
	    N1 = N
	),
	chain( Etre >> lexical @ subject >> Subj::node{} ),
	%% Right =< Left,
	conll_rep(QueNId,QuePos),
	conll_rep(NId,NPos),
	conll_rep(VNId,VPos),
	conll_rep(EtreNId,EtrePos),
	record( Shift::conll_edge_shift(NPos,VPos,QuePos,EtrePos) ),
	%% format( '## register edge shift ~w\n',[Shift]),
	(   Type = ats, Dep = N, Head = Etre
	;   Type = mod_rel, Dep = V, Head = Etre
	;   Type = suj, Dep = Subj, Head = Etre
	)
	.

%% a prep modifying a prep becomes the head
conll!relation{ type => obj, dep => Dep, head => Head, name => 'R_prep_on_prep' } :-
	edge{ type => adj,
	      label => prep,
	      source => Dep::node{ cat => prep },
	      target => Head::node{ cat => prep }
	    }
	.


conll!relation{ type => mod,
		dep => N1::node{ cat => Cat::cat[adv,advneg], cluster => cluster{ left => L1, right => R1 }},
		head => N2::node{ cat => Cat2::cat[v,adj,adv], cluster => cluster{ left => L2, right => R2 } },
		name => 'R_robust1'
	      } :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj] },
	(  R1 = L2, N2, \+ ( Cat=advneg, Cat2=cat[adv,adj] )
	xor L1 = R2, Cat2=v, N2
	xor L1 is R2+1, Cat2=v, 
	   node{ cat => cln, cluster => cluster{ left => R2, right => L1}},
	   N2
	)
	.

conll!relation{ type => mod,
		dep => N1::node{ cat => cat[adj], cluster => cluster{ left => L1, right => R1 }},
		head => N2::node{ cat => cat[nc], cluster => cluster{ left => L2, right => R2 } },
		name => 'R_robust2'
	      } :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj,lexical] },
	(  R1 = L2, N2
	xor L1 = R2, N2
	)
	.

%% short sentences as incises
conll!relation{ type => mod, dep => Dep, head => Head, name => 'R_shortS_as_incise' } :-
	chain( S::node{} << subst @ 'S_incise' << node{} << adj << Head::node{} ),
	chain( S::node{} >> subst @ start >> node{ cat => start }),
	
	( node!empty(S) ->
	  chain( S::node{} >> subst @ Label >> Mod::node{} ),
	  Label \== start,
	  ( node!empty(Mod) ->
	    chain( Mod >> subst >> Dep::node{} )
	  ;
	    Dep = Mod
	  )
	;
	 Dep = S
	)
	.

%% Pas as starter is not the root of a short sentence
conll!relation{ type => mod, dep => Pas, head => Head, name => 'R_advneg_starter' } :-
	chain( Pas::node{ cat => advneg}
	     << lexical @ advneg << S::node{}
	     >> subst @ start >> node{}
	     ),
	( node!empty(S) ->
	  chain( S::node{} >> subst @ Label >> Mod::node{} ),
	  Label \== start,
	  ( node!empty(Mod) ->
	    chain( Mod >> subst >> Head::node{} )
	  ;
	    Head = Mod
	  )
	;
	 Head = S
	)
	.

%% pas as mod on coord
conll!relation{ type => mod, dep => Pas, head => Head, name => 'R_advneg_start_coo'} :-
	chain( Pas::node{ cat => advneg  }
	     << lexical @ advneg << COO::node{ cat => coo }
	     << adj << Head::node{} )
	.

:-std_prolog conll_up_till_sep_S/2.

%% climb up subst edges trough empty nodes till a S sep node (between two sentences)

conll_up_till_sep_S(N,A) :-
	node!empty(N),
	(   chain( N::node{} << subst << _N::node{} ) ->
	    conll_up_till_sep_S(_N,A)
	;
	    chain( N >> (lexical @ void) >> node{ lemma => L }),
	    domain(L,[',',';',':','.','...']),
	    chain( N << adj << A::node{} )
	)
	.

:-rec_prolog conll_cat/5.

conll_cat(cat[cln,cla,cld,clr,clg,ilimp,cll],'CL',_,N,_).
conll_cat(cat[clneg],'ADV',_,N,_).
conll_cat(cat[v,aux],T,_,N::node{},_) :-
	( single_past_participiale(N) -> T = 'A' ; T = 'V' ).

conll_cat(cat[det,number],L,_,N,Lex) :-
	( %fail,
	  domain(Lex,['millions','milliards','million','milliard',
		      'centaine','centaine','millier','milliers']) ->
	  L = 'N'
	;
	  L = 'D'
	).

conll_cat(Cat::cat[nc,ncpred,np,title],L,Lemma,N::node{ form => Form },Lex) :-
	( domain(Lemma,['_NUMBER','_NUM',un,autre,aucun,'le sien']),
	  \+ ( domain(Lex,['millions','milliards','million','milliard',
			    'centaine','centaine','millier','milliers',
			    'dizaine','dizaines',
			    'douzaine','douzaines',
			    'vingtaine','vingtaines'
			   ]),
	       chain( N >> adj >> node{ cat => prep, lemma => de } )
	     )
	->
	  L = 'PRO'
	; Lemma = '_ETR' ->
	  L = 'ET'
	; domain(Lex,['-']) ->
	  L = 'PONCT'
	; Cat = np,
	  domain(Lex,[f�d�ral,f�d�rale,
		      saoudite,
		      international,internationales,internationaux,
		      nouvel,
		      europ�en,europ�enne,
		      g�n�ral,g�n�rale,
		      central
		     ]) ->
	  L = 'A'
	;
	  L = 'N'
	).

conll_cat(adj,L,Lemma,N,Lex) :-
	( Lemma = '_ETR' ->
	  L = 'ET'
	; domain(Lex,['millions','milliards','million','milliard',
			    'centaine','centaine','millier','milliers',
			    'dizaine','dizaines',
			    'douzaine','douzaines',
			    'vingtaine','vingtaines'
			   ]) ->
	  L = 'N'
	;
	  L = 'A'
	).
	
conll_cat(cat[adv,advneg,que_restr,predet],'ADV',_,N,_).
conll_cat(cat[ponctw,poncts],'PONCT',_,N,_).
conll_cat(cat[csu,coo,que],'C',_,N,_).
conll_cat(cat[prep],'P',_,N,_).
conll_cat(cat[pro,prel,xpro,ce,caimp],'PRO',_,N,_).
conll_cat(cat[pri],T,Lemma,N,_) :-
	( pri(Lemma,'GR') -> T = 'ADV'
	; fail, domain(Lemma,['o�']) -> T = 'ADV' % to check
	; domain(Lemma,['quel?']) -> T = 'ADJ'
	;   T = 'PRO'
	)
	.
conll_cat(cat[pres],'I',_,N,_).
conll_cat('_','PONCT',Lemma,N,_) :-
	domain(Lemma,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_'])
	.
conll_cat('_','ADV',Lemma,N,_) :-	domain(Lemma,['etc.']).  %% should replace by a coanchor in FRMG
conll_cat('_','PRO',Lemma,N,_) :-	domain(Lemma,[ce]).  %% should replace by a coanchor in FRMG
conll_cat('_','P',Lemma,N,_) :-	domain(Lemma,[en]).  %% should replace by a coanchor in FRMG
conll_cat('_','C',Lemma,N,_) :-	domain(Lemma,[ou]).  %% should replace by a coanchor in FRMG

conll_cat('_','ADV',Lemma,N,_) :-
	recorded( mode(robust) ),
	domain(Lemma,[ne]).

conll_cat(_,'PONCT',Lemma,N,_) :- domain(Lemma,['_EPSILON','_META_TEXTUAL_PONCT']).

conll_cat(cat[adjPref,advPref],'PREF',_,N,_).

%%conll_cat(_,'ET','_ETR',_).

:-rec_prolog conll_fullcat/6.

conll_fullcat(cat[v,aux],CCat,OId,N,FullCat,_) :-
	OId \== '_',
	CCat \== 'A',
%	format('chek op feature ~w\n',[OId]),
	check_op_top_feature(OId,mode,Mode),
%	format('mode ~w ~w\n',[OId,Mode]),
	( chain(N::node{} >> (adj @ 'Infl') >> node{}) ->
	  FullCat = 'VPP'
	;   chain(N::node{} >> (adj @ 'V') >> node{ cat => v}) ->
	  FullCat = 'VINF'
	; Mode = infinitive ->
	  FullCat = 'VINF'
	;   Mode = participle ->
	    FullCat = 'VPP'
	;   Mode = gerundive ->
	  FullCat = 'VPR'
	;   Mode = imperative ->
	  FullCat = 'VIMP'
	;   Mode = indicative ->
	  FullCat = 'V'
	;   Mode = subjonctive ->
	  FullCat = 'VS'
	;   
	  FullCat = 'V'
	).

conll_fullcat(cat[csu,que],_,_,_,'CS',_).
conll_fullcat(coo,_,_,_,'CC',_).
conll_fullcat('_','C',_,_,'CC',_). 	% case of lexical ou (should replace by a coanchor in FRMG)
conll_fullcat(cat[nc,ncpred,title],'N',
	      _,N::node{ lemma => Lemma,
			 cluster => cluster{ left => Left,
					     lex => Lex } },
	      L,_) :-
	( fail,
	  %% some NC are labelled NPP
	  %% but difficult to find them !
	  Left > 0,
	  \+ edge{ source => N, label => 'Monsieur'},
	  capitalized_cluster(Lex) ->
	  L = 'NPP'
	;
	  L = 'NC'
	)
	.

conll_fullcat(_,'D',_,node{ lemma => Lemma, cluster => cluster{ token => Token } },L,_) :-
	( domain(Lemma,[quel]) -> L = 'DETWH'
	;   L = 'DET'
	).
%conll_fullcat(cat[ponctw,poncts],_,_,_,'PONCT').
%conll_fullcat(cat[prep],_,_,_,'P').
conll_fullcat(clr,_,_,_,'CLR',_).
conll_fullcat(cat[cln,ilimp],_,_,_,'CLS',_).
conll_fullcat(cat[cla,cld,clg,cll],_,_,_,'CLO',_).
%conll_fullcat(clneg,_,_,_,'ADV').
conll_fullcat(np,conll_cat[~ ['A']],_,
	      N::node{ lemma => Lemma,
		       cluster => cluster{ left => Left,
					   token => Token,
					   lex => Lex } },L,_) :-
%%	format('token=~w lex=~w\n',[Token,Lex]),
	( domain(Token,['pib','rmi','smic','pnb','sa','tva','soci�t�','groupe','banque','csa','btp','hlm','ces']) -> L = 'NC'
	; L = 'NPP'
	)
	.
conll_fullcat(cat[np,adj,v],'A',_,_,'ADJ',_).
conll_fullcat(adj,'N',_,_,'NC',_).
%conll_fullcat(cat[adv,advneg,que_restr,predet],_,_,_,'ADV').
%conll_fullcat(cat[pro,xpro,ce,caimp],_,_,_,'PRO').
conll_fullcat(cat[prel],'PRO',_,_,'PROREL',_).
conll_fullcat(cat[pri],'ADV',_,_,'ADVWH',_).
conll_fullcat(cat[pri],'PRO',_,_,'PROWH',_).
conll_fullcat(cat[pri],'ADJ',_,_,'ADJWH').
conll_fullcat(cat[pres],_,_,_,'I',_).

:-std_prolog conll_last_cat/2.

conll_last_cat([Cat|L],LastCat) :-
	( L=[] -> LastCat = Cat ; conll_last_cat(L,LastCat) )
	.

:-std_prolog conll_collect_cats/5, conll_collect_cats_aux/3.

conll_collect_cats(N::node{ lemma => Lemma, form => Form},CCat1,FullCat1,CCat,FullCat) :-
	recorded( conll_forward(N,Next) ),
	conll_collect_cats_aux(Next,_LCat,_LFullCat),
	LCat = [ CCat1 | _LCat ],
	LFullCat = [ FullCat1 | _LFullCat ],
	conll_last_cat(LCat,LastCat),
	conll_last_cat(LFullCat,LastFullCat),
%%	conll_verbose('collect ~w ~w cat1=~w cat=~w\n',[LCat,LFullCat,CCat1,LastCat]),
	( LFullCat = ['P','PROREL'] ->
	  CCat = 'P+PRO',
	  FullCat = 'P+PRO'
	; LFullCat = ['P','PRO'] ->
	  CCat = 'P+PRO',
	  FullCat = 'P+PRO'
	;   LCat = ['P','D'],
	    domain(Lemma,[�,de])
	->  
	  CCat = 'P+D',
	  FullCat = 'P+D'
	; LCat = ['N','D'] ->
	  CCat = 'D',
	  FullCat = 'DET'
	; LCat = ['N','PONCT'] ->
	  CCat = 'N',
	  FullCat = FullCat1
	; LCat = ['ADV','ADV'] ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LCat = ['ADV','D'] ->
	  CCat = 'P',
	  FullCat = 'P'
	; LFullCat = ['CLO','VPR'] -> % en chantant 
	    CCat = 'ADV',
	    FullCat = 'ADV'
	; LCat = ['PRO','ADV'] ->
	    CCat = 'ADV',
	    FullCat = 'ADV'
	;   LCat = ['ADV','PRO'] ->
	    CCat = 'C',
	    FullCat = 'CS'
	;   LCat = ['ADV','D'] ->
	    CCat = 'ADV',
	    FullCat = 'ADV'
	; LFullCat = ['ADV','CS'] ->
	  CCat = 'C',
	  FullCat = 'CS'
	; LFullCat = ['NPP','ADJ'] ->
	    CCat = 'N',
	    FullCat = 'NPP'
	;   LFullCat = ['ADV','PROREL'] ->
	    CCat = 'C',
	    FullCat = 'CS'
	;   LFullCat = ['ADV','ADJ'] ->
	    CCat = 'A',
	    FullCat = 'ADJ'
	; LFullCat = ['ADJ','ADJ'] ->
	  CCat = 'A',
	  FullCat = 'ADJ'
	; LFullCat = ['ADV','CC','ADV'] ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LFullCat = ['NC','PP','VINF'] ->
	  CCat = 'N',
	  FullCat = 'NC'
	; LFullCat = ['CLS','V','ADJ'] ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LFullCat = ['PRO','PONCT','PRO'] ->
	  CCat = 'N',
	  FullCat = 'NC'
	; CCat1 = 'PREF' ->
	  CCat = LastCat,
	  FullCat = LastFullCat
	; CCat1 = 'V' ->
	  ( LastCat = 'CL' ->
	    CCat = 'CL',
	    FullCat = 'CLS'
	  ;
	    CCat = 'V',
	    FullCat = FullCat1
	  )
	; LCat = ['CL','CL','V'] -> % il y a
	  CCat = 'P',
	  FullCat = 'P'
	; LastCat = 'P' ->
	  CCat = 'P',
	  FullCat = 'P'
	; CCat1 = 'ADV', LastCat = 'D' ->
	  CCat = 'P',
	  FullCat = 'P'
	; CCat1 = 'P', LastCat = 'D' ->
	  CCat = 'P',
	  FullCat = 'P'
	; CCat1 = 'P', LastCat = 'C' ->
	  CCat = 'C',
	  FullCat = 'CS'
	; CCat1 = 'P' ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LastCat = 'N' ->
	  CCat = 'N',
	  ( FullCat1 = 'NNP' ->
	    FullCat = 'NNP'
	  ;
	    FullCat = 'NC'
	  )
	; LastCat = 'D',
	  domain('P',LCat) ->
	  CCat = 'P',
	  FullCat = 'P'
	; 
	 name_builder('~L',[['~w','+'],LCat],CCat),
	 name_builder('~L',[['~w','+'],LFullCat],FullCat)
	),
%	format('=> ~w ~w\n',[CCat,FullCat]),
	true
	.

conll_collect_cats_aux(N::node{ id => NId,
				cat => Cat,
				form => Form,
				cluster => cluster{ token => Token },
				lemma => Lemma },
			LCat,LFullCat) :-
	 ( node2op(NId,OId),
	    op{ id => OId }
	  -> true
	  ;
	    OId = '_'
	  ),
	(conll_cat(Cat,CCat,Lemma,N,Token) xor CCat = Cat),
	(conll_fullcat(Cat,CCat,OId,N,FullCat,Token) xor FullCat = CCat),
	( recorded( conll_forward(N,Next) ) ->
	  conll_collect_cats_aux(Next,_LCat,_LFullCat),
	  LCat = [CCat|_LCat],
	  LFullCat = [FullCat|_LFullCat]
	;
	  LCat = [CCat],
	  LFullCat = [FullCat]
	)
	.
	
:-extensional node2wid/2.

:-light_tabular source2wid/3.
:-mode(source2wid/3,+(+,+,-)).

source2wid(NId,CId,WId) :-
	( node2wid(NId,WId) -> true
	; node{ id => NId1, cluster => cluster{ id => CId } },
	  NId1 \== NId ->
	  source2wid(NId1,CId,WId)
	;
	  edge{ source => node{ id => NId1, cluster => cluster{ id => CId1 } },
		target => node{ id => NId }
	      } ->
	  source2wid(NId1,CId1,WId)
	; recorded( erased(edge{ source => node{ id => NId1, cluster => cluster{ id => CId1 } },
				 target => node{ id => NId }
			       } ) ) ->
	  source2wid(NId1,CId1,WId)
	;
	  WId = none
	)
	.

:-light_tabular target2wid/3.
:-mode(target2wid/3,+(+,+,-)).

target2wid(NId,CId,WId) :-
	( node2wid(NId,WId) -> true
	; node{ id => NId1, cluster => cluster{ id => CId }},
	  NId1 \== NId ->
	  target2wid(NId1,CId,WId)
	;
	  edge{ target => node{ id => NId1, cluster => cluster{ id => CId1 } },
		source => node{ id => NId }
	      } ->
	  target2wid(NId1,CId1,WId)
	;
	  recorded( erased(edge{ target => node{ id => NId1, cluster => cluster{ id => CId1 } },
				source => node{ id => NId }
			      }
			 )
		 ) ->
	  target2wid(NId1,CId1,WId)
	;
	  WId = none
	)
	.


:-std_prolog depxml_emit/1.

depxml_emit(Disamb) :-
	( Disamb == disamb -> Stage = 'DEPXML' ; Stage = 'NODIS'),
	emit_multi(Stage),
	sentence(SId),
	recorded(mode(Mode)),
	event_ctx(Ctx,0),
	Handler=default([]),
	event_process(Handler,start_document,Ctx),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	event_process(Handler,xmldecl,Ctx),
	( recorded(mode(R)) ->
	  Attrs = [mode:R]
	;
	  Attrs = []
	),
	xml!wrapper(Handler,
		    Ctx,
		    'dependencies',
		    [id:SId|Attrs],
		    event_process(Handler,[depclusters,depnodes,depedges,depops,dephts,depcost],Ctx)
		   ),
	xevent_process(Handler,end_document,Ctx,Handler),
	format('\n',[]),
	true
	.

dstats_emit :-
	emit_multi('DSTATS'),
	mutable(MClusters,0,true),
	every(( cluster{},
		mutable_add(MClusters,1)
	      )),
	mutable_read(MClusters,NClusters),
	%%
	mutable(MNodes,0,true),
	mutable(MDerivs,0,true),
	every(( node{ deriv => Derivs },
		mutable_add(MNodes,1),
		length(Derivs,K),
		mutable_add(MDerivs,K)
	      )),
	mutable_read(MNodes,NNodes),
	mutable_read(MDerivs,NDerivs),
	%%
	mutable(MEdges,0,true),
	every(( edge{},
		mutable_add(MEdges,1)
	      )),
	mutable_read(MEdges,NEdges),
	%%
	Amb is 1.0 * (1 + NEdges - NClusters) / NClusters,
	AvgDerivs is 1.0 * NDerivs / NNodes,
	format('Dependency stats: ~w ambiguity ~w clusters ~w nodes ~w edges ~w derivs ~w avgderivs\n',
	       [Amb,NClusters,NNodes,NEdges,NDerivs,AvgDerivs]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% loading easy disamb hints

:-std_prolog easy_disamb_info/0.

easy_disamb_info :-
	W = 5000,
	every(( sentence(SId),
		( recorded(easy!group(SId,Left,Right,GType,First_Left:First_Right,Last_Left:Last_Right)),
		  ( GType = 'GP' ->
		    record_without_doublon( extra_node_cost( node{ cat => prep,
								   cluster => cluster{ left => First_Left,
										       right => First_Right
										     }
								 }
							   )
					  )
		  ; GType = 'PV' ->
		    record_without_doublon( extra_node_cost( node{ cat => prep,
								   cluster => cluster{ left => First_Left,
										       right => First_Right
										     }
								 }
							   )
					  ),
		    record_without_doublon( extra_node_cost( node{ cat => v,
								   cluster => cluster{ left => Last_Left,
										       right => Last_Right
										     }
								 }
							   )
					  ),
		      true
		  ; GType = 'GN' ->
		    record_without_doublon( extra_node_cost( node{ cat => nominal[],
								   cluster => cluster{ left => Last_Left,
										       right => Last_Right
										     }
								 }
							   )
					  )
%		  ; GType = 'NV' -> fail
		  ;
		      true
		  ),
%		  ( GType = 'NV' -> fail ; true),
%		    fail,
		    record( extra_chunk_cost(Left,Right,GType,W) ),
		    true
		  ;
%%		  fail,
		  recorded(easy!rel(SId,Source:_,Target:TargetRight,RType)),
		  ( RType = rel['SUJ-V','MOD-V','CPL-V','COD-V','ATB-SO','AUX-V'] ->
		    SCat = cat[v,aux]
		  ; RType = rel['MOD-N'] ->
		    SCat = nominal[]
		  ; RType = rel['MOD-A'] ->
		    SCat = cat[adj]
		  ; RType = rel['MOD-R'] ->
		    SCat = cat[adv,advneg]
		  ; RType = rel['MOD-P'] ->
		    SCat = cat[prep]
		  ; RType = rel['COMP'] ->
		      fail
		  ;
		    true
		  ),
		    ( RType = rel['AUX-V'] ->
			TCat = cat[aux],
			Type = adj,
			Label = 'Infl'
		    ;	RType = 'SUJ-V' ->
			Label = label[subject,impsubj],
			Type = edge_kind[subst,lexical],
			every((
			       edge{ source => node{ cat => v, cluster => cluster{ left => _Source } },
				     target => node{ cat => cat[v,aux], cluster => cluster{ left => Source } },
				     type => adj,
				     label => label['V','Infl']
				   },
			       record_without_doublon(
						      extra_elem_cost(
								      edge{ source => node{ cat => cat[v,aux],
											    cluster => cluster{ left => _Source }},
									    target => node{ cat => cat[v,aux],
											    cluster => cluster{ left => Target }},
									    type => Type,
									    label => Label
									  },
								      W
								     )
						     )
			      ))
		    ;	% RType = rel['CPL-V','MOD-N','MOD-A','MOD-R','MOD-P'] ->
		      RType = rel['CPL-V','MOD-N','MOD-A','MOD-R','MOD-P'] ->
			every((
%			       fail,
			       edge{ source => node{ cat => prep, cluster => cluster{ left => PrepLeft} },
				     target => node{ cluster => cluster{ left => Target, right => TargetRight } },
				     type => edge_kind[subst,lexical]
				   },
			       recorded( easy!group(SId,PrepLeft,_TargetRight,_Const::const['GP','PV'],_,_) ),
			       ( _TargetRight = TargetRight xor _Const = 'PV'),
			       record_without_doublon( extra_elem_cost(
								       edge{ source => node{ cat => SCat,
											     cluster => cluster{ left => Source }},
									     target => node{ %% cat => prep,
											     cluster => cluster{ left => PrepLeft }},
									     type => Type,
									     label => Label
									   },
								       W
								      )
						     )
			      ))
		    ;	RType = rel['COD-V'] ->
			Label = label[object,xcomp,ncpred],
			record( extra_elem_cost(
						edge{ target => node{ cat => cat[v,aux],
								      cluster => cluster{ left => Source }},
						      source => node{ cat => cat[v,aux],
								      cluster => cluster{ left => Target }},
						      type => adj,
						      label => 'V'
						    },
						W
					       )
			  ),
			every(( Label = xcomp,
				edge{ source => node{ cat => v, cluster => cluster{ left => _Target } },
				      target => node{ cat => cat[v,aux], cluster => cluster{ left => Target } },
				      type => adj,
				      label => label['V','Infl']
				   },
				record_without_doublon(
						       extra_elem_cost(
								       edge{ source => node{ cat => cat[v,aux],
											     cluster => cluster{ left => Source }},
									     target => node{ cat => cat[v,aux],
											     cluster => cluster{ left => _Target }},
									     label => Label
									   },
								       W
								      )
						      )
			      
			      ))
		    ;	true
		    ),
		    record( extra_elem_cost(
					    edge{ source => node{ cat => SCat,
								  cluster => cluster{ left => Source }},
						  target => node{ cat => TCat,
								  cluster => cluster{ left => Target }},
						  type => Type,
						  label => Label
						},
					    W
					   )
			  )
		)
	      )
	     )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Disambiguisation

:-light_tabular best_parse/5.
:-mode(best_parse/5,+(+,+,+,+,-)).

%% Pb with NId=root IncomingSpan=dummy

best_parse(NId,OId,Span,Constraints,DStruct) :-
	%% best parse for triple <NId,Spam,OId>
	%% actually, Span may be derived from OId (= Op Id)
	%% note that several NId may be head of a same OId
	%% actually, it should be enough to find best parses for OId
	N::node{ id => NId, lemma => Lemma },
	verbose('Try find best parse from ~w entering with span=~w and oid=~w and cst=~w\n',
		[NId,Span,OId,Constraints]),
	mutable(BPL,[],true),
	every((
	       
	       best_parse_in( N,
			      W_Below,
			      _DStruct::dstruct{ w => _W,
						 node => NId,
						 span => Span,
						 deriv => Deriv,
						 children => Children,
						 oid => OId,
						 constraint => Constraints
					       }
			    ),

	       ( ( potential_coord(NId), _Delta = 2000
		 xor Lemma == faire, _Delta = 1000
		 xor _Delta = 0
		 ),
		 \+ (
		     mutable_read(BPL,dstruct{ w => _W1}),
		     W_Below + _Delta < _W1 
		    )
	       ),
	       
%	       verbose('Found pre parse from ~E for deriv=~w: ~w\n',[N,Deriv,W]),
	       sum_node_cost_regional(NId,Children,Deriv,Span,W_Region),
	       _W is W_Below+W_Region,
	       verbose('Found parse from ~w: ~w\n',[NId,_DStruct]),

	       ( mutable_read(BPL,dstruct{ w => _W1}),
		 _W =< _W1 ->
		 true		% keep old best parse
	       ; % add or update with current parse
		 mutable(BPL,_DStruct)
	       )
	      )),
	( mutable_read(BPL,DStruct::dstruct{ w => W}),
	  verbose('Found best parse from ~E: weight=~w span=~w oid=~w dstruct=~w cst=~w\n',[N,W,Span,OId,DStruct,Constraints])
	xor 
	verbose('No best parse for ~w with span=~w and oid=~w\n',[NId,Span,OId]),
	  fail
	),
	true
	.

:-xcompiler
best_parse_in( N::node{ id => NId,
			deriv => Derivs
		      },
	       W,
	       DStruct::dstruct{ % w => W_All,
				 node => NId,
				 span => Span,
				 deriv => Deriv,
				 children => Children,
				 oid => OId,
				 constraint => Constraints
			       }
	     ) :-

	verbose('All edges from ~E inspan=~w oid=~w derivs=~w\n',[N,Span,OId,Derivs]),

%%	verbose('All edges: enter n=~w span=~w oid=~w\n',[NId,Span,OId]),
	verbose('All edges: here1 ~w\n',[Derivs]),
	( NId = root ->
	  edge{ id => RootEId::root(NId1),
		deriv => [Deriv],
		type => virtual
	      },
	  node2op(NId1,OId1),
	  op{ id => OId1, span => Span },
	  best_parse(NId1,OId1,Span,[],dstruct{ w => W1}),
	  edge_cost(RootEId,WE,_,Constraints),
	  W is WE+W1,
	  Children ::= [dinfo(OId1,RootEId,NId1,Constraints)]
	; Derivs = [] ->
	  Deriv = [],
	  Edges = [],
	  W = 0,
	  Children = []
	; %op{ id => OId, deriv => XDerivs }  ->
	  node_op2deriv(NId,OId,_) ->
	  verbose('All edges: here2 oid=~w\n',[OId]),
	  %%
	  %% domain(Deriv,XDerivs),
	  %% select a derivation, compatible with OId and NId
	  node_op2deriv(NId,OId,Deriv), 
	  %% deriv2node_and_edges(Deriv,NId,DerivEdges),
	  verbose('All edges: deriv n=~w span=~w deriv=~w\n',[NId,Span,Deriv]),
	  '$interface'('Easyforest_DList_Init'(),[return(M:ptr)]),
	  every((
		 %% select all edges for Deriv
		 %% A deriv is characterized by maximal span and children span (OId1)
		 %% several edges may be in competition for a given children span
		 %% => several (EId1,NId1) pairs are possible for a given (OId1,Span1) pair
		 %% domain(info(EId1,OId,OId1,NId1,Span1),DerivEdges),
		 deriv2edge(Deriv,info(EId1,OId,OId1,NId1,Span1)),
		 verbose('deriv nid=~w nid1=~w d=~w e=~w span=~w oid=~w oid1=~w\n',[NId,NId1,Deriv,EId1,Span1,OId,OId1]),
%		 verbose('Edge examine ~E deriv=~w oid=~w\n',[E1,Deriv,OId1]),
		 ( recorded(edge_best_parse(EId1,OId1,Constraints,XW1))
		 xor
		 edge_best_parse_constraint(EId1,Constraints1),
		 best_parse(NId1,OId1,Span1,Constraints1,dstruct{ w => W1, children => Children1 }),
		   verbose('fetch answer edge_cost ~w ~w\n',[EId1,Constraints]),
		   '$answers'(edge_cost(EId1,WE,_,Constraints)),
		   verbose('got answer edge_cost ~w ~w => w=~w\n',[EId1,Constraints,WE]),
		   %% (	 edge_weight_distrib(EId1,NId1,Children1,WE,WDistrib) xor WDistrib = 0),
		   %% XW1 is W1+WE+WDistrib,
		   XW1 is W1+WE,
		   record(edge_best_parse(EId1,OId1,Constraints,XW1))
		 ),
		 verbose('register for nid=~w cst=~w: xw1=~w dinfo=~w\n',[NId,Constraints,XW1,dinfo(OId1,EId1,NId1,Constraints)]),
		 '$interface'('Easyforest_DList_Add'(M :ptr,
						     XW1 :int,
						     OId1 :term,
						     dinfo(OId1,EId1,NId1,Constraints) :term),
			      [return(none)])
		)),
	  '$interface'('Easyforest_DList_Get'(M:ptr,W: -int, Children:term), [])
	;
	  verbose('Pb with ~E derivs=~w\n',[N,Derivs]),
	  fail
	),
	verbose('best_parse_in: deriv n=~w span=~w deriv=~w w=~w children=~w\n',[NId,Span,Deriv,W,Children]),
	true
	.

:-light_tabular edge_rank/3.
:-mode(edge_rank/3,+(+,-,-)).

edge_rank( EId,
	   Rank,
	   Dir
	 ) :-
	E::edge{ id => EId,
		 source => N::node{},
		 target => T::node{ cluster => cluster{ left => Left, right => Right } },
		 deriv => Derivs
	       },
	(   edge_to_right(EId) ->
	    Dir = right,
	    (	source2edge(
			    E1::edge{ id => EId1,
				      source => N,
				      target => T1::node{ cluster => cluster{ left => Left1,
									      right => Right1
									    }
							},
				      deriv => Derivs1
				    }
			   ),
		EId1 \== EId,
		edge_to_right(EId1),
		Right1 < Left,
		\+ ( source2edge(
				 E2::edge{ id => EId2,
					   source => N,
					   deriv => Derivs2,
					   target => T2::node{ cluster => cluster{ left => Left2,
										   right => Right2
										 }
							     }}
				),
		     EId1 \== EId2,
		       Right2 < Left,
		       Right1 =< Left2,
		     % ( domain(_D,Derivs),
		     %   domain(_D,Derivs1),
		     %   domain(_D,Derivs2)
		     % xor fail
		     % ),
%%		      format('test ~E left=~w left1=~w right1=~w left2=~w right2=~w E=~E E1=~E E2=~E\n',[N,Left,Left1,Right1,Left2,Right2,E,E1,E2]),
		     true
		   )
	    ->	
		edge_rank(EId1,Rank1,right),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	;   
	    Dir = left,
	    (	source2edge(
			    E1::edge{ id => EId1,
				      source => N,
				      target => T1::node{ cluster => cluster{ left => Left1,
									      right => Right1
									    }
							}}
			   ),
		EId1 \== EId,
%		domain(_D,Derivs),
%		domain(_D,Derivs1),
		\+ edge_to_right(EId1),
		Right < Left1,
		\+ ( source2edge(
				 E2::edge{ source => N,
					   target => T2::node{ cluster => cluster{ left => Left2,
										   right => Right2
										 }
							     }}
				),
		     Right < Left2,
		     Right2 =< Left1,
		     EId1 \== EId2,
		     % ( domain(_D,Derivs),
		     %   domain(_D,Derivs1),
		     %   domain(_D,Derivs2) xor fail
		     % ),
		     true
		   ) ->  
		edge_rank(EId1,Rank1,left),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	),
	verbose('Edge rank ~w ~w ~E\n',[Rank,Dir,E]),
	true
	.

:-light_tabular edge_to_right/1.

edge_to_right( EId ) :-
	edge{ id => EId,
	      source => node{ cluster => cluster{ right => Right } },
	      target => node{ cluster => cluster{ left => Left } }
	    },
	Right =< Left
	.

:-rec_prolog bound_max/3.

bound_max([],Boundary,Boundary).
bound_max([CId|CIds],Boundary,NewBoundary) :-
	cluster{ id => CId, left => Left, right => Right },
	( Right > Boundary ->
	  bound_max(CIds,Right,NewBoundary)
	;
	  bound_max(CIds,Boundary,NewBoundary)
	)
	.

:-light_tabular cluster_overlap/2.

cluster_overlap( CId1, CId2 ) :-
	%% overlap should now work with empty clusters
	%% verbose('Checking overlap ~E ~E\n',[C1,C2]),
	C1::cluster{ lex => Lex1, id => CId1, left => Left1, right => Right1 },
	Lex1 \== '',
	C2::cluster{ lex => Lex2, id => CId2, left => Left2, right => Right2 },
	Lex2 \== '',
	(   CId1 == CId2
	xor Left1 =< Left2, Left2 < Right1
	xor Left2 =< Left1, Left1 < Right2
	xor Left1 == Left2, Right1 == Right2
	),
	verbose('Overlap ~E ~E\n',[C1,C2]),
	true
	.

:-light_tabular edge_cost/4.
:-mode(edge_cost/4,+(+,-,-,+)).

edge_cost(EId,W,L,Constraints) :-
%%	Edge::edge{ id => EId, source => node{}, target => node{} },
	every(( t_edge_cost_elem(_,EId,_,Constraints) )),
	mutable(WM,0,true),
	mutable(LM,[],true),
%%	verbose('Computing edge cost ~E\n',[Edge]),
	every((
		( use_feature_cost ->
		  '$answers'(t_edge_cost_elem(Name,EId,_WName,Constraints)),
		  ( %% (\+ rule_no_cost(Name)),
		    edge_cost_modify(EId,Name,Constraints,_DW) ->
		    _W is _WName + _DW
		  ;
		    _W = _WName
		  )
		;
		  '$answers'(t_edge_cost_elem(Name,EId,_W,Constraints))
		),
%%		verbose('Edge cost ~w ~w ~E\n',[Name,_W,Edge]),
	       mutable_add(WM,_W),
	       _Next ::= Name:_W,
	       mutable_list_extend(LM,_Next)
	      )),
	mutable_read(WM,W),
	mutable_read(LM,L),
%%	verbose('TOTAL Edge cost ~w ~E\n',[W,Edge]),
	true
	.

:-extensional use_feature_cost/0.

:-extensional use_cluster_feature/0.

:-light_tabular t_edge_cost_elem/4.
:-mode(t_edge_cost_elem/4,+(-,+,-,+)).

t_edge_cost_elem(XName,EId,W,Constraints) :-
	E::edge{ id => EId,
		 type => Type,
		 label => Label,
		 source => node{ cat => SCat, lemma => SLemma },
		 target => node{ cat => TCat, lemma => TLemma }
	       },
	( edge_cost_elem(E,Name,_W)
	; edge_cost_elem_type(Type,E,Name,_W)
	; edge_cost_elem_label(Label,E,Name,_W)
	; edge_cost_elem_label_type(Label,Type,E,Name,_W)
	; edge_cost_elem_tcat(TCat,E,Name,_W)
	; edge_cost_elem_scat(SCat,E,Name,_W)
	; edge_cost_elem_lc(Label,SCat,TCat,E,Name,_W)
	; edge_cost_elem_tc(Type,SCat,TCat,E,Name,_W)
	; edge_cost_elem_label_tcat(Label,TCat,E,Name,_W)
	; edge_cost_elem_label_scat(Label,SCat,E,Name,_W)
	; edge_cost_elem_cats(SCat,TCat,E,Name,_W)
	; edge_cost_elem_tlemma(TLemma,E,Name,_W)
	; edge_cost_elem_slemma(SLemma,E,Name,_W)
	),
	(extra_indirect_cost_elem(EId,Name,Constraints,W1)->
%%	 format('adding extra cost eid=~w cst=~w name=~w ~w\n',[EId,Constraints,Name,W1]),
	 W is _W + W1
	;
	 W = _W
	),
	( rule_no_cost(Name) ->
	  W2 is W / 100,
	  rule_decorate(Name,W2,XName)
	;
	  XName = Name
	),
	true
	.

:-extensional extra_indirect_cost_elem/4.

:-light_tabular rule_decorate/3.
:-mode(rule_decorate/3,+(+,+,-)).

rule_decorate(Name,W,XName) :-
	( W > 0 ->
	  name_builder('~w_~w',[Name,W],XName)
	; W < 0 ->
	  W2 is - W,
	  name_builder('~w_m~w',[Name,W2],XName)
	;
	  XName = Name
	)
	.


:-light_tabular edge_cost_modify/4.
:-mode(edge_cost_modify/4,+(+,+,+,-)).

edge_cost_modify(EId,
		 Name,
		 Constraints,
		 W
		) :-
	E::edge{ id => EId, type => Type, label => Label,
		 target => node{ cat => TCat, cluster => cluster{ id => CId}},
		 source => node{ cat => SCat }
	       },
	mutable(MW,0,true),
	every((
	       ( edge_cost_extra_features(EId,Constraints,Key,Values)
	       ; fail,
		 F=f2,
		 '$answers'(t_edge_cost_elem(FV,EId,Constraints,_))
	       ; fail,
		 E2::edge{ id => EId2, type => Type2, label => Label2,
			   source => node{ cat => SCat2 },
			   target => node{ cat => TCat2, cluster => cluster{ id => CId } }
			 },
		 EId2 \== EId,
		 F='tcat+otcat',
		 FV=TCat+TCat2
	       ),
%	       recorded( FCost ),
%	       feature_cost(Name,Label,Type,F,FV,_W),
%	       format('feature_cost ~w ~w=~w => ~w\n',[EId,F,FV,_W]),
	       feature_cost_check(Key,[Name,Label,Type|Values],_W),
%	       format('extra add eid=~w key=~w name=~w label =~w type=~w values=~w => dw=~w\n',[EId,Key,Name,Label,Type,Values,_W]),
	       mutable_add(MW,_W),
	       true
	      )),
	mutable_read(MW,W),
	\+ W == 0,
%	format('extra cost eid=~w name=~w ~E extra=~w\n',[EId,Name,E,W]),
	true
	.

:-light_tabular sentence_length/2.
:-mode(sentence_length/2,+(-,-)).

sentence_length(Length,SLength) :-
	span_max([0,Max]),
	Length is Max+1,
	( Length > 20 -> SLength = 20
	; Length > 10 -> SLength = 10
	; SLength = Length
	).

:-light_tabular node_vmode/2.
:-mode(node_vmode/2,+(+,-)).

node_vmode( NId, VMode ) :-
	N::node{ id => NId, cat => cat[v,aux] },
	( node2op(NId,OId),
	  check_op_top_feature(OId,mode,Mode) ->
	  true
	;
	  fail
	),
	( chain(N::node{} >> (adj @ 'Infl') >> node{}) ->
	  VMode = 'VPP'
	;   chain(N::node{} >> (adj @ 'V') >> node{ cat => v}) ->
	  VMode = 'VINF'
	; Mode = infinitive ->
	  VMode = 'VINF'
	;   Mode = participle ->
	  VMode = 'VPP'
	;   Mode = gerundive ->
	  VMode = 'VPR'
	;   Mode = imperative ->
	  VMode = 'VIMP'
	;   Mode = indicative ->
	  VMode ='V'
	;   Mode = subjonctive ->
	  VMode = 'VS'
	;  Mode = conditional ->
	  VMode = 'VCOND'
	;   
	  VMode = 'V'
	).

node_vmode( NId, SemType ) :-
	N::node{ id => NId, cat => cat[nc] },
	node2op(NId,OId),
	check_op_top_feature(OId,semtype,SemType),
	( SemType = event
	xor SemType = bodypart
	)
	.

:-light_tabular node_subcat/2.
:-mode(node_subcat/2,+(+,-)).

node_subcat(NId,Subcat) :-
	N::node{ id => NId, cat => v, deriv => DIds },
	( domain(DId,DIds),
	  deriv2htid(DId,HTId),
	  hypertag{ id => HTId, ht => ht{ arg0 => arg{ function => Fun0},
					  arg1 => arg{ function => Fun1},
					  arg2 => arg{ function => Fun2}
					}} ->
%%	  format('nid=~w did=~w htid=~w => fun0=~w fun1=~w fun2=~w\n',[NId,DId,HTId,Fun0,Fun1,Fun2]),
	  Fun0 ?= '-',
	  Fun1 ?= '-',
	  Fun2 ?= '-',
	  name_builder('~w_~w_~w',[Fun0,Fun1,Fun2],Subcat)
	;
	  fail
	)
	.

:-light_tabular edge_cost_extra_features/4.
:-mode(edge_cost_extra_features/4,+(+,+,-,-)).

edge_cost_extra_features( EId, _Cst, F, V) :-
	edge{ id => EId,
	      source => node{ id => SNId,
			      cat => SCat, lemma => SLemma, form => SForm,
			      xcat => SXCat, tree => XSTree,
			      cluster => cluster{ left => SLeft,
						  right => SRight,
						  token => SForm2,
						  lex => SLex
						}},
	      target => node{ id => TNId,
			      cat => TCat, lemma => TLemma, form => TForm,
			      xcat => TXCat, tree => XTTree,
			      cluster => cluster{ left => TLeft,
						  right => TRight,
						  token => TForm2,
						  lex => TLex
						}},
	      label => Label,
	      type => Type
	    },
	(XSTree=[STree|_] xor XSTree = STree),
	(XTTree=[TTree|_] xor XTTree = TTree),
	next_and_prev_forms(SLeft,SRight,PSForm,NSForm),
	prev2_form(SLeft,PPSForm),
	next_and_prev_forms(TLeft,TRight,PTForm,NTForm),
	prev2_form(TLeft,PPTForm),
	%%	( SLeft < TLeft -> Dir = right ; Dir = left ),
	( TLeft == 0 -> Pos = start
	; span_max([0,TRight]) -> Pos = end
	; Pos = middle
	),
	edge_abstract_rank(EId,Rank,Dir),
	edge_delta(SLeft,TLeft,Dir,Delta),

	( node_vmode(SNId,SVMode) xor SVMode = none),
	( node_vmode(TNId,TVMode) xor TVMode = none),
	( capitalized_cluster(SLex) -> SCap = 1 ; SCap = 0 ),
	( capitalized_cluster(TLex) -> TCap = 1 ; TCap = 0 ),
%	( node_subcat(SNId,SSubcat) xor SSubcat = 'none'),
%	( node_subcat(TNId,TSubcat) xor TSubcat = 'none'),
	suffix3(TForm,TSuff),
	suffix3(SForm,SSuff),
	form2cluster_feature(TForm2,TForm,TCluster),
	form2cluster_feature(SForm2,SForm,SCluster),
	
	%% The feature sets
	( F=features_tcat, V=[TCat]
	; F=features_scat, V=[SCat]
	; F=features_tlemma_tcat, V=[TLemma,TCat]
	; F=features_slemma_scat, V=[SLemma,SCat]
	; F=features_tform_tlemma_tcat, V=[TForm,TLemma,TCat]
	; F=features_sform_slemma_scat, V=[SForm,SLemma,SCat]
	; F=features_tform2_tlemma_tcat, V=[TForm2,TLemma,TCat]
	; F=features_delta, V=[Delta]
	; F=features_pos, V=[Pos]
	; F=features_pos_tcat, V=[Pos,TCat]
	; F=features_pos_tcat_tlemma,V=[Pos,TCat,TLemma]
	; F=features_tlemma_tcat_scat, V=[TLemma,TCat,SCat]
	; F=features_slemma_scat_tcat, V=[SLemma,SCat,TCat]
	; F=features_tcat_scat, V=[TCat,SCat]
	; F=features_tcat_rank_dir_scat, V=[TCat,Rank,Dir,SCat]
	; F=features_tcat_scat_tvmode_svmode, V=[TCat,SCat,TVMode,SVMode]
	; F=features_tcap_tcat, V=[TCap,TCat]
	; F=features_scap_scat, V=[SCap,SCat]
%	; F=features_psform_nsform, V=[PSForm,NSForm]
%	; F=features_ptform_ntform, V=[PTForm,NTForm]
	; F=features_psform, V=[PSForm]
	; F=features_nsform, V=[NSForm]
	; F=features_ptform, V=[PTForm]
	; F=features_ntform, V=[NTForm]
	; F=features_psform_scat, V=[PSForm,SCat]
	; F=features_nsform_scat, V=[NSForm,SCat]
	; F=features_ptform_tcat, V=[PTForm,TCat]
	; F=features_ntform_tcat, V=[NTForm,TCat]
	; F=features_pptform, V=[PPTForm]
	; F=features_ppsform, V=[PPSForm]
	; F=features_tsuff, V=[TSuff]
	; F=features_ssuff, V=[SSuff]
	; F=features_tsuff_tcat, V=[TSuff,TCat]
	; F=features_ssuff_scat, V=[SSuff,SCat]
	; F=features_tcluster, V=[TCluster]
	; F=features_scluster, V=[SCluster]
	; F=features_tcluster_scluster, V=[TCluster,SCluster]

	
				%	; F=features_tform_scluster, V=[TForm,SCluster]
				%	; F=features_sform_tcluster, V=[SForm,TCluster]
				%	; F=features_tsubcat, V=[TSubcat]
				%	; F=features_ssubcat, V=[SSubcat]
				% ; F=features_empty, V=[]
				% ; F='features_sform2_slemma_scat', V=[SForm2,SLemma,SCat]
				% ; F='features_dir', V=[Dir]
				% ; F='features_followponct', V = [FollowPonct]
				% ; F='features_type', V = [Type]
				% ; F='features_slength', V= [SLength]
				% ; F='features_tcat_slength', V = [TCat,SLength]
				% ; F='features_delta_tcat_scat', V = [Delta,TCat,SCat]
				% ; F= 'features_delta_pos_tcat', V = [Delta,Pos,TCat]
				% ; F = 'features_txcat', V = [TXCat]
				% ; F = 'features_sxcat', V = [SXCat]
				% ; F = 'features_tcat_scat_txcat_sxcat', V = [TCat,SCat,TXCat,SXCat]
	; F = 'features_ttree', V = [TTree]
	; F = 'features_stree', V = [STree]
				% ; F = 'features_ttree_delta_stree', V = [TTree,Delta,STree]
				% ; F = 'features_rank_delta', V = [Rank,Delta]

	%% feature related to parent edge, when present (ss* features)
	; \+ _Cst = [],
	  edge{ id => _Cst, source => node{ id => SSNId,
					    lemma => SSLemma,
					    form => SSForm,
					    cat => SSCat,
					    cluster => cluster{ id => SSCId,
								left => SSLeft,
								right => SSRight,
								token => SSForm2,
								lex => SSLex
							      }
					  }},
	  ( node2conll(SSNId,SSPos) -> true
	  ; node{ id => _SSNId, cluster => cluster{ id => SSCId } },
	    node2conll(_SSNId,SSPos) -> true
	  ; node{ id => _SSNId, cluster => cluster{ left => SSLeft } },
	    node2conll(_SSNId,SSPos) -> true
	  ; node{ id => _SSNId, cluster => cluster{ right => SSRight } },
	    node2conll(_SSNId,SSPos) -> true
	  ;
	    SSPos = 0
	  ),
	  form2cluster_feature(SSForm2,SSForm,SSCluster),
	  (node_vmode(SSNId,SSVMode) xor SSVMode = none),
	  suffix3(SSForm,SSSuff),
	  edge_abstract_rank(_Cst,SRank,SDir),
	  edge_delta(SSLeft,TLeft,SDir,SDelta),
	  %%	 format('here cst=~w sslemma=~w ssform=~w\n',[_Cst,SSLemma,SSForm]),
	  (
	   F=features_sscat, V  = [SSCat]
	  ; F=features_sslemma, V  = [SSLemma]
	  ; F=features_sslemma_tlemma, V  = [SSLemma,TLemma]
	  ; F=features_ssform, V  = [SSForm]
				%	; F=features_sssuff, V  = [SSSuff]
	  ; F=features_ssvmode, V  = [SSVMode]
	  ; F=features_sscat_tcat, V  = [SSCat,TCat]
	  ; F=features_sscluster, V  = [SSCluster]
	  ; F=features_tcluster_sscluster, V  = [TCluster,SSCluster]
	  
	  ; F=features_sdelta, V=[SDelta]
	  ; F=features_sdelta_delta_sscat_tcat, V=[SDelta,Delta,SSCat,TCat]
	  ; F=features_tcat_sscat_tvmode_ssvmode, V=[TCat,SSCat,TVMode,SSVMode]
	  ; F=features_tcat_srank_sdir_sscat, V=[TCat,SRank,SDir,SSCat]
	  ),
%	  format('features eid=~w cst=~w f=~w v=~w\n',[EId,_Cst,F,V]),
	  true

	)
	.

:-xcompiler
edge_delta(SLeft,TLeft,Dir,Delta) :-
	_Delta is abs(SLeft-TLeft),
	( _Delta > 15 -> Delta1 = 15
	;   _Delta > 6 -> Delta1 = 6
	 %%	;   _Delta > 3 -> Delta = 3
	;   Delta1 = _Delta
	),
	( Dir == left -> Delta is - Delta1 ; Delta = Delta1 ),
	true
	.

:-xcompiler
edge_abstract_rank(EId,Rank,Dir) :-
	edge_rank(EId,_Rank,Dir),
	( _Rank > 10 -> Rank = 10
	;   _Rank > 5 -> Rank = 5
	;   Rank = _Rank
	)
	.

:-light_tabular feature_cost_db/1.
:-mode(feature_cost_db/1,+(-)).

feature_cost_db(DB) :-
	recorded(feature_cost_table(File)),
	sqlite!open(File,DB)
	.

:-light_tabular feature_cost_stmt/2.
:-mode(feature_cost_stmt/2,+(+,-)).

feature_cost_stmt(Key,PStmt) :-
	feature_cost_db(DB),
%	format('cost db ~w\n',[DB]),
	recorded(feature_cost_prepare(Key,Stmt)),
%	format('cost prepare ~w => ~w\n',[Key,Stmt]),
	sqlite!prepare(DB,Stmt,PStmt)
	.

:-light_tabular feature_cost_check/3.
:-mode(feature_cost_check/3,+(+,+,-)).

feature_cost_check(Key,Values,W) :-
%	format('cost check ~w ~w\n',[Key,Values]),
	feature_cost_stmt(Key,Stmt),
%	format('cost stmt ~w => ~w\n',[Key,Stmt]),
	sqlite!reset_and_bind(Stmt,Values),
	(sqlite!tuple(Stmt,[W]) xor fail),
%	format('cost weight ~w ~w => ~w\n',[Key,Values,W]),
	true
	.

:-light_tabular cluster_feature_db/1.
:-mode(cluster_feature_db/1,+(-)).

cluster_feature_db(DB) :-
	recorded(cluster_feature_table(File)),
	sqlite!open(File,DB)
	.

:-light_tabular cluster_feature_stmt/1.
:-mode(cluster_feature_stmt/1,+(-)).

cluster_feature_stmt(PStmt) :-
	cluster_feature_db(DB),
	recorded(cluster_feature_prepare(Stmt)),
	sqlite!prepare(DB,Stmt,PStmt)
	.

:-light_tabular form2cluster_feature/3.
:-mode(form2cluster_feature/3,+(+,+,-)).

form2cluster_feature(Form,NodeForm,Cluster) :-
%	format('form2cluster <~w> <~w>\n',[Form,NodeForm]),
	( ( Form \== '',
	    cluster_feature_stmt(Stmt),
	    sqlite!reset_and_bind(Stmt,[Form]),
	    sqlite!tuple(Stmt,[Cluster])
	  ) ->
	  true
	;
	  ( NodeForm \== '',
	    cluster_feature_stmt(Stmt),
	    sqlite!reset_and_bind(Stmt,[NodeForm]),
	    sqlite!tuple(Stmt,[Cluster])
	  ) ->
	  true
	; simple_form2cluster_feature(NodeForm,Cluster) ->
	  true
	;
	  Cluster = -1
	),
%	format('=>~w\n',[Cluster]),
	true
	.

:-extensional simple_form2cluster_feature/2.

simple_form2cluster_feature(date[],-2).
simple_form2cluster_feature(entities['_NUMBER'],-3).
simple_form2cluster_feature(entities['_NUM','_ROMNUM'],-4).
simple_form2cluster_feature(entities['_PERSON','_PERSON_m','_PERSON_f'],-5).
simple_form2cluster_feature(entities['_LOCATION'],-6).
simple_form2cluster_feature(entities['_COMPANY','_ORGANIZATION'],-7).
simple_form2cluster_feature(entities['_PRODUCT'],-8).

:-rec_prolog
	edge_cost_elem/3,
	edge_cost_elem_type/4,
	edge_cost_elem_label/4,
	edge_cost_elem_label_type/5,
	edge_cost_elem_tcat/4,
	edge_cost_elem_scat/4,
	edge_cost_elem_lc/6,
	edge_cost_elem_tc/6,
	edge_cost_elem_label_tcat/5,
	edge_cost_elem_label_scat/5,
	edge_cost_elem_cats/5,
	edge_cost_elem_tlemma/4,
	edge_cost_elem_slemma/4
	.

%% Penalty for adj dependencies
%% edge_cost_elem( edge{ type => adj }, -1 ).

:-light_tabular rule_weight/3.

:-extensional rule_weight/2.

%:-xcompiler
rule_weight(Name,Weight,Default) :-
%	format('rule weight ~w\n',[Name]),
	( rule_weight(Name,Weight) xor Weight = Default ).

%% Get user-provided extra cost
edge_cost_elem(%none,
	       E::edge{},
	       Name::'UserExtraCost',
	       W
	      ) :-
	extra_elem_cost(E,W)
	.

%% Get user-provide extra cost for a node
edge_cost_elem(%none,
	       E::edge{ target => N::node{} },
	       Name::'UserNodeCost',
	       W
	      ) :-
	extra_node_cost(N,W)
	.

%% Favor subst and lexical dependencies
edge_cost_elem_type(subst,
	       edge{ type => subst ,
		     source => node{ cluster => cluster{ lex => Lex }}},
	       Name::'+SUBST',
	       W ) :-
	rule_weight(Name,W,10),
	Lex \== ''.

edge_cost_elem_type(lexical,
	       E::edge{ type => lexical ,
			source => node{ cluster => cluster{ lex => Lex }},
			target => node{ cluster => cluster{ token => Token }}
		      },
	       Name::'+LEXICAL',
	       W ) :-
	%% should avoid favoring over skippable punctuations
	Token \== '_EPSILON',
	%%	format('+LEXICAL ~E\n',[E]);
	%% format('Try lexical ~E lex=~w tok=~w\n',[E,Lex,Token]),
	rule_weight(Name,W,20),
	Lex \== ''.


edge_cost_elem_type(epsilon,
	       edge{ type => epsilon, target => node{ cat => cat[~ [epsilon,sbound,meta]] } },
	       Name::'-skip_in_robust',
	       W
	      ) :-
	recorded( mode(robust) ),
	rule_weight(Name,W,-2000)
	.

/*
edge_cost_elem(
	       label,
	       skip
	      edge{ type => epsilon, label => skip },
	       Name::'+skip',
	       W
	      ) :- rule_weight(Name,W,20)
	.
*/

%% Favor ncpred
edge_cost_elem_tc(lexical,v,ncpred,
		edge{ type => lexical, source => node{ cat => v, lemma => VLemma }, target => node{ cat => ncpred, lemma => NcLemma } },
		Name,
		W) :-	
	rule_weight(_Name::'+NCPRED',W,500),
	( Name = _Name
	;
	  name_builder('+NCPRED_~w_~w',[VLemma,NcLemma],Name)
	)
	.

%% Favor ncpred mod
edge_cost_elem_lc(ncpred,ncpred,adj,
		edge{ type => adj,
		      label => ncpred,
		      source => node{ cat => ncpred },
		      target => node{ cat => adj}
		    },
		Name::'+NCPREDMOD',
		W) :-
	rule_weight(Name,W,30).


%% but penalize det on ncpred
edge_cost_elem_lc(det,v,det,
		edge{ source => V::node{ cat => v },
		      target => node{ cat => det },
		      label => det,
		      type => subst
		    },
		Name::'-DET_ON_NCPRED',
		W
	      ) :-
	fail,
	rule_weight(Name,W,-200),
	chain( V >> (lexical @ ncpred) >> node{ cat => ncpred })
	.

%% Favor verbal argument, except if date
edge_cost_elem_label(Label,
		E::edge{ id => EId,
			 label => Label::label[object,preparg,comp,xcomp],
			 source => N1::node{ cat => Cat1 },
			 target => node{ id => NId, lemma => Lemma }},
		     Name::'+ARG',
		     W
	      ) :-
	%% CALL EDGE
%	format('+ARG test1 ~w ~w\n',[EId,E]),
	\+ domain(Lemma,date[]),
%	format('+ARG test2 ~w\n',[EId]),
	\+ source2edge(
		       edge{ source => N1,
			     target => node{ cat => start },
			     label => start,
			     type => subst
			   }
		      ),
%	format('+ARG test3 ~w\n',[EId]),
	\+ check_node_top_feature(NId,time,true_time[]),
%	format('+ARG test4 ~w\n',[EId]),
	\+ ( Cat1=adj,
	     Label=xcomp,
	     chain( N1 >> (lexical @ prep) >> node{ cat => prep } )
	   ),
%	format('+ARG test5 ~w\n',[EId]),
	rule_weight(Name,W,1000)
	.

%% favor modal verb, almost as arg
edge_cost_elem_lc('V',v,v,
	       edge{ label => 'V',
		     type => adj,
		     source => node{ cat => v },
		     target => node{ cat => v }
		   },
	       Name::'+Modal',
	       W
	      ) :-
	rule_weight(Name,W,1000)
	.

%% Favor preparg->de over object
edge_cost_elem_label(preparg,
		edge{ label => label[preparg],
		      target => node{ form => de }},
		Name::'+ARG_prepobj_de',
		W
	      ) :-
	rule_weight(Name,W,1000).

%% FAVOR object->de over PP-attach on nouns
edge_cost_elem_label(object,
		edge{ label => label[object],
		      target => N::node{ cat => nc } },
		Name::'+ARG_object_de',
		W
	      ) :-
	rule_weight(Name,W,W3),
	source2edge( edge{ source => N,
			   target => node{ cat => det,
					   lemma => L,
					   form => F }
			 }
		   ),
	( F == du ->
	  W1=300
	;
	  domain(L,[un,du]),
	  F=de_form[des,'de la','de l'''],
	  W1=400
	),
	%% re-inforce if coord
	( chain( N
	       >> adj @ 'N2'
	       >> node{ cat => coo }
	       >> subst @ coord3
	       >> node{}
	       >> subst @ det
	       >> node{ cat => det,
			form => F2
		      }
	       ),
	  domain(F2,[des,'de la','de l''']) ->
	W2=300
	;
	  W2  = 0
	),
	W3 is W1 + W2
	.


%% FAVOR acomp->de over PP-attach on nouns
edge_cost_elem_label(comp,
		edge{ label => comp,
		      target => Comp::node{ cat => comp } },
		Name::'+ARG_acomp_de',
		W
	      ) :-
	rule_weight(Name,W,W3),
	chain( Comp
	     >> subst @ 'N2'
	     >> N::node{ cat => nc }
	     >> subst @ det
	     >> node{ cat => det,
		      lemma => L,
		      form => F }
	     ),
	( F == du ->
	  W1=300
	;
	  domain(L,[un,du]),
	  F=de_form[des,'de la','de l'''],
	  W1=400
	),
	%% re-inforce if coord
	( chain( N
	       >> adj @ 'N2'
	       >> node{ cat => coo }
	       >> subst @ coord3
	       >> node{}
	       >> subst @ det
	       >> node{ cat => det,
			form => F2
		      }
	       ),
	  F2=de_form[des,'de la','de l'''] ->
	W2=300
	;
	    W2  = 0
	),
	W3 is W1 + W2
	.


%% Penalize object to prep � in infinitive relative (une pomme � croquer)
edge_cost_elem_lc(object,v,prep,
	       edge{ label => object,
		     source => node{ cat => v},
		     target => node{ cat => prep },
		     type => lexical
		   },
	       '-object_to_prep',
	       W
	      ) :-
	rule_weight(Name,W,-1000)
	.

%% Penalise long PP-args over a completive or an infinitive
%% to avoid bad attachement in sentence such as
%% ex: il dit (que Paul mange une tarte) (aux fruits).
edge_cost_elem_label(preparg,
		edge{ label => preparg,
		      source => V::node{ cat => v,
					 cluster => cluster{ right => V_Right}
				       },
		      target => node{ cluster => cluster{ left => PP_Left }}
		    },
		Name::'-preparg_over_comp',
		W
	      ) :-
	rule_weight(Name,W,-900),
	chain( V
	     >> ( subst @ xcomp )
	     >> node{ cluster => cluster{ left => S_Left,
					  right => S_Right } }
	     ),
	V_Right < S_Left,
	S_Right < PP_Left
	.

%% Penalize obj or acomp starting with de, des, ... after noun or adj
edge_cost_elem_label_type(det,subst,
		edge{ label => label[det],
		      type => subst,
		      target => node{ form => Form::de_form[],
				      cluster => cluster{ left => Left }
				    }
		    },
		Name::'-de_after_noun',
		W
	      ) :-
%	rule_weight(Name,W,-700),
	rule_weight(Name,W,-500),
%%	domain(Form,[de,des,du,'de la','de l''']),
	node{ cat => cat[nc,adj,np],
	      cluster => cluster{ right => Left }
	    }
	.


%% Penalty on comp when N2, (confusion with inverted subject or object or even past participle)
edge_cost_elem_label_tcat(Label,TCat,
		edge{ label => Label::label[comp],
		      source => node{ lemma => L,
				      cluster => CV::cluster{ right => RV } },
		      target => N::node{ cat => TCat::cat[comp] }
		    },
		Name::'-N2asComp',
		W
	      ) :-
	rule_weight(Name,W,-600),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => cat[nc,np],
				    cluster => C::cluster{ left => LN }},
		    label => 'N2',
		    type => subst
		  }
	     ),
	( targetcluster2edge(
		edge{ label => label[subject,object],
		      target => node{ cluster => C }
		    }
	       ),
	  \+ L=�tre
	;
%	  sourcecluster2edge(
		edge{ source => node{ cluster => C },
		      label => 'Infl',
		      target => node{ cat => aux }
		    }
%	       )
	)
	.

%% Favor comp to adj (rather than N2)
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[comp],
		      source => V::node{ cat => SCat::v },
		      target => node{ cat => TCat::comp },
		      type => subst
		    },
		Name::'-comp_as_N2',
		W
	      ) :- rule_weight(Name,W,-1000),
	%% CALL EDGE
	source2edge(
	      edge{ label => label[comp],
		    source => V,
		    target => node{ cat => adj },
		    type => subst
		  }
	     )
	.

edge_cost_elem_lc(Label,SCat,TCat,
	       edge{ label => Label::label[comp],
		     source => node{ cat => SCat::v, lemma => V_Lemma },
		     target => Prep::node{ cat => TCat::prep,
					   lemma => Prep_Lemma,
					   cluster => cluster{ left => L,
							       right => R
							     }
					 },
		     type => subst
		   },
%	       Name::'-comp_as_PP',
		  Name,
	       XW
	      ) :-
	rule_weight(_Name:: '-comp_as_PP',W1,-600),
	( domain(V_Lemma,[�tre,sembler]) -> W2 = 0 ; W2 = -600),
	( domain(Prep_Lemma,[en,sans,pour,contre,comme]) -> W3 = 0 ;
	  domain(Prep_Lemma,[de,�]) -> W3 = -300 ;
	  W3 = -400
	),
	( node{ cat => cat[adv,prep],
		cluster => cluster{ left => L2, right => R2 }
	      },
	  R2-L2 > 1,
	  L2 =< L,
	  R2 >= R,
	  (L2 < L xor R2 > R) ->
	  W4 = -800
	;
	  W4 = 0
	),
	( chain(Prep >> subst >> node{ cat => XCat, lemma => XLemma }),
	  (XCat = cat[np,v,pro]
	   xor XLemma = entities[]
	   xor XLemma = date[]
	  ) ->
	  W5 = -500
	;
	  W5 = 0,
	  XLemma = 'void',
	  XCat = 'voidcat'
	),
	W is W1 + W2 + W3 + W4 + W5,
	XW = W,
	Name = _Name,
	true
	.

/*
%% penalize date as args
%% 25/05/08: seems redundant with +ARG
edge_cost_elem( 
		edge{ label => label[object,preparg,comp,xcomp],
		      target => node{ lemma => Lemma, deriv => Derivs }},
		'-date_as_ARG',
		-1000 ) :-
	( domain(Lemma,date[])
	xor 
	domain(D,Derivs),
	  deriv(D,EId,_,OId,_),
	  check_op_top_feature(OId,time,true_time[])
	)
	.
*/

%% Favor subject, except for gerundive and particiales
edge_cost_elem_label(Label,
		edge{ id => EId, label => Label::label[subject] },
		Name::'+SUBJ',
		W
	      ) :-
	rule_weight(Name,W,1100),
	\+ ( edge2sop(EId,OId),
	     check_op_top_feature(OId,mode,gerundive),
	     %%	     format('found gerundive ~w\n',[Op]),
	     true
	   )
	.

%% Favor impersonal subject over normal subjects
edge_cost_elem_label(Label,
		edge{ id => EId,
		      label => Label::label[impsubj],
		      source => N,
		      target => node{ cluster => cluster{ left => ImpLeft }}
		    },
		Name::'+IMPSUBJ',
		W
	      ) :-
	rule_weight(Name,W,1400),
	\+ ( node!older_ancestor(N,cat[v,aux],P::node{},['Infl','V']),
	     %% CALL EDGE
	     source2edge(
		   edge{ source => P,
			 label => 'subject',
			 target => node{ cluster => cluster{ right => SubjRight }}
		       }
		  ),
	     SubjRight < ImpLeft
	   )
	.

%% Penalize empty subject as strace
edge_cost_elem_label(strace,
		edge{ label => strace, source => S::node{} },
		Name::'-strace',
		W
	      ) :-
	rule_weight(Name,[W1,W2],[-100,-5000]),
	( chain( S << subst @ label[coord2,coord3] << node{ cat => coo } ) ->
	  W = W1
	;
	  W = W2
	)
	.

%% Favor verb at infinitive, against unsaturaed noun
edge_cost_elem_tcat(v,
		edge{ id => EId, target=> node{ cat => v, cluster => C } },
		Name::'+inf_verb_vs_noun',
		W ) :-
	rule_weight(Name,W,1000),
	N::node{ cluster => C, cat => nc },
	edge2top(EId,TId),
	check_op_top_feature(TId,mode,infinitive)
	.

/*
%% Sligtly Favor post-verbal clitics
edge_cost_elem( 
		edge{ label => subject,
		      source => node{ cluster => cluster{ right => R } },
		      target => node{ cat => cln, cluster => cluster{ left => L } }
		    },
		'+post_verb_clitic',
		1000
	      ) :-
	R =< L
	.
	*/

%% Favor qui as subject when possible
edge_cost_elem_label(subject,
		edge{ label => subject,
		      target => node{ form => qui,
				      cat => cat[pri,prel] }},
		Name::'+qui_as_subj',
		W
	      ) :- rule_weight(Name,W,200).

/*
%% Penalize subject for gerundive and particiales
%% 25/05/08: seems redundant with +SUBJ
edge_cost_elem( edge{ id => EId, label => label[subject], deriv => Derivs },
		'-SUBJ',
		-300 ) :-
	domain(D,Derivs),
	deriv(D,EId,_,OId,_),
	check_op_top_feature(OId,mode,gerundive)
	.
*/

%% Penalize sentential subject
edge_cost_elem_label_tcat(subject,v,
		edge{ id => EId,
		      label => label[subject],
		      target => node{ cat => Cat::cat[v] },
		      source => node{ cat => v }
		    },
		Name::'-S_as_Subj',
		W ) :- rule_weight(Name,W,-800).

edge_cost_elem_label_tcat(subject,TCat,
		edge{ id => EId,
		      label => label[subject],
		      target => node{ cat => TCat::cat[prep] },
		      source => N::node{ cat => v }
		    },
		Name::'-PrepS_as_Subj',
		W ) :-
	rule_weight(Name,W,-1200),
	%% inverted subject because of impersonal subjects
	%% are more easily introduced by 'de'
	\+ chain( N >> (lexical @ impsubj) >> node{} )
	.


%% Favor xcomp over alternate construction with adjoining
%% but penalize (for citations) "participiale" xcomp
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      id => EId,
		      source => V1::node{ cat => SCat::cat[v,adj] },
		      target => V2::node{ cat => TCat::cat[v,'S'] }
		    },
		Name::'+ARG_XCOMP',
		W )
	:- rule_weight(Name,W1,800),
		(   edge2top(EId,TId),
		    check_op_top_feature(TId,mode,Mode),
		    domain(Mode,[gerundive,participle]) -> W is W1 - 300
		;
		    W = W1
		)
		.

edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      source => V1::node{ cat => SCat::cat[nc] },
		      target => V2::node{ cat => TCat::cat[v,'S']}
		    },
		Name::'+NARG_XCOMP',
		W )
	:- rule_weight(Name,W,2000).

edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      source => V1::node{ cat => SCat::cat[adv,advneg] },
		      target => V2::node{ cat => TCat::cat[v,'S']}
		    },
		Name::'+ADVARG_XCOMP',
		W )
	:- rule_weight(Name,W,2000).

edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label[xcomp],
		      source => V1::node{ cat => SCat::cat[adj] },
		      target => V2::node{ cat => TCat::cat[v,'S']}
		    },
		Name::'+ADJARG_XCOMP',
		W )
	:- rule_weight(Name,W,1500),
		\+ chain( V1 >> (lexical @ 'prep') >> node{ cat => prep } ),
		\+ ( chain( V1
			  << (subst @ comp) << node{ cat => v}
			  >> (subst @ subject) >> Subj::node{}
			  ),
		       precedes(V1,Subj)
		   )
		.

edge_cost_elem_label_scat(Label,SCat,
		edge{ label => Label::label[subject],
		      source => V1::node{ cat => SCat::cat[adj] },
		      target => node{}
		    },
		Name::'+ADJARG_SUBJ',
		W ) :- rule_weight(Name,W,1300),
	chain( V1 >> (lexical @ impsubj) >> node{} )
	.

%% Favor xcomp built by adjoining
edge_cost_elem_lc('S',v,v,
		edge{ label => 'S',
		      source => node{ cat => v },
		      target => node{ cat => v, tree => Tree },
		      type => adj
		    },
		Name::'+ARG_XCOMP_ADJ',
		W
	      ) :- rule_weight(Name,W,1000),
	domain('arg1:real_arg_xcomp_by_adj',Tree)
	.

edge_cost_elem_label_type(csu,lexical,
		edge{ type => lexical,
		      source => V1::node{ cat => cat[v,adj] },
		      target => node{ lemma => que, cluster => C },
		      label => csu
		    },
		Name::'+que_lexical',
		W
	      ) :-
	rule_weight(Name,W,700),
	edge{ type => subst,
	      source => V2::node{ cat => cat[v,adj] },
	      target => node{ cat => csu, cluster => C }
	    },
	precedes(V2,V1)
	.

%% Favor cleft constructions
edge_cost_elem_label(Label,
	       edge{ label => Label::'CleftQue' },
	       Name::'+CLEFT',
	       W ) :-
	rule_weight(Name,W,1000).

%% and peuso-cleft
edge_cost_elem_lc(csu,aux,que,
		edge{ label => 'csu',
		      target => node{ cat => que},
		      source => node{ cat => aux, lemma => �tre }
		    },
		Name::'+PSEUDOCLEFT',
		W ) :-
	rule_weight(Name,W,800).


%% Penalty for long distance dependencies, except for root node
edge_cost_elem( %none,
		Edge::edge{ source => node{ id => IdA, cat => SCat, cluster => cluster{ left => LA, right => RA }},
			    target => node{ cat => TCat, cluster => cluster{ left => LB, right => RB }},
			    type => Type::edge_kind[~virtual],
			    label => Label
			  },
		Name,		% -LONG%n
		W
	      ) :-
	SCat \== incise,
	verbose('Try length penalty on ~E\n',[Edge]),
	( RA =< LB -> 
	    D1 is  (LB - RA)
	;   
	    D1 is (LA - RB)
	),
%%	D1 > 3,
	( D1 > 9 ->
	  Name = '-LONG9',
	  Default is 1400-200*min(15,D1)
	; D1 > 6 ->
	  Name = '-LONG6',
	  Default is -100*(D1-6)
	; D1 > 3 ->
	  Name = '-LONG3',
	  Default is 15-10*D1
	;
	  Name = '-LONG0',	%'0
	  Default is -5*D1
	),
	( Label == subject ->
	  ( RA =< LB ->
	    %% inverted subject are closer => increase penalties
	    Default2 is Default * 4
	  ;
	    Default2 is Default * 3 / 2
	  )
	; Label == 'Infl' ->
	  Default2 is Default * 30
	; SCat=v, TCat = adv, LB = 0 ->
	  %% almost no penalties for adv at the beginning of a sentence
	  Default2 is -5 * D1
	; Label = coord3, TCat = nominal[] ->
	  Default2 is Default * 2
	; Type = adj, Label = label[prep,csu] -> % quantity mod
	  Default2 is Default * 10
	;
	  Default2 = Default
	),
	rule_weight(Name,W,Default2),
	W < 0
	.

%% Favour adj attribute rather than noun
edge_cost_elem_label_tcat(comp,adj,
		edge{ label => comp, target => node{ cat => adj } },
		Name::'+ATTR',
		W ) :-
	rule_weight(Name,W,100).

%% Favour det rather than adj or noun or verb
%% edge_cost_elem( edge{ target => node{ cat => det } }, 40 ).


%% Favour noun over verb in first position, overcoming penalty for virtual edges
%% SHOULD ADD SOMETHING SIMILAR FOR 'start'
edge_cost_elem_tcat(TCat,
		E::edge{ target => node{ cat => TCat::cat[nc,np],
					 cluster => C::cluster{ left => 0 } } },
		Name::'+NOUN/VERB',
		W ) :-
	rule_weight(Name,W,1700),
%%	format('Try Activation ~w\n',[E]),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ target => V::node{ cat => v, cluster => C } }
			  ),
	\+ chain( V << subst << node{} << adj << node{ cat => cat[v,adj] } ),
%%	format('Activation\n',[]),
	true
	.

%% Penalty for raw pronon 'ce'
edge_cost_elem_tcat(TCat,
		edge{ target => N::node{ cluster => cluster{ lex => Lex }, cat => TCat::pro } },
		Name::'-CE',
		W
	      ) :-
	rule_weight(Name,W,-50),
	%% CALL EDGE
	\+ source2edge( edge{ source => N } ),
	label2lex(Lex,[Ce],_),
	domain(Ce,[ce,'Ce'])
	.

%% Penalty for raw 'ce' as subject
edge_cost_elem_label(subject,
		edge{ target => N::node{ cluster => cluster{ lex => ce }},
		      source => V::node{ cat => v, lemma => L },
		      label => 'subject'		      
		    },
		Name::'-CE_as_subject',
		W
	      ) :-
	rule_weight(Name,W,-2000),
	%% CALL EDGE
	\+ source2edge( edge{ source => N } ),
	\+ domain(L,['�tre'])
	.



%% Favour coordinations
edge_cost_elem_label(Label,
		edge{ label => Label::label[coo,coo2,coord2,coord3],
		      type => Type::edge_kind[subst,lexical]
		    },
		Name::'+COORD',
		W) :-
	rule_weight(Name,W,20).

%% Favor coord built on same prep
%% specially when potential confusion with an article
edge_cost_elem_cats(prep,coo,
		edge{ source => node{ cat => prep,
				      form => Form1,
				      lemma => Lemma },
		      target => COO::node{ cat => coo }
		    },
		Name::'+coord_same_prep',
		W
	      ) :-
	rule_weight(Name,W,500),
	%% CALL EDGE
	source2edge( edge{ source => COO,
		     target => node{ cat => prep, lemma => Lemma },
		     label => coord3
		   }
	     )
	.

%% favour coords on 'de' as det
edge_cost_elem_tcat(coo,
		edge{ source => N1::node{ cat => xnominal[] },
		      target => COO::node{ cat => coo },
		      type => adj,
		      id => EId1
		    },
		Name::'+coord_det_de',
		W
	      ) :-
	chain( COO
	     >> (subst @ coord3) >> node{ cat => xnominal[] }
	     >> (subst @ det) >> node{ cat => det, form => Form::de_form[] }
	     ),
%%	domain(Form,[de,des,du,'de la','de l''']),
	edge{ source => N1,
	      target => node{ cat => det },
	      type => subst,
	      label => det,
	      id => EId2
	    },
	have_shared_derivs(EId1,EId2),
	rule_weight(Name,W,600)
	.

%% But penalize some coords
edge_cost_elem_lc(prep,prep,coo,
		edge{ label => label[prep],
		      type => adj,
		      source => node{ cat => prep },
		      target => node{ cat => coo, lemma => Lemma }
		    },
		Name::'-COORD_ON_prep',
		W) :-
	\+ domain(Lemma,[et,ou]),
	rule_weight(Name,W,-1000).

edge_cost_elem_lc(det,det,coo,
		edge{ label => label[det],
		      type => adj,
		      source => node{ cat => det },
		      target => node{ cat => coo, lemma => Lemma }
		    },
		Name::'-COORD_ON_det',
		W) :-
	\+ domain(Lemma,[et,ou]),
	rule_weight(Name,W,-1000).


%% Penalize coords on S, with coord3 not a S
edge_cost_elem_label(coord3,
		edge{ source => COO::node{ cat => coo },
		      target => node{ cat => TCat },
		      label => coord3,
		      type => edge_kind[lexical,subst]
		    },
		Name::'-COORD_special_S',
		W
	      ) :-
	rule_weight(Name,W,-500),
	chain( COO << adj << node{ cat => v } ),
	\+ TCat = v
	.

%% Penalize adjoining edges on coo
edge_cost_elem_label_type(coo,adj,
		edge{ label => coo,
		      type => adj,
		      target => node{ cat => Cat },
		      source => COO::node{ cat => SCat::coo }
		    },
		Name::'-adj_on_coo',
		W
	      ) :-
	rule_weight(Name,[W1,W2],[-50,-500]),
	\+ chain( COO << adj << node{ cat => adv }),
	( Cat = cat[adv,prep] ->
	  W = W1
	;
	  W = W2
	)
	.

%% sligtly favor coord on adj
edge_cost_elem_lc(adj,adj,coo,
		edge{ source => node{ cat => adj },
		      target => node{ cat => coo },
		      label => adj,
		      type => adj
		    },
		Name::'+coord_on_ante_adj',
		W
	       ) :-
	rule_weight(Name,W,100)
	.

%% penalize verb ellipsis
edge_cost_elem_lc(coord3,coo,v,
		  edge{ label => coord3,
			type => subst,
			source => node{ cat => coo },
			target => node{ cat => v, cluster => cluster{ left => L, right => L } }
		      },
		  Name::'-Coord_with_verb_ellipse',
		  W
		 ) :-
	rule_weight(Name,W,-6000)
	.


edge_cost_elem_label(coord,
		edge{ label => label[coord],
		      source => N::node{},
		      target => T1::node{},
		      id => EId1
		    },
		Name::'+ENUM',
		W ) :-
	rule_weight(Name,W,40),
	%% CALL EDGE
	source2edge(
	      edge{ label => coord,
		    source => N,
		    target => T2,
		    id => EId2
		  }
	     ),
	T1 \== T2,
	have_shared_derivs(EId1,EId2)
	.

:-light_tabular have_shared_derivs/2.
:-mode(have_shared_derivs/2,+(+,+)).

have_shared_derivs(EId1,EId2) :-
	once(( EId1 \== EId2,
	       %% CALL EDGE
	       edge{ id => EId1, source => N::node{ deriv => Derivs}},
	       edge{ id => EId2, source => N },
	       domain(D,Derivs),
	       deriv2edges(D,Edges),
	       domain(info(EId1,_,_,_,_),Edges),
	       domain(info(EId2,_,_,_,_),Edges)
	     ))
	.

/*
%% favour cords of number on midi and minuit (hours)
%% exemple: entre midi et deux
edge_cost_elem(
	       edge{ source => node{ cat => nc, lemma => Lemma },
		     target => COO::node{ cat => coo, lemma => et },
		     type => adj
		   },
	       Name::'+coo_hours',
	       W
	      ) :-
	rule_weight(Name,W,200),
	domain(Lemma,[midi,minuit]),
	chain(COO >> (subst @ coord3) >> node{ lemma => '_NUMBER' })
	.
*/

%% Favor noun apposition, specially when an entity is involved
%% and when a "function" is involved
edge_cost_elem_label_scat('N2app','N2',
			  edge{ id => EId,
				source => N2::node{ cat => 'N2',
						    tree => Tree2
						  },
				target =>  node{ cat => Cat3, lemma => Lemma3 },
				label => 'N2app',
				type => subst
			      },
			  Name,
			  _W
			 ) :-
	rule_weight(_Name:: '+apposition',_W,5),
	edge{ id => _EId,
	      target => N2,
	      type => adj,
	      label => 'N2',
	      source => N1::node{ lemma => Lemma1, cat => Cat1 }
	    },
	( ( Cat1 = np ; Cat3 = np ),
	  ( Cat1 = nc ; Cat3 = nc ) ->
	  _W1 = 200
	;
	  _W1 = 0
	),
	\+ Lemma1 = date[],
	\+ Lemma3 = date[],
	( (appos_function(Lemma1) ; appos_function(Lemma3)) ->
	  W is _W1+200
	;
	  W is _W1
	),
	( Name = _Name
%	; name_builder('+apposition_~w_~w',[Cat1,Cat3],Name)
%	; name_builder('+apposition_~w',[Lemma3],Name)
	),
	record_without_doublon(extra_indirect_cost_elem(EId,Name,_EId,W)),
	true
	.

%% Penalize initial binary coords built on "Ou" that maybe interpreted as
%% a wh-pronoun
edge_cost_elem_label(starter,
		edge{ label => 'starter',
		      target => node{ cat => coo,
				      cluster => C}
		    },
		Name::'-InitOuAsCoord',
		W
	      ) :-
	rule_weight(Name,W,-200),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ target => node{ cat => pri,
						 cluster => C }
			       }
			  )
	.

%% Penaltie on transcategorization from adj to nc
edge_cost_elem_tcat(adj,
		edge{ target => node{ cat => adj, xcat => 'N2', tree => Tree } },
		Name::'-ADJ',
		W ) :-
	rule_weight(Name,W,-1000),
	domain('adj_as_cnoun',Tree).

%% but favorize adj (as nc) to adv in superlative ('le plus grand')
edge_cost_elem_cats(adj,adv,
	       edge{ source => node{ cat => adj },
		     target => node{ cat => adv, lemma => Lemma } },
	       Name::'+superlative_on_adj',
	       W
	      ) :-
	rule_weight(Name,W,500),
	domain(Lemma,[plus,moins,mieux])
	.

%% penalize vmod on adv (in comparative)
edge_cost_elem_label_scat(vmod,adv,
	       edge{ source => node{ cat => adv },
		     type => adj,
		     label => 'vmod'
		   },
	       Name::'-vmod_on_supermod_adv',
	       W
	      ) :-
	rule_weight(Name,W,-100)
	.

%% Favour det preceding numbers
edge_cost_elem_cats(number,det,
	       edge{ source => node{ cat => number }, target => node{ cat => det } },
	       Name::'+NUM.DET',
	       W) :-
	rule_weight(Name,W,20).

%% Favour number range construction

edge_cost_elem_label(number2,
	       edge{ source => node{ cat => number },
		      target => node{ cat => number },
%%		      type => lexical,
		      label => number2
		    },
	       Name::'+number_range_as_det',
	       W
	      ) :-
	%% should counter-balence alternate constructions with two det
	%% [(de dix � quinze) gar�ons] vs [(de dix) � (quinze garcons)]
	%%   1 det vs 2 det
	%% or should we add a rule favoring the longest det interpretation ?
	rule_weight(Name,W,800).


%% Favour predet constructions
edge_cost_elem_label_tcat(det,predet,
		edge{ source => node{ cat => cat[nc,adj,pro,det] },
		      target => node{ cat => predet },
		      label => det,
		      type => edge_kind[subst,adj]
		    },
		Name::'+PREDET',
		W) :-
	rule_weight(Name,W,100).

%% Favour det modifier
edge_cost_elem_lc(det,det,adj,
		edge{ source => node{ cat => det },
		      target => node{ cat => adj },
		      label => det,
		      type => adj
		    },
		Name::'+det_mod',
		W
	      ) :-
	rule_weight(Name,W,300)
	.

%% Favour preddet on pro
edge_cost_elem_lc(Label,pro,adj,
		edge{ source => node{ cat => pro },
		      target => node{ cat => adj },
		      label => Label::label[predet_ante,predet_post],
		      type => lexical
		    },
		Name::'+predet_on_pro',
		W
	      ) :-
	rule_weight(Name,W,500).


%% Favour aux-v rather rather than v-acomp
%% but strong penalty on distance
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[v,aux] }, target => node{ cat => TCat::aux } },
		Name::'+AUX-V',
		W ) :-
	rule_weight(Name,W,3000).

%% Penalty on person constructions (vs e.g participiales): Jean, viens manger !
edge_cost_elem_tcat('S',
		edge{ target => node{ cat => 'S', tree => Tree } },
		Name::'-PERS',
		W ) :-
	rule_weight(Name,W,-20),
	domain(person_on_s,Tree)
	.

%% Favorize time mode, but on nouns
edge_cost_elem_label_type(time_mod,subst,
		edge{ type => subst,
		      label => time_mod,
		      target => N::node{ form => Form },
		      source => node{ cat => SCat }
		    },
		Name::'+time_mod',
		W
	      ) :-
	rule_weight(Name,W,50),
	( Form == '�t�' ->
	  chain( N >> (subst @ det) >> node{ cat => det } )
	;
	  true
	)
	.

%% Penalize attachement of time, person, audience amd reference incises
%% on subbordonate sentences
edge_cost_elem_label_type(Label,adj,
		edge{ type => adj,
		      source => V::node{ cluster => cluster{ right => R_V }},
		      target => S::node{ cat => cat['S','VMod'] },
		      label => Label::label['S','S2',vmod],
		      id => EId
		    },
		Name::'-incise_on_subS',
		W
	      ) :-
	rule_weight(Name,W1,-50),
	%% CALL EDGE
	source2edge(
	      edge{ source => S,
		    type => subst,
		    label => label[person_mod,audience,reference,time_mod,'S_incise',position],
		    target => N::node{ cluster => cluster{ left => L_Incise },
				       lemma => Lemma,
				       cat => Cat
				     }
		  }
	     ),
	chain( V << edge_kind[~ virtual] << R::node{ tree => RTree } ),
	\+ chain( R >> (subst @ start) >> node{} ),
	\+ domain( sep_sentence_punct, RTree ),
	( %% fail,
	  chain( N >> adj >> node{} >> subst >> V) ->
	  %% to compensate participiale 
	  W2 = -600
	;
	  W2 = 0
	),
	( R_V =< L_Incise,
	  \+ chain( S >> (adj @ incise) >> node{ cat => incise } ) ->
	  W3 = 50
	;
	  W3 = 0
	),
	%% penalize such mod on participiales, ...
	( edge2sop(EId,OId),
	  check_op_top_feature(OId,mode,Mode),
	  domain(Mode,[participle,gerundive]) ->
	  W4 = -800
	;
	  W4 = 0
	),
	W is W1 + W2 + W3 + W4
	.

%% Penalize some attachement of person, audience amd reference incises
edge_cost_elem_label_type(Label,subst,
		edge{ source => node{ cat => cat['S','VMod'] },
		      type => subst,
		      label => Label::label[person_mod,audience,reference],
		      target => N::node{ cat => TCat, lemma => TLemma, cluster=> TC::cluster{ left => TLeft, right => TRight }}
		    },
%		Name::'-incises',
			  Name,
		W3
	      ) :-
	rule_weight(Name,_W,-800),
	( Label = label[person_mod,audience],
	  TCat = np,
	  TLemma = entities['_PERSON','_PERSON_m','_PERSON_f']
	->
	  W1 is _W + 150
	; TLemma = ce ->
	  W1 = -5000 
	;
	  W1 = _W
	),
	(   AltN::node{ cat => adv, cluster => _TC::cluster{ left => _TLeft, right => _TRight }},
	    ( TC = _TC ;
		(_TLeft =< TLeft, TRight =< _TRight )
	    )
	->
	    %% node_weight(AltN,WAlt),
	    WAlt = 0,
	    W2 is W1-WAlt
	;   
	    W2 = W1
	),
	( Label = label[audience] ->
	  ( recorded(audience(TLemma)) ->
	    W3 is W2 + 300,
	    Name = '-incises_audience_class'
	  ; chain( N >> (lexical @ 'Monsieur') >> node{} ) ->
	    W3 is W2 + 400,
	    Name = '-incises_audience_Monsieur'
	  ; chain( N >> (adj @ 'N') >> node{ cat => adj, lemma => AdjLemma } ),
	    domain(AdjLemma,[cher,honor�,estim�]) ->
	    W3 is W3 + 300,
	    Name = '-incises_audience_adjective'
	  ; domain(TCat,[pri,pro]) ->
	    W3 is W2 - 5000,
	    Name = '-incises_audience_pro'
	  ;
	    W3 is W2 - 500,
	    Name = '-incises_audience'
	  )
	;
	  W3 = W2,
	  Name = '-incises'
	),
	true
	.

audience(pr�sident).
audience('Monsieur').
audience(commissaire).
audience(coll�gue).
audience(professeur).
audience(capitaine).
audience(homme).
audience(ami).
audience(soeur).
audience(fr�re).
audience(p�re).
audience(m�re).
audience(tante).
audience(oncle).
audience(ministre).
audience('Premier ministre').
audience(d�put�).
audience(s�nateur).
audience(d�l�gu�).
audience(partlementaire).
audience(dieu).
audience(rapporteur).
audience(compagnon).
audience(confr�re).
audience(consoeur).
audience(chef).
audience(fille).
audience(fils).
audience(roi).
audience(reine).
audience(prince).
audience(princesse).
audience(militant).
audience(auteur).
audience(commandant).
audience(entities['_PERSON','_PERSON_m','_PERSON_f']).

%% Penalize s_modifiers on N2
edge_cost_elem_label_type( Label,subst,
			   edge{ source => node{ cat => 'N2' },
				 type => subst,
				 label => Label::label[person_mod,audience,reference,time_mod,position]
			       },
			   Name:: '-s_modifier_on_N2',
			   W
			 ) :-
	rule_weight(Name,W,-200)
	.

edge_cost_elem( %none,
		edge{ target => N::node{ cat => Cat,
					 cluster => cluster{ left => Left, right => Right } } },
		Name::'+LONGLEX3',
		W
	      ) :-
	Right - Left > 1,
	( Cat = cat[det] ->
	    W = -300
	;
	    node_weight(N,W)
	)
	.

:-light_tabular node_weight/2.
:-mode(node_weight/2,+(+,-)).

%% to compensate closed words in locutions
node_weight( node{ lemma => Lemma,
		   cluster => cluster{ left => Left, right => Right } }, W) :-
	mutable(WM,0,true),
%%	format('try node add left=~w right=~w lemma=~w\n',[Left,Right,Lemma]),
	every(( node{ cat => Cat::cat[det,prep,csu,coo],
		      cluster => cluster{ left => _Left, right => _Right },
		      lemma => _Lemma
		    },
		Left =< _Left,
%%		format('*** node add ~w ~w l=~w r=~w\n',[_Lemma,Cat,_Left,_Right]),
		_Right =< Right,
		\+ (Left = Left, Right = _Right ),
		mutable_add(WM,1300)
	      )),
	mutable_read(WM,W),
	W > 0
	.


%% penalize some incise missing coma
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ source => node{ cat => SCat::cat[v,adj], cluster => cluster{ left => L} },
		      label => Label::label['S','S2'],
		      target => S::node{ cat => TCat::'S', cluster => cluster{ right => R }},
		      type => adj
		    },
		Name::'-pseudo_incise',
		W
	      ) :-
	R =< L,
	chain( S >> subst >> node{ cat => prep } ),
	\+ chain( S >> (adj @ incise) >> node{ cat => incise } ), 
	rule_weight(Name,W,-100)
	.

%% slightly favorize vmod->pro
edge_cost_elem_lc(Label,v,pro,
		edge{  source => v,
		       target => pro,
		       type => adj,
		       label => Label::label['S','S2',vmod]
		    },
		Name::'+pro_as_mod',
		W
	      ) :- rule_weight(Name,W,100).

%% penalize default incise as paren
edge_cost_elem_label_type(Label,subst,
		edge{ source => node{ cat => cat['S','VMod']},
		      type => subst,
		      label => Label::label['S_incise']
		    },
		Name::'-paren_incise',
		W
	      ) :-
	rule_weight(Name,W,-1000)
	.


%% but favor S_incise for citations
edge_cost_elem_label_type(Label,subst,
		edge{ source => node{ cat => cat['S','VMod']},
		      target => V::node{ lemma => Lemma,
					 cat => cat[v],
					 cluster => cluster{ right => Right} },
		      type => subst,
		      label => Label::label['S_incise']
		    },
		Name::'+citation_incise',
		W
	      ) :-
	rule_weight(Name,W,3500),
	%format('here1 lemma=~w\n',[Lemma]),
	(   chain( V >> edge_kind[subst,lexical] @ subject >> Subj::node{ cluster => cluster{ left => Left } } ) ->
	    Right =< Left
	;
	    chain( V >> adj @ label['Infl','V'] >> V1::node{ cat => cat[v,aux] } >> lexical @ subject >> Subj)
	),
	%format('here2 lemma=~w\n',[Lemma]),
	\+ ( chain( V >> edge_kind[subst,lexical] @ label[object] >> node{ cluster => cluster{ left => _Left}}),
	       _Left \== Left
	   ),
	%format('here2.1 lemma=~w\n',[Lemma]),
	\+ chain( V >> lexical @ label[prep,csu] >> node{} ),
	%format('here3 lemma=~w\n',[Lemma]),
	%% tmp hack: use list of citation verbs
	%% but better  to add a specific mechanism into FRMG
	domain(Lemma,[dire,indiquer,mentionner,poursuivre,continuer,affirmer,
		      confirmer,accuser,assurer,avancer,conclure,interroger,lancer,
		      noter,raconter,pr�venir,regretter,souligner,admettre,r�sumer,insister,
		      souligner,remarquer,annoncer,protester
		     ]
	      )
	.

/*
%% favor balanced incise marks
edge_cost_elem( 
		edge{ target => node{ cat => incise, tree => Tree },
		      label => incise,
		      type => adj
		    },
		Name::'+balance_incise_marks',
		W
	      ) :- rule_weight(Name,W,300),
	domain('incise_strict',Tree)
	.
*/

%% favour genitive over locative
edge_cost_elem_label(clg,
		edge{ target => node{ cat => clg }, label => clg },
		Name::'+CLG',
		W ) :- rule_weight(Name,W,50).

/*
%% Favour auxiliary over verbs
edge_cost_elem( 
		edge{ target => node{ cat => aux }},
		Name::'+AUX/V',
		W ) :- rule_weight(Name,W,2000).
*/

%% Favour 'est' as verb
%% edge_cost_elem( edge{ target => node{ cluster => cluster{ }}, 1000 ).

%% Penalties on filler in robust parsing
edge_cost_elem_tcat(unknown,
		edge{ target => node{ cat => unknown } },
		Name::'-UNK',
		W) :- rule_weight(Name,W,-3000).

%% Extra penalties on unknown trees in robust parsing
edge_cost_elem_scat(unknown,
		edge{ source => node{ cat => unknown,
				      tree => [unknown]
				    }
		    },
		Name::'-UNK2',
		W
	      ) :- rule_weight(Name,W,-10000)
	.

%% Extra penalties on unknown v fillers
edge_cost_elem_cats(unknown,v,
		edge{ source => node{ cat => unknown },
		      target => node{ cat => cat[v] }
		    },
		Name::'-UNK3',
		W
	      ) :- rule_weight(Name,W,-1000)
	.

%% Penalties on virtual edges
edge_cost_elem_type(virtual,
		edge{ type => virtual },
		Name::'-VIRTUAL',
		W) :-
	rule_weight(Name,W,-3000).

%% But less penalties on virtual edges leading to verbs, not in imperative
edge_cost_elem_type(virtual,
		edge{ id => EId,
		      type => virtual,
		      target => node{ cat => v }
		    },
		Name::'+VIR->V',
		W ) :- rule_weight(Name,W,500).


%% Penalties on imperative verbs when in robust mode
edge_cost_elem_scat(v,
		edge{ id => EId, source=> node{ cat => v, cluster => C }},
		Name::'-Imp_Verbs_when_Robust',
		W ) :-
	rule_weight(Name,W,-4000),
	recorded( mode(robust) ),
	edge2sop(EId,OId),
	check_op_top_feature(OId,mode,imperative)
	.

%% Penalize clitics on infinitive and participial
%% when possible interpretation as det
edge_cost_elem_tcat(cla,
		edge{ source => V::node{ cat => v },
		      target => Cl::node{ cat => cat[cla], cluster => C},
		      type => lexical
		    },
		Name::'-cl_vs_det',
		W
	      ) :-
	rule_weight(Name,W,-1500),
	%% CALL EDGE
	\+ source2edge( edge{ source => V, label => subject } ),
	targetcluster2edge( edge{ target => node{ cluster => C, cat => det } } )
	.


%% Penalties on virtual edges leading to empty short sentence
edge_cost_elem_type(virtual,
		edge{ type => virtual,
		      target => node{ cat => 'S',
				      cluster => cluster{ lex => '' }}
		    },
		Name::'-SHORTVIRT',
		W ) :-
	rule_weight(Name,W,-500),
	recorded(mode(robust)).


%% Favour closed categories interpretations over others
edge_cost_elem_tcat(Cat,
		edge{ target => node{ lemma => L,
				      cat => Cat::cat[csu,prep,coo,det,pri,pro,prel,coo,cln,cla,clr,clg,cll,cld,ilimp,caimp,predet,title],
				      form => F,
				      cluster => C::cluster{ left => Left, right => Right } } },
		Name,		% +CLOSED_%type
		W ) :-
%%	rule_weight(Name,[W1,W2,W3,W4],[1700,1500,1200,600]),
	L \== unknown[],
	\+ (L  = number[]),
	\+ (L  = tout ),	% tout maybe too many things (det, adv, pro)
	( Cat = cat[predet] ->
	  Name = '+CLOSED_predet',
	  rule_weight(Name,W,1700),
	  \+ ( domain(L,[nombre]),
	       edge{ source => node{ cat => nc, cluster => C},
		     target => node{ cat => cat[adj,det,predet] },
		     type => edge_kind[adj,subst]
		   }
	     ),
	  true
	; Cat = cat[det] ->
	  Name = '+CLOSED_det',
	  ( ( C1::cluster{ left => Left, right => Middle },
	      node{ cluster => C1, cat => prep },
	      cluster{ left => Middle, right => Right }
	    ) ->
	    %% for cases like 'il mange [de la] viande'	 
	    rule_weight(Name,W,2900)
	  ;
	    rule_weight(Name,W,1700)
	  )
	; Cat = cat[cln,cla,clr,clg,cll,cld,ilimp,caimp],
	  (\+ domain(F,[en])) ->
	  Name = '+CLOSED_cl',
	  rule_weight(Name,W,1500)
	; Cat = cat[que] -> 
	  Name = '+CLOSED_que',
	  rule_weight(Name,W,1200)
	; Cat = cat[prep,csu,coo] ->
	  Name = '+CLOSED_conj_or_prep',
	  rule_weight(Name,W,1200)
% 	; Cat = cat[aux] ->
% 	  Name = '+CLOSED_aux',
% 	  rule_weight(Name,W,500)
	;
	  Name = '+CLOSED_misc',
%%	  \+ domain(L,[tout,aucun,autre]),
	  rule_weight(Name,W,600)
	)
	.
%%	edge{ target => node{ cat => cat[nc,np,adj,adv,v], cluster => C } }.

%% Strongly penalize det as noun or adj

%% Strongly penalize que or qu' as pri after verb
edge_cost_elem_label_tcat(object,pri,
		edge{ target => node{ lemma => quepro[],
				      cat => pri,
				      cluster => cluster{ left => L} },
		      source => node{ cluster => cluster{ right => R }},
		      type => subst,
		      label => object
		    },
		Name::'-queAsObject',
		W
	      ) :-
	rule_weight(Name,W,-10000),
	R =< L
	.

edge_cost_elem_tlemma(TLemma,
		edge{ source => N::node{ cat => comp},
		      target =>  node{ lemma => TLemma::quepro[],
				       cat => pri,
				       cluster => cluster{ left => L} },
		      type => subst,
		      label => 'N2'
		    },
		Name::'-queAsComp',
		W
	      ) :-
	rule_weight(Name,W,-20000),
	%% CALL EDGE
	target2edge(
		    edge{ target => N,
			  source => node{ cluster => cluster{ right => R }},
			  label => comp,
			  type => subst
			}
		   ),
	R =< L
	.


%% penalize que as pri in xcomp constructions
edge_cost_elem_tlemma(TLemma,
		edge{ source => V::node{ cat => v },
		      target => node{ cat => TCat::cat[pri],
				      lemma => TLemma::quepro[]
				    },
		      type => subst,
		      label => Label::label[comp,object]
		    },
		Name::'-que_as_pri',
		W) :-
	rule_weight(Name,W,-2000),
	%% CALL EDGE
	target2edge(
		    edge{ target => V,
			  source => VS,
			  type => subst,
			  label => xcomp
			}
		   )
	.

%% favor relative sentence over other constructions
%% weaker if the relative sentence is preceded by a comma
edge_cost_elem_label_type(label['N2',adj],adj,
		edge{ source => N1::node{},
		      target => N2::node{},
		      type => adj,
		      label => 'N2'
		    },
		Name::'+relative',
		W
	      ) :-
	rule_weight(Name,[W1,W2],[1100,1200]),
	node!empty(N2),
	%% CALL EDGE
	source2edge(
	      edge{ source => N2,
		    target => V::node{ cat => v, cluster => cluster{ left => Left} },
		    label => 'SRel',
		    type => subst
		  }
	     ),
	( chain( N2 >> adj >> node{ cat => incise } >> lexical 
	       >> node{ lemma => ',',
			cluster => cluster{ right => InciseRight }}
	       ),
	  InciseRight =< Left ->
	  W=W1
	;
	  W=W2
	)
	.

%% Strongly penalize relative on non-saturated nouns
%% except if in PP or if an NP
edge_cost_elem_label_type('N2',adj,
		edge{ source => N1::node{ cat => N1Cat::xnominal[nc] },
		      target => N2::node{},
		      type => adj,
		      label => 'N2'
		    },
		Name::'-relative_on_unsat_noun',
		W
	      ) :-
	rule_weight(Name,W,-4000),
	node!empty(N2),
	chain( N2 >> (subst @ 'SRel') >> node{} ),
	\+ chain( N1 >> (subst @ det) >> node{ cat => det } ),
	\+ chain( N1 << (subst @ 'N2') << node{ cat => prep } )
	.
		
%% favor qui as relative
edge_cost_elem_label_type('N2',adj,
		edge{ source => N1,
		      target => N2::node{ cat => 'N2',
					  cluster => cluster{ left => L,
							      right =>L }},
		      type => adj,
		      label => 'N2'
		    },
		Name::'+quiAsRel',
		W
	      ) :-
	rule_weight(Name,W,100),
	chain( N2 >>  (subst @ 'SRel')
	     >> V::node{ cat => v } >> (lexical @ subject)
	     >> node{ cat => prel }
	     )
	.

%% Penalize qui as rel pron without antecedent
edge_cost_elem_label_type('SRel',subst,
		edge{ source => N1::node{ cat => 'N2' },
		      label => 'SRel',
		      type => subst
		    },
		Name::'-quiAsRelNoAntecedent',
		W
	      ) :-
	rule_weight(Name,W,-3000),
	node!empty(N1),
	%% CALL EDGE
	\+ target2edge(
		       edge{ label => 'N2',
			     type => adj,
			     target => N1
			   }
		       )
	.

%% Penalties on _uw and _Uw words
edge_cost_elem_tlemma(uw,
		edge{ target => node{ lemma => 'uw' } },
		Name::'-UW1',
		-1100
	      ) :- rule_weight(Name,W,-1100).

edge_cost_elem_tlemma(uw,
		edge{ target => node{ cat => nc, lemma => uw } },
		Name::'+UWasNc',
		W
	      ) :- rule_weight(Name,W,100).

edge_cost_elem_tlemma(uw,
		edge{ target => node{ cat => cat[adj,adv], lemma => uw } },
		Name::'+UWasAdjorAdv',
		W
	      ) :- rule_weight(Name,W,50)
	      .

edge_cost_elem_tlemma('_Uw',
		edge{ target => node{ lemma => '_Uw' } },
		Name::'-UW2',
		W
	      ) :- rule_weight(Name,W,-1000)
	      .

%% Penalize unknown words as verbs at beginning of sentence
edge_cost_elem_tlemma(uw,
		edge{ target => node{ lemma => uw,
				      cat => cat[v,aux],
				      cluster => cluster{ left => 0 }
				    } },
		Name::'-uw_as_v_on_start',
		W ) :- rule_weight(Name,W,-1000).


%% Penalize capitalization when there is a non-capitalized word
%% except if there is not det for the word
edge_cost_elem_tcat(np,
		edge{ target => N::node{ cat => np, lemma => L, cluster => C } },
		Name::'-Capitalize',
		W
	      ) :-
	rule_weight(Name,W,-700),
	L \== unknown[],
	node{ cat => cat[nc,adj], cluster => C },
	chain( N >> (subst @ det ) >> node{ cat => det } )
	.

%% Penalties on sequence of Nc
edge_cost_elem_label('Nc2',
		Edge::edge{ source => node{ cat => nc },
		      target => node{ cat => nc, lemma => Lemma },
		      label => 'Nc2' },
		Name::'-NcSeq',
		W ) :-
	rule_weight(Name,W,-1000),
	Lemma \== number[],
	verbose('I am here ~E\n',[Edge])
	.

%% Favor sequence of Nc leading to numbers
edge_cost_elem_label('Nc2',
		Edge::edge{ source => node{ cat => nc },
		      target => node{ cat => nc, lemma => number[] },
		      label => 'Nc2' },
		Name::'+NcSeq_Num',
		W ) :-
	rule_weight(Name,W,+1050)
	.

%% Penalties on use of comma as sentence separator
%% Extremely strong penalties
%% Should only be used as very last option
edge_cost_elem_tcat('S',
		Edge::edge{ type => adj, target => N::node{ cat => 'S', tree => Tree }},
		Name::'-COMASEP',
		W ) :-
	rule_weight(Name,W,-10000),
%	format('test comasep ~w\n',[Edge]),
	domain(sep_sentence_punct_coma,Tree),
%	format('success comasep ~w\n',[Edge]),
	true
	.

%% Penalties on sentence separator applied on subordonnate sentences
edge_cost_elem_tcat('S',
	       Edge::edge{ type => adj,
			   target => N::node{ cat => 'S', tree => Tree },
			   source => V::node{}
			 },
	       Name::'-sentence_sep_on_subs',
	       W
	      ) :-
	fail,
	rule_weight(Name,W,-5000),
	node!empty(N),
	chain( V << _ << node{} ),
	chain( N >> lexical >> node{ lemma => Lemma} ),
	domain(Lemma,[(?),('...'),';',',','!','?','!?','?!','!!!','_SENT_BOUND'])
	.

%% Favor main verbal node (for verbal sentences) for full sentences
edge_cost_elem_type(virtual,
		edge{ id => root(NId),
		      target => node{ id=> NId, cat => v },
		      type => virtual
		    },
		Name::'+verbal_sentence',
		W
	      ) :-
	recorded( mode(parse_mode[full,corrected]) ),
	rule_weight(Name,W,300),
	%%	format('verbal_sentence\n',[]),
	true
	.

edge_cost_elem_tcat('S',
		Edge::edge{ type => adj, target => N::node{ cat => 'S' } },
		Name::'+QUOTED_SENT_MOD',
		W
	      ) :-
	rule_weight(Name,W,-1000), % strange pb => prefer penalize
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    type => subst,
		    label => quoted_S
		  }
	     )
	.

%% penalize quoted_S in N2 context
edge_cost_elem_label_type(quoted_S,subst,
	       edge{ source => node{ cat => 'N2' },
		     label => quoted_S,
		     type => subst
		   },
	       Name::'-quoted_S_as_N2',
	       W
	       ) :-
	rule_weight(Name,W,-1000)
	.

/*
%% Bonuses on use of comma in enumerations
%% depends on presence of ending ... and number of ','
edge_cost_elem( 
		Edge::edge{ type => adj, target => N::node{ tree => Tree }},
		Name::'+COMAENUM',
		K
	      ) :-
	rule_weight(Name,W,300),
	domain('N2_enum',Tree),
	mutable(M,W,true),
	%% CALL EDGE
	every(( source2edge(
		      edge{ source => N,
			    type => lexical,
			    target => node{ cluster => cluster{ lex => Lex }}}
		     ),
		label2lex(Lex,[X],_),
		mutable_read(M,_K),
		( X == (',') ->
		    New_K is 2*_K
		;   X == '...' ->
		    New_K is 3*_K
		;
		    New_K is _K
		),
		mutable(M,New_K)
	      )),
	mutable_read(M,K)
	.
*/

%% Favour adj->adv edges
edge_cost_elem_cats(adj,adv,
		edge{ source => node{ cat => cat[adj] },
		      target => node{ cat => adv },
		      type => adj
		    },
		Name::'+ADVMODADJ',
		W ) :- rule_weight(Name,W,150).

%% Favour adv->adv edges
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[adv,advneg] },
		      target => node{ cat => TCat::adv },
		      type => adj
		    },
		Name::'+ADVMODADV',
		W ) :- rule_weight(Name,W,170).

%% Favour adj->pro edges
edge_cost_elem_cats(pro,adv,
		edge{ source => node{ cat => cat[pro] },
		      target => node{ cat => adv },
		      type => adj
		    },
		Name::'+ADVMODPRO',
		W ) :- rule_weight(Name,W,200).


%% Penalize mutiple adv edges on adj or Prep on same side
edge_cost_elem_cats(SCat,TCat,
		edge{ source => N::node{ cat => SCat::cat[adj,prep], cluster => cluster{ left => NLeft, right => NRright } },
		      target => node{ cat => TCat::adv, cluster => cluster{ right => R }},
		      type => adj
		    },
		Name::'-multipleAdvOnAdjorPrep',
		W
	      ) :-
	rule_weight(Name,W,-1000),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => adv, cluster => cluster{ left => L } },
		    type => adj
		  }
	     ),
	R =< L,
	(L =< NLeft xor NRight =< R)
	.

%% Penalize adv on prep
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[prep] },
		      target => node{ cat => TCat::adv },
		      type => adj
		    },
		Name::'-ADVONPREP',
		W ) :- rule_weight(Name,W,-500).

/*
%% Penalties on edge coming from empty cluster
edge_cost_elem( 
		Edge::edge{ source => node{ cluster => cluster{ lex => '' }}},
		Name::'-EMPTY',
		W ) :-
	rule_weight(Name,W,-20),
	verbose('Empty cluster penalty ~E\n',[Edge])
	.
*/

%% Favour long clusters, especially for closed cats
edge_cost_elem( %none,
		Edge::edge{ target => node{ cat => Cat,
					    lemma => Lemma,
					    form => Form,
					    cluster => cluster{ lex => Lex,
								left => Left,
								right => Right }}},
		Name::'+LONGLEX',
		K
	      ) :-
	rule_weight(Name,[F1,F2],[500,100]),
	Lex \== '',
	Right > Left + 1,
%%	format('LONGLEX ~w ~w ~w\n',[Left,Right,Lex]),
%%	\+ agglutinate(_,_,Form),
	\+ Form = de_form['de la','du','des','de l'''],
	\+ ( Cat = np,
	     node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	     LX >= Left,
	     RX =< Right,
%%	     ( LX \== Left xor RX \== Right),
	     \+ ( node{ cat => np,
			form=> Lemma,
			cluster => cluster{ left => LX, right => RX } },
		  Lemma \== '_Uw'
		),
	     true
	   ),
	( %% Cat = cat[det,pri,prel,adv,advneg,prep,csu,que] -> Factor = F1
	  %% *** WARNING *** not sure it is the best choice for prep
	  Cat = cat[det,pri,prel,csu,que,prep] -> Factor = F1
	; Factor = F2
	),
	K is (Right - Left) * (Right - Left) * Factor,
	K > 0
	.

%% Favour long clusters case 2
edge_cost_elem( %none,
		Edge::edge{ target => node{ cat => Cat,
					    cluster => cluster{ lex => Lex,
								left => Left,
								right => Right
							      }}},
		Name::'+LONGLEX2',
		K
	      ) :-
	rule_weight(Name,[F1,F2],[500,100]),
	Lex \== '',
	label2lex(Lex,L,_),
	( L = [_,_|_] ->
	  Right is Left+1,
	  length(L,N)
	;
	  Right > Left+1,
	  Cat = cat[adv,advneg,prep],
	  N is (Right - Left)
	),
	\+ ( Cat = np,
	     node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	     LX >= Left,
	     RX =< Right,
	     \+ node{ cat => np, cluster => cluster{ left => LX, right => RX } }
	   ),
%%	format('Trying LONGLEX2 ~E\n',[Edge]),
%%	format('Trying2 LONGLEX2 ~E\n',[Edge]),
	( Cat = cat[det,pri,prel,prep,adv,advneg,prep,csu,que] -> Factor = F1
	; Factor = F2
	),
	K is N*N*Factor,
%%	format('Trying3 LONGLEX2 L=~w ~E\n',[L,Edge]),
	K > 0
	.

%% Favor long prep but in a decomposed way !
edge_cost_elem_tcat(prep,
		edge{ %% source => node{ cat => nc },
		      target => Last::node{ cat => prep,
					    cluster => cluster{ left => L,
								right => R,
								lex => Lex
							      }
					  }
		      %% label => 'N2',
		      %% type => adj
		    },
		Name::'+long_prep',
		W
	      ) :-
	recorded( opt( nocompound )),
	rule_weight(Name,W,500),
	%% CALL EDGE
	targetcluster2edge(
	      edge{ target => node{ cat => prep,
				    cluster => cluster{ left => L2,
							right => R,
							lex => Lex2,
							token => Token2
						      }
				  }}
	     ),
%%	format('try lex=~w lex2=~w token2=~w\n',[Lex,Lex2,Token2]),
	(\+ passage_compound(Token2)),
	Lex \== Lex2,		% not an explicit composed form for Easy
	L2 < L
	.

%% penalize adjonction on potential adv composed forms
%% these forms are decomposed for Passage but should preserve
%% their composed properties as much as possible
edge_cost_elem_type(adj,
		edge{ source => node{ cluster => cluster{ left => L, right => R }},
		      type => adj
		    },
		Name::'-Postmod_on_compound_adv',
		W
	      ) :-
	rule_weight(Name,W,-1000),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ target => node{ cat => adv,
						 cluster => cluster{ left => L1, right => R }
					       }
			       }
			  ),
	L1 < L,
	true
	.

edge_cost_elem_cats(TCat,prep,
		edge{ source => N2::node{ cat => TCat::cat[nc,np,v],
					  lemma => Target
					},
		      target => Prep::node{ cat => prep },
		      type => adj
		    },
		Name::'-RESTR_mod_on_term',
		W
	      ) :-
	chain( N2
	     << subst
	     << node{ cat => prep, lemma => Rel }
	     << adj
	     << node{ cat => SCat, lemma => Source }
	     >> adj
	     >> Prep
	     ),
	check_term(Source,SCat,Target,TCat,Rel,W)
	.

%%% favorize passage compound
edge_cost_elem( %none,
		edge{ target => node{ cluster => cluster{ token => Token, left => L, right => R }}},
		Name::'+passage_compound',
		W
	      ) :-
	rule_weight(Name,W,500),
	L + 1 < R,
	passage_compound(Token)
	.

%% Penalize long lex when NP and reading with some non np components
edge_cost_elem_tcat(np,
		Edge::edge{ target => node{ cat => np,
					    cluster => cluster{ lex => Lex, left => Left, right => Right }}},
		Name::'-NP_as_LONGLEX',
		W ) :-
	rule_weight(Name,W,-20),
	Right > Left + 1,
	node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	LX >= Left,
	RX =< Right,
	\+ ( node{ cat => np, lemma => Lemma, cluster => cluster{ left => LX, right => RX } },
	     Lemma \== '_Uw'
	   ),
	true
	.

%% Favour prep->X edges
edge_cost_elem_scat(prep,
		Edge::edge{ source => node{ cat => prep },
			    target => node{ cat => cat[~ [adj,adv]] },
			    type => edge_kind[subst,lexical] },
		Name::'+PREP->X',
		W
	      ) :- rule_weight(Name,W,400).

%% Favour prep->nc|np edges
edge_cost_elem_cats(prep,TCat,
		Edge::edge{ source => node{ cat => prep },
			    type => edge_kind[subst,lexical],
			    target => node{ cat => TCat::cat[nc,np] }
			  },
		Name::'+PREP->n',
		W ) :- rule_weight(Name,W,200).

%% Favour lexical v->prep edges
edge_cost_elem_tc(lexical,v,prep,
		Edge::edge{ source => node{ cat => v },
			    target => node{ cat => prep },
			    type => edge_kind[lexical] },
		Name::'+V->PREP',
		W
	      ) :- rule_weight(Name,W,800).

%% Penalize PP attachement of +time on nouns and adj
edge_cost_elem_tc(adj,SCat,TCat,
		Edge::edge{
			   source => node{ cat => SCat::cat[nc,np,adj] },
			   type => adj,
			   target => Prep::node{ cat => TCat::prep }
			  },
		Name::'-N2->prep->+time',
		W
	      ) :-
	rule_weight(Name,W,-200),
	%% CALL EDGE
	source2edge(
	      edge{ source => Prep,
		    type => edge_kind[subst,lexical],
		    %% label => 'N2',
		    target => N2::node{ id => NId2,
					cat => N2Cat,
					lemma => Lemma
				      }
		  }
	     ),
	( domain(Lemma,date[])
	xor N2Cat=adv
	xor check_node_top_feature(NId2,time,true_time[])
	)
	.

%% very low bonus for prep->ENTITY on verbs
%% mostly used to anchor cost-based tuning
edge_cost_elem_tc(adj,SCat,TCat,
		  Edge::edge{
			     source => Source::node{ cat => SCat::cat['VMod',nc,np,adj], lemma => _SLemma },
			     type => Type,
			     target => Prep::node{ cat => TCat::prep, lemma => PLemma }
			    },
		  Name,
		  W
		 ) :-
	%% CALL EDGE
%	format('test0 entity ~w\n',[Edge]),
	source2edge(
	      edge{ source => Prep,
		    type => edge_kind[subst,lexical],
		    target => node{ id => NId2,
				    lemma => Lemma::entities[]
				  }
		  }
	     ),
%	format('test1 entity ~w\n',[Edge]),
	( SCat = 'VMod' ->
	  Type = subst,
	  chain( Source << adj << node{ cat => v, lemma => SLemma } )
	;
	  SLemma = _SLemma,
	  Type = adj
	),
	( Name = '+prep->ENTITY'
	; name_builder( '+prep->ENTITY_~w_~w',[PLemma,Lemma], Name )
	; name_builder( '+prep->ENTITY_~w_~w_~w',[SLemma,PLemma,Lemma], Name )
	),
	rule_weight(Name,W,+2)
	.

%% favor PP[pour]+inf v
edge_cost_elem_lc('PP',SCat,TCat,
		Edge::edge{
			   source => node{ cat => SCat::cat['VMod','S'] },
			   type => subst,
			   label => 'PP',
			   target => Prep::node{ cat => TCat::prep,
						 lemma => L
					       }
			  },
		Name::'+v->pour->Sinf',
		W
	      ) :-
	rule_weight(Name,_W,130),
	domain(L : W2 ,[pour : 0 ,
			'afin de' : 100,
			'sans' : 0,
			'quand �' : 100,
			'avant de' : 100,
			'en dehors de' : 100,
			'en d�pit de' : 100,
			'en fait de' : 100,
			'en plus de' : 100,
			'en train de' : 100,
			'en voie de' : 100,
			'en vue de' : 100,
			'entre' : 0,
			'hormis' : 0,
			'non sans' : 100,
			'plut�t que de' : 100,
			'sauf �' : 100,
			'si pr�s de' : 0,
			'� force de' : 100
		       ]),
	W is _W + W2,
	chain( Prep >> (subst @ 'S') >> node{ cat => v } )
	.

%% Favour participial on subjects
edge_cost_elem_lc(Label,v,NCat,
		edge{ source => V::node{ cat => v },
		      target => N::node{ cat => NCat::cat['S','VMod'], tree => Tree },
		      type => adj,
		      label => Label::label['S','S2',vmod] },
		Name::'+Participiale_on_Subject',
		W3 ) :-
	rule_weight(Name,W,200),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => Part::node{ cat => v },
		    label => 'SubS',
		    type => 'subst',
		    id => EId
		  }
	     ),
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle,gerundive]),
	%% CALL EDGE
	( source2edge(edge{ source => N,
		      label => incise,
		      type => adj
		    }) ->
	  W1 is W+150
	;
	  W1 is W
	),
	( source2edge(
		edge{ source => V,
		      label => subject
		    }
	       )->
	  W2 is W1+ 300
	;
	  W2 is W1
	),
	( chain( Part
	       << subst
	       << node{}
	       << adj
	       << N2::node{ cat => xnominal[] }
	       ) ->
	    %% but favour attachement on nouns when possible
	    fail
	;
	    W3 = W2
	)
	.

%% Favour participle attachement on subjects
edge_cost_elem_label_tcat('N2',TCat,
		edge{ source => N1::node{ cat => SCat::xnominal[] },
		      target => N2::node{ cat => TCat::'N2' },
		      label => 'N2',
		      type => adj
		    },
%%		Name::'+Participiale_on_Subject2',
			  Name,
		W
	      ) :-
	rule_weight(Name,W,50),
	chain(N1 << (subst @ subject) << node{ cat => v }),
	chain(N2 >> (adj @ incise) >> node{ cat => incise }),
	edge{ source => N2,
	      target => node{ cat => v},
	      label => 'SubS',
	      type => subst,
	      id => EId
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle,gerundive]),
	( Mode = participle ->
	  Name = '+Participiale_on_Subject2_participle'
	;
	   Name = '+Participiale_on_Subject2_gerundive'
	)
	.

%% Penalize subject on participles
edge_cost_elem_label_scat(subject,v,
		edge{ source => V::node{ cat => v },
		      label => subject
		    },
		Name::'-subject_on_participle',
		W
	      ) :-
	edge{ target => V,
	      label => 'SubS',
	      type => subst,
	      id => EId
	    },
	rule_weight(Name,W,-500),
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle])
	.
	      

%% Penalize SubS on SubS
edge_cost_elem_cats(v,TCat,
		edge{ source => V::node{ cat => v },
		      target => S::node{ cat => TCat::cat['S','VMod'] },
		      type => adj
		    },
		Name::'-participiale_on_participiale',
		W
	      ) :-
	chain( S >> (subst @ 'SubS') >> node{ cat => v } ),
	chain( V << (subst @ 'SubS') << node{ cat => 'N2' } ),
	rule_weight(Name,W,-1000)
	.

%% Favour v->VMOd->prep edges for participials
edge_cost_elem_cats(v,'VMod',
		Edge::edge{ source => V::node{ cat => v },
			    target => VMod::node{ cat => 'VMod' },
			    type => adj
			  },
		Name,
		W
	      ) :-
	rule_weight(Name:: '+V->VMOD->PREP',W,400),
	%% CALL EDGE
	source2edge(
	      edge{ source => VMod,
		    target => node{ cat => prep, lemma => PLemma },
		    type => subst,
		    label => 'PP'
		  }
	     ),
	target2edge(
		    edge{ source => node{ cat => 'N2' },
			  target => V,
			  type => subst,
			  label => 'SubS'
			}
		   ),
	true
	.

%% rule v->prep to be used as a feature in the cost mechanism 
edge_cost_elem_cats(v,'VMod',
		    E::edge{ source => node{ cat => v, lemma => VLemma },
			     target => VMod::node{ cat => 'VMod' },
			     type => adj
			   },
		    Name,
		    W
		   ) :-
	chain( VMod >> (subst @ 'PP') >> node{ cat => prep, lemma => PrepLemma } ),
	W is 5,
	name_builder('+PP_V_~w_~w',[VLemma,PrepLemma],Name)
	.

% edge_cost_elem( edge{ source => node{ lemma => Source },
% 		      target => N::node{},
% 		      type => adj
% 		    },
% 		Name,
% 		W
% 	      ) :-
% 	( node!empty(N),
% 	  chain( N >> subst >> T::node{ lemma => Target } ),
% 	  \+ node!empty(T),
% 	  W is 2,
% 	  name_builder('indirect_~w',[Target],Name)
% 	xor fail
% 	)
% 	.

%% Favor v->prep if prep modified by adv
edge_cost_elem_cats(SCat,prep,
		edge{ source => node{ cat => SCat::cat['S','VMod',v] },
		      target => P::node{ cat => prep },
		      type => subst
		    },
%%		Name::'+V->prep+adv',
		    Name,
		W
	      ) :-
	rule_weight(Name:: '+V->prep+adv',W,250),
	%% CALL EDGE
	source2edge(
	      edge{ source => P,
		    target => node{ cat => adv, lemma => AdvLemma },
		    type => adj,
		    label => 'PP'
		  }
	     )
	.

%% Penalty on v->prep[de,d',des] edges going to verbs
edge_cost_elem_cats(SCat,prep,
		Edge::edge{ source => node{ cat => SCat::cat['S','VMod','N2'] },
			    target => N::node{ cat => prep,
					       lemma => 'de'
					     }
			  },
		Name::'-V->de',
		W ) :-
	rule_weight(Name,W,-100),
	%% CALL EDGE
	\+ source2edge(edge{ source => N, target => node{ cat => v } })
	.

%% Favor [adj,nc,np]->de edges instead of 'de X' as GN
edge_cost_elem_cats(SCat,TCat,
		Edge::edge{ source => N::node{ cat => SCat::cat[nc,np,adj,pro] },
			    target => Prep::node{ cat => TCat::prep, lemma => 'de', cluster => cluster{ left => Left } } },
		Name::'+->de',
		W
	      ) :-
	rule_weight(Name,W,500),
	\+ chain( Prep >> (subst @ 'N2')
		>> node{} << (subst @ object) << node{}
		),
	\+ ( chain( N >> (subst @ xcomp) >> node{ cluster => cluster{ right => Right }} ),
	       Right =< Left
	   )
	.

%% Favor de->np as prep (not sure it is useful)
edge_cost_elem_lc('N2',prep,np,
		edge{ source => node{ cat => prep, lemma => 'de' },
		      target => node{ cat => np },
		      type => subst,
		      label => 'N2'
		    },
		Name::'+de->np',
		W
	      ) :- rule_weight(Name,W,500).


%% Favor nc1->prep->nc2 if nc2 is not saturated
edge_cost_elem_lc('N2',prep,nc,
		edge{ source => node{ cat => prep },
		      target => N2::node{ cat => nc },
		      type => subst,
		      label => 'N2'
		    },
		Name::'+nc->prep->unsat_nc',
		W
	      ) :-
	rule_weight(Name,W,100),
	%% CALL EDGE
	\+ source2edge(
		 edge{ source => N2,
		       target => node{ cat => cat[det,predet] },
		       type => subst
		     }
		)
	.


%% Penalize adjoint on unsaturated nc preceded by a prep
edge_cost_elem_scat(nc,
		edge{ source => N1::node{ cat => nc },
		      target => node{ cat => cat[~prep] },
		      type => adj
		    },
		Name::'+-adj_on_unsat_nc_with_prep',
		W
	      ) :-
	rule_weight(Name,W,-10),
	%% CALL EDGE
	target2edge(
		    edge{ source => node{ cat => prep, lemma => Prep },
			  target => N1,
			  id => NId1
			}
		   ),
	domain(Prep,[de]),
	\+ ( chain( N1 << subst @ prep << node{ cat => prep, id => NId2 } ),
	     NId1 \== NId2
	   ),
	\+ source2edge(
		 edge{ source => N1,
		       target => node{ cat => cat[det,predet] },
		       type => subst
		     }
		)
	.


%% Penalize N2 adj after a relative, but for coordinations
edge_cost_elem_label('N2',
		edge{ source => N::node{},
		      label => 'N2',
		      type => adj,
		      target => M :: node{ cat => cat[~ [coo]],
					   cluster => cluster{ left => LM }}
		    },
		Name::'-N2adj_after_rel',
		W
	      ) :-
	rule_weight(Name,W,-600),
	chain( N >> (adj @ 'N2')
	     >> node{} >> (subst @ 'SRel')
	     >> node{ cluster => cluster{ left => LM1 } }
	     ),
	LM1 < LM
	.

%% Penalty based on rank
edge_cost_elem( %none,
		Edge::edge{ id => EId },
		Name,		% -RANK%n
		W
	      ) :-
	edge_rank(EId,Rank,_),
	build_rule_rank_name(Rank,Name),
	Default is -5*Rank,
	rule_weight(Name,W,Default).

:-light_tabular build_rule_rank_name/2.
:-mode(build_rule_rank_name/2,+(+,-)).

build_rule_rank_name(Rank,Name) :-
	name_builder('-RANK~w',[Rank],Name)
	.

%% Favor 'est' as verb or aux
edge_cost_elem_tcat(TCat,
		edge{ target => node{ cat => TCat::cat[v,aux],
				      form => 'est'
				      }
		    },
		Name::'+est/v_or_aux',
		W ) :- rule_weight(Name,W,1000).


rule_no_cost('+RESTR_PP_V').

%% Use of restriction database: PP attachment on verbs
edge_cost_elem_lc(Label,v,TCat1,
		edge{ source => node{ lemma => Source, cat => v },
		      target => VMod::node{ cat => TCat1::cat['S','VMod'] },
		      label => Label::label[vmod,'S','S2'],
		      type => adj
		    },
		  %%		Name::'+RESTR_PP_V',
		  Name,
		  W ) :-
	%%	rule_weight(Name,W,300),
	recorded(opt(restrictions,DB)),
	%% CALL EDGE
	( chain( VMod >> edge{}
	       >> node{ cat => prep, lemma => Prep } >> subst
	       >> T::node{ lemma => Target, cat => TCat::cat[nc,np,v,adj] }
	       )
	xor fail
	),
	(
	 check_restriction(Source,v,Target,TCat,Prep,_W) ->
	  %% only count once, even if several possible Target
	  %% should try to find the best target
%%	  _W1 is _W + 40 % should try to partially compensate -LONG* and -RANK
	 _W1 = _W
	;
	 _W1 = 0
	),
	( chain( T >> adj >> node{ cat => coo }
	       >> (subst @ coord3 ) >> node{ lemma => Target3, cat => TCat3 }
	       ),
	  check_restriction(Source,v,Target3,TCat3,Prep,_W3) ->
	  XW is _W1 + _W3
	;
	  XW = _W1
	),
	XW \== 0,
	W is XW / 2,
	(
	 name_builder('+RESTR_PP_V_~w_~w_~w',[Source,Prep,Target],Name)
	;
	 name_builder('+RESTR_PP_V_~w_~w',[Source,Prep],Name)
	)
	.

rule_no_cost('+RESTR_PP_V_prel').

edge_cost_elem_label_type('N2',adj,
		edge{ source => node{ lemma => Target, cat => TCat::cat[nc,np,adj] },
		      target => N::node{},
		      type => adj,
		      label => 'N2'
		    },
		Name::'+RESTR_PP_V_prel',
		W) :-
	%%	rule_weight(Name,W,300),
	recorded(opt(restrictions,DB)),
	chain( N >> (subst @ 'SRel') >> node{ cat => v, lemma => Source }
	     >> ( adj @ 'S' ) >> S::node{}
	     >> (lexical @ prel ) >> node{ cat => prel }
	     ),
	chain( S >> (lexical @ prep) >> node{ cat => prep, lemma => Prep } ),
	check_restriction(Source,v,Target,TCat,Prep,W)
	.

rule_no_cost('+RESTR_ARG').

edge_cost_elem_scat(v,
		edge{ source => node{ lemma => Source, cat => v },
		      target => N::node{ lemma => Target, cat => TCat },
		      type => subst,
		      label => Label
		    },
%		Name::'+RESTR_ARG',
		    Name,
		W
	      ) :-
	recorded(opt(restrictions,DB)),
	( Label = subject ->
	  check_restriction(Source,v,Target,TCat,sujet,W),
	  Name = '+RESTR_ARG_subject'
	; Label = object ->
	  check_restriction(Source,v,Target,TCat,cod,W),
	  Name = '+RESTR_ARG_object'
	; Label = xcomp ->
	  check_restriction(Source,v,Target,TCat,cod,W),
	  Name = '+RESTR_ARG_xcomp'
	; Label = preparg ->
	  mutable(MW,0,true),
	  mutable(WL,none,true),
	  every((
		 chain( N >> (subst @ 'N2') >> node{ lemma => _Target2,
						     cat => TCat2 }
		      ),
		 check_restriction(Source,v,_Target2,TCat2,Target,_W),
		 _W > 0,
		 mutable_max(MW,_W),
		 mutable(WL,_Target2)
		)),
	  mutable_read(MW,W),
	  W > 0,
	  mutable_read(WL,Target2),
	  name_builder('+RESTR_ARG_preparg_~w_~w_~w',[Source,Target,Target2],Name)
	;
	  fail
	)
	.

rule_no_cost('+RESTR_ARG_prel').

edge_cost_elem_label_type('N2',adj,
		edge{ source => node{ lemma => Target, cat => TCat::cat[nc,np,adj] },
		      target => N::node{},
		      type => adj,
		      label => 'N2'
		    },
		Name::'+RESTR_ARG_prel',
		W
	      ) :-
	recorded(opt(restrictions,DB)),
	chain( N >> (subst @ 'SRel') >> V::node{ cat => v, lemma => Source }
	     >> (lexical @ Label ) >> node{}
	     ),
	( Label = subject ->
	  check_restriction(Source,v,Target,TCat,sujet,W)
	; Label = object ->
	  check_restriction(Source,v,Target,TCat,cod,W)
	; Label = preparg ->
	  chain( V >> (lexical @ prep) >> node{ lemma => Target2,
						cat => TCat2 }
	       ),
	  check_restriction(Source,v,Target2,TCat2,Target,W)
	;
	  fail
	)
	.

rule_no_cost('+RESTR_ADJ').

edge_cost_elem_tc(adj,SCat,TCat,
		edge{ source => node{ lemma => Source,
				      cat => SCat::cat[nc,np],
				      cluster => cluster{ left => L, right => R }
				    },
		      target => N::node{ lemma => Target, cat => TCat::adj,
					 cluster => cluster{ left => LAdj, right => RAdj }
				       },
		      type => adj
		    },
		Name::'+RESTR_ADJ',
		W
	      ) :-
	recorded(opt(restrictions,DB)),
	( R =<  LAdj ->
	  check_restriction(Source,SCat,Target,TCat,modifieur,W)
	; 
	  check_restriction(Source,SCat,Target,TCat,antemodifieur,W)
	)
	.

rule_no_cost('+RESTR_ADV').

edge_cost_elem_tc(adj,SCat,TCat,
		edge{ source => node{ lemma => Source, cat => SCat::cat[adj,adv,nc,v] },
		      target => N::node{ lemma => Target, cat => TCat::cat[adv,advneg] },
		      type => adj
		    },
		Name::'+RESTR_ADV',
		W
	      ) :-
	recorded(opt(restrictions,DB)),
	check_restriction(Source,SCat,Target,TCat,modifieur,W)
	.

rule_no_cost('+RESTR_TIME').

edge_cost_elem_scat(SCat,
		edge{ source => node{ lemma => Source, cat => SCat::cat[v,aux] },
		      target => N::node{},
		      type => adj
		    },
%		Name::'+RESTR_TIME',
		    Name,
		W
	      ) :-
	chain( N >> (subst @ time_mod) >> node{ lemma => Target, cat => TCat }),
	recorded(opt(restrictions,DB)),
	check_restriction(Source,SCat,Target,TCat,modifieur,W1),
	W is W1*3,
	name_builder('+RESTR_TIME_~w',[Target],Name)
	.

%rule_no_cost('+RESTR_COORD').

edge_cost_elem_cats(Cat1,coo,
		edge{ source => node{ lemma => L1, cat => Cat1::cat[nc,adj,v] },
		      target => Coo::node{ cat => coo },
		      type => adj
		    },
		XName::'+RESTR_COORD',
%		    Name,
		W
	      ) :-
	chain( Coo >> (subst @ coord3) >> node{ lemma => L2, cat => Cat2::cat[nc,adj,v] } ),
%%	recorded(opt(restrictions,DB)),
	( L1=L2 ->
	  W = 100,
	  Name = '+RESTR_COORD'
	;
	  check_sim(L1,Cat1,L2,Cat2,W),
	  name_builder('+RESTR_COORD_~w_~w',[L1,L2],Name)
	)
	.


edge_cost_elem_cats(prep,coo,
		edge{ source => P1::node{ cat => prep },
		      target => Coo::node{ cat => coo },
		      type => adj
		    },
		XName::'+RESTR_COORD_prep',
%		    Name,
		W
	      ) :-
	chain( P1 >> subst >> node{ lemma => L1, cat => Cat1::cat[nc,v] } ),
	chain( Coo >> (subst @ coord3) >> node{ cat => prep } >> subst >> node{ lemma => L2, cat => Cat2::cat[nc,v] } ),
%%	recorded(opt(restrictions,DB)),
	(L1=L2 ->
	 W = 100,
	 Name = '+RESTR_COORD_prep'
	;
	 check_sim(L1,Cat1,L2,Cat2,W),
	 name_builder('+RESTR_COORD_prep_~w_~w',[L1,L2],Name)
	)
	.


%% Favor 'en' for gerundives (participiale)
edge_cost_elem_type(lexical,
		edge{ source => N::node{},
		      target => N2::node{ lemma => Lemma, cat => prep},
		      type => lexical,
		      id=> _EId
		    },
		Name::'+en_as_GERUND',
		W		% need to compensate penalty for empty source
	      ) :-
	domain(Lemma,[en,'tout en']),
	rule_weight(Name,W,2000),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => v },
		    type => subst,
		    id =>EId
		  }
	     ),
	edge2top(EId,OId),
	verbose('Try gerundive ~w\n',[OId]),
	check_op_top_feature(OId,mode,gerundive),
	have_shared_derivs(_EId,EId),
	verbose('Found gerundive ~E\n',[N2]),
	true
	.

/*
edge_cost_elem( 
		edge{ label => 'SubS',
		      id => EId,
		      target => node{ cat => v },
		      source => N::node{ cat => 'S' },
		      type => subst
		    },
		Name::'+GERUNDIV_WITH_SUBJ',
		W
	      ) :-
	rule_weight(Name,W,1000),
	edge{ source => N,
	      label => incise,
	      type => adj
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[gerundive,participle]),
	edge{ source => V::node{ cat => v},
	      target => N,
	      type => adj
	    },
	edge{ source => V,
	      label => subject
	    }
	.
*/


rule_no_cost('+RESTR_PP_N').

%% Use of restriction database: PP attachment on nouns
edge_cost_elem_lc(Label,SCat,prep,
		edge{ source => node{ lemma => Source, cat => SCat::cat[nc,adj] },
		      target => N::node{ cat => prep, lemma => Prep },
		      label => Label::cat['N2',adjP],
		      type => adj,
		      id => EId
		    },
%%		Name::'+RESTR_PP_N',
		  Name,
		W) :-
	%%	rule_weight(Name,W,300),
	recorded(opt(restrictions,DB)),
	( source2edge(
		      edge{ source => N,
			    type => subst,
			    target => T::node{ lemma => Target, cat => TCat::cat[nc,np,v,adj] }
			  }
		     )
	xor fail
	),
	%% CALL EDGE
	( 
	  check_restriction(Source,SCat,Target,TCat,Prep,_W1)
	xor _W1 = 0
	),
	( chain( T >> adj >> node{ cat => coo }
	       >> (subst @ coord3 ) >> node{ lemma => Target3, cat => TCat3 }
	       ),
	  check_restriction(Source,SCat,Target3,TCat3,Prep,_W3) ->
	  W1 is _W1 + _W3
	;
	  W1 = _W1
	),
%%	format('Test rank for ~w source=~w target=~w\n',[EId,Source,Target]),
	edge_rank(EId,Rank,Dir),
	XW is 2*(W1-(Rank-1)*10),
%%	format('Rank is ~w ~w\n',[Rank,Dir]),
	XW > 0,
	W is XW / 2,
	(
	 name_builder('+RESTR_PP_N_~w_~w_~w',[Source,Prep,Target],Name)
	;
	 name_builder('+RESTR_PP_N_~w_~w',[Source,Prep],Name)
	),
	true
	.

edge_cost_elem_lc(vmod,v,'VMod',
		edge{ source => node{ cat => v },
		      target => N::node{ cat => 'VMod'},
		      label => 'vmod',
		      type => adj
		    },
		Name::'+MOD_PP_V',
		W ) :-
	rule_weight(Name,W,400),
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ cat => prep, lemma => Lemma },
		    type => subst,
		    label => 'PP'
		  }
	     ),
	domain(Lemma,[pendant,durant])
	.

%% Penalize PP attachments on adjP, except on attribute position
edge_cost_elem_lc(adjP,adj,prep,
		edge{ source => Adj::node{ cat => adj, cluster => C },
		      target => node{ cat => prep, cluster => CPrep },
		      type => adj,
		      label => 'adjP'
		    },
		Name::'-PP_ON_ADJP',
		W
	      ) :-
	rule_weight(Name,W,-400),
	%% CALL EDGE
	\+ target2edge(
		       edge{ target => Adj,
			     label => comp,
			     type => subst
			   }
		      ),
	\+ targetcluster2edge(
			      edge{ target => node{ cat => v,
						    cluster => C } }
			     ),
%	targetcluster2edge(
			   edge{ source => NC::node{ cat => cat[nc,v], cluster => C1 },
				 target => Prep::node{ cat => prep, cluster => CPrep }
			       },
%			  ),
	\+ chain( Adj >> adj >> node{ cat => adv, cluster => C1 } ),
%	format('penalize PP adj=~w nc=~w prep=~w\n',[Adj,NC,Prep]),
	true
	.

edge_cost_elem_lc(adjP,adj,prep,
		edge{ source => Adj::node{ cat => adj },
		      target => node{ cat => prep },
		      type => adj,
		      label => 'adjP'
		    },
		Name::'-PP_ON_COMP',
		W
	      ) :-
	rule_weight(Name,W,-200),
	%% CALL EDGE
	target2edge(
		     edge{ target => Adj,
			   label => comp,
			   type => subst
			 }
		    )
	.

%% Penalize PP attachments on np and pro
%% but for demonstrative pronouns
edge_cost_elem_lc('N2',SCat,prep,
		edge{ source => node{ cat => SCat::cat[np,pro], lemma => Lemma },
		      target => node{ cat => prep },
		      type => adj,
		      label => 'N2'
		    },
		Name::'-PP_ON_np',
		W
	      ) :-
	rule_weight(Name,W,-400),
	\+ domain(Lemma,[celui])
	.

%% Penalize PP built upon an adj
edge_cost_elem_cats(prep,adj,
		edge{ source => node{ cat =>  prep },
		      target => node{ cat =>  adj }
		    },
		Name::'-PP_UPON_ADJ' ,
		W
	      ) :- rule_weight(Name,W,-500).


%% Penalize PP build upon entitie over dates
edge_cost_elem_label_tcat('N2',prep,
		edge{ source => N::node{ id => NId },
		      target => P::node{ cat => prep },
		      type => adj,
		      label => 'N2'
		    },
		Name::'-PPentity_UPON_date',
		W
	      ) :-
	rule_weight(Name,[W1,W2],[-300,-200]),
	chain( P >> subst >> T::node{ cat => TCat, lemma => TLemma, id => TId } ),
	node2op(NId,OId),
	check_op_top_feature(OId,time,true_time[]),
	((TCat = cat[v,adj] ; TLemma = entities[]) ->
	 W = W1
	; TCat=nc ->
	 node2op(TId,TOId),
	 ( check_op_top_feature(TOId,time,TTime) ->
	   TTime = (-)
	 ;
	   true
	 ),
	 W=W2
	;
	 W=W2
	)
	.

edge_cost_elem_tlemma(�,
		edge{ source => N::node{ id => NId },
		      target => P::node{ cat => prep, lemma => � },
		      type => adj,
		      label => 'N2'
		    },
		Name::'+date_a_date',
		W
	      ) :-
	rule_weight(Name,W,200),
	node2op(NId,OId),
	check_op_top_feature(OId,time,true_time[]),
	chain( P >> subst >> node{ cat => nc, id => TId } ),
	node2op(TId,TOId),
	check_op_top_feature(TOId,time,true_time[])
	.

%% Strongly penalize nc/adj/... over det (for words such as un, le, la, une , ...)
edge_cost_elem_tcat(TCat,
		edge{ target => node{ cat => TCat::cat[nc,pro,cla,adj],
				      lemma => L,
				      cluster => C } },
		Name::'-N2_VS_DET',
		W
	      ) :-
	rule_weight(Name,W,-2000),
	%% CALL EDGE
	\+ domain(L,[tout]),
	targetcluster2edge(
			   edge{ target => node{ cat => cat[det], cluster => C } }
			  )
	.

%% Strongly penalize adv/... over prep (for words such as avec, ...)
edge_cost_elem_tcat(adv,
		edge{ target => node{ cat => cat[adv], cluster => C } },
		Name::'-ADV_VS_PREP',
		W
	      ) :-
	rule_weight(Name,W,-3000),
	%% CALL EDGE
	targetcluster2edge( edge{ target => node{ cat => cat[prep], cluster => C }} )
	.

%% Strongly penalize adv/... over csu (for words such as si, ...)
edge_cost_elem_tcat(adv,
		edge{ target => node{ cat => cat[adv], cluster => C } },
		Name::'-ADV_VS_CSU',
		W
	      ) :-
	rule_weight(Name,W,-500),
	%% CALL EDGE
	targetcluster2edge( edge{ target => node{ cat => cat[csu], cluster => C }} )
	.


%% Favor comme-csu on N2 and Adj
edge_cost_elem_tcat(csu,
		edge{ source => node{ cat => xnominal[] },
		      target => node{ cat => csu },
		      type => adj
		    },
		Name::'+comme_csu',
		W ) :-
	rule_weight(Name,W,500)
	.

%% Favor csu attached on tensed verbs
edge_cost_elem_tcat(csu,
		edge{ source => node{ id => NId, cat => cat[v,adj] },
		      target => node{ cat => csu },
		      type => adj
		    },
		Name::'csu_on_tense_verb',
		W ) :-
	rule_weight(Name,W,100),
	\+ ( check_node_top_feature(NId,mode,Mode),
	     domain(Mode,[infinitive,gerundive,participle])
	   )
	.

%% Favor adv on v after rather than on preceding aux,v when in the middle (but for negation)
edge_cost_elem_cats(v,adv,
		edge{ source => V::node{ cat => v,
				      cluster => cluster{ right => R1 }
				    },
		      target => node{ cat => adv,
				      cluster => cluster{ left => L, right => R }
				    }
		    },
		Name::'+ADV_ON_PREC_V',
		W ) :-
	rule_weight(Name,W,200),
	R =< R1,
	chain( V >> (adj @ 'Infl') >> node{ cat => aux,
					    cluster => cluster{ right => R_Aux}
					  }
	     ),
	R_Aux =< L
	.


%% But favor advneg over adv (and also over comp) when possible
%% if a negation is possible
edge_cost_elem_tcat(advneg,
		edge{ source => V::node{ id => NId, cat => cat[v,aux], cluster => cluster{ right => V_Right } },
		      target => node{ cat => advneg, lemma => Lemma, cluster => cluster{ left => Adv_Left} }
		    },
		Name::'+AdvNeg',
		W
	      ) :-
	rule_weight(Name,[W1,W2],[2100,2000]),
	node2op(NId,OId),
	(   chain( V >> lexical >> node{ cat => clneg }) ->
	    W = W1
	;   check_op_top_feature(OId,neg,+),
	    W = W2
	;   V_Right =< Adv_Left, domain(Lemma,[pas,point]) ->
	    W = W1
	)
	.

:-std_prolog has_post_cl_subject/1.

has_post_cl_subject(N::node{ cat => cat[v,aux]}) :-
	( source2edge(
		      edge{ source => N,
			    target => Modal::node{ cat => cat[v] },
			    type => adj,
			    label => 'V'
			  }
		     )->
	  ( source2edge(
			edge{ source => Modal,
			      target => node{ cat => cln },
			      label => subject
			    }
		       )
	  xor has_post_cl_subject(Modal)
	  )
	; source2edge(
		      edge{ source => N,
			    target => Aux::node{ cat => cat[v,aux] },
			    type => adj,
			    label => 'Infl'
			  }
		     ) ->
	  ( source2edge(
			edge{ source => Aux,
			      target => node{ cat => cln },
			      label => subject
			    }
		       )
	  xor has_post_cl_subject(Aux)
	  )
	;
	  fail
	)
	.

%% Slightly penalize subject when post-clitic subject
edge_cost_elem_label_scat(subject,SCat,
		edge{ label => subject,
		      source => V::node{ cat => SCat::cat[v,aux] }
		    },
		Name::'-double_subj',
		W
	      ) :-
	rule_weight(Name,W,-150),
	has_post_cl_subject(V)
	.

%% Penalize inverted subjects (especially for robust mode)
%% when there exists some way to have a canonical subj
edge_cost_elem_label(subject,
		edge{ label => subject,
		      source => V::node{ cat => cat[~ [adj,aux]],
					 cluster => cluster{ right => R } },
		      target => Subj::node{ cat => cat[~ cln],
					    cluster => cluster{ left => L } }
		    },
		Name::'-INVERTED_SUBJ',
		W
	      ) :-
	rule_weight(Name,W,-1000),
	\+ chain( V >> (lexical @ impsubj) >> node{}),
	once(( chain( V >> subject >> node{ cluster => cluster{ left => _L } } ),
	       _L =< R
	     ;
	       chain( V >> subst @ object >> Subj )
	     )),
	
	R =< L
	.

%% Favor inverted_subj for adj and for impersonal constructions
edge_cost_elem_label_type(subject,subst,
		edge{ label => subject,
		      source => Adj::node{ cat => adj },
		      target => node{ cat => cat[~cln] },
		      type => subst
		    },
		Name::'-INVSUBJ_ON_ADJ',
		W
	      ) :-
	rule_weight(Name,W,1500),
	chain( Adj >> (lexical @ impsubj) >> node{ cat => cat[ilimp,caimp] }),
	true
	.

%% But compensate inversion when topic xcomp
%% furthemore, the xcomp head and the main verb may be far away
edge_cost_elem_label_type(xcomp,subst,
		Edge::edge{ label => xcomp,
			    type => subst,
			    source => V::node{ lemma => Lemma, cluster => cluster{ left => L } },
			    target => Topic::node{ cat => Topic_Cat, cluster => cluster{ right => R }}
			  },
		Name::'+XCOMP_TOPIC',
		W
	      ) :-
	\+ domain(Lemma,[faire,�tre]), %some verb have xcomp function but are not citation verbs 
	rule_weight(Name,W1,+2000),
	R =< L,
	%% CALL EDGE
	\+ ( chain( V >> adj
		  >> node{ cat => incise } >> (lexical @ void)
		  >> node{ lemma => ',',
			   cluster => cluster{ left => L_Incise }
			 }
		  ),
	     L_Incise < R
	   ),
%%	edge_cost_elem('-LONG',Edge,W2),
%%	W is  W1 - W2,
	%% favor real topic sentence rather than short sentences
	( Topic_Cat = cat[v] ->
	    W2 = 200
	; Topic_Cat = cat[adj],
	    chain(Topic >> (subst @ subject) >> node{}) ->
	    W2 = 200
	; W2 = 0
	),
	W is W1+W2
	.

%% But compensate inversion when causative
edge_cost_elem_label_type(subject,subst,
		edge{ label => subject,
		      type => subst,
		      source => V::node{ cat => v, lemma => Lemma },
		      id => EId1
		    },
		Name::'+CAUSATIVE',
		W
	      ) :-
	\+ Lemma = faire,
	Edge::edge{ label => causative_prep,
		    type => lexical,
		    source => V,
		    target => node{ cat => prep, cluster => C },
		    id => EId2
		  },
	\+ edge{ source => V, target => node{ cluster => C }, label => preparg, type => subst },
	%% check that V has a faire aux (seems to be wrong in some case !!)
	 ( chain( V << subst @ xcomp << node{ lemma => faire })
	 ; chain( V >> adj @ 'S' >> node{ lemma => faire } )
	 ),
	have_shared_derivs(EId1,EId2),
	rule_weight(Name,W,+1100)
	.

%% Favor existence of an object if there is a causative_prep
edge_cost_elem_label(object,
		edge{ label => object,
		      source => V,
		      id => EId1
		    },
		Name::'+CAUSATIVE_OBJ',
		W
	      ) :-
	%% CALL EDGE
	source2edge(
	      edge{ label => causative_prep,
		    type => lexical,
		    source => V::node{ cat => v,
				       lemma => Lemma },
		    target => node{ cat => prep, cluster => C },
		    id => EId2
		  }
	     ),
	\+ Lemma = faire,
	rule_weight(Name,W,+4000),
	\+ edge{ source => V, target => node{ cluster => C }, label => preparg, type => subst },
	%% check that V has a faire aux (seems to be wrong in some case !!)
	( chain( V << subst @ xcomp << node{ lemma => faire })
	; chain( V >> adj @ 'S' >> node{ lemma => faire } )
	),
	have_shared_derivs(EId1,EId2)
	.


%% Pb in FRMG: a causative may arise, even if 'faire' is not present
%% until the pb is fixed, we use a very strong penalty !
edge_cost_elem_label_type(causative_prep,lexical,
		edge{ label => causative_prep,
		      type => lexical,
		      source => V::node{ cat => v }
		    },
		Name::'-wrong_causative',
		W
	      ) :-
	rule_weight(Name,W,-10000),
	chain( V << subst @ xcomp << node{ lemma => Lemma } ),
	Lemma \== faire
	.

%% penalize non-saturated objects (possible with il y a)
edge_cost_elem_label_tcat(object,TCat,
		edge{ label => object,
		      target => N::node{ cat => TCat::cat[nc,adj] },
		      type => subst
		    },
		Name::'-unsat_obj',
		W
	      ) :-
	rule_weight(Name,W,-500),
	\+ chain( N >> ( subst @ det) >> node{ cat => det } )
	.

%/*
%% Penalties on object in presence of a clitic object or genitive,
%% specially if introduced by de
%% not sur it is useful !
edge_cost_elem_label_tcat(object,TCat,
		edge{ label => object,
		      source => V::node{},
		      target => Obj::node{ cat => TCat::cat[nc,np] },
		      type => subst
		    },
		Name::'-obj_plus_clitic',
		W
	      ) :-
	rule_weight(Name,W1,-200),
	chain( V >> (lexical @ label[object,clg]) >> node{} ),
	( chain( Obj >> (subst @ det) >> node{ cat => det, form => de_form[] } ) ->
	  W2 = -500
	;
	  W2 = 0
	),
	W is W1 + W2
	.
%*/

/*
%% Don't know exactly the role of next two rules !
%% Strongly penalize que? in N2
edge_cost_elem( 
		edge{ target => node{ lemma => 'que?', xcat => 'N2' } },
		Name::'-que?inN2',
		W
	      ) :- rule_weight(Name,W,-10000).

%% Penalize wh in N2 when in concurrence whith non subject wh-pronoun
edge_cost_elem( 
		edge{ label=> subject,
		      target => node{ cat => pri, xcat => 'N2' } },
		Name::'-whinN2Subj',
		W
	      ) :- rule_weight(Name,W,-2000).
*/

%% favor adjective reading for attributes over noun reading
edge_cost_elem_lc(comp,v,adj,
		edge{ label => comp,
		      source => node{ cat => v },
		      target => node{ cat => adj },
		      type => subst
		    },
		Name::'+adjAsComp',
		W
	      ) :- rule_weight(Name,W,400).

%% favor Adj on Nouns rather than comp
edge_cost_elem_label_type(comp,subst,
		edge{ target => node{ cat => cat[comp,adj],
				      cluster => C2
				    },
		      label => comp,
		      type => subst
		    },
		Name::'-adjAsComp1',
		W
	      ) :-
	rule_weight(Name,W,-1400),
	%% CALL EDGE
	targetcluster2edge(
			   edge{ source => node{ cat => nominal[~ [np]] },
				 target => node{ cat => adj, cluster => C2},
				 type => adj,
				 label => 'N2'
			       }
			  ),
	true
	.

%% favor Adj on Nouns rather than comp
edge_cost_elem_label_type(comp,subst,
		edge{ target => node{ cat => cat[comp,adj],
				      cluster => C1
				    },
		      label => comp,
		      type => subst
		    },
		Name::'-adjAsComp2',
		W
	      ) :-
	rule_weight(Name,W,-1400),
	%% CALL EDGE
%	sourcecluster2edge(
			   edge{ source => node{ cat => nominal[],
						 cluster => C1
					       },
				 target => node{ cat => adj,
						 cluster => C2},
				 type => adj,
				 label => 'N'
			       },
%			  ),
	true
	.

%% slightly favor post adjectives
%% but strongly favor if superlative (to avoid confusion with a separate noun
edge_cost_elem_lc('N2',SCat,adj,
		edge{ source => node{ cat =>SCat::nominal[nc,np] },
		      target => Adj::node{ cat => adj },
		      type => adj,
		      label => 'N2'
		    },
		Name::'+post_adj',
		W
	      ) :-
	rule_weight(Name,[W1,W2],[100,500]),
	( chain( Adj >> (adj @ adj) >> node{ cat => adv } >> (subst @ det) >> node{ cat => det }) ->
	  W = W2
	;
	  W = W1
	)
	.

rule_no_cost('+CatPref').

%% Use preferences
edge_cost_elem( %none, 
		edge{ target => node{ form => F,
				      cat => _C,
				      lemma => _L
				    }},
		Name::'+CatPref',
		W
	      ) :-
	verbose('Test catpref F=~w C=~w L=~w\n',[F,_C,_L]),
	%% avoid catpref on closed cats and function words
%%	\+ domain(_C,[det,prep,csu,coo]),
	( _C = cat[ilimp,caimp] ->
	    C = cln,
	    L = cln
	;   _C = C,
	    _L = L
	),
	check_catpref(F,L,C,W)
	.

%% Favor quote constructions
%% should give 1000, because used twice, one for each quote
%% Quote is inversely proportional to the length of the quoted part
%% but should not include subquotes of same kind
edge_cost_elem_label(void,
		edge{ source => N1::node{ tree => Tree },
		      target => node{ cluster => C1::cluster{ left => Left }},
		      type => lexical,
		      label => void
		    },
		Name::'+Quoted',
		W
	      ) :-
%	fail,
	rule_weight(Name,W,900),
%	rule_weight(Name,W,1500),
	domain(quoted[],Tree)
	.

%% Penalize spelling correction done by SxPipe
edge_cost_elem( %none,
		edge{ target => N::node{ form => Form,
					 cluster => cluster{ token => Token }
				       }
		    },
		Name::'-Spelling',
		W
	      ) :-
	\+ (Form = entities[]),
	\+ (Form = date[]),
	\+ part_of_agglutinate(Form,Token),
	\+ domain(Form,['l''un','l''une']),
	Form  \== '_EPSILON',
	Form \== Token,
%%	format('Spelling form=~w token=~w node=~w\n',[Form,Token,N]),
	rule_weight(Name,W,-500)
	.
							 
%% Favour _PERSON , _ORGANIZATION in subject or object position
edge_cost_elem_tlemma(Lemma,
		edge{ target => node{ lemma => Lemma::entities['_PERSON',
							       '_PERSON_m',
							       '_PERSON_f',
							       '_ORGANIZATION',
							       '_COMPANY'
							      ]
				    },
		      type => subst,
		      label => label[subject]
		    },
		Name::'+np_as_subj',
		W
	      ) :- rule_weight(Name,W1,20),
	( Lemma = entities['_ORGANIZATION','_COMPANY'] ->
	  W is W1 + 5
	;
	  W = W1
	).

edge_cost_elem_tlemma(Lemma,
		edge{ target => node{ lemma => Lemma::entities['_PERSON',
							       '_PERSON_m',
							       '_PERSON_f',
							       '_ORGANIZATION',
							       '_PRODUCT',
							       '_LOCATION',
							       '_COMPANY'
							      ]
				    },
		      type => subst,
		      label => label[object]
		    },
		Name::'+np_as_obj',
		W
	      ) :- rule_weight(Name,W1,20),
	( Lemma = entities['_ORGANIZATION','_COMPANY','_PRODUCT'] ->
	  W is W1 + 5
	;
	  W = W1
	).



edge_cost_elem_scat(prep, 
		E::edge{ target => node{ form => Entity },
			 type => subst,
			 source => node{ cat => prep,
					 lemma => Prep
				       },
			 label => 'N2'
		       },
		Name::'+location_in_prep',
		W
	      ) :-
%%	format('edge ~q\n',[E]),
	rule_weight(Name,W,100),
	prep_entity(Prep,Entity),
	true
	.

%% favorize prep attachement given a preference
edge_cost_elem_tcat(prep,
		edge{ target => node{ lemma => Prep, cat => prep },
		      source => node{ lemma => Governor,
				      cat => Cat::cat[]
				    },
		      type => adj
		    },
		Name::'+prep_pref',
		W
	      ) :-
	prep_pref(Governor,Cat,Prep,W).

%% favorize prep attachement given a preference
edge_cost_elem_tcat(prep,
		edge{ target => N::node{ lemma => Prep, cat => prep },
		      source => node{ lemma => Governor,
				      cat => Cat::cat[]
				    },
		      type => adj
		    },
		Name::'+prep_pref_full',
		W
	      ) :-
	%% CALL EDGE
	source2edge(
	      edge{ source => N,
		    target => node{ lemma => Governee },
		    type => subst
		  }
	     ),
	rpref(Governor,Prep,Governee,W).

%% penalize attachement on non-head on "compond form" with rpref
edge_cost_elem_type(adj,
		edge{ source => Mod1,
		      target => Mod,
		      type => adj
		    },
		Name::'+prep_pref_head',
		W
	      ) :-
	chain( Mod::node{} << adj
	     << node{ lemma => Governor } >> edge_kind[adj,subst]
	     >> node{ cat => prep, lemma => Prep } >> subst
	     >> Mod1::node{ lemma => Governee }
	     ),
	rpref(Governor,Prep,Governee,W1),
	W is - W1
	.


%% Penalize short sentences
edge_cost_elem_label(start,
		E::edge{ target => node{ cat => start },
			 source => node{ cat => 'S' },
			 label => start,
			 type => subst
		       },
		Name::'-start',
		W
	      ) :-
	rule_weight(Name,W,-1000)
	.

%% but still prefer short nominal sentences over other kinds
%% specially if there is some PP attachement
edge_cost_elem_lc('N2',comp,TCat,
	       edge{ target => N2::node{ cat => TCat::cat[nc,np] },
		     source => N1::node{ cat => comp },
		     label => 'N2',
		     type => subst
		   },
	       Name::'+short_N2',
	       W
	      ) :-
	chain( N1 << ( subst @ comp )
	     << node{ cat => 'S' } >> ( subst @ start )
	     >> node{ cat => start }
	     ),
	rule_weight(Name,W1,100),
	(   source2edge(
			edge{ source => N2,
			      target => node{ cat => prep },
			      type => adj
			    }
		       ) ->
	    W is 2*W1
	;   
	    W = W1
	)
	.

%% penalize vmod on start sentences
edge_cost_elem_label_type(Label,adj,
		edge{ source => S::node{ cat => 'S' },
		      label => Label::label[vmod,'S'],
		      type => adj
		    },
		Name::'-vmod_on_short_sentence',
		W
	      ) :-
	rule_weight(Name,W,-600),
	chain( S >> subst>> node{ cat => start } ),
	true
	.

%% Penalize incise after que without coma
edge_cost_elem_slemma(SLemma,
		edge{ source => node{ lemma => SLemma::quepro[] },
		      target => node{ cat => 'N2' },
		      label => 'N2',
		      type => adj
		     },
	       Name::'-incise_after_que',
	       W
	      ) :-
	rule_weight(Name,W,-1000)
	.

%% Penalize wh-comp args starting with que
edge_cost_elem_tlemma(TLemma,
		edge{ source => V::node{ cat => v,
					 cluster => cluster{ left => L }
				       },
		      target => node{ cat => pri,
				      lemma => TLemma::quepro[],
				      cluster => cluster{ right => R }
				    },
		      type => subst,
		      label => label[object,comp]
		    },
		Name::'-que_whcomp',
		W
	      ) :-
	rule_weight(Name,W,-2000),
	R =< L,
	edge{ target => V,
	      label => xcomp,
	      type => subst
	    }
	.

%% Penalize 'que' as whpro in robust partial parses
edge_cost_elem_tlemma(TLemma,
		edge{ source => node{ cat => comp },
		      target => node{ lemma => TLemma::quepro,
				      cluster => cluster{ left => L }},
		      type => subst,
		      label => 'N2'
		    },
		Name::'-que_whpro_robust',
		W
	      ) :-
	L > 0,
	rule_weight(Name,W,-3000)
	.

%% favor que as que_restr in negative sentences
edge_cost_elem_lc(advneg,v,que_restr,
		edge{ source => V::node{ cat => cat[v] },
		      target => node{ cat => que_restr },
		      label => advneg,
		      type => lexical
		    },
		Name::'+que_restr',
		W
	      ) :-
	rule_weight(Name,W,2200),
	chain( V >> (lexical @ clneg) >> node{ cat => clneg } ),
	\+ chain( V >> _ >> node{ cat => advneg } )
	.

%% Strongly penalize pronoun in enum
edge_cost_elem_lc(coord,'N2',pro,
		edge{ source => N2::node{ cat => 'N2' },
		      target => node{ cat => pro },
		      type => subst,
		      label => coord
		    },
		Name::'-pro_in_enum',
		W
	      ) :-
	target2edge(
		    edge{ source => node{ cat => Cat },
			  target => N2,
			  type => adj,
			  label => 'N2'
			}
		   ),
	Cat \== pro,
	rule_weight(Name,W,-3000).

%% Favour superlative construction with possible
edge_cost_elem_tlemma(possible,
		edge{ type => lexical,
		      label => 'Modifier',
		      source => node{ cat => supermod },
		      target => node{ lemma => possible, cat => adj }
		    },
		Name::'+superlative_possible',
		W
	      ) :-
	rule_weight(Name,W,2000).

%% Favor constructions with  supermod
edge_cost_elem_label_type(supermod,adj,
		edge{ label => supermod,
		      type => adj
		      },
		Name::'+superlative',
		W
	      ) :-
	rule_weight(Name,W,2000).


%% Favor constructions with  supermod
edge_cost_elem_label_type(quantity,adj,
		edge{ label => quantity,
		      type => adj,
		      target => N::node{ cat => nominal[] }
		      },
		Name::'+quantity_on_superlative',
		W
	      ) :-
	rule_weight(Name,W,2000),
	chain( N >> (subst @ det) >> node{ cat => det, lemma => Lemma } ),
	domain(Lemma,['_NUMBER',un,une])
	.

%% Favor antepos adj when specified
edge_cost_elem_lc(Label,SCat,TCat,
		edge{ label => Label::label['N',ncpred],
		      source => N::node{ cat => SCat::cat[nc,np,ncpred],
					 cluster => cluster{ left => L }
				       },
		      target => Adj::node{ cat => TCat::adj,
					   lemma => Lemma,
					   cluster => cluster{ right => R }
					 },
		      type => adj
		    },
		Name::'+ante_adj',
		W
	      ) :-
	R =< L,
	ante_adj_pref(Lemma,W)
	.


%% Penalize postpos adj when antepos is specified
%% except if the adj is modified
edge_cost_elem_lc('N2',SCat,TCat,
		edge{ label => 'N2',
		      source => N::node{ cat => SCat::cat[nc,np],
					 cluster => cluster{ right => R }
				       },
		      target => Adj::node{ cat => TCat::adj,
					   lemma => Lemma,
					   cluster => cluster{ left => L }
					 },
		      type => adj
		    },
		Name::'-ante_adj_used_as_post',
		W
	      ) :-
	R =< L,
	ante_adj_pref(Lemma,W1),
	\+ source2edge( edge{ source => Adj, type => adj}),
	W is -W1
	.

%% Penalize gender_alternative constructions
edge_cost_elem_type(adj,
		edge{ type => adj,
		      target => node{ tree => Tree }
		    },
		Name::'-gender_alternative',
		W
	      ) :-
	rule_weight(Name,W,-1000),
	domain(K,[gender_alternative_pri,
		  gender_alternative_pro,
		  gender_alternative_cln
		 ]),
	domain(K,Tree)
	.


%% Favour date number on year (should also try to cover things like "les ann�es Mitterand")
edge_cost_elem_slemma(ann�e,
		edge{ type => lexical,
		      label => 'Nc2',
		      source => node{ lemma => ann�e },
		      target => node{ lemma => '_NUMBER' }		      
		    },
		Name::'+number_on_year',
		W
	      ) :- rule_weight(Name,W,100)
	.


%% Favour modifier factorization when coordinnation
%% but avoid too much climbing to find a coordination
edge_cost_elem_cats(SCat,TCat,
		edge{ source => N1::node{ cat => SCat::cat[nc,adj] },
		      target => N2::node{ cat => TCat::cat[prep,adj],
					  cluster => cluster{ left => N2_Left }},
		      type => adj
		    },
		Name::'+mod_fact_coord',
		W
	      ) :-
	source2edge(
		    edge{ source => N1,
			  target => COO::node{ cat => coo,
					       lemma => COO_Lemma,
					       cluster => cluster{ right => COO_Right,
								   left => COO_Left
								 }
					     },
			  type => adj
			}
		   ),
	domain(COO_Lemma,[et,ou]),
	source2edge(
		    edge{ source => COO,
			  target => Last::node{ cluster => cluster{ right => Last_Right
								  }
					      },
			  label => coord3
			}
		   ),
	COO_Right < N2_Left,
	\+ ( source2edge(
			 edge{ source => N1,
			       target => N3::node{ cat => cat[prep,adj],
						   cluster => cluster{ right => N3_Right }
						 },
			       type => adj
			     }
			),
	     N3_Right =< COO_Left
	   ),
	\+ ( source2edge(
			 edge{ source => Last,
			       target => LastMod::node{ cat => cat[prep,adj],
							cluster => cluster{ left => LastMod_Left,
									    right => LastMod_Right
									  }
						      },
			       type => adj
			     }
			),
	     Last_Right =< LastMod_Left,
	     LastMod_Right =< N2_Left
	   ),
	rule_weight(Name,W,50)
	.
		      
edge_cost_elem_cats(SCat,TCat,
		edge{ source => node{ cat => SCat::cat[prep,csu],
				      lemma => SLemma,
				      cluster => cluster{ left => Left } },
		     target => N::node{ cat => TCat::nominal[],
					cluster => cluster{ right => Right } },
		     type => adj
		   },
	       Name::'-quantity_as_prep_mod',
	       W
	      ) :-
	rule_weight(Name,W,-3000),
	chain( N >> (subst @ det) >> node{ cat => det, lemma => Lemma } ),
	\+ ( domain(Lemma,['_NUMBER',un,une]),
	       domain(SLemma,[avant,apr�s,devant,derri�re,'au-del�','au-dessus','au-dessous','au-devant',
			      dessus,dessous,
			      'avant que','apr�s que'
			     ])
	   )
	.

:-extensional prep_entity/2.

prep_entity(avec,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY','_PRODUCT']).
prep_entity(dans,entities['_LOCATION','_ORGANIZATION','_COMPANY']).
prep_entity(contre,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(vers,entities['_PERSON','_PERSON_m','_PERSON_f','_LOCATION']).
prep_entity(pour,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(dans,entities['_LOCATION','_ORGANIZATION','_COMPANY']).
prep_entity('jusqu''�',entities['_LOCATION']).
prep_entity(chez,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(contre,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).
prep_entity(comme,entities['_PERSON','_PERSON_m','_PERSON_f','_ORGANIZATION','_COMPANY']).

/*
%% Favor dependencies whose source and targets are in the same EASy F
edge_cost_elem( 
		edge{ source => node{ cluster => cluster{ lex => Lex }},
		      target => node{ cluster => cluster{ lex => Lex }}
		    },
		'+Dep_In_Same_F',
		1000
	      ) :-
	label2lex(Lex,_,[_|_])
	.
*/


edge_cost_elem_label_tcat(object,prep,
	       edge{ type => lexical,
		     label => object,
		     target => node{ cat => prep, lemma => � }
		   },
	       Name::'-�_as_obj',
	       W
	      ) :-
	rule_weight(Name,W,-2000)
	.

%% penalize partitives on numbers
edge_cost_elem_lc(prep,SCat,prep,
	       edge{ label => prep,
		     type => lexical,
		     source => node{ cat => SCat::cat[pro,number],
				     lemma => '_NUMBER'
				   },
		     target => node{ cat => prep, lemma => de }
		   },
	       Name::'-partitive_on_number',
	       W
	      ) :- rule_weight(Name,W,-3000)
	.

%% penalize extra ponctuation
edge_cost_elem_label(Label::label['ExtraWPunct','ExtraSPunct'],
		     edge{ label => Label, target => node{ cat => cat[poncts,ponctw] } },
		     Name::'-extra_punct',
		     W
		    ) :- rule_weight(Name,W,-400).

%% penalize post adv on adj
edge_cost_elem_cats(SCat,adv,
	       edge{ type => adj,
		     source => node{ cat => SCat::cat[nc,adj], cluster => cluster{ right => Right }},
		     target => node{ cat => adv, cluster => cluster{ left => Left }}
		   },
	       Name::'-post_adv_on_adj',
	       W
	      ) :-
	Right =< Left,
	rule_weight(Name,W,-100)
	.
	      
%% penalize vmod adj on comparative adv without supermod adj
%% should be blocked by FRMG
edge_cost_elem_label_scat(vmod,adv,
		edge{ type => adj,
		      label => vmod,
		      source => Adv::node{ cat => adv }
		    },
		Name::'-bad_adv_vmod',
		W
	      ) :-
	rule_weight(Name,W,-5000),
	\+ chain( Adv >> adv @ supermod >> node{} ) 
	.

%Favor deep modifier extractions in xcomp args
edge_cost_elem_lc( 'S',v,v,
		   edge{ type => adj,
			 source => V::node{ cat => v, cluster => C::cluster{} },
			 target => V1::node{ cat => v, cluster => C1::cluster{} },
			 label => 'S'
		      },
		   Name::'+mod_extraction_in_xcomp',
		   W
		 ) :-
	rule_weight(Name,W,1000),
	chain( V1alt::node{ cat => v, cluster => C1 } >> subst @ xcomp >> node{ cat => v, cluster => C }),
	chain( V1alt >> adj >> Mod::node{} << adj << V )
	.

%Penalize CleftQue when xcomp arg is possible
% example: c'est cette pomme que Paul veut que Pierre mange.
% we may need to compensate for an object in (que Paul veut)
% edge_cost_elem_label(Label,
% 		     edge{ type => lexical,
% 			   source => V::node{ cat => cat[v,aux,adj] },
% 			   target => node{ cat => prel, cluster => C::cluster{}},
% 			   label => Label::'CleftQue'
% 			 },
% 		     Name::'-cleft_vs_xcomp',
% 		     W
% 		    ) :-
% 	rule_weight(Name,W,-1700), 
% 	chain( V >> adj @ 'S' >> node{ lemma => XLemma, cat => cat[v,adj]} >> lexical @ csu >> node{ cat => que, cluster => C } ),
% 	\+ XLemma = �tre
% 	.

%% penalize que_prel vs que_csu
edge_cost_elem(
	       edge{ type => lexical,
		     source => V::node{ cat => cat[v,aux,adj], cluster => C::cluster{} },
		     target => node{ cat => prel },
		     label => object
		   },
	       Name::'-ante_que_prel_vs_post_que_csu',
	       W
	      ) :-
	rule_weight(Name,W,-1700),
	chain( node{ cluster => C } >> lexical @ csu >> node{ cat => que } )
	.
		   
%% END EDGE COST

:-xcompiler
edge_in_children(NId,E,Children) :-
%%	source2edge(NId,E::edge{ id => EId }),
	domain(dinfo(_,EId,_,_),Children),
	E::edge{ id => EId }
	.

:-rec_prolog robust_re_weight/5.

robust_re_weight(NId,Edges,Deriv,Span,-50000) :-
	recorded( mode(robust) ),
	N::node{ id => NId,
		 cat => v,
		 cluster => cluster{ left => L }},
	%% should check that the verb is a finite one
	verbose('Try Robust reweight ~w edges=~w deriv=~w span=~w\n',[NId,Edges,Deriv,Span]),
	( deriv(Deriv,_,_,OId,_) ->
	  check_op_top_feature(OId,mode,Mode),
	  \+ domain(Mode,[infinitive,gerundive,participle]),
	  true
	;
	  true
	),
	\+ ( edge_in_children( NId,
			       edge{ label => label[subject,impsubj],
				     source => N,
				     target => node{ cluster => cluster{ right => R } }
				   },
			       Edges
			     ),
	     R =< L ),
	verbose('Robust reweight ~w ~w\n',[NId,Edges]),
	true
	.

robust_re_weight(NId,Edges,Deriv,[Left,Right],W) :-
	recorded( mode(robust) ),
	( (Left = 0 xor node{ cluster => cluster{ right => Left }, cat => cat[poncts,ponctw] }) ->
	  WLeft = 0
	;
	  WLeft = -500
	),
	( ( node{ cluster => cluster{ left => Right }, cat => cat[poncts,ponctw] }
	  xor \+ cluster{ left => Right }
	  ) ->
	  WRight = 0
	;
	  WRight = -500
	),
	W is WLeft + WRight
	.
	

:-rec_prolog edge_weight_distrib/5.

%% not yet activated !
edge_weight_distrib(EId,NId,Children,W,WDistrib) :-
	fail,
	edge{ id => EId,
	      target => N::node{ id => NId, cat => cat[prep,nc,v,np,adj] }
	    },
	once((
	      edge_in_children( NId,
				edge{ type => adj,
				      id => _EId,
				      source => N,
				      target => COO::node{ cat => coo, id => COO_Id, deriv => COO_Derivs }
				    },
				Children
			      ),
%%	      format('weight distrib ~w\n',[NId]),
	      mutable(MW,0,true),
	      domain(dinfo(COO_OId,_EId,COO_Id,_Cst),Children),
	      '$answers'(best_parse(COO_Id,COO_OId,_,_Cst,
				    dstruct{ deriv => COO_Best_Deriv,
					     children => COO_Best_Children }
				   )
			),
	      every(( edge_in_children(COO_Id,
				       edge{ label => label[coord2,coord3] },
				       COO_Best_Children
				      ),
		      mutable_add(MW,W)
		    )),
	      mutable_read(MW,WDistrib),
%%	      format('distrib w ~w => ~w\n',[EId,WDistrib]),
	      true
	     ))
	.

:-xcompiler
sum_node_cost_regional(NId,Children,Deriv,InSpan,W) :-
	mutable(_WM,0,true),
	every((
	       ( 
		 node_cost_regional(_Name,NId,Children,Deriv,InSpan,_W_Reg)
	       ; 
		 node{ id => NId, cat => Cat::cat[v,pro] },
		 deriv2htid(Deriv,HTId),
		 node_cost_regional_cat(Cat,_Name,NId,Children,Deriv,InSpan,HTId,_W_Reg)
	       ),
	       verbose('Regional cost n=~w rule=~w w=~w\n',[NId,_Name,_W_Reg]),
	       mutable_add(_WM,_W_Reg)
	      )),
	mutable_read(_WM,W)
	.

:-rec_prolog
	node_cost_regional/6,
	node_cost_regional_cat/8.

%%:-light_tabular node_cost_regional/6.
%%:-mode(node_cost_regional/6,+(-,+,+,+,+,-)).

%% Strong penalty for verbs with subject after cod or xcomp (when cod after verb)
node_cost_regional_cat(v,Name::'-REG_v_obj_subj',NId,Edges::[_,_|_],Deriv,Span,_,-8000) :-
	once((
	      node{ id => NId,
		    cat => v,
		    cluster => cluster{ right => Pos}
		  },
	      edge_in_children(NId,
			       E2::edge{ id => EId2,
					 target => node{ cluster => cluster{ right => Right } },
					 label => label[object,xcomp]
				       },
			       Edges),
	      Pos =< Right,
	      edge_in_children(NId,
			       E1::edge{ id => EId1,
					 target => node{ cluster => cluster{ left => Left } },
					 label => label[subject,impsubj]
				       },
			       Edges),
	      Right =< Left,
	      verbose('Regional penalty cod before subj ~w ~w\n',[E1,E2]),
	      true
	     ))
	.

/*
node_cost_regional(root,Edges,Deriv,Span::[L,R],W) :-
	recorded( mode(robust) ),
	Delta is R-L,
	W is 1000 * Delta,
	verbose('Regional penalty robust ~w ~w\n',[Span,W])
	.
*/

/* tmp deactivated
node_cost_regional(root,Edges,Deriv,Span,-50000) :-
	\+ recorded( mode(robust) ),
	verbose('Try Regional penalty on incomplete root node ~w\n',[CIds]),
	_C::cluster{ id => _CId, lex => _Lex },
	_Lex \== '',
	\+ ( domain(_CId,CIds)
	   xor
	   domain(__CId,CIds),
	     cluster_overlap(_C,__C::cluster{ id => __CId }),
	     verbose('Overlap ~E ~E\n',[_C,__C])
	   ),
	verbose('Pb with ~E\n',[_C]),
	verbose('Found Regional penalty on incomplete root node ~w\n',[CIds]),
	true
	.
*/
	     
node_cost_regional(Name::'+REG_LemmaFreq',NId,Edges,Deriv,Span,WFreq) :-
%	fail,
	once((opt( weights ),
	      node{ id =>NId, w => Ws },
	      domain(lemmaFreq:W,Ws),
	      WFreq is W*10,
	      true
	     ))
	.

node_cost_regional(Name::'+REG_LemmaFreq',NId,Edges,Deriv,Span,WTag) :-
%	fail,
	once((
	      opt( weights ),
	      node{ id =>NId, w => Ws },
	      domain(matchTagger:1,Ws),
	      WTag is 1000,
	      true
	     ))
	.

:-light_tabular potential_coord/1.
:-mode(potential_coord/1,+(-)).

potential_coord(NId) :-
	chain( node{ cat => coo } << adj << node{ id => NId }).
potential_coord(NId) :-
	chain( node{} << (subst @ coord ) << node{} << adj << node{ id => NId }).

%% Favor coordination with same type of coords
node_cost_regional(Name::'+REG_coord',NId,Edges::[_|_],Deriv,Span,W) :-
	%%	fail, %% ++
	potential_coord(NId),
	verbose('Regional cost nid=~w deriv=~w edges=~w\n',[NId,Deriv,Edges]),
	once((
	      edge_in_children(NId,
			       edge{
				    id => EId,
				    source => S::node{ id => NId,
						       cat => _Cat,
						       lemma => _Lemma
						     },
				    target => COO::node{ cat => COO_Cat::cat[coo,'N2'],
							 id => COO_Id,
							 deriv => COO_Derivs
						       }	      
				   },
			       Edges
			      ),
	      ( COO_Cat = 'N2' ->
		chain(COO >> (subst @ coord) >> node{})
	      ;
		true
	      ),
	      domain(dinfo(COO_OId,EId,COO_Id,_Cst),Edges),
	      '$answers'(best_parse(COO_Id,COO_OId,_,_Cst,
				    dstruct{ deriv => COO_Best_Deriv,
					     children => COO_Best_Children }
				   )
			),
	      (node!empty(S) ->
	       edge_in_children(NId,
				edge{ source => S,
				      target => node{ cat => Cat, lemma => Lemma },
				      type => edge_kind[subst,lexical]
				    },
				Edges)
	      ; Cat = _Cat
	      ),
% 	\+ ( edge_in_children(NId,
% 			      E::edge{ source => COO,
% 				       label => label[coord,coord2,coord3],
% 				       target => node{ cat => _Cat }
% 				     },
% 			      Edges ),
% 	     _Cat \== Cat
% 	   ),
	      mutable(M,0,true),
	      mutable(MF,1,true),
	      mutable(MCount,0,true),
%	      verbose('Hello ~w ~w\n',[COO_Derivs,COO_OId]),
	      every((
		     source2edge(
				 E::edge{ source => COO,
					  label => label[coord,coord2,coord3],
					  target => _N::node{ cat => Cat2,
							      id => XNId,
							      lemma => Lemma2
							    },
					  id => XEId
					}
				),
		     domain(dinfo(_,XEId,XNId,_Cst),COO_Best_Children),
		     %% domain(COO_Best_Deriv,XDerivs),
		     %%  deriv(_D,XEId,_,COO_OId,_),
		     ( Cat = Cat2
		     xor Cat=adj, Cat2=v % participles
		     xor Cat=v, Cat2 = adj
		     xor Cat=nc, Cat2 = np
		     xor Cat=np, Cat2=nc
		     xor Cat=cat[nc,np], Cat2=pro
		     xor Cat=pro, Cat2=cat[nc,np]
		     ),
		     ( Cat = Cat2 -> _W3 = 100 ; _W3 = 0),
%%		     verbose('In regional: ~w\n',[E]),
		     _W2 is 400+_W3,
		     mutable_add(M,_W2),
		     mutable_inc(MCount,_),
		     true
		    )),
	      mutable_read(MCount,Count),
%	      (COO_Cat == coo xor Count > 1),
	      mutable_read(M,XW),
	      mutable_read(MF,F),
	      ( (COO_Cat == coo xor Count > 1 xor chain( COO >> lexical >> node{ form => '...' } ) ) ->
		W is XW*F
	      ; % penalize enum with only 2 components and no final ...
		W is -3000
	      ),
	      verbose('Regional cost COORD w=~w: nid=~w coo=~E source=~E edges=~w\n',[W,NId,COO,S,Edges]),
%	      format('Regional cost COORD w=~w xw=~w F=~w: nid=~w coo=~E source=~E edges=~w\n',[W,XW,F,NId,COO,S,Edges]),
	      true
	     ))
	.

:-xcompiler
multiple_adj_subst_edges(NId,Label,Edges) :-
	edge_in_children(NId,
			 edge{ id => E1, type => adj, target => N1::node{ id => NId1} },
			 Edges
			),
	source2edge(
		    edge{ source => N1,
			  id => EId1,
			  type => subst,
			  label => Label
			}
		   ),
	edge_in_children(NId,
			 edge{ id => E2, type => adj, target => N2::node{ id => NId2 } },
			 Edges
			),
	E1 \== E2,
	source2edge(
		    edge{ source => N2,
			  id => EId2,
			  type => subst,
			  label => Label
			}
		   ),

	domain(dinfo(OId1,E1,NId1,_Cst1),Edges),
	'$answers'(best_parse(NId1,OId1,_,_Cst1,dstruct{ children => Children1 })),
	domain(dinfo(_,EId1,_,_),Children1),
	
	domain(dinfo(OId2,E2,NId2,_Cst2),Edges),
	'$answers'(best_parse(NId2,OId2,_,_Cst2,dstruct{ children => Children2 })),
	domain(dinfo(_,EId2,_,_),Children2)
	
	.

%% strong penalties on multiple audience (or similar) on same node
node_cost_regional(Name::'-multiple_audience',NId,Edges::[_,_|_],Deriv,Span,W) :-
	%% fail, %% ++
	once((
	      multiple_adj_subst_edges(NId,Label::label['SRel',audience,reference,person_mod,time_mod,'N2app',position],Edges)
	     )),
	( Label = 'SRel' -> W = -10000 ;  W = -5000 )
	.

:-finite_set(cld,[cld,cld12,cld3]).

%% penalties on verbs with cld but no obj�-arg
node_cost_regional_cat(v,Name::'-cld_without_obj�',NId,Edges::[_|_],Deriv,Span,HTId,-2000) :-
	once((
	      %% node{ id => NId, cat => v },
	      edge_in_children(NId,
			       edge{ target => node{ cat => cld },
				     label => preparg
				   },
			       Edges
			      ),
	      \+ check_xarg_feature(HTId,args[arg1,arg2],obj�,_,cld[])
	     )).

%% counter-balance bonus on args, for Oblique args
%% maybe could add loc args
node_cost_regional_cat(v,Name::'-obl_args',NId,Edges::[_|_],Deriv,Span,HTId,W) :-
	%% fail, %+
	once((
	      check_xarg_feature(HTId,args[arg1,arg2],F::function[obl,obl2],prepobj,_),
	      (	F = obl ->
		  W = -800
	      ;	  
		%% F = obl2,
		W = -900
	      )
	     ))
	.

%% Penalize obj+comp subcat, that may be easily confused with a single obj
node_cost_regional_cat(v,Name::'-obj_and_comp',NId,Edges::[_,_|_],Deriv,Span,HTId,W) :-
	once((
	      node{ id => NId, cat => v, lemma => Lemma },
	      edge_in_children(NId,
			       edge{ 
				     target => Att::node{ cat => AttCat::cat[adj,comp,v,prep],
						     cluster => cluster{ left => L_Att
								       }
						   },
				     label => comp
				   },
			       Edges
			      ),
%%	      format('here ~w ~E\n',[Name,V]),
	      edge_in_children(NId,
			       edge{ 
				     target => Obj::node{ cat => cat[nc,np,adj],
							  cluster => cluster{ right => R_Obj }
							},
				     label => object
				   },
			       Edges),
	      R_Obj =< L_Att,
	      ( obj_and_att(Lemma,W1) xor W1=0),
	      ( AttCat=prep -> W2 = -200 ; W2 = 0 ),
	      W is -150+W2+W1+(R_Obj-L_Att) * 200,
%	      format('here ~w w=~w ~E att=~E\n',[Name,W,V,Att]),
	      true
	     ))
	.

:-extensional obj_and_att/2.

obj_and_att(faire,-200).
obj_and_att(aimer,-200).
obj_and_att(appeler,-200).

%% Penalize demonstrative pronoun with no modifier
node_cost_regional_cat(pro,Name::'-raw_dem_pro',NId,Edges::[_|_],Deriv,Span,HTId,-200) :-
	once((
	      node{ id => NId, cat => pro, lemma => Lemma::celui },
	      %%	     domain(Lemma,[celui]),
	      \+ edge_in_children(NId, edge{ type => adj }, Edges )
	    ))
	.

%% penalize comp+obj when obj may be confused with a de-PP
node_cost_regional_cat(v,Name::'-comp_and_obj',NId,Edges::[_,_|_],Deriv,Span,HTId,W) :-
	once((
	      node{ id => NId, cat => v, lemma => Lemma },
	      edge_in_children(NId,
			       edge{
				     target => Att::node{ cat => AttCat::cat[adj,comp,v,prep],
							  cluster => cluster{ right => R_Att
									    }
							},
				     label => comp
				   },
			       Edges
			      ),
	      %%	      format('here ~w ~E\n',[Name,V]),
	      edge_in_children(NId,
			       edge{
				     target => Obj::node{ cat => cat[nc,np],
							  cluster => cluster{ left => L_Obj }
							},
				     label => object
				   },
			       Edges),
	      R_Att =< L_Obj,
%%	      format('here1 ~w w=~w ~E att=~E\n',[Name,W,V,Att]),
	      ( chain( Obj >> (subst @ det ) >> node{ form => de_form[] } ),
		%%	      format('here2 ~w w=~w ~E att=~E form=~w\n',[Name,W,V,Att,Form]),
%%		domain(Form,[de,des,du,'de la','de l''']),
		W = -1200
	      xor AttCat = prep,
		  W = -600
	      xor W = -300
	      ),
%%	      format('here ~w w=~w ~E att=~E\n',[Name,W,V,Att]),
	      true
	     ))
	     .



:-xcompiler
multiple_adj_edges(NId,Label,Edges) :-
	edge_in_children(NId,
			 edge{ id => EId1, type => adj, label => Label },
			 Edges
			),
	edge_in_children(NId,
			 edge{ id => EId2, type => adj, label => Label },
			 Edges
			),
	EId1 \== EId2
	.

%% strong penalties on some multiple adj (like quantity)
node_cost_regional(Name::'-REG_multiple_adj',NId,Edges::[_,_|_],Deriv,Span, -10000) :-
	%% fail,
	once(( multiple_adj_edges(NId,Label::label[quantity,supermod],Edges) )),
	%% format('multiple audience label=~w\n',[Label]),
	true
	.

%% favor causative derivations, whenever possible
node_cost_regional_cat(v,Name::'+causative_deriv',NId,Edges::[_|_],Deriv,Span,HTId,1000) :-
	once((
	      node{ id => NId, cat => v, lemma => faire },
	      check_xarg_feature(HTId,arg1,_,fkind[vcompcaus],_),
	      edge_in_children(NId,
			       edge{ label => xcomp, type => subst },
			       Edges
			      )
	     ))
	.

%% User-provided info on chunks
%% *** to be checked and refined
node_cost_regional(Name::'UserChunkCost',NId,Edges,Deriv,Span::[XLeft,XRight|_],W) :-
	%% fail,
	extra_chunk_cost(Left,Right,Type,W),
	(var(Left) xor XLeft =< Left),
	(var(Right) xor XRight >= Right),
	N::node{ id => NId, xcat => XCat, cat => Cat, cluster => cluster{ left => NLeft, right => NRight}},
	Left =< NLeft,
	NRight =< Right,
	( Type = 'GN' ->
	    (	Left = XLeft
	    xor %%fail,
	    edge_in_children(NId,
				 edge{
				       target => _N::node{ cat => _Cat, cluster => cluster{ right => _Right } } },
				 Edges ),
		( _Right =< Left
		xor 
		    _Cat = det,
		    edge{ source => _N,
			  target => node{ cluster => cluster{ right => __Right } } },
		    __Right =< Left
		)
	    ),
	    XCat = 'N2',
	    Cat = nominal[]
	;   Type = 'GA' ->
	    (	Left = XLeft xor edge_in_children(NId,
						  edge{ 
							target => node{ cluster => cluster{ right => _Right } } },
						  Edges ),
		_Right =< Left
	    ),
	    XCat = cat['adjP',adj,'N',det],
	    Cat = cat[adj,v]
	;   Type = 'GP' ->
%	  fail,
	    Left = XLeft,
%	    XCat = cat['PP'],
	    % XCat = cat['PP','S','VMod'],
%	  XCat = cat['PP','S','VMod','N2','adjP'],
	    Cat = cat[prep],
	  true
	;   Type = 'PV' ->
	    (	%% PP with a verb
		%%	fail,
		Left = XLeft,
		XCat = cat['PP','S','VMod','N2','adjP'],
		Cat = cat[prep]
	    ;	%% xcomp verb argument introduced by a prep
		Cat = cat[v,adj],
		%%		format('here0 n=~w\n',[N]),
		edge_in_children(NId,
				 edge{ 
				       target => Prep::node{ cat => prep, cluster => cluster{ left => Left, right => Left2 } },
				       label => prep,
				       type => lexical
				     },
				 Edges
				),
%		format('here1 n=~w prep=~w\n',[N,Prep]),
		edge_in_children(NId,
				 edge{ 
				       target => XCOMP::node{ cat => v, cluster => cluster{ right => Right } },
				       label => xcomp,
				       type => subst
				     },
				 Edges
				),
%		format('here2 n=~w prep=~w xcomp=~w\n',[N,Prep,XCOMP]),
		true
	    )
	;   Type = 'GR' ->
	    ( Left = XLeft
	    xor edge_in_children(NId,
				 edge{ 
				       target => node{ cluster => cluster{ right => _Right } } },
				 Edges ),
		_Right =< Left
	    ),
	    XCat = cat['S','VMod',adv,advneg],
	    Cat = cat[adv,advneg]
	;   
	    Cat = cat[v],
	    XCat = cat['S']
	),
%	format('UserChunkCost ~w ~w ~w ~w\n',[N,Span,W,Edges]),
	true
	.


%% penalties on surrounding material for preceding citations
%% eg: X S Y, affirme-t-il, with X or Y attached to 'affirmer'
node_cost_regional_cat(v,Name::'-incise_with_surround',NId,Edges::[_,_|_],Deriv,Span,HTId,-2000) :-
%	fail,
	once((
	      node{ id => NId, cat => v, cluster => cluster{ left => VLeft } },
	      edge_in_children(NId,
			       edge{ 
				     label => xcomp,
				     type => subst,
				     target => node{ cluster => cluster{ left => TLeft, right => TRight } }
				   },
			       Edges
			      ),
	      TRight =< VLeft,
	      edge_in_children(NId,
			       edge{ 
				     label => label['S',vmod],
				     type => adj,
				     target => node{ cluster => cluster{ left => ModLeft, right => ModRight } }
				   },
			       Edges
			      ),
				% (   ModRight =< TLeft xor TRight =< ModLeft, ModRight =< VLeft )
	      ModRight =< VLeft
	     ))
	.
				
%% END REGIONAL COST 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edge transformations

:-std_prolog edge_reroot/2.

edge_reroot(E::edge{ id => EId,
		     source => node{ id => SId },
		     target => node{ id => TId },
		     deriv => Derivs
		   },
	    node{ id => Rerooted_From_Id }
	   ) :-
	record(E),
	alive_deriv(Derivs,Deriv),
	deriv(Deriv,EId,Span,_,_),
	record( reroot_source(EId,Span,SId,Rerooted_From_Id) )
	.

:-rec_prolog edge_transform/0.

edge_transform :-
	\+ opt(conll),
	E::edge{ source => V::node{ cat => v, id => NId },
		 target => Cl::node{ cat => cld[] },
		 type => lexical,
		 label => preparg,
		 deriv => Derivs,
		 id => EId
	       },
	chain( V >> (subst @ object) >> Obj::node{} ),
	\+ ( node2live_ht(NId,HTId),
	     check_arg_feature(HTId,args[arg1,arg2],function,obj�)
	   ),
	erase(E),
	edge_reroot( edge{ id => EId,
			   source => Obj,
			   target => Cl,
			   label => genitive,
			   type => lexical,
			   deriv => Derivs
			 },
		     V
		   )
	.
	
edge_transform :-
	E::edge{ source => V::node{ cat => v },
		 target => Cl::node{ cat => clg },
		 type => lexical,
		 label => clg,
		 id => EId,
		 deriv => Derivs
	       },
	chain( V >> (subst @ object ) >> Obj::node{} ),
	erase(E),
	edge_reroot( edge{ id => EId,
			   source => Obj,
			   target => Cl,
			   label => genitive,
			   type => lexical,
			   deriv => Derivs
			 },
		     V
		   )
	.

edge_transform :-
	\+ opt(conll),
	E::edge{ source => Aux::node{ cat => aux },
		 target => Subj,
		 type => lexical,
		 label => subject,
		 id => EId,
		 deriv => Derivs
	       },
	aux2main(Aux,V),
	Aux \== V,
	erase(E),
	edge_reroot( edge{ source => V,
			   target => Subj,
			   type => lexical,
			   label => subject,
			   id => EId,
			   deriv => Derivs
			 },
		     Aux
		   )
	.

:-std_prolog aux2main/2.

aux2main(Aux,V) :-
	( chain(Aux::node{ cat => cat[aux,v] } << (adj @ 'Infl') << _V::node{}) ->
	  aux2main(_V,V)
	;
	  V = Aux
	)
	.

edge_transform :-
	E::edge{ source => _V::node{},
		 type => adj,
		 label => E_Label,
		 target => Mod::node{}
	       },
	node!empty(Mod),
	\+ node!empty(_V),
	( chain( Mod >> subst >> ModAnchor::node{ cluster => cluster{ left => Left, right => Right }} ),
	  Right > Left
	->
	  Mode = subst,
	  true
	; chain( Mod >> lexical >> ModAnchor ),
	  Right > Left
	->
	  Mode = lexical,
	  true
% 	  \+ ( chain( Mod >> _ >> _ModAnchor::node{ cluster => cluster{ right => _Right }} ),
% %	       _Right =< Left
% 	       \+ ModAnchor = _ModAnchor
% 	     ) ->
% 	  true
 	;
 	  fail
	),
%	format('reroot at ~w\n',[E]),
	erase(E),
	erase(Mod),
	( aux2main(_V,V) xor _V = V ),
	every(( E1::edge{ source => Mod,
			  target => Target,
			  label => Label,
			  type => Type,
			  deriv => Derivs,
			  id => EId1
			},
		erase(E1),
		( Target = ModAnchor ->
		  (Mode = subst ->
		   XLabel = Label
		  ;
		   XLabel = E_Label
		  ),
		  edge_reroot( edge{ source => V,
				     target => Target,
				     label => XLabel,
				     type => adj,
				     deriv => Derivs,
				     id => EId1
				   },
			       Mod
			     )
		;
		  edge_reroot( edge{ source => ModAnchor,
				     target => Target,
				     label => Label,
				     type => Type,
				     deriv => Derivs,
				     id => EId1
				   },
			       Mod
			     )
		)
	      ))
	.

/*
edge_transform :-
	E1::edge{ source => V::node{},
		  target => Prep::node{ cat => prep },
		  type => subst,
		  label => preparg,
		  id => EId1,
		  deriv => Derivs1
		},
	E2::edge{ source => Prep,
		  target => Head::node{},
		  type => subst,
		  id => EId2,
		  deriv => Derivs2
		},
	erase(E1),
	erase(E2),
	record( edge{ id => EId1,
		      source => V,
		      target => Head,
		      type => subst,
		      label => preparg,
		      deriv => Derivs1
		    }
	      ),
	record( edge{ id => EId2,
			   source => Head,
			   target => Prep,
			   type => subst,
			   label => prep_intro,
			   deriv => Derivs2
			 }
	      )
	.
*/

%% some edge transformation are relation to CONLL conversion

%% CONLL: subject are attached to main verb (including modal verb)
edge_transform :-
	opt(conll),
	E1::edge{  label => 'V',
		   type => adj,
		   source => S,
		   target => T,
		   id => EId,
		   deriv => Derivs
		},
	erase(E1),
	record( conll_modal(S,T) ),
	edge_reroot( edge{ source => T,
			   type => subst,
			   label => object,
			   target => S,
			   deriv => Derivs,
			   id => EId
			 },
		     S
		   ).

%% CONLL: Monsieur is head of syntagm, but dependant in FRMG => we reroot !
edge_transform :-
	opt(conll),
	E1::edge{  label => 'Monsieur',
		   type => lexical,
		   source => S,
		   target => T,
		   id => EId,
		   deriv => Derivs
		},
	every((
	       E2::edge{  label => L2,
			  type => T2,
			  source => S2,
			  target => S,
			  id => EId2,
			  deriv => Derivs2
		       },
	       erase(E2),
	       edge_reroot(edge{ source => S2,
				 target => T,
				 label => L2,
				 type => T2,
				 id => EId2,
				 deriv => Derivs2
			       },
			   T)
	      )),
	erase(E1),
	edge_reroot( edge{ source => T,
			   type => lexical,
			   label => 'Monsieur',
			   target => S,
			   deriv => Derivs,
			   id => EId
			 },
		     S
		   ).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building words

:-std_prolog word/1.

word(_Left) :-
	verbose( 'Word at left=~w\n',[_Left]),
	%% find shortest (non empty non redirected) cluster closest from _Left
	(   C::cluster{ id=> Id, left => Left, right => Right, lex => Lex, token => Token },
	    _Left =< Left,
	    verbose('Found potential cluster ~E after ~w\n',[C,_Left]),
	    Lex \== '',
	    \+ ( cluster{ id => _Id, left => __Left, right => __Right, lex => __Lex },
		   \+ recorded( redirect(_Id,_) ),
		   __Lex \== '',
		   (   (__Left == Left, __Right < Right)
		   xor ( _Left =< __Left, __Left < Left)
		   )
	       ),
	    verbose('Found cluster ~E after ~w\n',[C,_Left]),
	    \+ recorded( redirect(Id,_) )
	->
	    rx!tokenize(Lex,' ',Words),
	    word_aux(Words,Left,C),
	    every((
		   C2::cluster{ id => Id2, left => Right, right => Right2, lex => Lex, token => Token2 },
		   agglutinate(Token,Token2,AggLex),
		   ( recorded( redirect(Id,_Id)) xor Id=_Id),
		   verbose('Register redirected ~w -> ~w [~w]\n',[Id2,_Id,AggLex]),
		   record_without_doublon( redirect(Id2,_Id))
		  )),
	    true
	;   
	    true
	)
	.

:-rec_prolog word_aux/3.

word_aux([],Left,Cluster::cluster{ left => Left , right => Right }) :-
	verbose('Restart word ~w\n',[Left]),
	word(Right)
	.

word_aux([Lex|Words],Left,Cluster::cluster{ id => CId, token => Token }) :-
	verbose( 'Word ~w in cluster ~w at left=~w\n',[Lex,CId,Left]),
	sentence(Sent),
	(   rx!tokenize(Lex,'|',[Label1,_Lex1|_R]),
	    Label1 \== ''
	->  
	    %% Pb when second '|' arise after first '|'
	    ( _R = [] -> Lex1 = _Lex1 ; Lex1 = Lex ),
%%	    name_builder('E~w~w',[Sent,Label1],Label),
	    ( recorded(opt(passage)),
	      rx!tokenize(Label1,'F',[SId,TId]) ->
	      name_builder('~wT~w',[SId,TId],Label)
	    ;
	      Label = Label1
	    ),
	    ( clean_lex(Lex1,Lex2) xor Lex1=Lex2 ),
	    ( fail, Lex2 == '*' ->
	      recorded(f{id=> Label, cid => CId0}),
	      record_without_doublon( redirect(CId,CId0) )
	    ;	recorded(f{id=> Label, cid => CId0}) ->
	      verbose('Redirect f label=~w lex=~w cluster=~E cid0=~w\n',[Label,Lex2,Cluster,CId0]),
	      record_without_doublon( redirect(CId,CId0) )
	    ;
	      update_counter(fids,Rank),
	      verbose('Register f rank=~w label=~w lex=~w cluster=~E\n',[Rank,Label,Lex2,Cluster]),
	      record(f{id=>Label, lex=>Lex2, cid => CId, rank => Rank})
	    )
	; Token = '_SENT_BOUND' ->
	    verbose('Ignore sentence boundary cluster=~E\n',[Cluster]),
	    true
	;
	    ( clean_lex(Lex1,Lex) xor Lex1=Lex ),
	    update_counter(fids,Rank),
	    ( opt(passage) ->
	      name_builder('~wT~w',[Sent,Rank],Label)
	    ;
	      name_builder('~wF~w',[Sent,Rank],Label)
	    ),
	    verbose('XRegister f rank=~w label=~w lex=~w cluster=~E\n',[Rank,Label,Lex1,Cluster]),
	    record(f{id=>Label, lex=>Lex1, cid => CId, rank => Rank})
	),
	word_aux(Words,Left,Cluster),
	true
	.

:-extensional clean_lex/2.

clean_lex('\\?','?').
clean_lex('\\!','!').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building constitutants

:-light_tabular constituant/3.

constituant(Left,Right,Const) :-
	'$answers'( Const::group(Type,Left,Right,N,Content) ),
	verbose('Potential const left=~w const ~w\n',[Left,Const]),
	\+ used(Const),
	verbose('alive\n',[]),
	true
	.

constituant(Left,Right,Const) :-
	Const::cluster{ id => Label, left => Left, right => Right, lex => Lex },
	Lex \== '',
	\+ used(Label)
	.

:-std_prolog x_command/2.

x_command(N1::node{},N2::node{}) :-
	(   edge{ source => N1, target => N2 }
	;   edge{ source => N3, target => N1, id => EId1 },
	    edge{ source => N3, target => N2, id => EId2 },
	    have_shared_derivs(EId1,EId2)
	)
	.

:-light_tabular build_group/1.

build_group(Type) :-
	verbose('Try build group ~w\n',[Type]),
	group(Type,N::node{ cluster => Cluster::cluster{ id => WId }},Nodes),
	verbose('Potential group ~w ~w ~w\n',[Type,N,Nodes]),
	\+ used(WId),
%%	verbose('TEST0 group ~w ~w ~w\n',[Type,N,Nodes]),
	%% In passage, a redirected cluster may be part of several groups
	\+ ( opt(easy),
	     recorded( redirect( WId,WId1) ), % Needed to avoid void group
	     ( used(WId1)
	     xor recorded( redirect( _WId,WId1) ), used(_WId)
	     )
	   ),
	verbose('TEST group ~w ~w ~w\n',[Type,N,Nodes]),
	( Nodes = group(_,_Left1,Right,_,Content1) ->
	  Cluster = cluster{ id => Id, left => Left, right => Left1 },
	  _Cluster = cluster{ id => _Id, right => _Left1 },
	  ( recorded( redirect( _Id, Id) ) xor Id=_Id ),
	  ( domain(WId,Content1) ->
	    Content = Content1
	  ;
%%	    verbose('Try Left cluster filler ~E ~w ~w => ~w\n',[Cluster,_Left1,Content1,Content]),
	    cluster!add_left_fillers(Content1,Content,_Left1,Cluster),
	    verbose('Left cluster filler ~E ~w ~w => ~w\n',[Cluster,_Left1,Content1,Content])
%%	    Content = [WId|Content1]
	  ),
	  mark_as_used(Content)
	;   
	  node!add(N,Nodes,Nodes2),
	  node!add_fillers(Nodes2,Nodes3),
	  node!group(Nodes3,Content,Left,Right)
	),
	verbose('TEST2 group ~w ~w ~w\n',[Type,N,Nodes]),
	( Left \== Right xor recorded(splitted(WId,_)) ),
	verbose('TEST3 group ~w ~w ~w\n',[Type,N,Nodes]),
	group(Type,Left,Right,N,Content)
	.

:-std_prolog cluster!add_left_fillers/4.

cluster!add_left_fillers(Content,XContent,
			Right,
			LeftCluster::cluster{ id => WId, right => R} ):-
	(R=Right ->
	 XContent = [WId|Content]
	; cluster{ id => _WId, left => _Left, right => Right },
	 _Left < Right,
	 R =< _Left,
%%	 verbose('Try left filler Left=~w newright=~w Right=~w\n',[R,_Left,Right]),
	 cluster!add_left_fillers([_WId|Content],XContent,_Left,LeftCluster)
	)
	.
	

:-light_tabular group/5.
:-mode(group/5,+(+,+,+,+,+)).

group(Type,Left,Right,N,Content) :-
	verbose('GROUP ~w ~w ~w ~E ~w\n',[Type,Left,Right,N,Content]),
	mark_as_used(Content)
	.

:-rec_prolog group/3.

group('NV',N,Nodes) :-
	N::node{ cat => Cat::cat[v,aux], cluster => cluster{ left => LeftN, right => RightN }},
	\+ single_past_participiale(N),
	node!first_v_ancestor(N,P1),
	node!collect( (N1::node{ cat => Cat1,
				 lemma => Lemma1,
				 form => Form1,
				 cluster => cluster{ left=> Left1, right => Right1}}) ^
		    (
			node!terminal(N,cat[v,aux],['Infl','V']),
			node!terminal(P1,cat[v,adj],['Infl','V']),
			%% N is first aux or v
			%% get subject of older ancestor P or from N (post subject clitic)
			node!older_ancestor(N,cat[v,aux,adj],P,['Infl','V']),
			record_without_doublon( terminal_v(P,N) ), %% for the SUJ-V relation
			verbose('Terminal_v p=~E n=~E p1=~E\n',[P,N,P1]),
			%%			format('OLD ANCESTOR ~w => ~w\n',[N,P]),
			Cat1 = cat[cln,ilimp,caimp,pro],
			(   node!dependency(out,P,N1,_)
			;   N \== P, node!dependency(out,N,N1,_)
			; %% robust mode
			    recorded(mode(robust)),
			    node!all_neighbours(left,N,
						_Model::node{ cat => cat[cln,ilimp,caimp,clg,cla,cld,cll,clr,clneg,advneg,pro,advPref]},
						N1),
			    \+ (node!neighbour(right,N1,node{ cat => cln })),
			    true
			),
			( Cat1 = cat[cln,ilimp,caimp]
			;
			  Cat1 = pro,
			  domain( Lemma1, ['�a','ceci','cela','ce'] ),
			  Right1 =< LeftN,
			  \+ ( 
			       edge{ source => N1::node{},
				     target => N1_Dep::node{ cluster => cluster{ left => N1_Dep_Left}}
				   }
			     ,
			       N1_Dep_Left >= Right1
			     ),
			  true
			),
		     true
		    ;	
			node!terminal(N,cat[aux],['Infl','V']),
			%% N is first aux or v in a local chain
			%% (may be preceded by a modal verb)
			%% get all clitics from first v ancestor
			(   Cat1 = cat[clg,cla,cld,cll,clr,clneg,advneg,adv],
			    ( node!dependency(out,P1,N2::node{ cat => Cat1 },[E])
			    ;	recorded(mode(robust)),
				node!all_neighbours(left,N,_Model,N1),
				\+ (node!neighbour(right,N1,node{ cat => cln }))
			    )
			; 
			    N \== P1,
			    Cat1 = cat[adv,advneg],
			    ( node!dependency(out,N,N2,[E])
			    ; recorded(mode(robust)),
			      node!all_neighbours(left,N,_Model,N1)
			    )
			),
			%% only keep adv or advneg if left of N and find all dep. from advneg
			%% and only if attached to v (not to S) or some advneg
			%% and preceded by a clneg !
			( Cat1 = cat[adv,advneg],
%			    format('ADV* ~w ~w\n',[N2,E]),
			    E=edge{ label => cat[v,advneg], type => adj },
			    edge{ source => N, target => node{ cat => clneg } },
			    node!neighbour(xleft,N,N2),
			    (	N1 = N2 ;
				node!safe_dependency(xout,N2,N1),
%%				format('NV XOUT cat=~w N=~w P1=~w N2=~w -> N1=~w\n',[Cat1,N,P1,N2,N1]),
				true
			    )
			;  Cat1 = cat[~ [adv,advneg]],
			    N1 = N2
			)
		    ),
		      Nodes ),
	true
	.

group('NV',N,[]) :-
	N::node{ lemma => L},
	domain(L,['il y a']).	

group('GN',N,Nodes) :-
	N::node{ cat => Cat::cat[adj,nc,pro,np,pri,prel,ncpred,xpro,ce],
		 lemma => L,
		 form => F,
		 cluster => _C_N::cluster{ left => Left }
	       },
	( Cat = adj ->
	    edge{ source => N, type => subst, label => det }
	;   Cat = cat[pri] ->
	  ( \+ edge{ source => node{ cat => 'S'}, target => N },
	    \+ edge{ source => node{ cat => v}, target => N, type => adj }
	  xor pri(L,'GN')),
%%	  \+ ( F \== 'qui',
%%	       edge{ target => N, label => 'CleftQue' }
%%	     ),
	  true
	;   Cat = cat[prel] ->
	  \+ edge{ source => node{ cat => 'S'}, target => N },
	  \+ domain(L,[dont]),
%%	  \+ ( F \== 'qui',
%%	       edge{ target => N, label => 'CleftQue' }
%%	     ),
	  ( edge{ source => V::node{ cat => v },
		  target => N,
		  label => 'CleftQue'
		} ->
	    edge{ source => V,
		  target => Clefted::node{ cluster => cluster{ right => Right }},
		  label => label[comp,subject,comp,impsubj],
		  type => subst
		},
	    Right =< Left
	  ; edge{ target => N,
		  label => 'CleftQue',
		  type => lexical
		} ->
	    fail
	  ;
	    true
	  ),
	  true
	; %% Cat = cat[nc,pro,np,ncpred,xpro,ce],
	  true
	),
%%	\+ node!neighbour(right,N,node{cat=>cat[nc,np]}),
	node!collect( (N1::node{ lemma => Lemma1,
				 cat => Cat1
			       })^(
		          ( edge{ source => N,
				target => _N1::node{ form => _F_N1,
						     lemma => _Lemma1,
						     cat => _Cat1
						   },
				label => _Label
			      }
			  ; %% for ncpred with det (for lglex)
			    %% not a very elegant solution (in terms of dependencies)
			    Cat = ncpred,
			    chain( N
				 << (lexical @ ncpred ) << node{ cat => v }
				 >> (subst @ det ) >> _N1
				 )
			  ),
			  ( N1=_N1
			  ; node!safe_dependency(xout,_N1,N1) ),
			  %%			   format('GN XOUT ~w -> ~w\n\tL=~L\n',[N,N1,['~E',' '],[]]),
%%				   format('TEST0 n1=~E\n',[N1]),
			  ( node!neighbour(xleft,N,N1),
			    _Label = label[~ ['N2']],
			  %%  format('TEST _lemma1=~w cat=~w n=~E\n',[_Lemma1,_Cat1,N1]),
			    ( Lemma1 = tout
			    xor \+ edge{ target => N1,
					 label => det,
					 type => adj
				       }
			    )
			  ;
			    N1=_N1, %only for immediate descendants of N
			    domain(_F_N1,['_-l�','_-ci'])
			  ;
%%			    format('TEST2 _lemma1=~w cat=~w n=~E\n',[_Lemma1,_Cat1,N1]),
			    N1=_N1, %only for naked immediate descendants of N
			    \+ edge{ source => N1,
				     target => node{ cat => incise },
				     type => adj
				   },
			    ( _Lemma1 = number[],
			      \+ edge{ source => N1 },
			      node!neighbour(left,N1,N)
			    ;
			      Cat1 = np,
			      Cat = np
			    )
			  )
			 )
		    , _Nodes ),
	left_reduce(_Nodes,Nodes),
%	format('Group GN ~w ~w\n',[N,Nodes]),
	true
	.

group('GN',N,[]) :-
	recorded(mode(robust)),
	N::node{ form => Form,
		 cat => cat[cla,cld],
		 cluster => cluster{ right => R }
	       },
	\+ edge{ source => node{ cat => v },
		 target => N },
	( domain(Form,[le,la,les,'l''']) ->
	  \+ node{ cat => cat[adj,nc,np],
		   cluster => cluster{ left => R }
		 }
	;
	  true
	)
	.

%% for META_TEXTUAL_GN
group('GN',N,[]) :-
	N::node{ cat => epsilon, xcat => 'N2' }
	.

%% Remove skip node on the left
:-rec_prolog left_reduce/2.
left_reduce([],[]).
left_reduce([N|L],XL) :-
	( edge{ target => N, label => skip, type => epsilon } ->
	  left_reduce(L,XL)
	; edge{ target => N, type => lexical },
	  N = node{ lemma => ',' } ->
	  left_reduce(L,XL)
	;
	  XL=[N|L]
	)
	.

group('GN',N,[]) :-
	N::node{ lemma => '_META_TEXTUAL_GN' }
	.


%/*
%% this rule should be refined
group('GN',N,[]) :-
	N::node{ lemma => number[], cat => adj, cluster => cluster{ right => Right_N }},
	\+ ( chain( N << adj << node{ cat => cat[nc,np], cluster => cluster{ left => Left }} ),
	       Right_N =< Left
	   )
	.
%*/

group('GP',N,Nodes) :-
	edge{ source => S::node{ cat => cat['S','N2']},
	      target => N::node{ cat => Cat::cat[prel],
				 cluster => cluster{ left => L} }},
	node!collect( N1^( node!safe_dependency(xout,N,N1),
			   node!neighbour(xleft,N,N1) )
		    , __Nodes ),
	( edge{ source => S,
		target => Prep::node{ cat => prep, cluster => cluster{ right => R}},
		label => prep
	      },
	  R =< L ->
	  _Nodes = [Prep|__Nodes]
	;
	  _Nodes = __Nodes
	),
	left_reduce(_Nodes,Nodes)
	.

%% Clefted constructions
group('GP',N,[]) :-
	edge{ source => V::node{ cat => cat[v]},
	      target => N::node{ cat => Cat::cat[prel],
				 cluster => cluster{ left => L} },
	      label => 'CleftQue'
	    },
	edge{ source => V,
	      target => Clefted::node{ cluster => cluster{ right => R }},
	      label => preparg,
	      type => subst
	    },
	R =< L
	.

group('GP',N,Nodes) :-
	edge{ source => S::node{ cat => 'S'},
	      target => N::node{ cat => Cat::cat[pri], lemma => L,
				 cluster => cluster{ left => Left }}},
	verbose('TRY GP ~E ~w\n',[N,L]),
	( edge{ source => S,
		target => Prep::node{ cat => prep, cluster => cluster{ right => Right }},
		label => prep
	      },
	  Right =< Left ->
	  Nodes = [Prep]
	;
	  pri(L,'GP'),
	  Nodes = []
	)
	.

group('GP',N,Group1) :-
	'$answers'(Group1::group(const['GN','GR','GP','GA'],Left1,_,Head1,_)),
	node!neighbour(left,Left1,_N::node{ cat => prep }),
	verbose('TRY GP ~E ~w ~E\n',[_N,Left1,Head1]),
	( x_command(_N,Head1)
	; %% relative witout antecedent
	  %% should factorize this path with a predicate
	  edge{ source => _N,
		target => _N2::node{ cat => 'N2' },
		type => subst,
		label => 'N2'
	      },
	  edge{ source => _N2,
		target => _V::node{ cat => v },
		type => subst,
		label => 'SRel'
	      },
	  edge{ source => _V,
		target => Head1,
		label => label[subject,impsubj]
	      }
	),
	easy_compound_prep(_N,N::node{ lemma => Lemma }),
	\+ not_a_prep(Lemma),
	true
	.


group('PV',N,Group2) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	%% node!neighbour(left,Left1,_N),
	recorded( terminal_v(Head2,Head1) ),
	leftward_till_prep(Left1,_N,N,Group1,Group2),
	verbose( 'PV: left=~E head1=~E head2=~E\n',[_N,Head1,Head2]),
	/*
	(   _N = node{ cat => prep },
	    easy_compound_prep(_N,N)
	;
	    N = _N,
	    _N = node{ %% cat => '_',
		       lemma => en }
	),
	*/
	x_command(_N,Head2),
	verbose( 'Found PV: left=~E head1=~E head2=~E\n',[N,Head1,Head2]),
	true
	.


:-std_prolog leftward_till_prep/5.

leftward_till_prep(Left,_Prep,Prep,OldGroup,NewGroup) :-
%	format('LEFTWARD from ~w\n',[Left]),
	node!neighbour(left,Left,N::node{ cat => Cat , lemma => L, tree => Tree, cluster=> cluster{ left => _Left }} ),
%	format('=> left=~w ~w from=~w\n',[_Left,N,Left]),
	( (   Cat = prep,
	      _Prep = N,
	      easy_compound_prep(_Prep,Prep)
	  xor
	      L=en,
	      Prep = N,
	      _Prep = Prep
	  ) ->
	  NewGroup = OldGroup
	; Cat = cat[adv,advneg] ->
	  leftward_till_prep(N,_Prep,Prep,[N|OldGroup],NewGroup)
	; Tree = [lexical] ->
	  leftward_till_prep(N,_Prep,Prep,[N|OldGroup],NewGroup)
	;
	  fail
	).

group('PV',N,Group1) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	node!neighbour(left,Left1,N),
	recorded( terminal_v(Head2,Head1) ),
	verbose( 'Try PV step1: head1=~E head2=~E\n',[Head1,Head2]),
	edge{ source => Head2,
	      target => M::node{ cat => v },
	      type => adj,
	      label => label['S','S2']
	    },
	verbose( 'Try PV step2: M=~E\n',[Edge]),
	edge{ source => M, target => N::node{ cat => prep }, type => lexical }
	.

group(Type,N,Group) :-
	N::node{ cat => cat[adv,advneg], cluster => cluster{ id => IdN }, lemma => L},
	\+ ( edge{ target => N, source => node{ cat=>cat[adj,nc,prep],
						cluster=> cluster{ id => IdM } }},
	     recorded(redirect(IdM,IdN))
	   ),
	( adv_as_pp(L,_Type) ->
	    Type = _Type,
	    Group = []
	;
	    Type = 'GR',
	    ( chain( N >> (subst @ det ) >> Det::node{ cat => det } ) ->
		% in superlative add det to GR
		Group = [Det]
	    ;
		Group = []
	    )
	)
	.

:-extensional adv_as_pp/2.

adv_as_pp('en effet','GP').
adv_as_pp('de m�me','GP').
adv_as_pp('par exemple','GP').
adv_as_pp('de moins','GP').
adv_as_pp('en moins','GP').
adv_as_pp('de plus','GP').
adv_as_pp('d''abord','GP').
adv_as_pp('en moyenne','GP').
adv_as_pp('en revanche','GP').
adv_as_pp('d''autre part','GP').
adv_as_pp('en particulier','GP').
adv_as_pp('au mieux','GP').

group('GR',N,[]) :-		% que restr ?
	edge{ target => N::node{ cat => cat[csu] },
	      label => advneg,
	      source => node{ cat => cat[v,aux]}
	    }
	.

group('GR',N::node{ cat => pri, lemma => L },[]) :-
	( edge{ source => node{ cat => 'S'}, target => N }
	;
	  chain( N << adj << node{ cat => v })
	),
	%% by default, pri which are not arguments are GR unless
	%% otherwise explicetely mentionned
	pri(L,const[~ ['GP','GN']])
	.

group('GR',N,[]) :-
	edge{ target => N::node{ cat => que_restr },
	      label => advneg,
	      type => lexical
	    }
	.

group('GA',N,[]) :-
	N::node{ cat => adj, lemma => Lemma },
	Lemma \== number[]
	.


group('GA',N,[]) :-
	%% Participiales on nouns, non gerundive (ie. past participle)
	single_past_participiale(N)	
	.

group('GA',N,[]) :-
	chain( node{ cat => supermod } >> lexical >> N::node{ lemma => possible } )
	.

:-light_tabular single_past_participiale/1.

single_past_participiale(N::node{}) :-
	edge{ source => node{ cat => 'N2' },
	      target => N::node{ cat => v, cluster => cluster{ lex => Lex } },
	      type => subst,
	      label => 'SubS'
	    },
	\+ edge{ source => N, label => label[object,arg,preparg,scomp,xcomp] },
	\+ edge{ source => N, target => node{ cat => aux } },
	\+ ( edge{ source => N, target => VMod::node{ cat => 'VMod' },
		   type => adj, label => vmod },
	     edge{ source => VMod, target => node{ cat => prep }, label => 'PP'}
	   ),
%%	format('Try1 participale2adj on ~w ~w\n',[Lex,TLex]),
	(label2lex(Lex,[TLex],_) xor TLex = Lex),
	verbose('Try2 participale2adj on ~w ~w\n',[Lex,TLex]),
	(   rx!match{ string => TLex, rx => rx{ pattern => 'ant$'} } ->
	    verbose('Match succeeded\n',[]),
	    fail
	;
	    verbose('Match failed\n',[]),
	    true
	)
	.

:-light_tabular easy_compound_prep/2.
:-mode(easy_compound_prep/2,+(+,-)).

%% Climb along a compound preposition like "� partir de"
%% when they form a single EASy cluster
easy_compound_prep( N1::node{ cat => prep, cluster => cluster{ id => Id1 }},
		    N2::node{ cat => prep, cluster => cluster{ id => Id2 }}
		  ) :-
	( edge{ source => M::node{ cluster => cluster{ id => IdM}},
		target => N1,
		type => adj
	      },
	  verbose('TRY EASY COMPOUND ~w ===> ~w\n',[N1,M]),
	 edge{ source => N3::node{ cat => prep, cluster => cluster{ id => Id3}},
	       target => M,
	       type => subst
	     },
	  verbose('TRY2 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N3]),
	  easy_compound_prep(N3,N2),
	  verbose('TRY3 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N2]),
	  recorded( redirect(Id1, Id2) ),
	  verbose('TRY4 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N2]),
	  true
	xor N1=N2
	),
	verbose('EASY COMPOUND ~w ===> ~w\n',[N1,N2]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extracting dependencies

:-std_prolog extract_relation/1.

extract_relation( R ) :-
	R::relation{ type => Type, arg1 => Arg1, arg2 => Arg2, arg3 => Arg3 },
	coord_reroot(R,XR::relation{ type => Type, arg1 => XArg1, arg2 => XArg2, arg3 => XArg3 }),
	arg2id(XArg1,Id1),
	arg2id(XArg2,Id2),
	arg2id(XArg3,Id3),
	( Id1 \== Id2 ),	% To avoid relations between same tokens
	relation( relation{ type => Type, arg1 => Id1, arg2 => Id2, arg3 => Id3 } )
	.

:-std_prolog arg2id/2.

arg2id(Arg,Id) :-
	( Arg = [] -> Id = []
	; opt(passage),
	  Arg = node{ id => Id } ->
	  true
	; opt(passage),
	  Arg = f(Id) ->
	  true
	;  Arg = node{ cluster => C::cluster{ id => CId , left => Left , right => Right}} ->
	    ( recorded( redirect(CId,_CId) ) ->
		_Left is Left - 1
	    ;	
		_Left = Left,
		_CId = CId
	    ), 
	    (	f{ cid => _CId, id => Id } 
	    xor	cluster_overlap(CId,CId2),
		f{ cid => CId2, id => Id }
	    )
	;   Id = Arg
	)
	.


:-std_prolog coord_reroot/2.

coord_reroot( R::relation{ type => Type,
			    arg1 => Arg1,
			    arg2 => Arg2,
			    arg3 => Arg3
			  },
	      XR::relation{ type => Type,
			    arg1 => XArg1,
			    arg2 => XArg2,
			    arg3 => XArg3
			  }
	    ) :-
	( Type = 'COORD' ->
	  R = XR
	%% the following case should be commented
	%% but currently, distrib of AUX-V over COORD is generally not done
	%% in the reference
	; Type = 'AUX-V' ->
	  R = XR
	; coord_relarg_reroot(Type,1,Arg1,XArg1,R),
	  coord_relarg_reroot(Type,2,Arg2,XArg2,R),
	  coord_relarg_reroot(Type,3,Arg3,XArg3,R)
	).

:-rec_prolog coord_relarg_reroot/5.

coord_relarg_reroot(Type,Pos,Arg,XArg,
		    R::relation{ arg1 => Arg1,
				 arg2 => Arg2
			       }
		   ) :-
	( \+ var(Arg),
	  Arg = node{ cluster => cluster{ left => L1, right => R1 } },
%%	  format('Try Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  ( _Arg = Arg
	  ; fail,
	    %% not yet ready for a right distrib
	    %% ex: il ach�te et mange une pomme.
	    %% but how to avoid: il sort et ach�te une pomme.
	    edge{ target => Arg,
		  label => coord3,
		  type => edge_kind[lexical,subst],
		  source => COO
		},
	    E
	  ; edge{ source => _Arg,
		  target => Arg,
		  type => edge_kind[subst,lexical],
		  label => label['N2']
		}
	  ),
	  E::edge{ source => _Arg::node{ cat => _Cat },
		   target => COO::node{ cat => coo,
					cluster => cluster{ left => CL,
							    right => CR }
				      },
		   type => adj
		 },
%%	  format('Try1 Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  \+ duplicate_in_coord(E,Type,Pos),
%%	  format('Try2 Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  \+ ( ( Pos=1 -> OtherArg=Arg2
	       ; Pos=2 -> OtherArg=Arg1
	       ; fail
	       ),
	       OtherArg = node{ cat => OtherArgCat,
				cluster => cluster{ left => OL,
						    right => OR }},
%%	       format('Other arg is ~E\n',[OtherArg]),
		 (   R1 =< OL,
		     OR =< CL
		 ;
		     OtherArgCat = cat[cla,cld,clg,cll,clr,clneg]
		 ;
		     _Cat = v,
		     \+ edge{ source => COO,
			      label => coord3,
			      type => subst,
			      target => node{ cat => v }
			    }
		 )
	     ),
	    allow_reroot(Type,Pos,Arg,XArg,R),
	    true
	->
	  %% XArg = COO,
	  %% old rule
	  %%	  find_first_coo(COO,XArg),
	  %% new rule seems to be to use the last coo
	  XArg = COO,
%%	  verbose('Coord reroot relarg ~w arg~w from ~E to ~E\n',[Type,Pos,Arg,XArg]),
%%	  format('Coord reroot relarg ~w arg~w from ~E to ~E\n',[Type,Pos,Arg,XArg]),
	  true
	;
	  XArg = Arg
	)
	.

:-light_tabular find_first_coo/2.
:-mode(find_first_coo/2,+(+,-)).

find_first_coo(Coo,First) :-
	( edge{ source => Coo::node{
				    cluster => cluster{ left => COO_Left }
				   },
		target => Coma::node{ 
				      cluster => cluster{ left => Coma_Left,
							  right => Coma_Right
							}
				    },
		type => lexical,
		label => void
	      },
	  Coma_Right < COO_Left,
%%	  format('COO coma_right=~w coo_left=~w\n',[Coma_Right,COO_Left]),
	  \+ ( edge{ source => Coo,
		     target => node{ 
				     cluster => cluster{ left => Coma2_Left }
				   },
		     type => lexical,
		     label => void
		   },
	       Coma2_Left < Coma_Left
	     ),
	  Coma = First
	xor Coo = First
	).
	

:-rec_prolog allow_reroot/5.

allow_reroot(rel[],_,_,_,_).

:-rec_prolog duplicate_in_coord/3.

%% This predicate is to avoid coord rerooting for cases like
%%   il parle et elle part
%% where the same role is duplicated in each coordinated
duplicate_in_coord( edge{ target => COO::node{} },
		    Type,
		    Pos ):-
	edge{ source => COO,
	      target => N,
	      label => coord3,
	      type => edge_kind[lexical,subst]
	    },
	( Pos=1 -> Arg1 = N
	; Pos=2 -> Arg2 = N
	; fail
	),
	relation{ type => Type,
		  arg1 => Arg1,
		  arg2 => Arg2
		}
	.

:-light_tabular nominal_head/2.
:-mode(nominal_head/2,+(+,-)).

nominal_head(_N1::node{}, N1 ) :-
	verbose('Search nominal head ~E\n',[_N1]),
	( node!empty(_N1) ->
	   edge{ source => _N1,
		 target => _N2::node{ cat => v },
		 label => 'SRel'
	       },
	  N1=node{ cat => prel },
	  ( edge{ source => _N2,
		  target => N1,
		  label => label[subject,impsubj]
		}
	  xor %% chain( _N2 >> adj >> node{ cat => 'S' } >> (lexical @ prel) >> N1)
	  edge{ source => _N2,
		target => _S::node{ cat => 'S' },
		type => adj
	      },
	    edge{ source => _S,
		  target => N1,
		  label => prel,
		  type => lexical
		}
	  )
	  xor edge{ source => _N1,
		    target => _N3,
		    label => quoted_N2,
		    type => subst
		  },
	    nominal_head(_N3,N1)
	  xor edge{ source => _N1,
		    target => _N3,
		    label => quoted_S,
		    type => subst
		  },
	    get_head(_N3,N1)
	;   
	    _N1 = N1
	),
	verbose('Found nominal head ~E => ~E\n',[_N1,N1]),
	true
	.

:-light_tabular relation/1.

relation(relation{ id => Label }) :- rel_gensym(Label).

:-std_prolog deep_subject/3.

deep_subject(N,Subj,L) :-
%%	format('start deep subject ~w\n',[N]),
	N::node{ cat => v },
	t_deep_subject(N,Subj,L),
%%	format('deep subject ~E sub=~E L=~w\n',[N,Subj,L]),
	true
	.

:-light_tabular t_deep_subject/3.
:-mode(t_deep_subject/3,+(+,-,-)).

t_deep_subject(N::node{ cat => v },Subj,L) :-
	edge{ source => N,
	      target => _Subj::node{ cluster => cluster{ right => Right }},
	      label => L::label[subject,impsubj]
	    },
	\+ (
	       chain( N >> (lexical @ 'CleftQue') >> Que ::node{ cat => prel, cluster => cluster{ left => Left } }),
	       Right =< Left
	   ),
	get_head(_Subj,Subj)
	.

t_deep_subject(N::node{ cat => v },Subj,L) :-
	edge{ source => N,
	      target => V::node{ cat => v},
	      label => label['V','S'],
	      type => adj
	    },
	%%	format('try climbing ~E => ~E\n',[N,V]),
	t_deep_subject(V,Subj,L)
	.

t_deep_subject(N::node{ cat => v}, Subj,L) :-
	edge{ source => N,
	      target => Aux::node{ cat => aux },
	      type => adj,
	      label => 'Infl'
	    },
	clitic_subj(Aux,Subj,L)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N3::node{ cat => cat[v,aux]},
	      target => _N1::node{ cat => _N1_Cat},
	      label => L::label[subject,impsubj] },
	( L = subject, _N1_Cat=cat[v,prep,csu] ->
	  %% to avoid a subject(manger,faut) in "il faut manger"
	  %% or in "il est recommand� de partir."
	  \+ edge{ label => impsubj, source => N3 }
	;
	  true
	),
	( _N1_Cat = cat[prep,csu] ->
	    chain( _N1 >> subst >> _N1_Head::node{} )
	;   
	    _N1_Head = _N1
	),
	nominal_head(_N1_Head,__N1),
	try_reroot_cleft(__N1,N3,N1),
%%	_N1=N1,
	(   %% recorded( terminal_v(N3,N2) )
	    verb_climbing(N3,N2)
	;
	    recorded(mode(robust)),
	    \+ verb_climbing(N3,_),
	    Cat1 = cat[cln,ilimp,caimp],
	    N2 = N3,
	    Left1 = Right2
	),
%%	format('SUBJ0 v=~E subj=~E\n',[N2,N1]),
	true
	.


relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	( edge{ source => _N2::node{ cat => adj },
		target => _N1,
		label => impsubj
	      }
	xor edge{ source => _N2,
		  target => _N1,
		  label => subject
		}
	),
	nominal_head(_N1,__N1),
	try_reroot_cleft(__N1,_N2,N1),
	verb_climbing(_N2,N2::node{ cat => cat[v,aux] })
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	%%	edge{ source => N3, target => _N1, label => label[subject,impsubj] },
	edge{ source => N3, target => N4, label => label['S','S2',vmod], type => adj },
	deep_subject(N3,_N1,_),
	node!empty(N4),
	edge{ source => N4, target => N5::node{ cat => v }, type => subst },
	\+ deep_subject(N5,_,_),
	verb_climbing(N5,N2),
	nominal_head(_N1,N1),
%%	format('SUBJA v=~E subj=~E\n',[N2,N1]),
	true
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => pro, tree => Tree },
	  arg2 => N2::node{}
	} :-
	edge{ source => N3,
	      target => N1,
	      label => label['S','S2','vmod'],
	      type => adj
	    },
	domain('pronoun_as_mod',Tree),
	verb_climbing(N3,N2)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	( deep_subject(_N2,N1,impsubj)
	xor deep_subject(_N2,N1,subject)
	),
	aux_climbing(_N2,N2)
	.

:-light_tabular clitic_subj/3.
:-mode(clitic_subj/2,+(+,-,-)).

clitic_subj(N1,N2::node{ cat => cln },L) :-
	edge{ source => N1,
	      target => N2,
	      label => _L::label[subject,impsubj],
	      type => lexical
	    },
	_L=L
	xor
	edge{ source => N1,
	      target => N3,
	      label => label['V','Infl'],
	      type => adj
	    },
	clitic_subj(N3,N2,L)
	.

:-light_tabular verb_climbing/2.
:-mode(verb_climbing/2,+(+,-)).

%% return all potential anchoring verb for a subject
%% example: il a voulu manger -> [a] and [manger]
verb_climbing(V,Anchor) :-
	(  
	   ( edge{ source => V,
		   target => Aux::node{ cat => cat[aux,v] },
		   label => 'Infl',
		   type => adj
		 } ->
	     verb_climbing(Aux,Anchor)
	   ;
	     V=Anchor
	   )
	;
	   edge{ source => V,
		 target => Modal::node{ cat => v },
		 label => 'V',
		 type => adj
	       },
	   verb_climbing(Modal,Anchor)
	),
%%	format('climbing ~E => ~E\n',[V,Anchor]),
	true
	.

:-light_tabular aux_climbing/2.
:-mode(aux_climbing/2,+(+,-)).

aux_climbing(V,Anchor) :-
	( edge{ source => V,
		target => Aux::node{ cat => cat[aux,v] },
		label => 'Infl',
		type => adj
	      } ->
	  aux_climbing(Aux,Anchor)
	;
	  V=Anchor
	)
	.

/*
%% A quoi sert la clause suivante ? pour les modaux
relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N2, target => _N1, label => label[subject,impsubj] },
	nominal_head(_N1,N1),
	\+ edge{ source => N2, target => node{ cat => aux }, label => 'Infl', type => adj },
%%	format('SUBJB v=~E subj=~E\n',[N2,N1]),
	true
	.
*/

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ lemma => N1_Lemma, form => N1_Form, cat => N1_Cat },
	  arg2 => N2::node{}
	} :-
	edge{ source => N3::node{ lemma => Lemma, id => N3Id },
	      target => _N2::node{},
	      label => xcomp },
	\+ edge{ source => _N2, label => label[subject,impsubj] },
	verbose('Trying ctrl verb: ~E ~E\n',[N3,_N2]),
	(   edge{ source => N3, target => N1, label => object }
	xor edge{ source => N3, target => _N1, label => preparg },
	    ( _N1=node{ cat => prep },
	      edge{ source => _N1, target => N1, type => subst }
	    xor N1 = _N1)
	xor deep_subject(N3,N1,subject),
	    %% edge{ source => N3, target => N1, label => label[subject] },
	    node2live_ht(N3Id,HTId),
	    %%	    format('SUJ-V CTR ~E ~E derivs=~w did=~w hid=~w\n',[N3,N2,Derivs,DId,HId]),
	    check_ht_feature(HTId,ctrsubj,suj),
	    Lemma \== '�tre'
	),
	\+ N1_Cat = cat[v,aux],
	\+ domain(N1_Lemma,[ce,cela]),
	aux_climbing(_N2,N2),
%%	format('SUBJC v=~E subj=~E\n',[N2,N1]),
	true
	.

relation{ type => 'SUJ-V',
	  arg1 => N1,
	  arg2 => XN2
	} :-
	E::edge{ source => N2::node{ cat => v },
		 target => N3::node{ cat => 'S', tree => Tree },
		 type => adj },
	domain('person_on_s',Tree),
	edge{ source => N3, target => N1, type => subst, label => 'N2' },
	verb_climbing(N2,XN2),
%%	format('SUBJD v=~E subj=~E\n',[N2,N1]),
	true
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ source => N1,
	      target => N3::node{},
	      type => adj,
	      label => 'N2'
	    },
	node!empty(N3),
	edge{ id => EId,
	      source => N3,
	      target => V::node{cat => v },
	      label => 'SubS',
	      type => subst
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,gerundive),
%%	(   recorded( terminal_v(V,N2) ) xor N2 = V )
	verb_climbing(V,N2),
%%	format('SUBJE v=~E subj=~E\n',[N2,N1]),
	true
	.

%% for "il sera � m�me de conduire"
%% the acomp arg introduces an infinitive
relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ label => comp,
	      source => V::node{ cat => v},
	      target => Comp,
	      type => subst
	    },
	edge{ label => xcomp,
	      source => Comp,
	      target => _N2
	    },
	edge{ label => prep,
	      source => Comp,
	      target => node{ cat => prep }},
	(  edge{ label => object,
		 target => N1,
		 source => V
	       } 
	xor
	   edge{ label => subject,
		 target => N1,
		 source => V
	       }
	),
	( node!first_main_verb(_N2,N2)
	xor N2=_N2
	)
	.

%% This rule should be tested !
%% it is not clear that the suject of a main clause
%% is also the subject of subordonate PVs
relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => XN2::node{}
	} :-
	edge{ label => label['S','S2','vmod'],
	      source => V::node{ cat => v},
	      target => Mod,
	      type => adj
	    },
	edge{ label => 'PP',
	      source => Mod,
	      target => Prep::node{ cat => prep }},
	\+ got_prep_locution(Prep),
	get_head(Prep,N2::node{ cat => v }),
	aux_climbing(N2,XN2),
	deep_subject(V,N1,subject),
	\+ deep_subject(N2,_,_)
	.

:-extensional prep_locution/3.

:-finite_set(prep_verb,[partir,compter,oublier,sortir,dire,passer,citer,mentionner,dater]).

prep_locution(�,prep_verb[partir,compter,sortir,dater],[]).
prep_locution(sans,prep_verb[compter,oublier,mentionner],[]).
prep_locution(pour,prep_verb[dire,passer,citer,mentionner],[]).

:-std_prolog got_prep_locution/1.

got_prep_locution( Prep::node{ cat => prep, lemma => Lemma} ) :-
	prep_locution(Lemma,VLemma,AuxPrep),
	edge{ source => Prep,
	      target => node{ cat => v, lemma => VLemma },
	      type => subst
	    }
	.

relation{ type => 'AUX-V',
	  arg1 => N1::node{ cat => aux},
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1, label => 'Infl' }
	.

relation{ type => Type,
	  arg1 => N1,
	  arg2 => N2,
	  arg3 => Arg3
	} :-
	E::edge{ source => N2::node{ id => N2Id},
		 target => _N1,
		 label => Label::label[object,ncpred,clg,clr],
		 type => edge_kind[subst,lexical]
	       },

	nominal_head(_N1,__N1),
	try_reroot_cleft(__N1,N2,N1),
	verbose('COD ~E ~E edge=~E\n',[N1,N2,E]),
	( _N1 = node{ cat => cla },
	  N2 = node{ lemma => Lemma },
	  domain(Lemma,[�tre,sembler,para�tre,demeurer,rester]) ->
	  %% this case would be better handled within the meta-grammar
	  Type = 'ATB-SO',
	  Arg3 = sujet
	; _N1 = node{ cat => clg },
	  node2live_ht(N2Id,HTId),
	  check_arg_feature(HTId,Arg::args[arg1,arg2],function,function[objde,att]) ->
	  Type = 'CPL-V'
	; _N1 = node{ cat => clr } ->
	  fail,			% not yet ready to be activated
	  node2live_ht(N2Id,HTId),
	  check_arg_feature(HTId,Arg,real,clr),
	  ( check_arg_feature(HTId,Arg,function,function[obj]) ->
	    Type = 'COD-V'
	  ; check_arg_feature(HTId,Arg,function,function[obj�]) ->
	    Type = 'CPL-V'
	  ;
	    fail
	  )
	; _N1 = node{ cat => prep } ->
	  %% accusative infinitive relatives
	  %% example: il a un livre � finir
	  fail
	;
	  Type = 'COD-V'
	),
	true
	.

relation{ type => Type,
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => v},
	  arg3 => Arg3
	} :-
	edge{ source => N2::node{ lemma => Lemma, id => N2Id },
	      target => _N1,
	      label => L::label[scomp,xcomp]
	    },
	try_reroot_cleft(_N1,N2,__N1),
	get_head(__N1,N1),
	( Lemma == �tre ->
	  Type = 'ATB-SO',
	  Arg3 = sujet
	; deep_subject(_N1,_,_) ->
	  Type = 'COD-V'
	;
	  ( \+ edge{ source => N2, label => object },
	    %%	  \+ edge{ source => N2, label => clr },
	    node2live_ht(N2Id,HTId),
	    ( check_xarg_feature( HTId,args[arg1,arg2],
				  obj,
				  fkind[scomp,vcomp,prepvcomp,prepscomp,whcomp,prepwhcomp,vcompcaus],
				  _
				)
	    )) ->
	  Type = 'COD-V'
	;
	  Type = 'CPL-V'
	)
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3, label => 'preparg' },
	edge{ source => N3, target => N1, label => 'S' },
	\+ edge{ source => N2, label => object }
	.

/*
relation{ type => 'COD-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3::node{ cat => prep, lemma => de}, label => preparg },
	edge{ source => N3, target => N1 },
	\+ edge{ source => N2, label => object }
	.
*/

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => cat[v,aux] },
	  arg2 => N2::node{ cat => v, id => N2Id }
	} :-
	edge{ source => _N1, target => N2, label => 'V' },
	\+ edge{ source => N2, label => object },
	node2live_ht(N2Id,HTId),
	check_arg_feature(HTId,args[arg1,arg2],function,function[obj]),
	( edge{ source => _N1, target => N1, label => 'Infl', type => adj },
	  N1 = node{ cat => cat[aux,v] }
	xor _N1=N1
	)
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v },
	  arg2 => N2::node{ cat => v, tree => Tree }
	} :-
	edge{ source => _N1::node{ cat => v },
	      target => N2,
	      type => adj,
	      label => 'S'
	    },
	domain('arg1:real_arg_xcomp_by_adj',Tree),
	node!first_main_verb(_N1,N1)
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N2,
	      target => _N1::node{ cat => v },
	      label => subject
	    },
	edge{ source => N2,
	      label => impsubj
	    },
	node!first_main_verb(_N1,N1)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3, lemma => Lemma3 },
	      label => label[preparg]
	    },
	( Cat3 = prep ->
	  /*
	    (	Lemma3 \== de 	% not true for all de-obj verb
	    xor edge{ source => N2, label => object }
	    ),
	  */
	  edge{ source => N3,
		target => _N1::node{ cluster => cluster{ right => R} },
		type => edge_kind[subst,lexical] },
	  try_reroot_cleft(_N1,N2,__N1)
	; Cat3 = cld ->
	  %% chain(N2 >> (subst @ comp) >> node{ cat => adj }),
	  has_cld(N2,_),
	  N3 = __N1
	;
	  N3 = __N1
	),
	get_head(__N1,N1)
	.

:-light_tabular try_reroot_cleft/3.

try_reroot_cleft( N::node{ cluster => cluster{ right => NR, left => NL  } },
		  V::node{},
		  M
		) :-
	( edge{ source => V,
		target => Que::node{ cat => prel,
				     cluster => cluster{ left => L }
				   },
		label => 'CleftQue'
	      },
	  chain( V >> (adj @ label['S','S2']) >> node{ cat => aux,
					   lemma => �tre,
					   cluster => cluster{ right => AuxR }
					 }),
	  NR =< L,
	  AuxR =< NL
	  ->
	  M = Que
	;
	  M = N
	)
	.


:-light_tabular get_head/2.
:-mode(get_head/2,+(+,-)).

get_head(N1,N3) :-
	( N1 = node{ cat => prep },
	  edge{ source => N1,
		target => N2,
		type => subst
	      }
	xor N1 = node{ cat => csu },
	  edge{ source => N1,
		target => N2,
		type => subst
	      }
	xor  node!empty(N1),
	  edge{ source => N1, target => _N2, type => subst },
	  get_head(_N2,N2)
	xor
	  N1 = N2
	),
	( node!first_main_verb(N2,N3)
	xor N3=N2
	)
	.


:-light_tabular get_head_no_climb/2.
:-mode(get_head_no_climb/2,+(+,-)).

get_head_no_climb(N1,N3) :-
	( N1 = node{ cat => prep },
	  edge{ source => N1,
		target => N2,
		type => subst
	      }
	xor N1 = node{ cat => csu },
	  edge{ source => N1,
		target => N2,
		type => subst
	      }
	xor  node!empty(N1),
	  edge{ source => N1, target => _N2, type => subst },
	  get_head_no_climb(_N2,N2)
	xor
	  N1 = N2
	),
	N3=N2
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat },
	      label => label['S','S2',vmod],
	      type => adj
	    },
	( node!empty(N3) ->
	    edge{ source => N3, target => N4::node{ cat => prep } },
	    true
	;   Cat = prep,
	    N3 = N4
	),
	(   \+ chain( N3 >> (lexical @ 'CleftQue') >> node{ cat => prel } ),
	    edge{ source => N4, target => N1, type => edge_kind[subst,lexical] }
	;
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] },
	    N1 = node{ cat => prel }
	),
	\+ node!empty(N1)
	.

%% For clefted sentence, such as c'est � Paul qu'il parle
%% the guide states that '� Paul' should be an ATB-SO
%% by many annotation samples use an CPL-V of �tre
relation{ type => 'CPL-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => aux }
	} :-
	(
	    chain( N2
		 << (adj @ 'S') << node{ cat => v }
		 >> (adj @ 'S') >> S::node{ cat => 'S' }
		 >> ( (lexical @ 'CleftQue') >> Que::node{ cat => prel,  cluster => cluster{ left => L} }
		    & (   subst @ 'PP') >> _N1::node{ cat => prep, cluster => cluster{ right => R} }
		    )
		 )
	;
	    chain( N2
		 << (adj @ 'S2') << V::node{ cat => v }
		 >> ( (lexical @ 'CleftQue') >> Que
		    &  (subst @ preparg) >> _N1
		    )
		 )
	),
	R =< L,
	get_head(_N1,N1)
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => v },
	  arg2 => N1::node{ cat => xnominal[] }
	} :-
	chain( _N2::node{ cat => v }
	     >> ( (adj @ 'S') >> node{ cat => 'S' }
		>> ( (lexical @ 'CleftQue') >> Que::node{ cat => prel}
		   & (subst @ 'PP') >> _N1::node{}
		   )
		;   ( (lexical @ 'CleftQue') >> Que
		    & (   subst @ label[preparg,subject,object,comp]) >> _N1
		    )
		)
	     ),
	precedes(_N1,Que),
	get_head(_N1,N1),
	node!first_main_verb(_N2,N2)
	.

relation{ type => 'ATB-SO',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => aux },
	  arg3 => sujet
	} :-
	chain( N2
	     << (adj @ 'S2') << node{ cat => v }
	     >> ( (lexical @ 'CleftQue') >> Que::node{ cat => prel,  cluster => cluster{ left => L} }
		& (subst @ label[object,subject]) >> _N1::node{ cluster => cluster{ right => R} }
		)
	     ),
        R =< L,
	get_head(_N1,N1)
	.

:-finite_set(progp,['o�','dont']).

relation{ type => 'CPL-V',
	  arg1 => N1::node{ cat => cat[pri,prel], lemma => progp[] },
	  arg2 => N2::node{ cat => v }
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','S2','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N1,
%%		  label => pri,
		  type => edge_kind[subst,lexical]
		},
	\+ edge{ source => N3,
		 target => node{ cat => prep }
	       }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[pri], lemma => Lemma },
	  arg2 => N2::node{ cat => v }
	} :-
	chain( N2 >> adj >> N1 ),
	pri(Lemma,GR)
	.

relation{ type => Type,	% participiales
	  arg1 => N1::node{ cat => cat[v] },
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{},
	      label => label['S','S2',vmod],
	      type => adj
	    },
	node!empty(N3),
	edge{ source => N3, target => _N1, type => subst, label => 'SubS' },
	node!first_main_verb(_N1,N1),
	( chain( N3 >> lexical >> node{ lemma => en } ) ->
	  Type = 'CPL-V'
	;
	  Type = 'MOD-V'
	)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1::node{ cat => cll }, label => cll }
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','S2','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N4::node{ cat => prep},
		  label => label['PP',wh], type => subst
		},
	\+ chain( N3 >> (lexical @ 'CleftQue' ) >> node{ cat => prel} ),
	E3::edge{ source => N4,
		  target => N1,
		  type => edge_kind[subst,lexical]
		},
	verbose('CPL-V ~E ~E ~E\n',[E1,E2,E3]),
	true
	.

relation{ type => 'CPL-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	chain( N2 >> (
		       (lexical @ impsubj) >> node{}
		     & (subst @ subject) >> node{ cat => prep } >> '$head' >> N1
		     )
	     )
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => cat[adv,advneg,nc] },
	  arg2 => N2::node{ cat => Cat }
	} :-
	edge{ source => _N2,
	      target => N1,
	      label => label['V','V1','S','v','S2','vmod',advneg], type => edge_kind[adj,lexical] },
	get_head_no_climb(_N2,N2),
	( Cat = cat[v,aux] ->
	    Type = 'MOD-V'
	; Cat = xnominal[] ->
	    Type = 'MOD-N'
	; Cat = cat[adv,advneg] ->
	    Type = 'MOD-R'
	; Cat = adj ->
	    Type = 'MOD-A'
	)
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => Cat::cat[adv,advneg], cluster => cluster{ left => L1, right => R1 }},
	  arg2 => N2::node{ cat => Cat2::cat[v,adj,adv], cluster => cluster{ left => L2, right => R2 } }
	} :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj] },
	(  R1 = L2, N2, \+ ( Cat=advneg, Cat2=cat[adv,adj] )
	xor L1 = R2, Cat2=v, N2
	xor L1 is R2+1, Cat2=v, 
	   node{ cat => cln, cluster => cluster{ left => R2, right => L1}},
	   N2
	),
	( Cat2 = cat[v,aux] -> Type = 'MOD-V'
	; Cat2 = adj -> Type = 'MOD-A'
	; Type = 'MOD-R'
	)
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N2,
	      target => N1,
	      label => advneg,
	      type => lexical
	    }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[pri,prel], lemma => Lemma },
	  arg2 => N2::node{ cat => v }
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','S2','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N1,
%%		  label => pri,
		  type => edge_kind[subst,lexical]
		},
	\+ edge{ source => N3,
		 target => node{ cat => prep }
	       },
	\+ Lemma = progp[]
	.

relation{ type => 'MOD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2::node{ cat => cat[v] },
	      target => N3::node{ cat => cat[csu] },
	      label => label['S','S2','vmod'],
	      type => adj },
	edge{ source => N3, target => _N1, label => label['SubS'], type => subst },
	get_head(_N1,N1)
	.

relation{ type => 'MOD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => V::node{ cat => v },
	      target => S::node{ cat => cat['VMod','S'] },
	      type => adj
	    },
	edge{ source => S,
	      target => N1,
	      type => subst,
	      label => time_mod
	    },
	get_head(V,N2)
	.

:-light_tabular node!first_main_verb/2.
:-mode(node!first_main_verb/2,+(+,-)).

node!first_main_verb( N1::node{cat => v},
		      N2::node{ cat => v }) :-
	( edge{ source => N1,
		target => _N2,
		label => 'V',
		type => adj
	      },
	  node!first_main_verb(_N2,N2)
	xor
	  N2 = N1
	).

relation{ type => 'COMP',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N3::node{ cat => cat[nc,v,adj]},
	      target => _N2,
	      label => label[scomp,xcomp],
	      type => subst
	    },
	edge{ source => N3, target => N1, label => label[csu], type => lexical },
	node!first_main_verb(_N2,N2)
	.

%% COMP pour csu sans que: quand il vient, il mange

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => cat[v,aux] }
	} :-
	edge{ source => N1,
	      target => _N2,
	      label => label['SubS','S','S2'],
	      type => subst
	    },
%%	recorded( terminal_v(_N2,N2) )
	node!first_main_verb(_N2,N2)
	.

%% COMP pour si and comme in whcomp

relation{ type => 'COMP',
	  arg1 => N1::node{ lemma => N1_Lemma },
	  arg2 => N2::node{ cat => cat[v,aux] }
	} :-
	domain(N1_Lemma,[si,comme]),
	chain( N1 << lexical << node{ cat => v } >> (subst @ xcomp) >> _N2::node{ cat => v } ),
	node!first_main_verb(_N2,N2)
	.

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => que },
	  arg2 => N2::node{}
	} :-
	edge{ target => Super::node{ cat => supermod },
	      label => supermod,
	      type => adj
	    },
	edge{ source => Super,
	      target => N1,
	      label => que,
	      type => lexical
	    },
	edge{ source => Super,
	      target => _N2::node{ cat => Cat },
	      label => 'Modifier',
	      type => subst
	    },
	get_head(_N2,N2)
	.

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => que },
	  arg2 => N2::node{ cat => v }
	} :-
	chain( N1 << lexical <<
	     node{ cat => aux, lemma => �tre } << (adj @ 'S') << N2 )
	.

%% The Passage manual is not very clear on cases such as
%% qu'est ce qu'il mange ?
%% we choose [qu']_1 as COD of mange and [qu']_2 as COMP
%% but another choice could be
%% [qu']_1 ATB-SO est, [qu']_2 COD of mange, and [qu']_2 MOD-N [qu']_1
%% corresponding as an wh-extraction of attribute from the cleft-extracted  COD.
relation{ type => 'COMP',
	  arg1 => Que::node{ cat => prel, lemma => que },
	  arg2 => V::node{ cat => v }
	} :-
	chain( Que << (lexical @ 'CleftQue') << V
	     >> _ >> X::node{ cluster => cluster{ right => XR }}
	     ),
	chain(V >> (adj @ 'S') >> node{ cat => aux,
					lemma => �tre,
					cluster => cluster{ left => AuxL }
				      }),
	XR =< AuxL
	.

%% COMP entre Prep et (GN GA ou NV) quand discontinus

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => prep, cluster => cluster{ right => Right1} },
	  arg2 => N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj,ilimp,caimp],
			    cluster => cluster{ id => CId2, left => Left2 }}
	} :-
	x_command(N1,_N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj,ilimp,caimp] }),
	/*
	\+ ( edge{ source => _N3,
		   target => N1,
		   label => preparg,
		   type => subst
		 },
	     edge{ source => _N3,
		   target => _N2,
		   label => label[xcomp,object,comp],
		   type => subst
		 }
	    ),
	*/
	( chain( N1 >> _ >> _N2)
	; chain( N1 << (lexical @ prep ) << node{} >> (subst @ xcomp) >> _N2)
	),
	verbose('Found pre potential COMP ~E ~E\n',[N1,_N2]),
	( _N2 = N2,
	  \+ '$answers'( group(const['GP','PV'],_,_,N1,_) )
	; edge{ source => _N2, target => COO::node{ cat => coo, lemma => COO_Lemma }, type => adj },
	  domain(COO_Lemma,[et,ou]),
	  edge{ source => COO,
		target => N2::node{ lemma => N2_Lemma },
		label => label[coord,coord2, coord3] },
	  %% avoid locution 'ou non' (should not occur)
	  \+ (COO_Lemma = ou, N2_Lemma=non)
	),
	Right1 =< Left2,
	verbose('Found potential COMP ~E ~E\n',[N1,N2]),
	'$answers'( group(const['GN','NV','GA'],_,_,N2,_) ),
	\+ ( '$answers'( group(const['GP','PV'],_,_,N1,Content) ),
	     verbose('Content ~E ~L\n',[N1,['~E ', ' '],Content]),
	     domain(CId2,Content)
	   )
	.

relation{ type => 'ATB-SO',
	  arg1 => YN1,
	  arg2 => N2::node{ cat => v},
	  arg3 => SO
	} :-
	edge{ source => N2, target => _N1::node{ cat => _Cat1}, label => comp },
	( _Cat1 = cat[comp,prep] ->
	  edge{ source => _N1, target => __N1::node{ cat => X}, type => subst }
	;
	  _N1 = __N1
	),
	( X = 'N2' ->
	  nominal_head(__N1,N1)
	;
	  __N1=N1
	),
	( edge{ source => N2, label => object } -> SO = objet ; SO = sujet ),
	try_reroot_cleft(N1,N2,XN1),
	get_head(XN1,YN1)
	.

relation{ type => 'ATB-SO',
	  arg1 => N2,
	  arg2 => N1,
	  arg3 => sujet
	} :-
	edge{ source => N2::node{ cat => adj },
	      target => N1::node{ cat => v },
	      label => 'Infl',
	      type => adj
	    }.

relation{ type => Type,
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::cat[adj,nc] },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => N1,
	      type => subst,
	      label => label[preparg,xcomp]
	    },
	( N2Cat = nc -> Type = 'MOD-N'
	; N2Cat = adj -> Type = 'MOD-A'
	; fail
	)
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::xnominal[] , xcat => N2XCat },
	  arg3 => faux
	} :-
	( edge{ source => N2,
		target => N3::node{ cat => Cat3::cat[~ coo] },
		%%	      type => edge_kind[adj,lexical]
		type => edge_kind[adj],
		label => Label1
	      },
	  (N2XCat='N2'
	  xor
	  %% case of on quantity-N2 modifying a prep
	  N2XCat=prep, N2Cat=nc
	  )
	;
	  N2Cat = ce,
	  chain( N2 << (lexical @ ce) << node{ cat => coo }
	       >> (adj @ vmod) >> node{} >> subst >> N3
	       )
	),
	( N2Cat = adj ->
	  \+ domain(Label1,['V','Infl']),
	  Type = 'MOD-A' ;
	  Type = 'MOD-N' ),
	( node!empty(N3) ->
	  %% Relatives, csu, participiales, ... 
	  edge{ source => N3, target => _N1, type => subst, label => Label },
	  ( _N1 = node{ cat => cat[v] },
	    node!first_main_verb(_N1,N1)
	  ; _N1 = node{ cat => cat[adj] } ->
	    edge{ source => _N1,
		  target => _V::node{ cat => v},
		  type => adj,
		  label => 'Infl'
		},
	    node!first_main_verb(_V,N1)
	  ; %% for cases like '3 enfants dont deux filles'
	    Label = 'N2Rel',
	    _N1=N1
	  )
	;   Cat3 = cat[prep,csu] ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	  \+ (Cat3 = np),
	  N3 = N1
	)
	.

/*
%% titles
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => title },
	  arg2 => N2::node{ cat => cat[nc,np,pro] },
	  arg3 => faux
	} :-
	chain( N2 >> ( lexical @ _ ) >> N1 )
	.
*/

%% Predet (to be checked)
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => predet },
	  arg2 => N2::node{ cat => xnominal[nc,pro,np,adj] },
	  arg3 => faux
	} :-
	(
	 chain( N2 >> (subst @ det) >> N1 )
	;
	 chain( N2 >> (subst @ det) >> node{ cat => det } >> (adj @ det) >> N1 )
	)
	.

%% Special cases for ce
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::xnominal[ce], xcat => 'N2' },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => _N1::node{ cat => Cat3::cat[~ coo] },
	      label => 'SRel',
	      type => edge_kind[subst]
	    },
	node!first_main_verb(_N1,N1)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::xnominal[ce] },
	  arg3 => faux
	} :-
	chain( N2 << (lexical @ coord3) << node{ cat => coo } >> adj >> _N1::node{} ),
	get_head(_N1,N1)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj,adv] },
	  arg2 => N2::node{ cat => ncpred },
	  arg3 => faux
	} :-
	edge{ source => N3::node{ cat => v},
	      target => N1,
	      type => adj,
	      label => ncpred
	    },
	edge{ source => N3,
	      target => N2,
	      type => lexical,
	      label => ncpred
	    }
	.

%% Case for genitives through N2Rel
%% il a deux enfants dont trois filles
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => prel },
	  arg2 => N2::node{ cat => N2Cat::xnominal[], xcat => 'N2' },
	  arg3 => faux
	} :-
	edge{ source => N3, target => N1, label => prel },
	edge{ source => N3, target => N2, label => 'N2Rel' }
	.

%% Case for adj_on_s
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj] },
	  arg2 => N2::node{},
	  arg3 => faux
	} :-
	edge{ target => N1,
	      type => adj,
	      source => V::node{ cat => cat[v] }
	    },
	deep_subject(V,N2,subject)
	.

:-extensional det_as_modn/1.

det_as_modn(number[]).
det_as_modn('quelque').
det_as_modn('diff�rent').
det_as_modn('divers').
%% det_as_modn('chaque').
%% det_as_modn('tout').
%det_as_modn('tel').
%det_as_modn('telle').
%det_as_modn('telles').
%det_as_modn('tels').

relation{ type => Type,
	  arg1 => N1::node{ cat => cat[det,number,adj], lemma => Lemma },
%	  arg2 => N2::node{ cat => N2Cat::xnominal[] , xcat => 'N2' },
	  arg2 => N2::node{ cat => N2Cat::xnominal[] },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => _N1::node{ cat => cat[det,number], lemma => _Lemma },
	      type => subst,
	      label => det
	    },
	( N1 = _N1,
	  det_as_modn(Lemma)
	;
	  edge{ source => _N1,
		target => N1,
		type => edge_kind[lexical,adj]
	      },
	  %%	  \+ domain(Lemma,[tout]), %tout seems to be a special case
	  true
	),
	( N2Cat = adj -> Type = 'MOD-A' ; Type = 'MOD-N' ),
	true
	.

relation{ type => 'MOD-N',
	  arg1 => N1,
	  arg2 => N1,
	  arg3 => vrai
	} :-
	N1::node{ cat => np, cluster => cluster{ left => Left, right => Right} },
	Left + 1 < Right
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => terminal[] },
	  arg2 => N1::node{ cat => nominal[] },
	  arg3 => faux
	} :-
	edge{ source => N1, target => N2::node{ cat => np }, label => 'np',
	      type => edge_kind[~ [lexical]]
	    },
	\+ edge{ source => N1, label => 'MLex', type => lexical }
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{},
	  arg2 => N1::node{},
	  arg3 => faux
	} :-
	fail,
	edge{ source => V::node{cat => v},
	      target => S::node{cat => cat['S','VMod']},
	      type => adj,
	      label => label['S','vmod','S2'] },
	edge{ source => S,
	      target => N2::node{ cat => v },
	      label => 'SubS',
	      type => 'subst',
	      id => EId
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle]),
	deep_subject(V,N1,subject)
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => que_restr,
			    cluster => cluster{ right => R }},
	  arg2 => N3
	} :-
	'$answers'( group(GType,R,_,N2,Content) ),
	N1,
	( GType = 'GN' -> Type = 'MOD-N', N3=N2
	; GType = 'GA' -> Type = 'MOD-A', N3=N2
	; GType = 'GR' -> Type = 'MOD-R', N3=N2
	; GType = 'NV' -> Type = 'MOD-V', N3=N2
	; GType = 'GP' -> Type = 'MOD-N',
	  ( edge{ source => N2,
		  target => N3,
		  type => subst
		}
	  xor edge{ source => V,
		    target => N1
		  },
	    edge{ source => V,
		  target => N3::node{ cluster => cluster{ id=> N3_CId } }
		},
	    domain(N3_CId,Content)
	  )
	; GType = 'PV' -> Type = 'MOD-V',
	  ( edge{ source => N2,
		  target => N3,
		  type => subst
		}
	  xor edge{ source => V,
		    target => N1
		  },
	    edge{ source => V,
		  target => N3::node{ cluster => cluster{ id=> N3_CId } }
		},
	    domain(N3_CId,Content)
	  )
	; fail
	).

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj], cluster => cluster{ left => L1, right => R1 }},
	  arg2 => N2::node{ cat => cat[nc], cluster => cluster{ left => L2, right => R2 } }
	} :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj,lexical] },
	(  R1 = L2, N2
	xor L1 = R2, N2
	)
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => nominal[] },
	  arg2 => N1::node{ cat => terminal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[], lemma => number[] },
	      label => 'Nc2',
	      type => lexical
	    }
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => nominal[] },
	  arg2 => N1::node{ cat => terminal[], lemma => Lemma1 }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[] },
	      label => 'Np2',
	      type => lexical
	    },
	\+ appos_function(Lemma1)
	.

relation{ type => 'MOD-N',
	  arg1 => Adj::node{ cat => cat[adj] },
	  arg2 => N::node{ cat =>nominal[] }
	} :-
	chain( Adj << (lexical @ label[predet_ante,predet_post]) << N )
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adj] , xcat => cat[~ ['N2']] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj,
	      label => Label
	    },
	\+ domain(Label,['V','Infl']),
	( node!empty(N3) ->
	    %% not sure it may arise for adjectives !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

%% For impersonal adjectival construction such as
%% il est judicieux qu'il vienne
%% 'qu'il vienne' is a deep subjet but a shallow MOD-A
relation{ type => 'MOD-A',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => adj }
	} :-
	edge{ source => N2,
	      label => impsubj
	    },
	edge{ source => N2,
	      target => _N1,
	      label => subject
	    },
	get_head(_N1,N1)
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => cld, form => Form },
	  arg2 => N2::node{ cat => adj }
	} :-
	\+ Form = y,
	chain( N2 >> (lexical @ preparg) >> N1)
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => cld, form => Form },
	  arg2 => N2::node{ cat => adj }
	} :-
	\+ Form = y,
	chain( N2 << (subst @ comp) << V::node{ cat => v } >> (lexical @ preparg) >> N1 ),
	\+ has_cld(V,_)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cld, form => Form },
	  arg2 => N2::node{}
	} :-
	\+ Form = y,
	chain( N2 << (_ @ object) << V::node{ cat => v } >> (lexical @ preparg) >> N1 ),
	\+ has_cld(V,_)
	.

:-light_tabular has_cld/2.
:-mode(has_cld/2,+(+,-)).

has_cld(V::node{ id => NId },Arg) :-
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],obj�,_,cld[])
	.

relation{ type => 'MOD-R',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adv,advneg] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj
	    },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{
	 type => 'MOD-R',
	 arg1 => N1::node{ cat => terminal[] },
	 arg2 => N2::node{ cat => cat[adv,advneg] }
	} :-
	( edge{ source => N2,
		target => N1,
		type => lexical,
		label => void
	      },
	  N1 = node{ cat => pro, form => ce }
	xor
	edge{ source => N2,
	      target => N3,
	      type => subst,
	      label => Label::label[preparg,xcomp]
	    },
	  ( Label = xcomp ->
	    N3 = N1
	  ;
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	  )
	).
	
relation{ type => 'MOD-R',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adv] }
	} :-
	edge{ source => N2,
	      target => Super::node{ cat => supermod },
	      label => supermod,
	      type => adj
	    },
	(
	 edge{ source => Super,
	       target => CSU::node{ cat => csu }
	     },
	 edge{ source => CSU,
	       target => N1,
	       type => edge_kind[subst,lexical]
	     }
	;
	 edge{ source => Super,
	       target => N1,
	       label => 'Modifier',
	       type => subst
	     }
	)
	.

relation{ type => 'MOD-P',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[prep] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for prepositions !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for prepositions !
	  %% edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	  N3=N1
	;   Cat3 = adv ->
	  \+ ( edge{ source => node{ cat => cat[v,'S','VMod'] },
		     target => N2
		   }
	     ),
	  N3 = N1
	;
	  N3 = N1
	)
	.


relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => adv },
	  arg2 => N2::node{ cat => v}
	} :-
	( edge{ source => _N2::node{ cat => cat[v] },
		target => P::node{ cat => prep }
	      }
	;
	  edge{ source => _AUX::node{ cat => cat['S','VMod'] },
		target => P::node{ cat => prep }
	      },
	  edge{ source => _N2,
		target => _AUX,
		type => adj
	      }
	),
	edge{ source => P,
	      target => N1,
	      type => adj,
	      label => 'PP'
	    },
	node!first_main_verb(_N2,N2)
	.

%% MOD-V are also present when a floating adv is attached to a coo
relation{ type => Type,
	  arg1 => Mod::node{},
	  arg2 => N::node{ cat => Cat }
	} :-
	edge{ source => COO::node{ cat => coo },
	      target => _Mod::node{ cluster => cluster{ right => _Mod_Right }},
	      type => edge_kind[adj,lexical],
	      label => label[coo,pas]
	    },
	edge{ source => COO,
	      target => _N::node{ cluster => cluster{ left => _N_Left }},
	      label => label[coord2,coord3,coord],
	      type => edge_kind[subst,lexical]
	     },
	_Mod_Right =< _N_Left,
%%	format('potential mod on coo ~E and ~E\n',[_Mod,_N]),
%%	climb_till_condition(N,V),
	get_head(_Mod,Mod),
	get_head(_N,N),
	( Cat = v ->
	    Type = 'MOD-V'
	; Cat = adv ->
	    Type = 'MOD-R'
	; Cat = xnominal[] ->
	    Type = 'MOD-N'
	)
	.

%% MOD-R in superlative with 'possible'
relation{ type => 'MOD-R',
	  arg1 => Mod::node{ lemma => possible },
	  arg2 => Adv::node{ cat => adv }
	} :-
	chain(Adv >> (adj @ supermod) >> node{ cat => supermod } >> lexical >>  Mod)
	.

:-std_prolog climb_till_condition/2.

climb_till_condition(N::node{}, V::node{}) :-
	( N=V
	xor
	  chain( N << _ << _N::node{}),
	  climb_till_condition(_N,V)
	)
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => vide,
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{ cat => 'S' },
	      target => Coord::node{ cat => cat[coo] },
	      type => lexical,
	      label => starter
	    },
	edge{ source => N3,
	      target => Start,
	      type => adj
	    }
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => XN2::node{},
	  arg3 => XN3::node{}
	} :-
	edge{ source => Start::node{},
	      target => LastCoord::node{ cat => cat[coo] },
	      type => adj,
	      label => StartLabel
	    },
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	\+ node!empty(Coord),
	( node!empty(_N2) ->
	  edge{ source => _N2,
		target => N2,
		type => edge_kind[subst,lexical]
	      },
	  N2=node{ cat => cat[v,prep,nc,np] }
	; _N2 = Start, StartLabel = xcomp ->
	  %% coordination on sentential argument of a verb
	  %% need to follow an indirection to retrieve the argument
	  edge{ source => Start,
		target => N2,
		label => StartLabel,
		type => edge_kind[~ [adj]]
	      }
	;
	  N2 = _N2
	),
	( node!first_main_verb(N2,XN2)
	xor get_head(N2,XN2)
	),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	%%( node!first_main_verb(N3,XN3) xor N3=XN3 ),
	get_head(N3,XN3),
	true
	.

/*
relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => XN2::node{},
	  arg3 => XN3::node{}
	} :-
  */
relation{ type => 'JUXT',
	  arg1 => XN2::node{},
	  arg2 => XN3::node{}
	} :-
	edge{ source => Start::node{},
	      target => Enum::node{ tree => Tree },
	      type => adj
	    },
	domain('N2_enum',Tree),
	LastCoord = Enum,
%	format('test enum lastcord=~w\n',[LastCoord]),
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord }
	),
	coord_next(LastCoord,_N2,Coord),
%	format('phase1 next2=~w coord=~w\n',[_N2,Coord]),
	coord_next(LastCoord,Coord,_N3),
%	format('phase2 coord=~w next3=~w\n',[Coord,_N3]),
	( node!empty(_N2) ->
	  edge{ source => _N2,
		target => N2,
		type => edge_kind[subst,lexical]
	      },
	  N2=node{ cat => cat[v,prep,nc,np] }
	;   
	  N2 = _N2
	),
	( node!first_main_verb(N2,XN2) xor N2=XN2 ),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	( node!first_main_verb(N3,XN3) xor N3=XN3 ),
	XN2 \== XN3,
	true
	.

:-std_prolog coord_next/3.

coord_next( LastCoord::node{ cat => LastCat, cluster => cluster{ left => LastLeft }},
	    Start::node{ cluster => cluster{ right => StartRight }},
	    Next::node{ id => NId,
			cluster => cluster{ left => NextLeft,
					    right => NextRight,
					    token => NextToken
					  }}
	  ) :-
	verbose('Search coord next: ~E ~E\n',[Start,LastCoord]),
	(   Next = LastCoord,
	    \+ node!empty(Next)
	;   edge{ source => LastCoord,
		  target => Next,
		  label => label[coord2,coord3,coord,xcomp],
		  type => edge_kind[~ [adj]]
		}
	;  edge{ source => LastCoord,
		 target => Next,
		 label => void
	       },
	    ( LastCat == coo -> NextRight < LastLeft ; true),
	    NextToken = (','),
%%	    format('last=~E start=~E next=~E\n',[LastCoord,Start,Next]),
	    true
	),
	NextLeft >= StartRight,
	verbose('Potential next=~w\n',[Next]),
	\+ ( ( Edge2::edge{ source => LastCoord,
			    target => Next2::node{ id =>NId2,
						   cluster => cluster{ token => NextToken2,
								       left => NextLeft2,
								       right => NextRight2 }},
			    label => Label2::label[void,coord2,coord3,coord] },
	       NId2 \== NId,
	       NextLeft2 >= StartRight,
	       NextRight2 =< NextLeft,
	       ( Label2 = void ->
		 NextToken2 = (','),
		 %% LastCoord may be preceded by a coma
		 \+ ( NextRight2 = NextLeft,
		      Next = LastCoord
		    )
	       ;
		 true
	       ),
	       /*
	       \+ ( %% LastCoord may be preceded by a coma
		    Label2 = void,
		    NextRight2 = NextLeft,
		    Next = LastCoord
		  ),
	       */
	       verbose('coord discard case1 next=~w next2=~w\n',[Next,Next2]),
	       true
	     ;	
		 Next2 = LastCoord,
	       \+ node!empty(Next2), % not the enum case but a true coord
	       NextLeft2 >= StartRight,
	       NextRight2 =< NextLeft,
	       verbose('coord discard case2 next=~w next2=~w\n',[Next,Next2]),
	       true
	     )
	   ),
	verbose('\n\t ~E => ~E\n',[Start,Next]),
	true
	. 

relation{ type => 'APPOS',
	  arg1 => N2::node{ cat => terminal[] },
	  arg2 => N1::node{ cat => nominal[] }
	} :-
	edge{ source => _N2, target => N3::node{ cat => 'N2' }, type => adj },
	node!empty(N3),
	edge{ source => N3, target => N1, type => subst, label => 'N2app' },
	nominal_head(_N2,N2),
	true
	.

/*
relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => nominal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => np },
	      label => 'np',
	      type => lexical
	    },
%%	edge{ source => N1, label => 'MLex', type => lexical },
	true
	.
*/

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => Cat1::nominal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => np },
	      label => 'N2',
	      type => adj
	    },
	Cat1 = nc,
%%	edge{ source => N1, label => 'MLex', type => lexical },
	true
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[], lemma => N2L },
	      label => 'Nc2',
	      type => lexical
	    },
	\+ N2L=number[]
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[], lemma => Lemma1 },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[] },
	      label => 'Np2',
	      type => lexical
	    },
	appos_function(Lemma1)
	.

%% N_1->S_2 S_2 S_2->+N_2
%% or S_1 ->+ N_1 S_1 -> S_2 S_2->+N_2
relation{ type => 'JUXT',
	  arg1 => N1::node{ },
	  arg2 => N2::node{ }
	} :-
	edge{ source => S1::node{ cat => CatS1 },
	      target => S2::node{ cat => cat['S'] },
	      label => label['S','S2'],
	      type => adj
	    },
	node!empty(S2),
	edge{ source => S2, target => _N2::node{ cat => _Cat2 }, type => subst, label => label['S'] },
	edge{ source => S2,
	      target => N4::node{ cluster => cluster{ token => Token }},
	      type => lexical },
	domain(Token,[',',';','.',':']),
	( _Cat2 = cat[v] -> recorded(terminal_v(_N2,N2 ))
	; _Cat2 = cat['S'],
	  edge{ source => _N2, target => Comp2, type => subst, label => comp },
	  edge{ source => Comp2, target => N2, type => subst, label => label[comp,'N2'] }
	),
	( CatS1 = cat[v] -> recorded(terminal_v(S1,N1 ))
	; CatS1 = cat['S'],
	  edge{ source => S1, target => Comp1, type => subst, label => comp },
	  edge{ source => Comp1, target => N1, type => subst, label => label[comp,'N2'] }
	)
	.

relation{ type => 'JUXT',
	  arg1 => First,
	  arg2 => Second
	} :-
	edge{ source => N2::node{ cat => cat[v] ,
				  cluster => cluster{ left => L2}},
	      target => N3::node{ cat => cat['S','VMod'] },
	      type => adj,
	      label => label['S','S2',vmod]
	    },
	edge{ source => N3,
	      target => N1::node{ %% cat => xnominal[],
				  cluster => cluster{ left => L1 }
				},
	      type => subst,
	      label => label[person_mod,audience,reference,'S_incise',position]
	    },
	node!first_main_verb(N2,_N2),
	( L1 < L2 ->
	  First =N1, Second = _N2
	;
	  First = _N2, Second = N1
	)
	.

relation{ type => 'JUXT',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	fail,			% handled as a coord (see above)
	edge{ source => N1,
	      target => N3,
	      type => adj,
	      label => 'N2'
	    },
	edge{ source => N3,
	      target => N2,
	      type => subst,
	      label => coord
	    }
	.

%%% Intertoken relations
%% for instance, for 'dimanche prochain'

relation{ type => 'MOD-N',
	  arg1 => f(FId1),
	  arg2 => f(FId2)
	} :-
	fail,
	opt(passage),
	N::node{ cat => nc,
		 lemma => date[],
		 cluster => cluster{ id => CId }
	       },
	f{ cid => CId, id => FId1, lex => Lex, rank => R },
	domain(Lex,[ suivant,suivante,suivants,
		     prochain,prochaine,prochains,
		     dernier,derni�re,derniers
		   ]),
	S is R-1,
	f{ cid => CId, id => FId2, rank => S },
	format('Intertoken ~E fid1=~w lex=~w rank=~w fid2=~w\n',[N,FId1,Lex,R,FId2]),
	true
	.


%% JUXT: to be done

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Decoding relations

:-extensional xrelation/2.

xrelation( relation{ type => Type::'SUJ-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(sujet,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'AUX-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(auxiliaire,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(cod,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'CPL-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(complement,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COMP', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arghead(complementeur,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'ATB-SO', arg1 => Id1, arg2 => Id2, arg3 => SO, id => Id },
	   xrelation(Type,Id,[ arg(attribut,Id1), arg(verbe,Id2), so(SO) ] )
	 ).

xrelation( relation{ type => Type::'MOD-N', arg1 => Id1, arg2 => Id2, arg3 => Prop, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(nom,Id2), 'a-propager'(Prop) ] )
	 ).

xrelation( relation{ type => Type::'MOD-A', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(adjectif,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-R', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(adverbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-P', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(preposition,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COORD', arg1 => Id1, arg2 => Id2, arg3 => Id3, id => Id },
	   xrelation(Type,Id,[ arg(coordonnant,Id1),arg('coord-g',Id2), arg('coord-d',Id3) ] )
	 ).

xrelation( relation{ type => Type::'APPOS', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(appose,Id2) ] )
	 ).

xrelation( relation{ type => Type::'JUXT', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(suivant,Id2) ] )
	 ).

:-rec_prolog f2groups/3.

f2groups([],[],_).

f2groups([Arg|Args],[XArg|XArgs],All) :-
	f2groups(Args,XArgs,All),
	(   Arg = arg(Type,FId),
	    f2group(FId,GId),
	    \+ ( f2group(FId2,GId),
		 FId2 \== FId,
		 domain( arg(_,FId2), All) ) ->
	    XArg = arg(Type,GId)
	; Arg = arghead(Type,FId) ->
	    XArg = arg(Type,FId)
	;   
	    XArg = Arg
	)
	.

/*
f2groups([Arg|Args],[XArg|XArgs]) :-
	f2groups(Args,XArgs),
	( Arg = arghead(Type,FId) ->
	  XArg = arg(Type,FId)
	;   
	  XArg = Arg
	)
	.
*/


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

:-std_prolog
	xevent_process/4.

:-rec_prolog
	xevent_process/5.

xevent_process(Handler,Event,Ctx,OrigHandler) :-
        ( Event = [_Event|Events2] ->
%%          format('Event process: handler=~w Event=~w\n',[Handler,_Event]),
            xevent_process(OrigHandler,_Event,Ctx,OrigHandler),
%%          format('Event processed: handler=~w Event=~w\n',[Handler,_Event]),
            xevent_process(OrigHandler,Events2,Ctx,OrigHandler)
%%          every((domain(_Event,Event), event_process(Handler,_Event,Ctx)))
        ;   Event == [] ->
            true
        ;
            mutable_read(Ctx,Ctx1),
            (	( xevent_process(Handler,Event,Ctx1,Ctx2,OrigHandler)
                xor event_super_handler(Handler,Super_Handler),
                    xevent_process(Super_Handler,Event,Ctx1,Ctx2,OrigHandler)),
                mutable(Ctx,Ctx2)
            xor true            % 'do nothing' as Default handler
            )
        )
        .

event_process(Handler,Event,Ctx1,Ctx2) :-
	(   xevent_process(Handler,Event,Ctx1,Ctx2,Handler)
	xor  event_super_handler(Handler,Super_Handler),
	    (	xevent_process(Super_Handler,Event,Ctx1,Ctx2,Handler)
	    xor event_process(Super_Handler,Event,Ctx1,Ctx2)
	    )
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing DEP XML objects

event_process( H::default(Stream), depclusters, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( C::cluster{},
		event_process(depxml(Stream), C, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       cluster{ id => Id,
			left => Left,
			right => Right,
			token => Token,
			lex => Lex
		      },
	       Ctx1,
	       Ctx2
	     ) :-
	(Id = root ->
	 Ctx1 = Ctx2
	;
	 cid2xmlid(Id,CId),
	 event_process(default(Stream),
		       element{ name => cluster,
				attributes => [ id: CId,
						left: Left,
						right: Right,
						token: Token,
						lex: Lex
					      ]
			      },
		       Ctx1,
		       Ctx2
		      )
	).
	
event_process( H::default(Stream), depnodes, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( N::node{}, event_process(depxml(Stream), N, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       node{ id => Id,
		     cat => Cat,
		     xcat => XCat,
		     tree => Tree,
		     lemma => Lemma,
		     lemmaid => LemmaId,
		     deriv => Derivs,
		     cluster => cluster{ id =>CId },
		     form => _Form,
		     w => W
		   },
	       Ctx1,
	       Ctx2
	     ) :-
	( Id = root ->
	  Ctx1 = Ctx2
	;
	  name_builder('~L',[['~w',' '],Tree],L_Tree),
	  nid2xmlid(Id,NId),
	  cid2xmlid(CId,XCId),
	  (_Form = ellipsis(Form) xor _Form = Form),
	  Attrs =  [ id: NId,
		     cat: Cat,
		     (tree): L_Tree,
		     lemma: Lemma,
		     lemmaid: LemmaId,
		     cluster: XCId,
		     form: Form
		   ],
	  ( var(XCat) -> Attrs1=Attrs ; Attrs1 = [xcat:XCat|Attrs]),
	  ( Derivs = [[]] ->
	    Attrs2 = Attrs1
	  ;
	    mutable(MWAll,[],true),
	    ( recorded(disambiguated) ->
	      mutable(M,[],true),
	      every((
		     node2live_deriv(Id,DId),
		     mutable_list_extend(M,DId),
		     recorded(opt(cost)),
		     %% format('emit did ~w ~w\n',[Id,DId]),
		     deriv2best_parse(DId,dstruct{ w => _W, constraint => _Cst }),
		     mutable_list_extend(MWAll,_W)
		    )),
	      mutable_read(M,Derivs2)
	    ;
	      Derivs2 = Derivs
	    ),
	    ( Derivs2 = [_|_] ->
	      name_builder('~L',[['d~w',' '],Derivs2],L_Derivs),
	      _Attrs2 = [deriv:L_Derivs|Attrs1],
	      ( mutable_read(MWAll,WAll2::[_|_]) ->
		name_builder('~L',[['~w',' '],WAll2],L_WAll),
		Attrs2 = [wall:L_WAll|_Attrs2]
	      ;
		Attrs2 = _Attrs2
	      )
	    ;
	      Attrs2=Attrs1
	    )
	  ),
	  ( W=[] ->
	    Attrs3 = Attrs2
	  ;	
	    name_builder('~U',[['~w:~w',' '],W],L_W),
	    %%		format('Emitting attr ~w => ~w\n',[W,L_W]),
	    Attrs3=[w:L_W|Attrs2]
	  ),
	  event_process(default(Stream),
			element{ name => node,
				 attributes => Attrs3
			       },
			Ctx1,
			Ctx2
		       )
	).

event_process( H::default(Stream), depedges, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( E::edge{}, event_process(depxml(Stream), E, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

:-light_tabular cid2xmlid/2.
:-mode(cid2xmlid/2,+(+,-)).

cid2xmlid(CId,XCId) :-
	sentence(SId),
	cluster{ id => CId, left => Left, right => Right },
	name_builder('~wc_~w_~w',[SId,Left,Right],XCId)
	.


:-light_tabular nid2xmlid/2.
:-mode(nid2xmlid/2,+(+,-)).

nid2xmlid(NId,XNId) :-
%%	\+ var(NId),
	sentence(SId),
	name_builder('~wn~w',[SId,NId],XNId)
	.

:-light_tabular eid2xmlid/2.
:-mode(eid2xmlid/2,+(+,-)).

eid2xmlid(EId,XEId) :-
	sentence(SId),
	name_builder('~we~w',[SId,EId],XEId)
	.

:-light_tabular oid2xmlid/2.
:-mode(oid2xmlid/2,+(+,-)).

oid2xmlid(OId,XOId) :-
	sentence(SId),
	name_builder('~wo~w',[SId,OId],XOId)
	.

:-light_tabular hid2xmlid/2.
:-mode(hid2xmlid/2,+(+,-)).

hid2xmlid(HId,XHId) :-
	sentence(SId),
	name_builder('~wht~w',[SId,HId],XHId)
	.

event_process( H::depxml(Stream),
	       E::edge{ id => Id,
		     source => node{ id => SId },
		     target => node{ id => TId },
		     type => Type,
		     label => Label,
		     deriv => Derivs
		   },
	       Ctx1,
	       Ctx2
	     ) :-
	( Id = root(_) ->
	  Ctx1 = Ctx2
	;
	  (edge_cost(Id,_W,_Vector,_Cst) ->
	   _Attrs = [w: _W,
		     ws: _Vector]
	  ;
	   _Attrs=[]
	  ),
%%	  ( (var(SId) xor var(TId)) -> format('pb edge ~w\n',[E]) ; true ),
	  eid2xmlid(Id,EId),
	  nid2xmlid(SId,NSId),
	  nid2xmlid(TId,NTId),
	  Attrs =  [ id: EId,
		     source: NSId,
		     target: NTId,
		     type: Type,
		     label: Label
		   | _Attrs
		   ],
	  event_process(default(Stream),
			start_element{ name => edge,
				       attributes => Attrs
				     },
			Ctx1,
			Ctx3
		       ),
	  mutable(Ctx,Ctx3,true),
	  every(( domain(Deriv,Derivs),
		  event_process(H,deriv(Deriv,Id),Ctx)
		)),
	  mutable_read(Ctx,Ctx4),
	  event_process(default(Stream), end_element{ name => edge },
			Ctx4,
			Ctx2)
	).

event_process( H::depxml(Stream), deriv(DId,EId), Ctx1, Ctx2 ) :-
	deriv(DId,EId,XSpan,SOP,TOP),
	(recorded(disambiguated) -> recorded( keep_deriv(DId) ) ; true),
	( recorded(reroot_source(EId,XSpan,NId,_NId)),
	  edge{ id => EId, source => node{ id => NId } } ->
	  nid2xmlid(_NId,_XNId),
%	  format('EMIT REROOT _nid=~w _xnid=~w\n',[_NId,_XNId]),
	  _Attrs = [reroot_source : _XNId]
	;
	  _Attrs = []
	),
	name_builder('~L',[['~w',' '],XSpan],Span),
	name_builder('d~w',[DId],XDId),
	oid2xmlid(SOP,XSOP),
	oid2xmlid(TOP,XTOP),
	Attrs = [ names: XDId,
		  source_op: XSOP,
		  target_op: XTOP,
		  span: Span
		| _Attrs
		],
	event_process(default(Stream),
		      element{ name => deriv,
			       attributes => Attrs
			     },
		      Ctx1,
		      Ctx2
		     )
	.

event_process( H::default(Stream), depops, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( O::op{},
		event_process(depxml(Stream), O, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       op{ id => Id,
		   span => XSpan,
		   cat => Cat,
		   deriv => Derivs,
		   top => Top,
		   bot => Bot
		 },
	       Ctx1,
	       Ctx2
	     ) :-
	( recorded(disambiguated) ->
	  mutable(M,[],true),
	  every((
		 alive_deriv(Derivs,DId),
		 mutable_list_extend(M,DId)
		)),
				%	mutable_read(M,Derivs2::[_|_]),
	  mutable_read(M,Derivs2)
	;
	  Derivs2 = Derivs
	),
	name_builder('~L',[['d~w',' '],Derivs2],L_Derivs),
	oid2xmlid(Id,OId),
	( Top = [] -> Content1 = []
	; Content1 = [narg(top,Top)]
	),
	( Bot = [] -> Content = Content1
	; Content = [narg(bot,Bot)|Content1]
	),
	name_builder('~L',[['~w',' '],XSpan],Span),
	( Content = [] ->
	  event_process(default(Stream),
			element{ name => op,
				 attributes => [ id:OId,
						 cat: Cat,
						 span: Span,
						 deriv: L_Derivs
					       ]
			       },
			Ctx1,
			Ctx2
		       )
	;
	 fs_analyze(Content,Bindings),
%%	 format('\nXML FS ctx1=~w content=~w bindings=~w\n',[Ctx1,Content,Bindings]),
%%	 verbose('FS content: ~w\n',[Content]),
	  event_process(default(Stream),
			start_element{ name => op,
				       attributes => [ id:OId,
						       cat: Cat,
						       span: Span,
						       deriv: L_Derivs
						      ]
				     },
			Ctx1,
			Ctx3
		       ),
	  mutable(Ctx,fs(Ctx3,Bindings)),
	  event_process(default(Stream),Content,Ctx),
	  mutable_read(Ctx,fs(Ctx4,Bindings)),
	  event_process(default(Stream),
			end_element{ name => op },
			Ctx4,
			Ctx2
		       )
	)
	.

:-xcompiler
fs_analyze(T,Bindings) :-
	mutable(M,[],true),
	fs_analyze_aux(T,M),
	mutable_read(M,Bindings1),
	mutable(M,[]),
	every(( domain(Var:2,Bindings1),
		mutable_read(M,_Bindings),
		length(_Bindings,Name),
		mutable(M,[Var:Name|_Bindings])
	      )),
	mutable_read(M,Bindings)
	.

:-std_prolog fs_analyze_aux/2.

fs_analyze_aux(T,M) :-
	( (var(T) xor T =.. ['$SET$'|_]) ->
	  mutable_read(M,Bindings),
	  ( fs_search(T,Bindings,N) ->
	    N2 is N+1
	  ;
	    N2 = 1
	  ),
	  ( N2 == 3 xor mutable(M,[T:N2|Bindings]))
	; atomic(T) ->
	  true
	; T = [_|_] ->
	  every(( domain(_T,T), fs_analyze_aux(_T,M) ))
	; T =.. [_|Args],
	  every(( domain(_T,Args), fs_analyze_aux(_T,M)	))
	)
	.

:-xcompiler
fs_search(Var,Bindings,N) :-
	once(( domain(_Var:N,Bindings),
	       _Var == Var
	     ))
	.

event_process( H::default(Stream),narg(Kind,FS), fs(Ctx1,Bindings),fs(Ctx2,Bindings) ) :-
	event_process(H,
		      start_element{ name => narg,
				     attributes => [type:Kind]
				   },
		      Ctx1,
		      Ctx3),
	event_process(fsxml(Stream), fs(FS), fs(Ctx3,Bindings),fs(Ctx4,Bindings)),
	event_process(H, end_element{ name => narg }, Ctx4,Ctx2 )
	.

event_process( H::fsxml(Stream),fs(FS),fs(Ctx1,Bindings),fs(Ctx2,Bindings)) :-
	event_process(default(Stream),
		      start_element{ name => fs, attributes => [] },Ctx1,Ctx3),
	mutable(Ctx,fs(Ctx3,Bindings),true),
	every(( inlined_feature_arg(FS,F,_,V),
		event_process( fsxml(Stream), feature(F,V), Ctx )
	      )),
	mutable_read(Ctx,fs(Ctx4,Bindings)),
	event_process(default(Stream),
		      end_element{ name => fs }, Ctx4,Ctx2)
	.

:-xcompiler
is_finite_set(T) :- T =.. ['$SET$'|_].

event_process( fsxml(Stream), feature(Name,Values),fs(Ctx1,Bindings),fs(Ctx2,Bindings)) :-
	( fs_search(Values,Bindings,Var) ->
	  Attrs = [id:Var],
	  Bound == 1
	;
	  Attrs = []
	),
%	format('\nFSXML ~w bindings=~w ctx1=~w\n',[Values,Bindings,Ctx1]),
	( var(Values), var(Bound) ->
	  Ctx2 = Ctx1
	;
	  event_process(default(Stream),
			start_element{ name => f,
				       attributes => [name:Name|Attrs]
				     },
			Ctx1,
			Ctx3
		       ),
				%	format('\nhello1 ctx3=~w\n',[Ctx3]),
	  ( (atomic(Values) ; is_finite_set(Values)) ->
	    mutable(Ctx,fs(Ctx3,Bindings),true),
	    every(( domain(Value,Values),
		    event_process(fsxml(Stream),val(Value),Ctx)
		  )),
	    mutable_read(Ctx,fs(Ctx4,Bindings))
	  ; var(Values) ->
	    Ctx3=Ctx4
	  ; %% complex term
	    event_process(fsxml(Stream),fs(Values),fs(Ctx3,Bindings),fs(Ctx4,Bindings))
	  ),
	  %%	format('\nhello2 ctx4=~w\n',[Ctx4]),
	  event_process(default(Stream),end_element{ name=> f },Ctx4,Ctx2)
	)
	.

event_process( fsxml(Stream), val(-), fs(Ctx1,Bindings),fs(Ctx2,Bindings) ) :-
	event_process(default(Stream), element{ name => minus },Ctx1,Ctx2).

event_process( fsxml(Stream), val(+), fs(Ctx1,Bindings), fs(Ctx2,Bindings) ) :-
	event_process(default(Stream), element{ name => plus },Ctx1,Ctx2).

event_process( fsxml(Stream), val(V), fs(Ctx1,Bindings), fs(Ctx2,Bindings) ) :-
	\+ domain(V,[+,-]),
	mutable(Ctx,Ctx1,true),
	event_process(default(Stream),
		      [ start_element{ name => val },
			characters{ value => V },
			end_element{ name => val }
		      ],
		      Ctx
		     ),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::default(Stream), dephts, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1,true),
	every(( HT::hypertag{}, event_process(depxml(Stream), HT, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       hypertag{ id => Id,
			 ht => HT,
			 deriv => Derivs
		       },
	       Ctx1,
	       Ctx2
	     ) :-
	( recorded(disambiguated) ->
	  mutable(M,[],true),
	  every((
		 alive_deriv(Derivs,DId),
		 mutable_list_extend(M,DId)
		)),
	  mutable_read(M,Derivs2::[_|_])
	;
	  Derivs2 = Derivs
	),
	name_builder('~L',[['d~w',' '],Derivs2],L_Derivs),
	hid2xmlid(Id,HId),
	mutable(Ctx,Ctx1,true),
	%%	verbose('FS content: ~w\n',[Content]),
	event_process(default(Stream),
		      start_element{ name => hypertag,
				     attributes => [ id:HId,
						     derivs: L_Derivs%, op: OpId
						   ]
				   },
		      Ctx1,
		      Ctx3),
	event_process(fsxml(Stream),fs(HT),fs(Ctx3,[]),fs(Ctx4,[])),
	event_process(default(Stream),
		      end_element{ name => hypertag },
		      Ctx4,
		      Ctx2
		     )
	.

event_process( H::default(Stream), depcost, Ctx1, Ctx2 ) :-
	    mutable(Ctx,Ctx1,true),
	    ( recorded(opt(cost)) ->
	    every( ( '$answers'(edge_cost(EId,W,Ws,_Cst)),
		       ( recorded(keep_edge(EId)) ->
			   Kept = yes,
			   E::edge{ id => EId,
				    label => Label,
				    source => node{ cat => SCat,
						    cluster => cluster{ id => SCId,
									left => SLeft
								      }
						  },
				    target => node{ cat => TCat,
						    cluster => cluster{ id => TCId,
									right => TLeft
								      }
						  }
				  }
		       ;   
			   Kept = no,
			   recorded( erased(E) )
		       ),
		       ( SLeft < TLeft ->
			   Dir = right
		       ;   
			   Dir = left
		       ),
		     cid2xmlid(SCId,XSCId),
		     cid2xmlid(TCId,XTCId),
		       event_process(H,
				     element{ name => 'cost',
					      attributes => [eid:EId,
							     kept:Kept,
							     w:W,
							     ws:Ws,
							     info: [Dir,SCat,Label,TCat],
							     source: XSCId,
							     target: XTCId
							    ]
					    },
				     Ctx),
		       true
		   ))
	;
	      true
	),
	    mutable_read(Ctx,Ctx2)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing XML objects
%%   extended for EASy Objects

xevent_process( H::default(Stream), xmldecl, Ctx, Ctx, XH ) :-
	format(Stream,'<?~w~L?>',
	       [xml,
		[' ~A',' '],
		[ version:'1.0', %'
		  encoding:'ISO-8859-1']]
	      )
	.
	
xevent_process( H::default(_), C::cluster{ id => Id, left => Left },Ctx1,Ctx2,XH) :-
%%	format('Cluster ~w: ~w\n',[XH,C]),
	event_process(XH,iterate_in_cluster(Id,Left),Ctx1,Ctx2).

xevent_process( H::default(_), iterate_in_cluster(_CId,Left),Ctx1,Ctx2,XH ) :-
	mutable(M,[],true),
	every(( ( CId=_CId
		; recorded( redirect(_CId,CId) )),
	        F::f{ cid => CId },
		mutable_read(M,_L),
		f_sorted_add(F,_L,_LL),
		mutable(M,_LL)
	      )),
	mutable_read(M,L),
%%	format('Emit Cluster: ~w => ~w\n',[CId,L]),
	event_process( XH, fs(L), Ctx1,Ctx2 )
	.

:-rec_prolog f_sorted_add/3.

f_sorted_add(F,[],[F]).
f_sorted_add(F1::f{ rank => R1 }, L::[F2::f{ rank => R2 }|L2],XL) :-
	(  R1 =< R2 ->
	    XL = [F1|L]
	;
	    XL = [F2|XL2],
	    f_sorted_add(F1,L2,XL2)
	)
	.
	    
xevent_process( H::default(_),fs([]),Ctx,Ctx,_).
xevent_process( H::default(_),fs([F|L]),Ctx1,Ctx3,XH) :-
	event_process(XH,F,Ctx1,Ctx2),
	event_process(XH,fs(L),Ctx2,Ctx3)
	.

xevent_process( H::default(_),
		f{ id => Id, lex => Lex, cid => CId, rank => Rank },
		Ctx1,
		Ctx4,
		XH
	     ) :-
	verbose('Handling f id=~w lex=~w ctx=~w xh=~w\n',[Id,Lex,Ctx1,XH]),
	(   recorded( emitted(Id) ),
	    Ctx4 = Ctx1
	xor
	    \+ domain(Ctx1,[open(_,_,_),middle(_,_,_),close(_,_,_)]),
	    \+ used( CId ),
	    recorded( redirect(_CId,CId) ),
	    used(_CId),
	    Ctx4 = Ctx1,
	    verbose('To be emitted later in a group for cid=~w: f id=~w lex=~w ctx=~w\n',[_CId,Id,Lex,Ctx1]),
	    true
	xor  
	( domain(Lex,['�','"','''','�','&quot;']),
	  Ctx1 = open(_Ctx5,GId,Type) ->
	  Ctx4 = open(_Ctx4,GId,Type)
	; domain(Lex,['�','"','''','.','...','&quot;']),
	  Ctx1 = middle(_Ctx1,GId,Type),
	  \+ ( %% check Lex is indeed the last F of group GId
	       ( f{ id => _FId, cid => CId, rank => _Rank },
		 Rank < _Rank
	       ;
		 cluster{ id => CId, right => _Left },
		 cluster{ id => _CId, left => _Left, right => _Right },
		 _Left < _Right,
		 f{ id => _FId, cid => _CId }
	       ),
	       recorded( f2group(_FId,GId) )
	     )
	->
	  event_process( XH, end_element{ name => 'Groupe' }, _Ctx1, _Ctx5 ),
	  Ctx4=close(_Ctx4,GId,Type)
	; Ctx1 = open(_Ctx1,GId,Type) ->
	  event_process( XH, start_element{ name => 'Groupe',
					   attributes => [id:GId,type:Type] },
			 _Ctx1,
			 _Ctx5 ),
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	    mutable(MCat,[],true),
	    every((( recorded( redirect(_CId,CId) ),
		     node{ cluster => cluster{ id => _CId }, cat => _Cat }
		   ; node{ cluster => cluster{ id => CId }, cat => _Cat }
		   ),
%		   mutable_read(MCat,_LCat),
%		   mutable(MCat,[_Cat|_LCat])
		   mutable_list_extend(MCat,_Cat)
		  )),
	    mutable_read(MCat,LCat),
	    ( LCat = [] -> Cat = ''
	    ; LCat = [Cat] -> true
	    ; name_builder('~L',[['~w',' '],LCat],Cat)
	    ),
	    event_process( XH, start_element{ name => 'F', attributes => [id:Id,cat:Cat] }, _Ctx5, _Ctx2 ),
	    event_process( XH, characters{ value => Lex }, _Ctx2, _Ctx3 ),
	    event_process( XH, end_element{ name => 'F' }, _Ctx3, _Ctx4 ),
	    record_without_doublon( emitted(Id) )
	)
	.

:-extensional f2group/2.

xevent_process( H::default(_),group(Type,_,_,N::node{ id =>NId },Content),Ctx1,Ctx4, XH) :-
	group_gensym(Id),
	record_without_doublon( group_head(NId,Id) ),
%%	verbose('Handling group ~w ~w ~w\n',[Id,Type,Content]),
	every(( domain(_CId,Content),
		( CId = _CId
		;  recorded( redirect( _CId, CId) )
		),
		f{ cid => CId, id => FId },
		verbose('Handling cid=~w fid=>~w\n',[CId,FId]),
		record_without_doublon( f2group(FId,Id) ))),
	Ctx1=Ctx2,
	event_process( XH,
		       flist(Content),
		       open(Ctx2,Id,Type),
		       close(Ctx3,Id,Type)
		     ),
	Ctx3=Ctx4,
	true
	.

xevent_process( H::default(_), flist([]),
		open(Ctx,GId,Type),
		close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Void group: ~w ~w\n',[GId,Type])
	.

xevent_process( H::default(_), flist([]),
	       close(Ctx,GId,Type),
	       close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Group already closed: ~w ~w\n',[GId,Type])
	.

xevent_process( H::default(_), flist([]),
		middle(Ctx1,GId,Type),
		close(Ctx2,GId,Type),
		XH
	     ) :-
	event_process( XH, end_element{ name => 'Groupe' }, Ctx1, Ctx2 )
	.
	
xevent_process( H::default(_), flist([Id|L]), Ctx1, Ctx3,XH ) :-
	C::cluster{id => Id },
	event_process(XH,C,Ctx1,Ctx2),
	event_process(XH,flist(L),Ctx2,Ctx3)
	.

xevent_process( H::default(_), constituants, Ctx1, Ctx2, XH) :-
%%	event_process( H, start_element{ name => 'constituants', attributes => [] }, Ctx1, Ctx2 ),
	event_process( XH, iterate_constituant(0), Ctx1, Ctx2 )
	.

xevent_process( H::default(_), iterate_constituant(N), Ctx1, Ctx3, XH ) :-
%%	format('ITERATE CONSTITUANTS ~w',[N]),
	(   constituant(Left,Right,Const),
	    Left >= N,
	    \+ ( constituant(_Left,_,_), _Left >= N, Left > _Left )
	->
%%	    format('CONST ~w\n',[Const]),
	    event_process(XH,Const,Ctx1,Ctx2),
	    event_process(XH,iterate_constituant(Right),Ctx2,Ctx3)
	;   
%%	    event_process(H,end_element{ name => 'constituants' }, Ctx1,Ctx3)
	    Ctx1=Ctx3
	)
	.

xevent_process( H::default(_), relations, Ctx1, Ctx4,XH ) :-
	event_process(XH,start_element{ name => relations, attributes => []}, Ctx1, Ctx2 ),
	mutable(Ctx,Ctx2,true),
	every(( domain(Type,rel[]),
		'$answers'( relation(R::relation{ type => Type }) ),
		xrelation(R,XR::xrelation(Type,Id,Content)),
		(   f2groups(Content,Content1,Content)		   
		xor Content = Content1 ),
		event_process(XH,xrelation(Type,Id,Content1),Ctx)
	      )),
	mutable_read(Ctx,Ctx3),
	event_process(XH,end_element{ name => relations }, Ctx3, Ctx4 )
	.

xevent_process( H::default(_), xrelation(Type,Label,Content), Ctx1, Ctx4,XH) :-
	event_process(XH,
		      start_element{ name => relation,
				     attributes => [ xlink!type: extended,
						     type: Type,
						     id: Label ] },
		      Ctx1,
		      Ctx2 ),
	mutable(Ctx,Ctx2,true),
	event_process(relarg(XH),Content,Ctx),
	mutable_read(Ctx,Ctx3),
	event_process(XH, end_element{ name => relation }, Ctx3, Ctx4 )
	.

xevent_process( relarg(XH::default(_)), arg(Type,Label), Ctx1, Ctx2,_) :-
	event_process(XH,
		      element{ name => Type,
			       attributes => [ xlink!type: locator,
					       xlink!href: Label
					     ]
			     },
		      Ctx1, Ctx2
		     )
	.

xevent_process( relarg(XH), so(SO), Ctx1, Ctx2,_ ) :-
	event_process(XH,
		      element{ name => 's-o',
			       attributes => [valeur: SO]
			     },
		      Ctx1, Ctx2
		     ).

xevent_process( relarg(XH::default(_)), 'a-propager'(Prop), Ctx1, Ctx2,_ ) :-
	Prop ?= faux,
	event_process(XH,
		      element{ name => 'a-propager',
			       attributes=> [booleen: Prop]
			     },
		      Ctx1, Ctx2
		     )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing XML objects
%%   for Passage format

event_super_handler(passage(Stream),default(Stream)).

xevent_process( H::passage(_), 'TWG', Ctx1, Ctx2,XH) :-
	event_process( XH, iterate_twg(0), Ctx1, Ctx2 )
	.

xevent_process( H::passage(_), iterate_twg(N), Ctx1, Ctx3,XH ) :-
%%	format('ITERATE CONSTITUANTS ~w : ~w\n',[XH,N]),
	(   constituant(Left,Right,Const),
	    Left >= N,
	    \+ recorded(processed(Const)),
	    \+ ( constituant(_Left,_,_), _Left >= N, Left > _Left ),
	    %% to be sure that splitted constituants are not skipped
	    (
		% either this is a splitted constituant
		Left == Right
	    xor	% otherwise, all splitted const starting at Left have been processed
		\+ ( constituant(Left,Left,Const2),
		    \+ recorded(processed(Const2)) )
	    ),
	    true
	->
	    verbose('CONST h=~w xh=~w ~w\n',[H,XH,Const]),
	    record(processed(Const)),
	    event_process(XH,Const,Ctx1,Ctx2),
	    event_process(XH,twg(Const),Ctx1,Ctx2),
	    event_process(XH,iterate_twg(Right),Ctx2,Ctx3)
	;   
	    Ctx1=Ctx3
	)
	.

xevent_process( H::passage(_),
	       f{ id => Id, lex => Lex, cid => CId, rank => Rank },
	       Ctx1,
	       Ctx4,
		XH
	     ) :-
	verbose('Handling f id=~w lex=~w cid=~w ctx=~w\n',[Id,Lex,CId,Ctx1]),
	(   recorded( emitted(Id) ),
	    Ctx4 = Ctx1
	xor fail,
	    \+ domain(Ctx1,[open(_,_,_),middle(_,_,_),close(_,_,_)]),
	    \+ used( CId ),
	    recorded( redirect(_CId,CId) ),
	    used(_CId),
	    Ctx4 = Ctx1,
	    verbose('To be emitted later in a group for cid=~w: f id=~w lex=~w ctx=~w\n',[_CId,Id,Lex,Ctx1]),
	    true
	xor  
	( domain(Lex,['�','"','''','�','&quot;']),
	  Ctx1 = open(_Ctx5,GId,Type) ->
	  Ctx4 = open(_Ctx4,GId,Type)
	; domain(Lex,['�','"','''','.','...','&quot;']),
	  Ctx1 = middle(_Ctx1,GId,Type),
	  \+ ( %% check Lex is indeed the last F of group GId
	       ( f{ id => _FId, cid => CId, rank => _Rank },
		 Rank < _Rank
	       ;
		 cluster{ id => CId, right => _Left },
		 cluster{ id => _CId, left => _Left, right => _Right },
		 _Left < _Right,
		 f{ id => _FId, cid => _CId }
	       ),
	       recorded( f2group(_FId,GId) )
	     )
	->
%%	  event_process( H, end_element{ name => 'G' }, _Ctx1, _Ctx5 ),
	  _Ctx1=_Ctx5,
	  Ctx4=close(_Ctx4,GId,Type)
	; Ctx1 = open(_Ctx1,GId,Type) ->
%%	  event_process( XH, start_element{ name => 'G',
%%					    attributes => [id:GId,type:Type] },
%%			 _Ctx1,
%%			 _Ctx5 ),
	  _Ctx1=_Ctx5,
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	    mutable(MCat,[],true),
	    every((( recorded( redirect(_CId,CId) ),
		     node{ cluster => cluster{ id => _CId }, cat => _Cat }
		   ; node{ cluster => cluster{ id => CId }, cat => _Cat }
		   ),
%%		   mutable_read(MCat,_LCat),
%%		   mutable(MCat,[_Cat|_LCat])
		   mutable_list_extend(MCat,_Cat)
		  )),
	    mutable_read(MCat,LCat),
	    ( LCat = [] -> Cat = ''
	    ; LCat = [Cat] -> true
	    ; name_builder('~L',[['~w',' '],LCat],Cat)
	    ),
%%	    event_process( XH, element{ name => 'W', attributes => [id:Id,pos:Cat,form:Lex] }, _Ctx5, _Ctx4 ),
	    event_process( XH, start_element{ name => 'T',
					      attributes => [id:Id] }, _Ctx5, _Ctx51 ),
	    event_process( XH, characters{ value => Lex },_Ctx51,_Ctx52 ),
	    event_process( XH, end_element{ name => 'T' },_Ctx52,_Ctx4),
	    record_without_doublon( emitted(Id) )
	)
	.

xevent_process( H::passage(_), flist([]),
		middle(Ctx1,GId,Type),
		close(Ctx2,GId,Type),
		XH
	     ) :-
%%	event_process( XH, end_element{ name => 'G' }, Ctx1, Ctx2 )
	Ctx1=Ctx2
	.

xevent_process( H::passage(_),
		twg(C::cluster{ id => Id, left => Left }),
		Ctx1,Ctx2,XH) :-
%	format('twg cluster ~w\n',[C]),
	N::node{ id => NId, cluster => C, cat => Cat },
%	format('twg node ~w\n',[N]),
	event_process( XH,
		       N,
		       Ctx1,
		       Ctx2 )
	.

xevent_process( H::passage(_),
		twg(group(Type,_,_,N::node{ id => NId},Content)),
		Ctx1,Ctx2,XH ) :-
	recorded( group_head(NId,Id) ),
	verbose('Dealing with group ~w type=~w headed by ~w\n',[Id,Type,N]),
	event_process( XH,
		       wlist(Content),
		       open(Ctx1,Id,Type),
		       close(Ctx2,Id,Type)
		     ),
	true
	.

xevent_process( H::passage(_), wlist([]),
		open(Ctx,GId,Type),
		close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Void group: ~w ~w\n',[GId,Type])
	.

xevent_process( H::passage(_), wlist([]),
	       close(Ctx,GId,Type),
	       close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Group already closed: ~w ~w\n',[GId,Type])
	.

xevent_process( H::passage(_), wlist([]),
		middle(Ctx1,GId,Type),
		close(Ctx2,GId,Type),
		XH
	     ) :-
	event_process( XH, end_element{ name => 'G' }, Ctx1, Ctx2 )
	.
	
xevent_process( H::passage(_), wlist([Id|L]), Ctx1, Ctx3,XH ) :-
	N::node{ id => NId, cluster => cluster{ id => Id} },
	event_process(XH,N,Ctx1,Ctx2),
	event_process(XH,wlist(L),Ctx2,Ctx3)
	.

xevent_process( H::passage(_),
		N::node{ id => NId,
			 cluster => cluster{ id => _CId },
			 cat => _Cat,
			 lemma => Lemma,
			 form => Form,
			 deriv => Derivs
		       },
		Ctx1,
		Ctx4,
		XH ) :-
	%% Some tokens covered by a node may have to be removed
	%% to build their own form.
	%% this is the case for quotes, as in �lui�
%	format('Dealing with node ~w\n',[N]),
	(
	Ctx1 = open(_Ctx1,GId,Type) ->
	  event_process( XH,
			 start_element{ name => 'G',
					attributes => [id:GId,type:Type] },
			 _Ctx1,
			 _Ctx5 ),
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	( cat_abstract(_Cat,Cat) xor Cat=_Cat ),
	( Lemma == '_SENT_BOUND' ->
	  _Ctx4=_Ctx5
	;
	  w_gensym(WId),
	  record_without_doublon( node2wid(NId,WId) ),
	  mutable(MToks,[],true),
	  every(( ( CId=_CId
		  ; recorded( redirect(_CId,CId))),
		  F::f{ cid => CId },
		  mutable_read(MToks,_L),
		  f_sorted_add(F,_L,_LL),
		  mutable(MToks,_LL)
		)),
	  mutable_read(MToks,Tokens),
	  get_fids(Tokens,TokenIds),
	  verbose('tokens ~w ~w => ~w\n',[N,Tokens,TokenIds]),
	  name_builder('~L',[['~w',' '],TokenIds],TIds),
	  ( edge{ target => _Target,
		  source => _Source::node{id => _Derivs},
		  id => EId
		},
	    alive_deriv(_Derivs,DID),
	    (_Target=N,
	     deriv(DID,EId,_,_,OId)
	    ;_Source=N,
	     deriv(DID,EId,_,OId,_)
	    ),
	    op{ id => OId, top => fs(FS) },
	    \+ var(FS)
	  ->
	    %%	  format('FS ~w\n',[FS]),
	    fs2flatfs(FS,_MSTAG),
	    MSTAG1=[mstag:_MSTAG],
	    ( keep_ht(Cat),
	      deriv2ht(DID,hypertag{ ht => fs(HT) }),
	      \+ var(HT)
	    ->
%%	      format('HT ~w ~w\n',[OId,HT]),
	      fs2flatfs(HT,_HT),
	      MSTAG=[ht:_HT|MSTAG1]
	    ;
	      MSTAG=MSTAG1
	    )
	  ;
	    MSTAG=[]
	  ),
	  event_process( XH,
			 element{ name => 'W',
				  attributes => [id:WId,pos:Cat,lemma:Lemma,form:Form,tokens:TIds|MSTAG] },
			 _Ctx5,
			 _Ctx4
		       ),
	  ( Lemma = entities[] ->
	    ( node2live_ht(NId,HTId),
	      hypertag{ id => HTId, ht => fs(XHT) },
	      domain(id:[val(Entity_Id)],XHT)
	    xor Entity_Id=[]
	    ),
	    record( passage_entity(WId,Cat,Lemma,Form,MSTAG,Entity_Id) )
	  ; Lemma = date[] ->
	    record( passage_entity(WId,Cat,Lemma,Form,MSTAG,[]) )
	  ;
	    true
	  )
	
	)
	.

:-light_tabular fs2flatfs/2.

fs2flatfs(FS,MSTAG) :-
	fs2path(FS,Path),
	name_builder('~L',[['~w',' '],Path],MSTAG)
	.

:-std_prolog fs2path/2.

fs2path(FS,Path) :-
%	format('FS ~w\n',[FS]),
	mutable(M,[],true),
	every(( domain(F:Vals,FS),
		fs_keep(F),
		\+ (Vals = var(_,_)),
		( Vals = fs(_FS) ->
		  fs2path(_FS,SubPath),
		  every((domain(_P,SubPath),
			 name_builder('~w.~w',[F,_P],P),
%			 mutable_read(M,_Flat),
%			 mutable(M,[P|_Flat])
			 mutable_list_extend(M,P)
			))
		;
		  vals2flat(Vals,FlatVals),
		  name_builder('~w.~w',[F,FlatVals],FlatF),
%%		  mutable_read(M,_Flat),
%%		  mutable(M,[FlatF|_Flat])
		  mutable_list_extend(M,FlatF)
		)
	      )),
	mutable_read(M,Path)
	.

:-extensional fs_keep/1.

fs_keep(gender).
fs_keep(hum).
fs_keep(number).
fs_keep(tense).
fs_keep(mode).
fs_keep(time).
fs_keep(wh).
fs_keep(person).
fs_keep(case).
fs_keep(degree).
%fs_keep(pcas).
fs_keep(numberposs).
fs_keep(aux_req).
fs_keep(diathesis).
fs_keep(adv_kind).
fs_keep(def).
fs_keep(dem).
fs_keep(extracted).

fs_keep(arg0).
fs_keep(arg1).
fs_keep(arg2).
fs_keep(extraction).
fs_keep(kind).
fs_keep(pcas).
fs_keep(real).
fs_keep(subarg).
fs_keep(ctrsubj).
fs_keep(imp).
fs_keep(refl).
fs_keep(function).

:-extensional keep_ht/1.

keep_ht(cat[v,aux,adj,nc]).

:-light_tabular vals2flat/2.

vals2flat(Vals,MSTAG) :-
	mutable(M,[],true),
%%	format('Val ~w\n',[Vals]),
	every(( domain(_V,Vals),
		( _V = val(V) ->
		  true
		; _V == (+) ->
		  V=plus
		; _V == (-) ->
		  V=minus
		;
		  V=_V
		),
%		mutable_read(M,_Flat),
%		mutable(M,[V|_Flat])
		mutable_list_extend(M,V)
	      )),
	mutable_read(M,Flat),
	name_builder('~L',[['~w','.'],Flat],MSTAG)
	.

:-rec_prolog get_fids/2.

get_fids([],[]).
get_fids([f{ id => FId}|Tokens],[FId|FIds]) :-
	get_fids(Tokens,FIds).

xevent_process( H::passage(_), relations, Ctx1, Ctx2,XH ) :-
	mutable(Ctx,Ctx1,true),
	every(( domain(Type,rel[]),
		'$answers'( relation(R::relation{ type => Type }) ),
		xrelation(R,XR::xrelation(Type,Id,Content)),
		(   f2groups(Content,Content1,Content)		   
		xor Content = Content1 ),
		event_process(XH,xrelation(Type,Id,Content1),Ctx)
	      )),
	mutable_read(Ctx,Ctx2)
	.

xevent_process( H::passage(_), xrelation(Type,Label,Content), Ctx1, Ctx4,XH) :-
	event_process(XH,
		      start_element{ name => 'R',
				     attributes => [ type: Type,id: Label ] },
		      Ctx1,
		      Ctx2 ),
	mutable(Ctx,Ctx2,true),
	event_process(relarg(XH),Content,Ctx),
	mutable_read(Ctx,Ctx3),
	event_process(XH, end_element{ name => 'R' }, Ctx3, Ctx4 )
	.

xevent_process( relarg(XH::passage(_)), arg(Type,_Label), Ctx1, Ctx2,_) :-
	( Type == 'coord-g',
	  _Label = vide ->
	  Ctx1 = Ctx2
	;
	  ( recorded(node2wid(_Label,Label)) xor Label=_Label),
%	  format('arg in=~w out=~w\n',[_Label,Label]),
	  event_process(XH,
			element{ name => Type,
				 attributes => [  ref: Label  ]
			       },
			Ctx1, Ctx2
		       )
	)
	.

xevent_process( relarg(passage(_)), 'a-propager'(_), Ctx, Ctx,_ ).

xevent_process( H::passage(_), entities, Ctx1, Ctx2,XH) :-
	mutable(Ctx,Ctx1,true),
	every(( entity(Id,WId,Type,MSTAG,Entity_Id),
		_Attr = [id:Id,
			 lst:WId,
			 type:Type | MSTAG],
		( Entity_Id = [] ->
		  Attr=_Attr
		;
		  Attr=[eid:Entity_Id|_Attr]
		),
		event_process(H,
			      element{ name => 'NE',
				       attributes => Attr
				     },
			      Ctx)
	      )),
	mutable_read(Ctx,Ctx2)
	.

:-light_tabular entity/5.
:-mode(entity/4,+(-,-,-,-,-)).

entity(Id,WId,Type,MSTAG,Entity_Id) :-
	recorded(passage_entity(WId,Cat,Lemma,Form,MSTAG,Entity_Id)),
	( Lemma = entities['_PERSON',
			   '_PERSON_m',
			   '_PERSON_f'] ->
	  Type = individual,
	  SubType = person
	; Lemma = entities['_ORGANIZATION','_COMPANY'] ->
	  Type = organization
	; Lemma = entities['_PRODUCT'] ->
	  Type = mark
	; Lemma = entities['_LOCATION'] ->
	  Type = location
	; Lemma = entities['_NUMBER','_NUM','_ROMNUM'] ->
	  Type = measure
	; Lemma = date[] ->
	  Type = dateTime
	;
	  fail
	),
	ne_gensym(Id)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Dealing with features

:-light_tabular check_node_top_feature/3.
:-mode(check_node_top_feature/3,+(+,+,-)).

check_node_top_feature(NId,F,V) :-
	node2op(NId,OId),
	check_op_top_feature(OId,F,V).

:-light_tabular check_op_top_feature/3.
:-mode(check_op_top_feature/3,+(+,+,-)).

:-finite_set(binary,[+,-]).

check_op_top_feature(OId,F,V) :-
	op{ id  => OId, top => Top },
%	format('check op f=~w v=~w oid=~w top=~w\n',[F,V,OId,Top]),
	inlined_feature_arg(Top,F,_,V),
%	format('success op f=~w v=~w oid=~w top=~w\n',[F,V,OId,Top]),
	true
	.

:-light_tabular check_ht_feature/3.
:-mode(check_ht_feature/3,+(+,+,-)).

check_ht_feature(HTId,F,V) :-
	HT::hypertag{ id => HTId, ht => _HT::ht{} },
	inlined_feature_arg(_HT,F,_,V)
	.


:-light_tabular check_arg_feature/4.
:-mode(check_arg_feature/4,+(+,+,+,-)).

check_arg_feature(HTId,Arg,F,V) :-
	HT::hypertag{ id => HTId, ht => _HT::ht{} },
	inlined_feature_arg(_HT,Arg,_,ArgFs),
	inlined_feature_arg(ArgFs,F,_,V)
	.

:-light_tabular check_xarg_feature/5.
:-mode(check_xarg_feature/5,+(+,+,-,-,-)).

check_xarg_feature(HTId,Arg,Function,Kind,Real) :-
	hypertag{ id => HTId, ht => _HT::ht{} },
	inlined_feature_arg(_HT,Arg,_,ArgFs::arg{ function => Function, kind => Kind, real => Real})
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Primitives to navigates within dependencies

:-light_tabular node!neighbour/3.
:-mode(node!neighbour/3,+(+,+,-)).

node!neighbour(left, X, N::node{} ) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => P, left => _P },
	_N::node{ cluster => ClusterB, form => _Form },
	( _Form = '_EPSILON' ->
	  node!neighbour(left,_N,N)
	; P=_P ->
	  node!neighbour(left,_N,N)
	;
	  N=_N
	)
	.

node!neighbour(right, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P} } xor X = P ),
	ClusterB::cluster{ left => P },
	N::node{ cluster => ClusterB }
	.

node!neighbour(xleft, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => Q }, Q =< P,
	N::node{ cluster => ClusterB }
	.

node!neighbour(xright, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P } } xor X = P ),
	ClusterB::cluster{ left => Q }, P =< Q,
	N::node{ cluster => ClusterB }
	.

:-std_prolog node!all_neighbours/4.

node!all_neighbours(Mode,X,Model::node{},N::node{}) :-
	node!neighbour(Mode,X,Y),
	\+ \+ (Y=Model),
	( Y = N
	; node!all_neighbours(Mode,Y,Model,N)
	)
	.

:-std_prolog node!empty/1.

node!empty( node{ cluster => cluster{ lex => '' } } ).


:-light_tabular node!dependency/4.
:-mode(node!dependency/4,+(+,+,-,-)).

node!dependency(out,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ source => N1, target => N2 }
	.

node!dependency(in,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ target => N1, source => N2 }
	.

/*
node!dependency(xout,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ source => _N1, target => _N2 },
			\+ domain(edge{ source => _N2 },_L)
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.
*/

node!dependency(xout,N1::node{},N2::node{},L) :-
	node!dependency(out,N1,N3,[E]),
	N1 \== N3,
	(   N2 = N3,
	    L = [E]
	;   
	    node!dependency(xout,N3,N2,L1),
	    (\+ domain( edge{ target => N1 }, L1 )),
	    L = [E|L1]
	)
	.

node!dependency(xin,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ target => _N1, source => _N2 },
			\+ domain( edge{ target => _N2 },L),
			_N2 \== N1 % to avoid loop
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.

:-light_tabular node!safe_dependency/3.
:-mode(node!safe_dependency/4,+(+,+,-)).

node!safe_dependency(xout,N1::node{},N2::node{}) :-
	node!dependency(out,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xout,N3,N2)
	),
	N1 \== N2
	.

node!safe_dependency(xin,N1::node{},N2::node{}) :-
	node!dependency(in,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xin,N3,N2)
	),
	N1 \== N2
	.

:-xcompiler
node!collect(X^G,L) :-
	mutable(M,[],true),
	every((G,
	       mutable_read(M,L1),
	       (	 \+ domain(X,L1)),
	       X = node{ cluster => cluster{ lex => LexX}},
	       LexX \== '',		     
	       node!add(X,L1,L2),
	       mutable(M,L2)
	      )),
	mutable_read(M,L).

:-rec_prolog node!add/3.

node!add( N::node{},[],[N]).
node!add( N::node{ cluster => Cluster::cluster{ left => Left}},
	  L1::[N1::node{ cluster => Cluster1::cluster{ left => Left1}}|XL1],
	  L2
	) :-
	( Cluster == Cluster1 ->
	    L2 = L1
	;   
	    Left < Left1 ->
	    L2 = [N|L1]
	;   
	    node!add(N,XL1,XL2),
	    L2 = [N1|XL2]
	),
%%	format('NODE ADD ~w\n',[L2]),
	true
	.

:-rec_prolog node!add_fillers/2.

node!add_fillers( L::[N::node{}], L ).
node!add_fillers( L::[N1::node{ cluster => cluster{ left => Left1, right => Right1 }},
		      N2::node{ cluster => cluster{ left => Left2, right => Right2 }} | LL ],
		  XL::[N1|XL2]
		 ) :-
	(   N::node{ cluster => cluster{ lex => _Lex, left => Right1, right => _Left } },
	    _Lex \== '',
	    _Left =< Left2 ->
	    node!add_fillers([N,N2|LL],XL2)
	;
	    node!add_fillers([N2|LL],XL2)
	)
	.

:-std_prolog node!terminal/3.

node!terminal(N,Cat,Path) :-
	\+ ( node!dependency(out,N, node{ cat => Cat },[edge{ label => Label} ]),
	       domain(Label,Path)
	   )
	.

:-std_prolog node!parent/4.

node!parent(N,Cat,P,Path) :-
	node!dependency(in,N,P::node{ cat => XCat },[edge{ label => Label }]),
	domain(Label,Path),
	domain(XCat,Cat)
	.

:-std_prolog node!older_ancestor/4.

node!older_ancestor(N,Cat,P,Path) :-
	(   node!parent(N,Cat,P1,Path),
	    node!older_ancestor(P1,Cat,P,Path)
	xor P = N
	).

:-std_prolog node!first_v_ancestor/2.

/*
node!first_v_ancestor(N::node{cat => Cat::cat[aux,v,adj], tree => Tree}, N1) :-
	( Cat == cat[v,adj] ->
	    N1 = N
	;   Cat == aux, domain(cleft_verb,Tree) ->
	    N1 = N
	;   
	    node!parent(N,cat[aux,v,adj],N2,['Infl','V']),
	    node!first_v_ancestor(N2,N1)
	)
	.
*/

node!first_v_ancestor(N::node{cat => Cat::cat[aux,v], tree => Tree}, N1) :-
	( Cat == cat[v] ->
	    N1 = N
	;   Cat == aux, domain(cleft_verb,Tree) ->
	    N1 = N
	;   
	    node!parent(N,cat[aux,v],N2,['Infl','V']),
	    node!first_v_ancestor(N2,N1)
	)
	.


%% BUG SOMEWHERE IN DYALOG on @*

:-light_tabular node!group/4.
:-mode(node!group/4,+(+,-,-,-)).

node!group(Nodes,Words,Left,Right) :-
	%%	format('Start ~w\n',[Nodes]),
	@*{ goal => ( _Nodes1 = [ N::node{ cluster => cluster{ id => Id,
							       left => CLeft,
							       right => CRight }}
				| _Nodes],
			( domain(Id,Words) ->
			    _Words1 = _Words
			;   
			    _Words1 = [Id|_Words]
			),
%%			format('HERE ~w ~w ~w\n',[_Nodes,_Left,_Right]),
			( CLeft < _Left -> NLeft = CLeft ; NLeft = _Left ),
			( CRight > _Right -> NRight = CRight ; NRight = _Right ),
			true
		    ),
	    %%	    from => 0,
	    %%	    vars => [Words],
	    collect_first => [Nodes,Words,1000,0],
	    collect_last => [[],[],Left,Right],
	    collect_loop => [_Nodes1,_Words1,_Left,_Right],
	    collect_next => [_Nodes,_Words,NLeft,NRight]
	  },
	%% format('End ~w ~w ~w\n',[Words,Left,Right]),
	true
	.


:-light_tabular label2lex/3.
:-mode(label2lex/3,+(+,-,-)).

label2lex(Label,Lex,FIds) :-
	rx!tokenize(Label,' ',Labels),
	label2lex_aux(Labels,Lex,FIds).

:-std_prolog label2lex_aux/3.

label2lex_aux(Labels,Lex,FIds) :-
	(   Labels = [] -> Lex=[], FIds = []
	;   Labels = [X|Labels2],
	    (	rx!tokenize(X,'|',[FX,LX]) xor LX=X, FX='' ),
	    Lex = [LX|Lex2],
%%	    format('Try match ~w\n',[FX]),
	    rx!match{ string => FX,
		      rx => rx{ pattern => '\\([0-9][0-9]*\\)$' },
		      substrings => [_FX],
		      subs_flag => 1
		    },
%%	    format('=> ~w\n',[_FX]),
	    FIds = [_FX|FIds2],
	    label2lex_aux(Labels2,Lex2,FIds2)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Derivs

:-extensional deriv/2.

:-xcompiler
register_deriv(DId,NId) :-
	(   deriv(DId,_)
	xor
            '$interface'('Deriv_New'(NId:term),[return(Deriv:ptr)]),
	    record(deriv(DId,Deriv))
	).

:-xcompiler
zderiv(DId,NId,OId,Span,HTId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Info'(Deriv:ptr,NId:term,OId:term,Span:term,HTId:term),[]),
%	format('deriv info ~w => nid=~w oid=~w span=~w htid=~w\n',[DId,NId,OId,Span,HTId]),
	true
	.

:-xcompiler
deriv2node(DId,NId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Get_Node'(Deriv:ptr,NId:term),[]).

:-xcompiler
deriv2node_and_edges(DId,NId,Edges) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Get_Node_And_Edges'(Deriv:ptr,NId:term,Edges:term),[]).

:-xcompiler
deriv2htid(DId,HTId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Get_Hypertag'(Deriv:ptr,HTId:term),[]).

:-xcompiler
deriv2ht(DId,HT) :-
	deriv2htid(DId,HTId),
	HT::hypertag{ id => HTId }
	.

:-xcompiler
deriv2edges(DId,Edges) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Edges'(Deriv:ptr,Edges:term),[])
	.

:-xcompiler
deriv2edge(DId,Info) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Edges'(Deriv:ptr,Edges:term),[]),
	domain(Info,Edges),
	true
	.

:-xcompiler
deriv(DId,EId,Span,SOP,TOP) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Edges'(Deriv:ptr,Edges:term),[]),
	domain(info(EId,SOP,TOP,_,Span),Edges)
	.

:-xcompiler
deriv_set_op(DId,OId,Span) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Set_Op'(Deriv:ptr,OId:term,Span:term,NId:term),[]),
	record_without_doublon( node2op(NId,OId) ),
	record(K::node_op2deriv(NId,OId,DId))
	.

:-xcompiler
deriv_set_ht(DId,HTId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Set_Hypertag'(Deriv:ptr,HTId:term),[return(none)])
	.

:-xcompiler
deriv_add_edge(DId,GId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Add_Edge'(Deriv:ptr,GId:term),[return(none)]),
%	format('added edge did=~w info=~w\n',[DId,GId]),
	true
	.

:-xcompiler
deriv2best_parse(DId,DStruct::dstruct{ constraint => _Cst }) :-
	zderiv(DId,NId,OId,_,_),
	'$answers'( best_parse(NId,OId,_,_Cst,DStruct) )
	.

:-xcompiler
alive_deriv(Derivs,Deriv) :-
	domain(Deriv,Derivs),
	recorded( keep_deriv(Deriv) ),
	true
	.

:-xcompiler
alive_ht(Derivs,HTId) :-
	alive_deriv(Derivs,DId),
	deriv2htid(DId,HTId).

:-light_tabular node2live_deriv/2.
:-mode(node2live_deriv/2,+(+,-)).

node2live_deriv(NId,DId) :-
	N::node{ id => NId, deriv => Derivs },
	alive_deriv(Derivs,DId)
	.

:-light_tabular node2live_ht/2.
:-mode(node2live_ht,+(+,-)).

node2live_ht(NId,HTId) :-
	node2live_deriv(NId,DId),
	deriv2htid(DId,HTId)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Restrictions

:-light_tabular prepare_restriction/1.

prepare_restriction([PStmt1,PStmt2,PStmt3,PStmt4,PStmt5,PStmt6]) :-
	recorded( opt(restrictions,DB) ),
	sqlite!prepare(DB,
		       'select dep.weight
    from lemma as source,
         lemma as rel,
         lemma as target,
         dep
    where source.lemma=? and source.pos=?
      and target.lemma=? and target.pos=?
      and rel.lemma=? and rel.pos=?
      and dep.source=source.rowid
      and dep.rel=rel.rowid
      and dep.target=target.rowid',PStmt1),
	
	sqlite!prepare(DB,
		       'select dep2.weight
    from lemma as source,
         lemma as rel,
         dep2
    where source.lemma=? and source.pos=?
      and rel.lemma=? and rel.pos=?
      and dep2.source=source.rowid
      and dep2.rel=rel.rowid',PStmt2),

	sqlite!prepare(DB,
		       'select dep3.weight
    from lemma as target,
         lemma as rel,
         dep3
    where target.lemma=? and target.pos=?
      and rel.lemma=? and rel.pos=?
      and dep3.target=target.rowid
      and dep3.rel=rel.rowid',PStmt3),

	sqlite!prepare(DB,
		       'select form2lemma.w
		      from form, lemma, form2lemma
		      where form.form=? and lemma.lemma=? and lemma.pos=?
		        and form2lemma.form=form.rowid
		        and form2lemma.lemma=lemma.rowid', PStmt4),

	sqlite!prepare(DB,
		       'select sim.weight
		      from lemma as l1, lemma as l2, sim
		      where l1.lemma=? and l1.pos=?
		      and l2.lemma=? and l2.pos=?
		      and l1.rowid=sim.l1 and l2.rowid=sim.l2', PStmt5),


		sqlite!prepare(DB,
		       'select term.weight
    from lemma as source,
         lemma as rel,
         lemma as target,
         term
    where source.lemma=? and source.pos=?
      and target.lemma=? and target.pos=?
      and rel.lemma=? and rel.pos=?
      and term.source=source.rowid
      and term.rel=rel.rowid
      and term.target=target.rowid',PStmt6)

	.

:-light_tabular check_restriction/6.
:-mode(check_restriction/6,+(+,+,+,+,+,-)).

check_restriction(Source,SCat,Target,TCat,Rel,W) :-
	'$answers'(prepare_restriction([PStmt1,
					PStmt2,
					PStmt3,
					PStmt4,
					PStmt5,
					PStmt6
				       |_])),
	sqlite!reset_and_bind(PStmt1,[Source,SCat,Target,TCat,Rel,prep]),
	sqlite!reset_and_bind(PStmt2,[Source,SCat,Rel,prep]),
	sqlite!reset_and_bind(PStmt3,[Target,TCat,Rel,prep]),
	sqlite!reset_and_bind(PStmt6,[Source,SCat,Target,TCat,Rel,prep]),
%%	format('Bind ~w\n',[L]),
	verbose('restr tried ~w_~w ~w_~w ~w_~w\n',[Source,SCat,Rel,prep,Target,TCat]),
%%	format('*** restr tried ~w_~w ~w_~w ~w_~w\n',[Source,SCat,Rel,prep,Target,TCat]),
	(   sqlite!tuple(PStmt2,[W2]) xor W2=0),
	(   sqlite!tuple(PStmt3,[W3]) xor W3=0),
	(   sqlite!tuple(PStmt1,[W1]) xor W1=0),
	(   sqlite!tuple(PStmt6,[W6]) xor W6=0),
%	W is W1+W2+W3+2*W6,
	W is 2*W1+W3+2*W6,
%	W >= 10,
	W > 0,
%%	format('Restriction found for source=~w target=~w rel=~w => ~w w1=~w w2=~w w3=~w w6=~w\n',[Source,Target,Rel,W,W1,W2,W3,W6]),
	verbose('Restriction found for source=~w target=~w rel=~w => ~w\n',[Source,Target,Rel,W])
	.

:-light_tabular check_term/6.

check_term(Source,SCat,Target,TCat,Rel,W) :-
	'$answers'(prepare_restriction([PStmt1,
					PStmt2,
					PStmt3,
					PStmt4,
					PStmt5,
					PStmt6
				       |_])),
	sqlite!reset_and_bind(PStmt6,[Source,SCat,Target,TCat,Rel,prep]),
	(   sqlite!tuple(PStmt6,[_W]) xor _W=0),
	_W >= 10,
	W is - _W,
	verbose('Term restriction found for source=~w target=~w rel=~w => ~w\n',[Source,Target,Rel,W]),
	true
	.

%:-extensional catpref/4.

:-light_tabular check_catpref/4.
:-mode(check_catpref/4,+(+,+,+,-)).

check_catpref(Form,Lemma,Cat,W) :-
	( 	  catpref(Form,Lemma,Cat,W)
	xor
%	format('check catpref restr form=~w lemma=~w cat=~w\n',[Form,Lemma,Cat]),
	'$answers'(prepare_restriction([_,_,_,Stmt|_])),
		  sqlite!reset_and_bind(Stmt,[Form,Lemma,Cat]),
		  once((sqlite!tuple(Stmt,[W1]))),
	    (	( domain(Form,[de,des,du,'d''',le,la,les,un,une])
		;   domain(Lemma,[tout]) 
		) 
	    -> 		% delicate frequent words
		W is W1 * 7 / 2
	    ;
		W is W1 * 7
	    ),
	    %%	    format('=> W=~w\n',[W]),
	    true
	)
	.

:-light_tabular check_sim/5.
:-mode(check_sim/5,+(+,+,+,+,-)).

check_sim(L1,Cat1,L2,Cat2,W) :-
	'$answers'(prepare_restriction([_,_,_,_,Stmt|_])),
	sqlite!reset_and_bind(Stmt,[L1,Cat1,L2,Cat2]),
	once(( sqlite!tuple(Stmt,[_W]) )),
	W is _W * 3
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Tools

%% defined in tag_generic
:-std_prolog name_builder/3.

% name_builder(Format,Args,Name) :-
%         string_stream(_,S),
%         format(S,Format,Args),
%         flush_string_stream(S,Name),
%         close(S)
%         .

:-xcompiler
is_capitalized(L) :-
	'$interface'('is_capitalized'(L:string),[])
	.

:-light_tabular capitalized_cluster/1.

capitalized_cluster(Lex) :-
	lex_tokenize(Lex,L),
	domain(_Lex,L),
	( rx!tokenize(_Lex,'|',[Label,__Lex]) xor __Lex = _Lex),
	is_capitalized(__Lex)
	.

:-light_tabular lex_tokenize/2.
:-mode(lex_tokenize/2,+(+,-)).

lex_tokenize(Lex,LLex) :- rx!tokenize(Lex,' ',LLex).

:-xcompiler
is_number(L) :-
	'$interface'('is_number'(L:string),[])
	.
:-xcompiler
has_suffix_n(L,N,S) :-
	'$interface'('n_suffix'(L:string, N:int),[return(S:string)])
	.

:-light_tabular suffix3/2.
:-mode(suffix3/2,+(+,-)).

suffix3(Lex,Suff) :- 
	( Lex = '' -> Suff = '-'
	; Lex = entities[] -> Suff = '-'
	;  has_suffix_n(Lex,3,_Suff), 
	  (_Suff==Lex -> Suff= '-' ; Suff = _Suff)
	).

:-xcompiler
mutable_list_extend(M,X) :- '$interface'('DyALog_Mutable_List_Extend'(M:ptr,X:term),[return(none)]).	

:-xcompiler
mutable_add(M,X) :- '$interface'('DyALog_Mutable_Add'(M:ptr,X:int),[return(none)]).	

:-std_prolog append/3.
append(L1,L2,L3) :-
	(L1=[] -> L2=L3
	; L1=[A|XL1],
	  L3=[A|XL3],
	 append(XL1,L2,XL3)
	).

:-xcompiler
fast_append(L1,L2,L3) :- '$interface'('Easyforest_Fast_Append'(L1:term,L2:term,L3:term),[]).


:-std_prolog utime/1.

utime(T) :- '$interface'('DyALog_Utime'(),[return(T:int)]).

:-xcompiler
abolish(F/N) :- '$interface'( 'Abolish'(F:term,N:int), [return(none)]).


:-std_prolog read_dbfile/1.
read_dbfile(File) :-
	open(File,read,S),
	repeat(( read_term(S,T,V),
		 ( T == eof
		 xor
		   record(T),
		   fail
		 )
	       )),
	close(S)
	.


%% to store a fact in DyALog persistent aread, to be restored at the next loop
%% NOTE: after restoration, the fact is removed from the persistent area
:-light_tabular persistent!add/1.

persistent!add(Fact) :-
	'$interface'('DyALog_Persistent_Add'(Fact:term),[return(none)]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Misc.

%% the notation X ::= t force a fesh copy of t
%% it assumes that t is ground
:-xcompiler
X ::= Y	:- '$interface'('DyALog_Copy'(Y:term,X:term),[]).

:-xcompiler
fast_mutable(M,T) :-
	X ::= T,
	mutable(M,X)
	.

:-xcompiler			%verbose
verbose(Msg,Args) :- (\+ opt(verbose) xor format(Msg,Args)).

%:-xcompiler((verbose(Msg,Args) :- true )).

:-xcompiler
record_without_doublon( A ) :-
        (recorded( A ) xor record( A ))
        .

:-std_prolog update_counter/2.

update_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
	  mutable_read(M,V),
	  mutable_add(M,1)
%            mutable_inc(M,V)
        ;   V=1,
            mutable(M,2),
            record( counter(Name,M) )
        )
        .

:-std_prolog value_counter/2.

value_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_read(M,V)
        ;
            V = 1
        )
        .

:-std_prolog group_gensym/1.

group_gensym(L) :-
	sentence(Sent),
	update_counter(group,X),
	name_builder('~wG~w',[Sent,X],L)
	.

:-std_prolog w_gensym/1.

w_gensym(L) :-
	sentence(Sent),
	update_counter(w,X),
	name_builder('~wF~w',[Sent,X],L)
	.

:-std_prolog rel_gensym/1.

rel_gensym(L) :-
	sentence(Sent),
	update_counter(relation,X),
	name_builder('~wR~w',[Sent,X],L)
	.

:-std_prolog ne_gensym/1.

ne_gensym(L) :-
	sentence(Sent),
	update_counter(ne,X),
	name_builder('~wN~w',[Sent,X],L)
	.

:-std_prolog mark_as_used/1.

mark_as_used(L) :-
	every(( domain( Label, L),
		record_without_doublon(used(Label)),
		verbose('mark as used ~w\n',[Label]),
		true
	      )).

:-std_prolog erase_relation/1.

erase_relation( Rel ) :-
	%% Erase both Call and Return items
	tab_item_term(I::'*RITEM*'(Call,_),Rel),
	every(( recorded(C::'*CITEM*'(Call,Call),Add),
		delete_address(Add)) ),
	every(( recorded(I,Add),
		delete_address(Add),
		true
	      )
	     ),
	true
	.

format_hook(0'E,Stream,[edge{ id => Id, source => N1, target => N2, label => L}|R],R) :- %'0
        format(Stream,'~w:~E-~w->~E',[Id,N1,L,N2])
        .

format_hook(0'E,Stream,[node{ id => Id,cat => Cat,cluster => C, lemma => L, form=> F }|R],R) :- %'0
	( L == '' ->
	  format(Stream,'~w/~w/~E',[Id,Cat,C])
	;
	  format(Stream,'~w/~w:~w__~w/~E',[Id,F,L,Cat,C])
	)
        .

format_hook(0'E,Stream,[cluster{ id => Id, lex => Lex, token => Token}|R],R) :- %'0
	(Token == Lex ->
	 format(Stream,'~w{~w}',[Id,Lex])
	;
	 format(Stream,'~w{~w:~w}',[Id,Lex,Token])
	)
	.

format_hook(0'e,Stream,[Id|R],R) :- %'0
	E::edge{ id => Id },
	format(Stream,'~E',[E])
	.

format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :- %'0
        mutable(M,0,true),
        every((   domain(A,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A]) ))
        .

format_hook(0'U,Stream,[[Format,Sep],AA|R],R) :- %'0
        mutable(M,0,true),
        every((   domain(A:B,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A,B]) ))
        .

:-light_tabular tab_item_term/2.
:-mode(tab_item_term/2,+(-,-)).

tab_item_term(I,T) :- item_term(I,T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-extensional pri/2.

pri('combien?','GR').
pri('quand?','GR').
%%pri('o�?','GP').
pri('o�?','GR').
pri('pourquoi?','GR').
pri('comment?','GR').
pri('commentComp?',GR).

:-extensional prel/2.

prel('dont','GP').
prel('o�','GP').

%% Prep:

%% v
%% pendant

%% nc

%% adj

%% favor adv with following verb rather than with aux

%% verify MOD-N entre dans 'quel X' 'chaque X'

%% verifier status de combien

%% Verifier MOD-N sur Revues dans am:33

%% Etrange SUJ-V sur am:43

%% Mauvais COMP dans am:54 + beaucoup d'autres phenomenes etranges

%% Verifier entre COD-V ou CPL-V pour "de Sinf"

%% Eviter interpretations de "prep que' comme GP et preferer 'csu' donc COMP

%% Renforcer encore "GN de GN" plutot que argument de verbe (COD-V)

%% am:243 traitement coordonant ternaire

%% am:244 coord sur ADV

%% am:246 coord sur ADJ

:-extensional not_a_prep/1.

not_a_prep('il y a').

:-extensional catpref/4.

catpref(que,_,csu,1000).
catpref(comme,_,csu,500).
catpref(mais,mais,coo,1000).
catpref(donc,donc,coo,1000).
catpref(plus,plus,adv,1500).
catpref(pas,pas,nc,-1000).
catpref(_,plus,coo,-1000).
catpref(aussi,aussi,adv,1000).
catpref(pour,pour,prep,1000).
catpref(alors,alors,adv,1000).
catpref(avoir,avoir,v,1000).
catpref(avoir,avoir,aux,1000).
catpref(concernant,concerner,v,1000).
catpref(la,_,nc,-1000).
catpref('par exemple',_,adv,3000).
catpref('eh bien',_,pres,2000).
catpref(avec,avec,prep,1000).
catpref(cent,_,det,1000).
catpref('au moins',_,adv,3000).
%%catpref('en particulier',_,adv,2000).
catpref('alors',_,adv,100).
catpref('m�me',_,adv,100).
catpref('m�me',_,pro,-1000).
catpref('d'' abord',_,adv,100).
catpref('en outre',_,adv,3000).
%%catpref('en effet',_,adv,2000).
catpref('� peine',_,adv,3000).
catpref('surtout',_,adv,2000).
catpref('maintenant',maintenant,adv,1000).
catpref('trop',trop,adv,1000).
catpref('sans doute',_,adv,2000).
catpref('en m�me temps',_,adv,2000).
catpref('sans cesse',_,adv,2000).
catpref('de m�me',_,adv,2000).
catpref('au mieux',_,adv,2500).
catpref('tout � l'' heure',_,adv,2500).
catpref('tout � fait',_,adv,2000).
%%catpref('en vigueur',_,adv,3000).
catpref(_,'afin de',prep,4000).
%%catpref(_,'� partir de',prep,2000).
%%catpref(_,'autour de',prep,1000).
catpref(_,'au dehors',adv,1500).
%%catpref(_,'au dehors de',prep,1000).
catpref('candidat','candidat',nc,1000).
catpref(_,'de plus en plus',adv,5000).
catpref(_,'de moins en moins',adv,5000).
catpref(_,'quelque chose',pro,3000).
catpref(_,'quelqu''un',pro,3000).
catpref(_,'fille',nc,500).
catpref(_,'dernier',adj,100).
catpref(_,'prochain',adj,200).
catpref(_,'europ�en',adj,1000).
catpref('il y a','il y a',prep,-2000).
catpref('en vain','en vain',adv,3000).
catpref(_,'autre',adj,500).
%%catpref(_,'en revanche',adv,2000).
catpref(_,'conform�ment �',prep,-2000).
%%catpref(_,'par ailleurs',adv,2000).
catpref('tout d''abord',_,adv,3000).
catpref(_,'en moyenne',adv,2000).
catpref(_,'d''ailleurs',adv,3000).
catpref(pas,pas,advneg,1000).
catpref(_,'par contre',adv,3000).
catpref(_,pendant,prep,2000).
catpref(_,'pr�s de',prep,2000).
catpref(_,'non',adv,2000).
catpref(_,'oui',adv,2000).
catpref('m�me','m�me',adv,800).
%%catpref(_,'d''accord',adv,1000).
%% catpref('avoir','avoir',nc,-500).
catpref(_,sur,prep,800).
catpref(_,mort,cat[adj,nc],500).
%% To rewrite some specialized cat into more abstract ones
cat_abstract(ilimp,cln).
cat_abstract(caimp,cln).
catpref(_,durant,prep,2000).
catpref(est,est,cat[nc],-1000).

catpref(_,'de plus',adv,1600).
catpref(_,tout,nc,-1000).

%% rather adj than past participle
catpref(_,clos,adj,1500).
catpref(_,'mort',adj,1500).
catpref(_,'maudit',adj,1500).
catpref(_,suivant,adj,200).
catpref(_,cependant,adv,1500).
catpref(nombre,_,v,-2000).
catpref(nombres,_,v,-2000).
%%catpref(_,si,csu,1000).
catpref(face,face,ncpred,500).	% to counterbalance 'face �' in faire face �

catpref(_,'ce que',csu,-2000).

catpref(_,'� ce que',csu,-5000).
catpref(_,'de ce que',csu,-5000).


catpref(_,aussi,csu,-4000).
catpref(_,si,adv,1200).
catpref(bref,bref,nc,-200).
catpref(�tre,�tre,nc,-200).
%%catpref(_,'plus du',det,-500).
catpref(_,plus,predet,1000).	%as strong as adv version
%% � m�me de: prep

catpref(_,tel,adj,1000).

%% ?? 'quelque chose de p�trifi�' construction specifique sur 'quelque chose de'

%% run27 lemonde:63 etrange !
%% run27 lemonde:78
%%       lemonde:189
%%       lemonde:205 double sujet dont un sur aux


:-extensional passage_pos/2.

%% Official names for Passage POS

passage_pos(nc,commonNoun).
passage_pos(det,definiteArticle). %% **
passage_pos(prep,preposition).
passage_pos(v,verb).
passage_pos(adj,qualifierAdjective). %% **
passage_pos(adv,adverb).
passage_pos(cln,personalPronoun).
passage_pos(coo,coordinatingConjunction).
passage_pos(np,properNoun).
passage_pos(aux,verb).
passage_pos(prel,relativePronoun).
passage_pos(clneg,negativeParticle).
passage_pos(pro,personalPronoun). %% **
passage_pos(advneg,adverb).
passage_pos(csu,subordinatingConjunction).
passage_pos(que,subordinatingConjunction).
passage_pos(ponctw,secondaryPunctuation).
passage_pos(cla,personalPronoun).
passage_pos(clr,personalPronoun).
passage_pos(pri,personalPronoun). %% **
passage_pos(cld,personalPronoun).
passage_pos(ce,personalPronoun). %% **
passage_pos(ncpred,commonNoun).
passage_pos(number,numeral).
passage_pos(cll,personalPronoun). %% **
%% passage_pos(que_restr,???). %% **
passage_pos(clg,personalPronoun).
%% passage_pos(adjPref,???). %% **
%% passage_pos(advPref,???). %% **
%% passage_pos(xpro,???). %% **
%% passage_pos(predet,???). %% **
passage_pos(pres,interjection). %% **

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Passage compound form -- October 2009

:-extensional passage_compound/1.

passage_compound('un peu').
passage_compound('quelque peu').
passage_compound('un petit peu').
passage_compound('pas du tout').
passage_compound('quelque fois').
passage_compound('nulle part').
passage_compound('bien entendu').
passage_compound('le cas �ch�ant').
passage_compound('autrement dit').
passage_compound('y compris').
passage_compound('peut-�tre').
passage_compound('n''est-ce pas').
passage_compound('� vrai dire').
passage_compound('� verse').
passage_compound('un tant soit peu').
passage_compound('qui plus est').
passage_compound('le cas �ch�ant').
passage_compound('autrement dit').
passage_compound('y compris').
passage_compound('peust-estre').
passage_compound('tout �  fait').
passage_compound('tout de m�me').
passage_compound('tout de suite').
passage_compound('tout � l''heure').
passage_compound('tout d''abord').
passage_compound('tout � coup').
passage_compound('tout d''un coup').
passage_compound('tout au plus').
passage_compound('tout � l''heure').
passage_compound('tout � fait').
passage_compound('tout-�-fait').
passage_compound('tout le temps').
passage_compound('tout le long').
passage_compound('peu � peu').
passage_compound('petit � petit').
passage_compound('tour � tour').
passage_compound('pied � pied').
passage_compound('mot � mot').
passage_compound('peu � peu').
passage_compound('�a et l� ').
passage_compound('bel et bien').
passage_compound('a priori').
passage_compound('a posteriori').
passage_compound('a fortiori').
passage_compound('de facto').
passage_compound('grosso modo').
passage_compound('bis').
passage_compound('ex aequo').
passage_compound('a priori').
passage_compound('le moins').
passage_compound('le plus').
passage_compound('le mieux').
passage_compound('l�-dessus').
passage_compound('l�-dedans').
passage_compound('ci-dessus').
passage_compound('ci-apr�s').
passage_compound('ci-dessous').
passage_compound('l�-dessous').
passage_compound('l�-bas').
passage_compound('quand m�me').
passage_compound('du tout').
passage_compound('de suite').
passage_compound('par contre').
passage_compound('pour de bon').
passage_compound('d''ici peu').
passage_compound('de rechef ').
passage_compound('au jour le jour').
passage_compound('ainsi de suite').
passage_compound('bon an mal an').
passage_compound('tel quel').
passage_compound('afin de').
passage_compound('� commencer par').
passage_compound('� compter de').
passage_compound('� fleur de').
passage_compound('� m�me ').
passage_compound('� moins de').
passage_compound('� partir de').
passage_compound('� savoir').
passage_compound('� travers').
passage_compound('� raison de').
passage_compound('aupr�s de').
passage_compound('au vu et au su de').
passage_compound('en ce qui concerne').
passage_compound('face �').
passage_compound('faute de').
passage_compound('gr�ce �').
passage_compound('histoire de').
passage_compound('le long de').
passage_compound('lors de').
passage_compound('quant �').
passage_compound('quitte �').
passage_compound('sans compter').
passage_compound('sauf �').
passage_compound('suite �').
passage_compound('tout au long de').
passage_compound('vis-�-vis de ').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Preferences for attachement

%% should use a SQLite database rather than this enumeration

:-extensional prep_pref/4.

%%% prep_pref(pourcent,nc,de,100). %% 21
%%% prep_pref(maladie,nc,de,100). %% 21
%%% prep_pref(nom,nc,de,100). %% 16
%% prep_pref(%,nc,des,100). %% 15
%%% prep_pref(�valuation,nc,de,100). %% 13
%%% prep_pref(cours,nc,de,100). %% 10
%%% prep_pref(syst�me,nc,de,100). %% 9
%%% prep_pref(�chelle,nc,de,100). %% 8
%%% prep_pref(test,nc,de,100). %% 8
%%% prep_pref(fonction,nc,de,100). %% 8
%%% prep_pref(fin,nc,de,100). %% 8
%%% prep_pref(ch�teau,nc,de,100). %% 8
%%% prep_pref(processus,nc,de,100). %% 7
%% prep_pref(nombre,nc,de,100). %% 7
%%% prep_pref(niveau,nc,de,100). %% 7
%%% prep_pref(mode,nc,de,100). %% 7
%%% prep_pref(mise,nc,en,100). %% 7
%%% prep_pref(lieu,nc,de,100). %% 7
%%% prep_pref(condition,nc,de,100). %% 7
%%% prep_pref(cadre,nc,de,100). %% 7
%%% prep_pref(technologie,nc,de,100). %% 6
%%% prep_pref(partie,nc,de,100). %% 6
%%% prep_pref(jour,nc,de,100). %% 6
%%% prep_pref(dur�e,nc,de,100). %% 6
%%prep_pref(diagnostic,nc,de,100). %% 6
%%% prep_pref(crit�re,nc,de,100). %% 6
%%% prep_pref(coup,nc,de,100). %% 6
%%% prep_pref(base,nc,de,100). %% 6
%%% prep_pref(absence,nc,de,100). %% 6
%%% prep_pref(�volution,nc,de,100). %% 5
%%% prep_pref(�tat,nc,de,100). %% 5
%%% prep_pref(�gard,nc,de,100). %% 5
%%% prep_pref(r�solution,nc,de,100). %% 5
%%% prep_pref(recherche,nc,de,100). %% 5
%%% prep_pref(pr�l�vement,nc,de,100). %% 5
%%% prep_pref(proposition,nc,de,100). %% 5
%%% prep_pref(occasion,nc,de,100). %% 5
%%% prep_pref(n�gociation,nc,de,100). %% 5
%%% prep_pref(n�gociations,nc,avec,100). %% 5
%%% prep_pref(niveau,nc,de,100). %% 5
prep_pref(mois,nc,apr�s,100). %% 5
%%% prep_pref(main,nc,de,100). %% 5
%%% prep_pref(liste,nc,de,100). %% 5
%%% prep_pref(histoire,nc,de,100). %% 5
%%% prep_pref(fille,nc,�,100). %% 5
%%% prep_pref(femme,nc,de,100). %% 5
%%% prep_pref(d�veloppement,nc,de,100). %% 5
%%% prep_pref(droit,nc,de,100). %% 5
%%% prep_pref(domaine,nc,de,100). %% 5
%%% prep_pref(cr�ation,nc,de,100). %% 5
%%% prep_pref(conclusion,nc,de,100). %% 5
%%% prep_pref(compte,nc,de,100). %% 5
%%% prep_pref(capacit�,nc,de,100). %% 5
%%% prep_pref(besoin,nc,de,100). %% 5
%%% prep_pref(conseil,nc,de,100). %% 5
%%% prep_pref(yeux,nc,de,100). %% 4
%% prep_pref(week-end,nc,�,100). %% 4
%%% prep_pref(ville,nc,de,100). %% 4
%%% prep_pref(trait�,nc,sur,100). %% 4
%%% prep_pref(temps,nc,de,100). %% 4
%%% prep_pref(taux,nc,de,100). %% 4
%%% prep_pref(s�rie,nc,de,100). %% 4
%%% prep_pref(r�glementation,nc,en,100). %% 4
%%% prep_pref(robe,nc,de,100). %% 4
%%% prep_pref(respect,nc,de,100). %% 4
%%% prep_pref(rapport,nc,de,100). %% 4
%%% prep_pref(qualit�,nc,de,100). %% 4
%%% prep_pref(pr�sence,nc,de,100). %% 4
%%prep_pref(politique,nc,de,100). %% 4
%%% prep_pref(point,nc,de,100). %% 4
%%% prep_pref(organisme,nc,de,100). %% 4
%%% prep_pref(objectif,nc,de,100). %% 4
%%% prep_pref(nombre,nc,de,100). %% 4
%%% prep_pref(m�moire,nc,de,100). %% 4
%%% prep_pref(mois,nc,de,100). %% 4
%%% prep_pref(mod�le,nc,de,100). %% 4
%%% prep_pref(milieu,nc,de,100). %% 4
%%% prep_pref(local,nc,de,100). %% 4
%%% prep_pref(int�rieur,nc,de,100). %% 4
prep_pref(infection,nc,par,100). %% 4
%%prep_pref(huile,nc,de,100). %% 4
%%% prep_pref(honneur,nc,de,100). %% 4
%%% prep_pref(groupe,nc,de,100). %% 4
%%% prep_pref(gestion,nc,de,100). %% 4
%%% prep_pref(fin,nc,de,100). %% 4
%%% prep_pref(fait,nc,de,100). %% 4
%%% prep_pref(existence,nc,de,100). %% 4
%%% prep_pref(d�but,nc,de,100). %% 4
prep_pref(d�bat,nc,sur,100). %% 4
%%% prep_pref(c�t�,nc,de,100). %% 4
prep_pref(consid�ration,nc,dans,100). %% 4
%%% prep_pref(cadeau,nc,de,100). %% 4
%% prep_pref(anni,nc,de,100). %% 4
%%% prep_pref(activit�,nc,de,100). %% 4
prep_pref(acc�s,nc,�,100). %% 4
prep_pref(bonjour,nc,�,100). %% 4
%%% prep_pref(adoption,nc,de,100). %% 4
%% prep_pref(�cus,nc,pour,100). %% 3
%%% prep_pref(�ge,nc,de,100). %% 3
%%% prep_pref(voie,nc,de,100). %% 3
%%% prep_pref(vie,nc,de,100). %% 3
%%% prep_pref(vente,nc,de,100). %% 3
%%% prep_pref(trouble,nc,de,100). %% 3
%%% prep_pref(transport,nc,de,100). %% 3
%%% prep_pref(traitement,nc,de,100). %% 3
%%% prep_pref(train,nc,de,100). %% 3
%%% prep_pref(tour,nc,de,100). %% 3
%%% prep_pref(th�orie,nc,de,100). %% 3
%%% prep_pref(temps,nc,en,100). %% 3
%%% prep_pref(temps,nc,de,100). %% 3
%%% prep_pref(succ�s,nc,de,100). %% 3
%%% prep_pref(sp�cificit�,nc,de,100). %% 3
%%% prep_pref(service,nc,de,100). %% 3
%%% prep_pref(sein,nc,de,100). %% 3
%%% prep_pref(r�le,nc,de,100). %% 3
%%% prep_pref(r�alisation,nc,de,100). %% 3
%%% prep_pref(r�gne,nc,de,100). %% 3
%%% prep_pref(r�leuse,nc,de,100). %% 3
%%% prep_pref(risque,nc,de,100). %% 3
%%% prep_pref(restructuration,nc,de,100). %% 3
%%% prep_pref(reste,nc,de,100). %% 3
%% prep_pref(repas,nc,de,100). %% 3
prep_pref(relations,nc,avec,100). %% 3
%%% prep_pref(question,nc,de,100). %% 3
%%% prep_pref(p�riode,nc,de,100). %% 3
%%% prep_pref(pr�sence,nc,de,100). %% 3
prep_pref(pr�l�vement,nc,sur,100). %% 3
%%% prep_pref(pr�l�vement,nc,de,100). %% 3
%%% prep_pref(projet,nc,de,100). %% 3
%%% prep_pref(progression,nc,de,100). %% 3
%%% prep_pref(produit,nc,de,100). %% 3
%%% prep_pref(production,nc,de,100). %% 3
%%% prep_pref(prise,nc,de,100). %% 3
%%% prep_pref(prise,nc,en,100). %% 3
%%% prep_pref(prestation,nc,de,100). %% 3
%%% prep_pref(pied,nc,sur,100). %% 3
%%% prep_pref(pays,nc,en,100). %% 3
%% prep_pref(pas,nc,de,100). %% 3
%%% prep_pref(parti,nc,de,100). %% 3
%%% prep_pref(ouverture,nc,de,100). %% 3
%%% prep_pref(origine,nc,de,100). %% 3
%%% prep_pref(ordre,nc,de,100). %% 3
%%prep_pref(oeuvre,nc,de,100). %% 3
%%% prep_pref(n�cessit�,nc,de,100). %% 3
%%% prep_pref(nature,nc,de,100). %% 3
%%% prep_pref(nature,nc,de,100). %% 3
%%% prep_pref(membre,nc,de,100). %% 3
%%% prep_pref(mati�re,nc,de,100). %% 3
%%% prep_pref(march�,nc,de,100). %% 3
prep_pref(mail,nc,sur,100). %% 3
%%% prep_pref(longueur,nc,de,100). %% 3
%%% prep_pref(lib�ration,nc,de,100). %% 3
%%% prep_pref(jour,nc,de,100). %% 3
%%% prep_pref(id�e,nc,de,100). %% 3
%%% prep_pref(gr�ce,nc,de,100). %% 3
%%% prep_pref(geste,nc,de,100). %% 3
%%% prep_pref(gens,nc,de,100). %% 3
%%% prep_pref(f�te,nc,de,100). %% 3
%%% prep_pref(fondation,nc,de,100). %% 3
%%% prep_pref(figure,nc,de,100). %% 3
%%% prep_pref(feuille,nc,de,100). %% 3
%%% prep_pref(fa�on,nc,�,100). %% 3
%%% prep_pref(facteur,nc,de,100). %% 3
%%% prep_pref(espoir,nc,de,100). %% 3
%% prep_pref(ergot,nc,de,100). %% 3
%%% prep_pref(envie,nc,de,100). %% 3
%%prep_pref(ensemble,nc,de,100). %% 3
%%% prep_pref(enfants,nc,de,100). %% 3
%%% prep_pref(effort,nc,pour,100). %% 3
%%% prep_pref(effort,nc,de,100). %% 3
%%% prep_pref(efficacit�,nc,de,100). %% 3
%%% prep_pref(effet,nc,de,100). %% 3
%% prep_pref(d�mence,nc,selon,100). %% 3
%%% prep_pref(d�l�gation,nc,de,100). %% 3
%%% prep_pref(dosage,nc,de,100). %% 3
%%% prep_pref(demande,nc,de,100). %% 3
%%% prep_pref(date,nc,de,100). %% 3
%%% prep_pref(croissance,nc,de,100). %% 3
%%% prep_pref(pouvoir,nc,de,100). %% 3
%%% prep_pref(crit�re,nc,de,100). %% 3
%%% prep_pref(crise,nc,de,100). %% 3
%%% prep_pref(courant,nc,de,100). %% 3
%%% prep_pref(corps,nc,de,100). %% 3
%%% prep_pref(coq,nc,�,100). %% 3
%%% prep_pref(choix,nc,de,100). %% 3
%%% prep_pref(chemin,nc,de,100). %% 3
%%% prep_pref(centre,nc,de,100). %% 3
%%% prep_pref(bruit,nc,de,100). %% 3
%%% prep_pref(bo�te,nc,de,100). %% 3
%%% prep_pref(besoin,nc,de,100). %% 3
%%% prep_pref(besoin,nc,pour,100). %% 3
%%% prep_pref(attitude,nc,de,100). %% 3
%%% prep_pref(application,nc,de,100). %% 3
%%% prep_pref(anniversaire,nc,de,100). %% 3
%%% prep_pref(allure,nc,de,100). %% 3
%%% prep_pref(air,nc,de,100). %% 3
%%% prep_pref(adh�sion,nc,de,100). %% 3
prep_pref(adh�sion,nc,�,100). %% 3
prep_pref(accord,nc,avec,100). %% 3
%% prep_pref(Restauration,nc,apr�s,100). %% 3
%%prep_pref(Ordre,nc,du,100). %% 3
%%prep_pref(Monde,nc,du,100). %% 3
%%% prep_pref(merci,nc,de,100). %% 3
prep_pref(merci,nc,pour,100). %% 3
%%prep_pref(Coop�ration,nc,au,100). %% 3
%% prep_pref(�quipes,nc,de,100). %% 2
%%% prep_pref(�quipe,nc,de,100). %% 2
%%% prep_pref(�preuve,nc,de,100). %% 2
%%% prep_pref(�poque,nc,�,100). %% 2
%%% prep_pref(�l�ve,nc,de,100). %% 2
prep_pref(�largissement,nc,�,100). %% 2
%%% prep_pref(�largissement,nc,de,100). %% 2
%%% prep_pref(�lan,nc,de,100). %% 2
%%% prep_pref(�dition,nc,en,100). %% 2
%%% prep_pref(�conomie,nc,de,100). %% 2
%% prep_pref(�clats,nc,de,100). %% 2
%%% prep_pref(�clat,nc,de,100). %% 2
%%% prep_pref(�change,nc,de,100). %% 2
%%% prep_pref(�me,nc,de,100). %% 2
%% prep_pref(yeux,nc,d',100). %% 2
%% prep_pref(v�sicule,nc,dans,100). %% 2
%% prep_pref(v�hicules,nc,de,100). %% 2
prep_pref(soutien,nc,�,200). %% 2
prep_pref(relation,nc,entre,200). %% 2
prep_pref(passage,nc,�,200). %% 2
prep_pref(opposant,nc,�,200). %% 2
%% prep_pref(machine,nc,�,200). %% 2
prep_pref(livre,nc,sur,200). %% 2
prep_pref(invitation,nc,�,200). %% 2
prep_pref(donn�e,nc,sur,200). %% 2
prep_pref(contraste,nc,avec,200). %% 2
prep_pref(continuit�,nc,avec,200). %% 2
prep_pref(confiance,nc,�,200). %% 2
prep_pref(avis,nc,sur,200). %% 2
prep_pref(attention,nc,sur,200). %% 2
prep_pref(aide,nc,�,200). %% 2
prep_pref(�quilibre,nc,entre,200). %% 1

%% for PP on AdjP, we have to counter the negative effect of -PP_ON_ADJP
%% prep_pref(possible,adj,de,700). %% 7
prep_pref(ok,adj,pour,630). %% 7
prep_pref(partant,adj,pour,630). %% 4
prep_pref(relatif,adj,�,630). %% 3
prep_pref(pr�t,adj,�,630). %% 3
prep_pref(proche,adj,de,630). %% 3
prep_pref(n�cessaire,adj,de,630). %% 3
prep_pref(inf�rieur,adj,�,630). %% 3
%% prep_pref(difficile,adj,de,630). %% 3
%% prep_pref(central,adj,de,630). %% 3
%% prep_pref(suspect,adj,de,630). %% 2
%% prep_pref(sp�cifique,adj,de,630). %% 2
prep_pref(responsable,adj,de,630). %% 2
prep_pref(pr�sent,adj,dans,630). %% 2
prep_pref(naturel,adj,de,630). %% 2
prep_pref(issu,adj,de,630). %% 2
prep_pref(indispensable,adj,pour,630). %% 2
%% prep_pref(indispensable,adj,de,630). %% 2
prep_pref(favorable,adj,�,630). %% 2
prep_pref(disponible,adj,dans,630). %% 2
prep_pref(disponible,adj,sur,630). %% 2
prep_pref(digne,adj,de,630). %% 2
prep_pref(contraire,adj,�,630). %% 2
prep_pref(capable,adj,de,630). %% 2
prep_pref(atteint,adj,de,630). %% 2
prep_pref(accessibles,adj,�,630). %% 2
prep_pref(�pris,adj,de,630). %% 1
prep_pref(seul,adj,avec,630). %% 2
prep_pref(utilisable,adj,par,630). %% 1
prep_pref(utilisable,avec,par,630). %% 1
%% prep_pref(utiles,adj,en,630). %% 1
prep_pref(utile,adj,de,630). %% 1
%% prep_pref(susceptible,adj,de,630). %% 1
prep_pref(sup�rieure,adj,�,630). %% 1
prep_pref(suffisant,adj,pour,630). %% 1
prep_pref(sourd,adj,�,600). %% 1
prep_pref(sensible,adj,�,600). %% 1
prep_pref(r�ticents,adj,�,630). %% 1
prep_pref(riche,adj,en,600). %% 1
prep_pref(respectueux,adj,de,630). %% 1
prep_pref(repr�sentatif,adj,de,600). %% 1
prep_pref(mort,adj,de,630). %% 1
prep_pref(impossible,adj,de,600). %% 1
prep_pref(identique,adj,�,630). %% 1
prep_pref(heureux,adj,de,600).
prep_pref(gauche,adj,de,600). %% 1
prep_pref(droit,adj,de,200). %% 1


:-extensional ante_adj_pref/2.

ante_adj_pref(petit,200).
ante_adj_pref(cher,200).
ante_adj_pref(grand,200).
ante_adj_pref(bon,200).
ante_adj_pref(premier,200).
ante_adj_pref(dernier,200).
ante_adj_pref(second,200).
ante_adj_pref(prochain,200).
ante_adj_pref(jeune,200).
ante_adj_pref(pauvre,200).
ante_adj_pref(vieil,200).
ante_adj_pref(beau,200).
ante_adj_pref(nouveau,200).
ante_adj_pref(ancien,200).
ante_adj_pref(gros,200).
ante_adj_pref(principal,200).
ante_adj_pref('_NUMBER',200).
%% extracted from frwiki (PassageEval2/DEP3)
ante_adj_pref(autre,200). %% ratio=88.93% n=82384
ante_adj_pref(nombreux,200). %% ratio=83.89% n=41236
ante_adj_pref(seul,200). %% ratio=83.46% n=40808
ante_adj_pref('m�me',200). %% ratio=91.44% n=21008
ante_adj_pref(meilleur,200). %% ratio=89.95% n=19861
ante_adj_pref(haut,200). %% ratio=58.24% n=17795
ante_adj_pref(bon,200). %% ratio=87.75% n=16363
ante_adj_pref(beau,200). %% ratio=76.01% n=13442
ante_adj_pref(saint,100). %% ratio=55.40% n=13382
ante_adj_pref(certain,200). %% ratio=78.78% n=12415
ante_adj_pref(futur,200). %% ratio=68.39% n=8558
ante_adj_pref('v�ritable',200). %% ratio=88.97% n=8462
ante_adj_pref(double,200). %% ratio=65.07% n=7257
ante_adj_pref(super,200). %% ratio=82.92% n=6576
ante_adj_pref(vrai,200). %% ratio=70.35% n=5623
ante_adj_pref(mauvais,200). %% ratio=82.76% n=4699
ante_adj_pref(vaste,200). %% ratio=73.00% n=4170
ante_adj_pref(fameux,200). %% ratio=79.80% n=3841
ante_adj_pref(immense,200). %% ratio=70.91% n=3070
ante_adj_pref(faux,200). %% ratio=75.72% n=2998
ante_adj_pref(ultime,200). %% ratio=60.68% n=2986
ante_adj_pref(excellent,200). %% ratio=82.11% n=2929
ante_adj_pref('�norme',200). %% ratio=66.95% n=2720
ante_adj_pref(moindre,200). %% ratio=60.18% n=2358
ante_adj_pref(prochain,200). %% ratio=64.54% n=2098
ante_adj_pref(magnifique,100). %% ratio=58.81% n=2088
ante_adj_pref(bref,100). %% ratio=52.76% n=2009
ante_adj_pref(joli,200). %% ratio=73.80% n=1309
ante_adj_pref(triple,200). %% ratio=69.52% n=1135
ante_adj_pref(superbe,200). %% ratio=65.88% n=970
ante_adj_pref(pire,200). %% ratio=72.76% n=826
ante_adj_pref(formidable,100). %% ratio=55.83% n=763
ante_adj_pref(demi,200). %% ratio=61.41% n=583
ante_adj_pref(innombrable,200). %% ratio=66.86% n=513
ante_adj_pref(fervent,100). %% ratio=53.51% n=456
ante_adj_pref(brave,200). %% ratio=53.18% n=393
ante_adj_pref(vilain,200). %% ratio=69.46% n=334
ante_adj_pref(bienheureux,200). %% ratio=75.69% n=255
ante_adj_pref(abominable,100). %% ratio=58.93% n=168
ante_adj_pref(adorable,200). %% ratio=70.06% n=167
ante_adj_pref('pi�tre',200). %% ratio=88.55% n=166
ante_adj_pref('avant-dernier',200). %% ratio=82.53% n=166
ante_adj_pref('v�n�rable',100). %% ratio=53.42% n=146
ante_adj_pref(richissime,200). %% ratio=63.57% n=140
ante_adj_pref(ravissant,200). %% ratio=74.24% n=132
ante_adj_pref(maint,200). %% ratio=68.42% n=95
ante_adj_pref('�ni�me',200). %% ratio=62.92% n=89
ante_adj_pref('dix-septi�me',200). %% ratio=85.00% n=80
ante_adj_pref('dix-huiti�me',200). %% ratio=93.59% n=78
ante_adj_pref('dix-neuvi�me',200). %% ratio=90.41% n=73
ante_adj_pref(incorrigible,200). %% ratio=62.00% n=50
ante_adj_pref(inqualifiable,200). %% ratio=66.67% n=15

ante_adj_pref(large,200). %% ratio=49.08% n=14998


:-extensional rpref/4.

%% Added
rpref('Jeux Olympiques','de','_DATE_arto',243). %% 59367
rpref('viser','�','article',146). %% 21456
rpref('conseil','de','_DATE_arto',138). %% 19083
rpref('article','de','r�glement',132). %% 17566
rpref('pr�voir','�','article',115). %% 13441
rpref('naissance','en','_DATE_year',113). %% 12772
rpref('commission','de','_DATE_arto',109). %% 12006
rpref('entrer','en','vigueur',107). %% 11536
rpref('modifier','par','r�glement',106). %% 11309
rpref('droits','de','homme',102). %% 10450
rpref('page','dans','section',99). %% 9957
rpref('conform�ment','�','article',99). %% 9949
rpref('mise','en','oeuvre',99). %% 9927
rpref('proposition','de','commission',99). %% 9815
rpref('remplacer','par','texte',96). %% 9365
rpref('d�c�s','en','_DATE_year',95). %% 9125
rpref('mettre','en','oeuvre',91). %% 8405
rpref('viser','�','paragraphe',91). %% 8313
rpref('point','de','vue',90). %% 8161
rpref('commission','de','communaut�',89). %% 7969
rpref('membre','de','commission',87). %% 7607
rpref('avis','de','comit�',86). %% 7416
rpref('num�ro','de','conseil',85). %% 7358
rpref('d�cision','de','commission',83). %% 7032
rpref('directive','de','conseil',83). %% 6909
rpref('annexe','de','r�glement',81). %% 6670
rpref('obligatoire','dans','�l�ment',81). %% 6643
rpref('mettre','en','place',78). %% 6132
rpref('publication','�','journal',76). %% 5887
rpref('prendre','en','consid�ration',75). %% 5692
rpref('jour','de','publication',75). %% 5671
rpref('disposition','de','article',74). %% 5569
rpref('autorit�','de','�tat',74). %% 5520
rpref('num�ro','de','commission',74). %% 5481
rpref('paragraphe','de','r�glement',73). %% 5447
rpref('application','de','article',73). %% 5417
rpref('ordre','de','jour',73). %% 5334
rpref('d�cision','de','conseil',72). %% 5296
rpref('figurer','�','annexe',72). %% 5250
rpref('si�cle','avant','_Uv',70). %% 4966
rpref('modalit�','de','application',67). %% 4520
rpref('�tre','en','mesure',66). %% 4452
rpref('article','de','directive',66). %% 4435
rpref('million','de','euro',66). %% 4367
rpref('jour','suivant','celui',66). %% 4359
rpref('organisation','de','march�',65). %% 4330
rpref('entr�e','en','vigueur',65). %% 4319
rpref('mener','�','cat�gorie',65). %% 4243
rpref('veiller','�','ce',64). %% 4184
rpref('application','de','r�glement',64). %% 4173
rpref('voir','pour','article',63). %% 4045
rpref('chemin','de','fer',63). %% 3978
rpref('ville','de','_Uv',62). %% 3959
rpref('conform�ment','�','disposition',62). %% 3955
rpref('sortir','en','_DATE_year',62). %% 3893
rpref('r�glement','de','commission',62). %% 3845
rpref('article','de','trait�',62). %% 3844
rpref('viser','�','point',60). %% 3692
rpref('prendre','en','compte',60). %% 3634
rpref('conform�ment','�','proc�dure',60). %% 3602
rpref('d�c�s','en','_NUMBER',59). %% 3564
rpref('cours','de','ann�e',59). %% 3554
rpref('p�riode','de','enqu�te',59). %% 3546
rpref('r�publique','de','_LOCATION',59). %% 3522
rpref('march�','dans','secteur',59). %% 3517
rpref('r�glement','de','base',58). %% 3463
rpref('n�','de','commission',57). %% 3363
rpref('sens','de','article',57). %% 3362
rpref('r�glement','de','conseil',57). %% 3353
rpref('annexe','de','directive',57). %% 3321
rpref('communiquer','�','commission',57). %% 3298
rpref('vertu','de','article',56). %% 3241
rpref('pr�voir','�','r�glement',56). %% 3186
rpref('comit�','de','gestion',56). %% 3155
rpref('cours','de','p�riode',56). %% 3149
rpref('conseil','de','union',55). %% 3122
rpref('fin','en','_DATE_year',55). %% 3085
rpref('pays','en','d�veloppement',55). %% 3072
rpref('modifier','par','directive',54). %% 2960
rpref('site','de','_Uv',54). %% 2941
rpref('fin','de','ann�e',53). %% 2830
rpref('journal','de','communaut�',53). %% 2828
rpref('na�tre','en','_DATE_year',53). %% 2809
rpref('_DATE_year','de','conseil',52). %% 2791
rpref('nom','de','groupe',52). %% 2772
rpref('rapport','de','_PERSON_m',52). %% 2745
rpref('pr�sident','en','exercice',52). %% 2730
rpref('mettre','�','disposition',51). %% 2688
rpref('_NUMBER','�','_NUMBER',51). %% 2664
rpref('pr�sident','de','conseil',51). %% 2626
rpref('conseil','de','_LOCATION',51). %% 2624
rpref('�tre','de','accord',51). %% 2618
rpref('journal','de','_Uv',51). %% 2615
rpref('pager','dans','cat�gorie',50). %% 2577
rpref('voter','en','faveur',50). %% 2573
rpref('district','de','_Uv',50). %% 2572
rpref('ann�e','de','calendrier',50). %% 2562
rpref('pr�senter','de','int�r�t',50). %% 2561
rpref('aide','de','�tat',50). %% 2551
rpref('pr�sident','de','commission',50). %% 2523
rpref('disposition','de','r�glement',49). %% 2496
rpref('pr�senter','pour','_Uv',49). %% 2495
rpref('mise','en','place',49). %% 2485
rpref('province','de','_LOCATION',49). %% 2452
rpref('cour','de','justice',49). %% 2432
rpref('plan','de','action',49). %% 2414
rpref('l�gislation','de','�tat',48). %% 2396
rpref('�tre','dans','cas',48). %% 2390
rpref('naissance','en','_NUMBER',48). %% 2383
rpref('pr�voir','par','r�glement',48). %% 2381
rpref('pays','de','origine',48). %% 2340
rpref('exercice','de','conseil',48). %% 2340
rpref('importation','de','produit',48). %% 2336
rpref('provenance','de','pays',48). %% 2333
rpref('partir','de','_DATE_arto',48). %% 2309
rpref('nom','de','commission',48). %% 2304
rpref('remplacer','�','article',47). %% 2283
rpref('num�ro','sous','_Uv',47). %% 2258
rpref('r�gion','de','_Uv',47). %% 2222
rpref('notifier','sous','num�ro',46). %% 2207
rpref('sein','de','_Uv',46). %% 2193
rpref('certificat','de','importation',46). %% 2182
rpref('organisme','de','intervention',46). %% 2176
rpref('restitution','�','exportation',45). %% 2114
rpref('�tre','en','effet',45). %% 2113
rpref('sein','de',entities['_ORGANIZATION','_COMPANY'],45). %% 2112
rpref('�tre','en','train',45). %% 2100
rpref('proposition','de','r�solution',45). %% 2091
rpref('r�giment','de','infanterie',45). %% 2087
rpref('avis','de',entities['_ORGANIZATION','_COMPANY'],45). %% 2087
rpref('viser','�','annexe',45). %% 2079
rpref('paragraphe','de','directive',45). %% 2072
rpref('membre','de','_Uv',45). %% 2071
rpref('pourcent','de','prix',45). %% 2054
rpref('base','de','donn�e',45). %% 2044
rpref('pr�voir','�','paragraphe',44). %% 2018
rpref('strat�gie','de',entities['_ORGANIZATION','_COMPANY'],44). %% 2015
rpref('site','de',entities['_ORGANIZATION','_COMPANY'],44). %% 2012
rpref('destinataire','de','d�cision',44). %% 2005
rpref('sein','de','commission',44). %% 1995
rpref('communication','de','commission',44). %% 1987
rpref('satisfaire','�','exigence',44). %% 1965
rpref('membre','de','_ORGANIZATION',44). %% 1960
rpref('directive','de','commission',44). %% 1953
rpref('mise','sur','march�',44). %% 1945
rpref('publier','�','journal',43). %% 1914
rpref('index','de','cat�gorie',43). %% 1910
rpref('acc�s','dans','index',43). %% 1910
rpref('produit','de','p�che',43). %% 1900
rpref('ann�e','de','d�c�s',43). %% 1899
rpref('d�finir','�','article',43). %% 1891
rpref('part','de','march�',43). %% 1883
rpref('article','de','accord',43). %% 1851
rpref('milliard','de','euro',42). %% 1844
rpref('journal','de','_ORGANIZATION',42). %% 1838
rpref('destinataire','de','pr�sente',42). %% 1837
rpref('territoire','de','�tat',42). %% 1836
rpref('histoire','de','_LOCATION',42). %% 1828
rpref('importation','en','provenance',42). %% 1805
rpref('demande','de','certificat',42). %% 1789
rpref('�tre','en','_DATE_year',42). %% 1778
rpref('d�but','de','ann�e',41). %% 1745
rpref('valeur','de','mati�re',41). %% 1743
rpref('application','de','r�gime',41). %% 1741
rpref('application','de','disposition',41). %% 1735
rpref('proposition','de','directive',41). %% 1727
rpref('na�tre','�','_LOCATION',41). %% 1727
rpref('�tat','de','droit',41). %% 1726
rpref('commune','de','_Uv',41). %% 1719
rpref('championnat','de','monde',41). %% 1708
rpref('march�','de','travail',41). %% 1704
rpref('situer','dans','d�partement',41). %% 1694
rpref('annexe','de','d�cision',41). %% 1694
rpref('respect','de','droits',41). %% 1691
rpref('canton','de','_LOCATION',41). %% 1687
rpref('_PERSON_m','de','_LOCATION',40). %% 1676
rpref('journal','de','union',40). %% 1666
rpref('d�lai','de','mois',40). %% 1657
rpref('article','de','d�cision',40). %% 1655
rpref('protection','de','environnement',40). %% 1646
rpref('commission','de','affaire',40). %% 1634
rpref('mati�re','de','position',40). %% 1627
rpref('conformer','�','pr�sente',40). %% 1623
rpref('chronologie','de','si�cle',40). %% 1618
rpref('paragraphe','de','trait�',40). %% 1616
rpref('provenance','de','_LOCATION',40). %% 1614
rpref('huile','de','olive',40). %% 1610
rpref('p�riode','de','an',40). %% 1604
rpref('organisation','de','producteur',40). %% 1602
rpref('applicable','�','partir',40). %% 1601
rpref('gaz','de','�chappement',39). %% 1591
rpref('site','de','Quid',39). %% 1587
rpref('paragraphe','de','article',39). %% 1586
rpref('conseil','de','communaut�',39). %% 1586
rpref('trait�','de','_LOCATION',39). %% 1581
rpref('�ge','de','an',39). %% 1554
rpref('d�but','de','r�gne',39). %% 1552
rpref('conform�ment','�','annexe',39). %% 1549
rpref('conform�ment','�','paragraphe',39). %% 1548
rpref('disposition','pour','conformer',39). %% 1545
rpref('aliment','pour','animal',39). %% 1542
rpref('lier','�','commune',39). %% 1541
rpref('fois','de','plus',39). %% 1533
rpref('adh�sion','de','_LOCATION',39). %% 1531
rpref('prix','de','vente',39). %% 1530
rpref('fixer','�','article',39). %% 1530
rpref('mettre','en','�vidence',39). %% 1523
rpref('�num�rer','�','annexe',39). %% 1522
rpref('programme','de','action',39). %% 1521
rpref('groupe','de','travail',38). %% 1509
rpref('mettre','�','jour',38). %% 1507
rpref('localisation','sur','carte',38). %% 1503
rpref('taux','de','int�r�t',38). %% 1502
rpref('comit�','de','_Uv',38). %% 1501
rpref('viser','�','alin�a',38). %% 1493
rpref('transport','de','marchandise',38). %% 1490
rpref('r�pondre','�','question',38). %% 1481
rpref('mesure','de','possible',38). %% 1476
rpref('appel','de','offre',38). %% 1473
rpref('ministre','de','affaire',38). %% 1471
rpref('r�gion','de','_LOCATION',38). %% 1466
rpref('secteur','de','viande',38). %% 1465
rpref('destiner','�','couvrir',38). %% 1459
rpref('conseil','de','ministre',38). %% 1455
rpref('titre','de','article',38). %% 1454
rpref('violation','de','droits',38). %% 1446
rpref('r�gime','de','aide',37). %% 1437
rpref('�conomie','de','march�',37). %% 1435
rpref('type','de','v�hicule',37). %% 1432
rpref('comit�','de','r�gion',37). %% 1431
rpref('date','de','entr�e',37). %% 1427
rpref('�tat','de','_ORGANIZATION',37). %% 1422
rpref('disposition','de','paragraphe',37). %% 1419
rpref('v�hicule','�','moteur',37). %% 1416
rpref('mettre','en','vigueur',37). %% 1416
rpref('acte','de','adh�sion',37). %% 1414
rpref('�change','de','information',37). %% 1401
rpref('rapport','de','commission',37). %% 1392
rpref('pomme','de','terre',37). %% 1386
rpref('principe','de','subsidiarit�',37). %% 1384
rpref('millilitre','de','solution',37). %% 1384
rpref('certificat','de','exportation',37). %% 1370
rpref('source','de','�nergie',37). %% 1369
rpref('animal','de','esp�ce',36). %% 1368
rpref('Plan','de','_LOCATION',36). %% 1364
rpref('royaume','de','_LOCATION',36). %% 1357
rpref('sommet','de','_LOCATION',36). %% 1353
rpref('conseil','de',entities['_ORGANIZATION','_COMPANY'],36). %% 1352
rpref('reprendre','�','annexe',36). %% 1349
rpref('sud','de','_LOCATION',36). %% 1347
rpref('modifier','par','d�cision',36). %% 1347
rpref('chiffre','de','affaire',36). %% 1345
rpref('question','de','savoir',36). %% 1344
rpref('charte','de','droits',36). %% 1339
rpref('transmettre','�','commission',36). %% 1337
rpref('int�r�t','pour','_Uv',36). %% 1336
rpref('bataille','de','_LOCATION',36). %% 1327
rpref('valeur','�','importation',36). %% 1321
rpref('localisation','de','_LOCATION',36). %% 1320
rpref('d�partement','de','_LOCATION',36). %% 1313
rpref('�tat','de','_Uv',36). %% 1312
rpref('sein','de','union',36). %% 1307
rpref('partir','de','_NUMBER',36). %% 1298
rpref('championnat','de','_LOCATION',36). %% 1298
rpref('pr�sident','de','_Uv',35). %% 1293
rpref('disposition','de','directive',35). %% 1293
rpref('demander','�','commission',35). %% 1293
rpref('gouvernement','de','_LOCATION',35). %% 1292
rpref('montant','de','aide',35). %% 1289
rpref('�tablissement','de','cr�dit',35). %% 1288
rpref('prestation','de','service',35). %% 1286
rpref('protection','de','donn�e',35). %% 1285
rpref('d�cision','de','_DATE_arto',35). %% 1285
rpref('mettre','�','point',35). %% 1283
rpref('mettre','sur','march�',35). %% 1282
rpref('proposition','de','r�glement',35). %% 1281
rpref('membre','de','famille',35). %% 1262
rpref('campagne','de','commercialisation',35). %% 1262
rpref('droits','de','douane',35). %% 1261
rpref('pr�sident','de','_LOCATION',35). %% 1256
rpref('lutte','contre','terrorisme',35). %% 1255
rpref('application','de','directive',35). %% 1254
rpref('jour','de','ann�e',35). %% 1252
rpref('date','de','_DATE_arto',35). %% 1251
rpref('texte','de','int�r�t',35). %% 1246
rpref('num�ro','de','_DATE_arto',35). %% 1246
rpref('m�thode','de','analyse',35). %% 1242
rpref('pays','de','_Uv',35). %% 1234
rpref('�tre','�','origine',35). %% 1233
rpref('conclusion','de','accord',34). %% 1223
rpref('march�','de','produit',34). %% 1220
rpref('publier','en','_DATE_year',34). %% 1218
rpref('aller','�','encontre',34). %% 1214
rpref('expiration','de','d�lai',34). %% 1213
rpref('r�alisation','de','objectif',34). %% 1209
rpref('position','de','conseil',34). %% 1206
rpref('mesure','�','prendre',34). %% 1199
rpref('pr�voir','par','d�cision',34). %% 1197
rpref('disposition','de','droit',34). %% 1186
rpref('pr�voir','�','annexe',34). %% 1185
rpref('trait�','de','_ORGANIZATION',34). %% 1182
rpref('programme','de','travail',34). %% 1181
rpref('appliquer','dans','cas',34). %% 1180
rpref('syst�me','de','qualit�',34). %% 1177
rpref('politique','en','mati�re',34). %% 1174
rpref('politique','de','p�che',34). %% 1168
rpref('d�rogation','�','article',34). %% 1167
rpref('cadre','de','programme',34). %% 1167
rpref('arr�ter','par','�tat',34). %% 1164
rpref('d�put�','de','_LOCATION',34). %% 1160
rpref('pi�ce','de','th��tre',34). %% 1159
rpref('protection','de','consommateur',34). %% 1157
rpref('cadre','de','adjudication',34). %% 1157
rpref('joueur','de','rugby',33). %% 1150
rpref('accompagner','de','r�f�rence',33). %% 1142
rpref('�tat','de','union',33). %% 1139
rpref('droits','�','importation',33). %% 1137
rpref('�galit�','de','chance',33). %% 1135
rpref('viande','de','volaille',33). %% 1130
rpref('accompagner','de','publication',33). %% 1130
rpref('exigence','en','mati�re',33). %% 1127
rpref('autorit�','de','pays',33). %% 1126
rpref('navire','de','p�che',33). %% 1124
rpref('abbaye','de','_LOCATION',33). %% 1121
rpref('peine','de','mort',33). %% 1120
rpref('syst�me','de','contr�le',33). %% 1119
rpref('modalit�','de','r�f�rence',33). %% 1119
rpref('conform�ment','�','r�glement',33). %% 1116
rpref('titre','de','r�glement',33). %% 1112
rpref('r�pondre','�','exigence',33). %% 1110
rpref('commission','de','budget',33). %% 1108
rpref('faveur','de','rapport',33). %% 1102
rpref('mati�re','de','s�curit�',33). %% 1101
rpref('repr�sentant','de','�tat',33). %% 1100
rpref('d�livrance','de','certificat',33). %% 1098
rpref('secteur','de','c�r�ale',33). %% 1096
rpref('produit','�','base',33). %% 1095
rpref('cadre','de','_Uv',33). %% 1094
rpref('cadre','de','proc�dure',33). %% 1092
rpref('�tat','de','origine',33). %% 1091
rpref('proposition','de','amendement',32). %% 1088
rpref('commission','de','environnement',32). %% 1088
rpref('mettre','sur','pied',32). %% 1084
rpref('�tre','de','avis',32). %% 1084
rpref('r�pondre','�','condition',32). %% 1082
rpref('application','de','mesure',32). %% 1082
rpref('obtenir','�','partir',32). %% 1077
rpref('portant','de','application',32). %% 1074
rpref('�quipe','de','_LOCATION',32). %% 1072
rpref('pacte','de','stabilit�',32). %% 1069
rpref('adopter','dans','domaine',32). %% 1068
rpref('lecture','en','premier',32). %% 1067
rpref('nord','de','_LOCATION',32). %% 1066
rpref('rapprochement','de','l�gislation',32). %% 1065
rpref('cr�er','en','_DATE_year',32). %% 1064
rpref('pr�sident','de',entities['_ORGANIZATION','_COMPANY'],32). %% 1063
rpref('utiliser','�','fin',32). %% 1059
rpref('bureau','de','douane',32). %% 1058
rpref('accord','de','coop�ration',32). %% 1058
rpref('adh�sion','�','union',32). %% 1057
rpref('d�cision','de','_Uv',32). %% 1055
rpref('pr�judice','de','disposition',32). %% 1051
rpref('demande','de','aide',32). %% 1050
rpref('sommet','de',entities['_ORGANIZATION','_COMPANY'],32). %% 1048
rpref('suivre','de','accord',32). %% 1047
rpref('avoir','pour','objet',32). %% 1046
rpref('application','de','paragraphe',32). %% 1045
rpref('circulation','de','marchandise',32). %% 1042
rpref('ressortissant','de','pays',32). %% 1039
rpref('marge','de','dumping',32). %% 1038
rpref('�tre','de','nature',32). %% 1038
rpref('fin','de','compte',32). %% 1037
rpref('accord','de','association',32). %% 1037
rpref('ville','de','_LOCATION',32). %% 1035
rpref('chronologie','de','_Uv',32). %% 1034
rpref('r�aliser','par','_PERSON_m',32). %% 1032
rpref('�tat','de','_LOCATION',32). %% 1031
rpref('chronologie','de','_LOCATION',32). %% 1031
rpref('repr�sentant','de','commission',32). %% 1030
rpref('d�cision','de',entities['_ORGANIZATION','_COMPANY'],32). %% 1029
rpref('base','de','viande',32). %% 1028
rpref('inf�rieur','�','pourcent',32). %% 1027
rpref('�valuation','de','risque',32). %% 1026
rpref('�tat','de','accueil',32). %% 1025
rpref('respect','de','disposition',32). %% 1024
rpref('avoir','pour','effet',32). %% 1024
rpref('accord','de','_LOCATION',31). %% 1023
rpref('d�c�s','�','_Uv',31). %% 1021
rpref('sein','de','comit�',31). %% 1018
rpref('pr�sidence','de','conseil',31). %% 1017
rpref('joueur','de','tennis',31). %% 1016
rpref('�tre','�','�gard',31). %% 1016
rpref('conseil','de','s�curit�',31). %% 1016
rpref('cat�gorie','en','_DATE_year',31). %% 1016
rpref('personnalit�','en','_DATE_year',31). %% 1014
rpref('�tre','en','fait',31). %% 1014
rpref('niveau','de','protection',31). %% 1009
rpref('mener','�','bien',31). %% 1008
rpref('entreprise','de','assurance',31). %% 1007
rpref('d�cision','de','comit�',31). %% 1006
rpref('degr�','de','_NUMBER',31). %% 1005
rpref('soci�t�','de','information',31). %% 1003
rpref('avoir','pour','but',31). %% 1000
rpref('arr�ter','selon','proc�dure',31). %% 997
rpref('�tat','de','_NUMBER',31). %% 996
rpref('prix','�','exportation',31). %% 994
rpref('club','de','football',31). %% 993
rpref('agir','de','question',31). %% 993
rpref('octroi','de','aide',31). %% 992
rpref('proposer','par','commission',31). %% 991
rpref('prendre','en','charge',31). %% 988
rpref('p�riode','de','_DATE_arto',31). %% 988
rpref('pays','de','_NUMBER',31). %% 986
rpref('maire','de','_LOCATION',31). %% 985
rpref('soin','de','sant�',31). %% 983
rpref('fabrication','�','partir',31). %% 982
rpref('voter','contre','rapport',31). %% 980
rpref('sein','de','conseil',31). %% 979
rpref('�l�ment','de','preuve',31). %% 979
rpref('�tre','�','_LOCATION',31). %% 978
rpref('processus','de','paix',31). %% 972
rpref('�le','de','_LOCATION',31). %% 972
rpref('usine','de','produit',31). %% 970
rpref('temps','de','travail',31). %% 970
rpref('objet','de','dumping',31). %% 969
rpref('�tre','en','cas',31). %% 969
rpref('exportation','de','produit',31). %% 968
rpref('conform�ment','�','directive',31). %% 968

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 'functions' in appositions

%% Initiated from data provide by Yayoi Nakamura-Delloye (Dec 2010)

:-extensional appos_function/1.

appos_function(d�put�).
appos_function(d�l�gu�).
appos_function(militant).
appos_function(�lu).
appos_function(docteur).
appos_function(professeur).
appos_function(pr�sident).
appos_function('vice-pr�sident').
appos_function(directeur).
appos_function(secr�taire).
appos_function(repr�sentant).
appos_function('porte-parole').
appos_function(s�nateur).
appos_function(maire).
appos_function(chef).
appos_function(patron).
appos_function('PDG').
appos_function(entraineur).
appos_function(ministre).
appos_function('Premier ministre').
appos_function(candidat).
appos_function(membre).
appos_function(rapporteur).
appos_function(fondateur).
appos_function(leader).
appos_function(expert).
appos_function(journaliste).
appos_function(ambassadeur).
appos_function(avocat).
appos_function(dirigeant).
appos_function(coll�gue).
appos_function(responsable).
appos_function(propri�taire).
appos_function(m�diateur).
appos_function(homologue).
appos_function(coordinateur).
appos_function(professeur).
appos_function(politologue).
appos_function(pr�d�cesseur).
appos_function(successeur).
appos_function(num�ro).
appos_function(pilier).
appos_function(�missaire).
appos_function(manager).
appos_function(cadre).
appos_function(sp�cialiste).
appos_function(recteur).
appos_function(doyen).
appos_function(champion).
appos_function(commandant).
appos_function(analyste).
appos_function(administrateur).

appos_function(meneur).
appos_function(gardien).
appos_function(attaquant).

appos_function(projet).
appos_function(syst�me).
appos_function(programme).
appos_function(logiciel).

appos_function(ambassadeur).

%% Lefff is missing good info on se_moyen ?

:-extensional conll_se_moyen/1.

conll_se_moyen(inscrire).
conll_se_moyen(�lever).
conll_se_moyen(trouver).
conll_se_moyen(produire).
conll_se_moyen(traduire).
conll_se_moyen(retrouver).
conll_se_moyen(�tablir).
conll_se_moyen(poursuivre).
conll_se_moyen(pr�senter).
conll_se_moyen(manifester).
conll_se_moyen(imposer).
conll_se_moyen(multiplier).
conll_se_moyen(terminer).
conll_se_moyen(exprimer).
conll_se_moyen(engager).
conll_se_moyen(am�liorer).
conll_se_moyen(accro�tre).
conll_se_moyen(faire).
conll_se_moyen(ouvrir).
conll_se_moyen(expliquer).
conll_se_moyen(exercer).
conll_se_moyen(g�n�raliser).
conll_se_moyen(enliser).
conll_se_moyen(engouffrer).
conll_se_moyen(�largir).
conll_se_moyen(endetter).
%conll_se_moyen(enfonce).
conll_se_moyen(effriter).
conll_se_moyen(effondrer).
conll_se_moyen(enthousiasmer).
conll_se_moyen(empiler).
conll_se_moyen(�mousser).
conll_se_moyen(effectuer).
conll_se_moyen(�courter).
conll_se_moyen(doubler).
conll_se_moyen(distinguer).
conll_se_moyen(diversifier).
conll_se_moyen(diffuser).
conll_se_moyen(dessiner).
conll_se_moyen(d�rouler).
conll_se_moyen(d�rober).
conll_se_moyen(d�finir).
conll_se_moyen(creuser).
conll_se_moyen(cr�er).
conll_se_moyen(constituer).
conll_se_moyen(consommer).
conll_se_moyen(consacrer).
conll_se_moyen(conjuguer).
conll_se_moyen(conclure).
conll_se_moyen(concerter).
conll_se_moyen(clarifier).
conll_se_moyen(colleter).
conll_se_moyen(chiffrer).
conll_se_moyen(comporter).
conll_se_moyen(composer).
conll_se_moyen(confirmer).
conll_se_moyen(caract�riser).
conll_se_moyen(borner).
conll_se_moyen(articuler).
conll_se_moyen(arranger).
conll_se_moyen(abstraire).
conll_se_moyen(accentuer).
conll_se_moyen(accompagner).
conll_se_moyen(affaiblir).
conll_se_moyen(situer).
conll_se_moyen(sp�cialiser).
conll_se_moyen(tasser).
conll_se_moyen(tisser).
conll_se_moyen(accomoder).
conll_se_moyen(stabiliser).
conll_se_moyen(solder).
conll_se_moyen(ranger).
conll_se_moyen(prononcer).
conll_se_moyen(inverser).

:-extensional conll_se_obj/1.

conll_se_obj(f�liciter).
conll_se_obj(occuper).
conll_se_obj(s�parer).
conll_se_obj(rapprocher).
conll_se_obj(nourrir).
conll_se_obj(charger).
conll_se_obj(doter).
conll_se_obj(replier).
conll_se_obj(amorcer).
conll_se_obj(gonfler).
conll_se_obj(enichir).
conll_se_obj(coucher).
conll_se_obj(tailler).
conll_se_obj(pourvoir).
conll_se_obj(tailler).
conll_se_obj(qualifier).
conll_se_obj(financer).
conll_se_obj(encombrer).

:-extensional conll_se_aobj/1.

conll_se_aobj(demander).
conll_se_aobj(succ�der).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Conll expansion for sxpipe compound

conll_simple_expansion(['l''', 'un'],[det: 1, head]).  %% n=54
conll_simple_expansion(['l''', 'une'],[det: 1, head]).  %% n=23
conll_simple_expansion(['michel', 'sapin'],[head, mod: -1]).  %% n=19
conll_simple_expansion(['air', 'france'],[head, mod: -1]).  %% n=19
conll_simple_expansion(['la', 'poste'],[det: 1, head]).  %% n=16
conll_simple_expansion(['les', 'autres'],[det: 1, head]).  %% n=16
conll_simple_expansion(['jean_-_claude', 'trichet'],[head, mod: -1]).  %% n=14
conll_simple_expansion(['mais', 'aussi'],[head, mod: -1]).  %% n=14
conll_simple_expansion(['m�me', 'si'],[obj: 1, head]).  %% n=13
conll_simple_expansion(['france', 't�l�com'],[head, mod: -1]).  %% n=13
conll_simple_expansion(['soci�t�', 'g�n�rale'],[head, mod: -1]).  %% n=13
conll_simple_expansion(['arabie', 'saoudite'],[head, mod: -1]).  %% n=13
conll_simple_expansion(['et', 'donc'],[head, mod: -1]).  %% n=11
conll_simple_expansion(['edouard', 'balladur'],[head, mod: -1]).  %% n=10
conll_simple_expansion(['pas', 'encore'],[mod: 1, head]).  %% n=10
conll_simple_expansion(['afrique', 'du', 'sud'],[head, dep: -1, obj: -1]).  %% n=9
conll_simple_expansion(['zenith', 'data', 'systems'],[head, dep: -1, dep: -2]).  %% n=9
conll_simple_expansion(['jacques', 'delors'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['r�serve', 'f�d�rale'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['us', 'air', 'force'],[head, mod: -1, mod: -2]).  %% n=8
conll_simple_expansion(['am�rique', 'du', 'nord'],[head, dep: -1, obj: -1]).  %% n=8
conll_simple_expansion(['elf', 'aquitaine'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['commission', 'europ�enne'],[head, mod: -1]).  %% n=8
conll_simple_expansion(['vieux', 'continent'],[mod: 1, head]).  %% n=8
conll_simple_expansion(['pavel', 'maly'],[head, mod: -1]).  %% n=7
conll_simple_expansion(['pour', 'autant'],[head, obj: -1]).  %% n=7
conll_simple_expansion(['d''', 'entre'],[head, obj: -1]).  %% n=7
conll_simple_expansion(['banque', 'mondiale'],[head, mod: -1]).  %% n=7
conll_simple_expansion(['euro', 'disney'],[head, mod: -1]).  %% n=7
conll_simple_expansion(['club', 'de', 'paris'],[head, dep: -1, obj: -1]).  %% n=6
conll_simple_expansion(['d''', 'espagne'],[head, obj: -1]).  %% n=6
conll_simple_expansion(['british', 'airways'],[dep: 1, head]).  %% n=6
conll_simple_expansion(['john', 'major'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['bernard', 'tapie'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['d''', 'italie'],[head, obj: -1]).  %% n=6
conll_simple_expansion(['ou', 'encore'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['depuis', 'longtemps'],[head, obj: -1]).  %% n=6
conll_simple_expansion(['ibm', 'france'],[head, mod: -1]).  %% n=6
conll_simple_expansion(['michel', 'giraud'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['xavier', 'de_boishebert'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['gaz', 'de', 'france'],[head, dep: -1, obj: -1]).  %% n=5
conll_simple_expansion(['comme', 'si'],[head, obj: -1]).  %% n=5
conll_simple_expansion(['raymond', 'lacombe'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['d''_ici', '�'],[head, obj: -1]).  %% n=5
conll_simple_expansion(['peter', 'sutherland'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['alain', 'denvers'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['de', 'plus_de'],[arg: 1, head]).  %% n=5
conll_simple_expansion(['gordon', 'roddick'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['daniel', 'yergin'],[head, mod: -1]).  %% n=5
conll_simple_expansion(['premier', 'ministre'],[mod: 1, head]).  %% n=5
conll_simple_expansion(['fonds', 'mon�taire', 'international'],[head, mod: -1, mod: -2]).  %% n=5
conll_simple_expansion(['la', 'plupart', 'des'],[det: 1, head, dep: -1]).  %% n=5
conll_simple_expansion(['les', 'uns'],[det: 1, head]).  %% n=4
conll_simple_expansion(['jean', 'arthuis'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['jacques', 'hersant'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['la', 'redoute'],[det: 1, head]).  %% n=4
conll_simple_expansion(['jacques', 'de_larosi�re'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['au', 'total'],[head, obj: -1]).  %% n=4
conll_simple_expansion(['l''', 'air', 'liquide'],[det: 1, head, mod: -1]).  %% n=4
conll_simple_expansion(['m.', 'de'],[head, det: -1]).  %% n=4
conll_simple_expansion(['helmut', 'schlesinger'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['sous', 'la', 'forme', 'd'''],[head, det: 1, obj: -2, dep: -1]).  %% n=4
conll_simple_expansion(['tout', 'comme'],[mod: 1, head]).  %% n=4
conll_simple_expansion(['m.', 'pierre'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['maison', 'blanche'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['yves', 'saint', 'laurent'],[head, mod: -1, mod: -2]).  %% n=4
conll_simple_expansion(['general', 'motors'],[dep: 1, head]).  %% n=4
conll_simple_expansion(['wall', 'street'],[dep: 1, head]).  %% n=4
conll_simple_expansion(['guy', 'ros�s'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['roberto', 'procopio'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['et', 'encore'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['francis', 'werner'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['le', 'reste'],[det: 1, head]).  %% n=4
conll_simple_expansion(['le', 'd�bat'],[det: 1, head]).  %% n=4
conll_simple_expansion(['non', 'sans'],[mod: 1, head]).  %% n=4
conll_simple_expansion(['steven', 'ross'],[head, mod: -1]).  %% n=4
conll_simple_expansion(['philippe', 'lagayette'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['bernard', 'lambert'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['bourse', 'de', 'londres'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['maintes', 'fois'],[det: 1, head]).  %% n=3
conll_simple_expansion(['nations', 'unies'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['edward', 'hall'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['caisse', 'des', 'd�p�ts'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['la', 'cour'],[det: 1, head]).  %% n=3
conll_simple_expansion(['au', 'bout', 'de'],[head, obj: -1, dep: -1]).  %% n=3
conll_simple_expansion(['parti', 'lib�ral'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['jean_-_fran�ois', 'colas'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['place', 'de', 'londres'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['union', 'centriste'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['deutsche', 'bank'],[dep: 1, head]).  %% n=3
conll_simple_expansion(['d''', 'une', 'fa�on', 'g�n�rale'],[head, det: 1, obj: -2, mod: -1]).  %% n=3
conll_simple_expansion(['gilles', 'oury'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['organisation', 'de', 'coop�ration', 'et', 'de', 'd�veloppement', '�conomique'],[head, dep: -1, obj: -1, coord: -2, dep_coord: -1, obj: -1, mod: -1]).  %% n=3
conll_simple_expansion(['en', 'place'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['george', 'soros'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['au', 'point', 'que'],[head, obj: -1, dep: -1]).  %% n=3
conll_simple_expansion(['cor�e', 'du', 'sud'],[head, dep: -1, obj: -1]).  %% n=3
conll_simple_expansion(['time', 'warner'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['et', 'm�me'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['en', 'vigueur'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['�', 'terme'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['cr�dit', 'agricole'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['nicolas', 'guilbert'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['d''_ici', '�_la_fin_de'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['le', 'monde'],[det: 1, head]).  %% n=3
conll_simple_expansion(['d''', 'epinal'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['banque', 'centrale', 'europ�enne'],[head, mod: -1, mod: -2]).  %% n=3
conll_simple_expansion(['prud''', 'hommes'],[mod: 1, head]).  %% n=3
conll_simple_expansion(['la', 'baule'],[det: 1, head]).  %% n=3
conll_simple_expansion(['soci�t�', 'marseillaise', 'de', 'cr�dit'],[head, mod: -1, dep: -2, obj: -1]).  %% n=3
conll_simple_expansion(['�', 'moins_de'],[head, obj: -1]).  %% n=3
conll_simple_expansion(['robert', 'maxwell'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['olivier', 'bouissou'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['patrick', 's�bastien'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['philip', 'morris'],[head, mod: -1]).  %% n=3
conll_simple_expansion(['avant', 'que'],[head, dep: -1]).  %% n=2
conll_simple_expansion(['au', 'centre', 'd'''],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['charbonnages', 'de', 'france'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['c�te', 'd''', 'azur'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['jean', 'stromboni'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['herv�', 'barr�'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['catherine', 'viannay'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'lieu', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['arthur', 'dunkel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['bernard', 'haemmerlin'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['tudor', 'banus'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['depuis', 'que'],[obj: 1, head]).  %% n=2
conll_simple_expansion(['reed', 'international'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['reporters', 'sans', 'fronti�res'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['d.', 'i.', 'oparine'],[head, mod: -1, mod: -2]).  %% n=2
conll_simple_expansion(['fran�ois', 'guillaume'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['m.', 'carlo', 'azeglio', 'ciampi'],[head, mod: -1, mod: -2, mod: -3]).  %% n=2
conll_simple_expansion(['fran�ois', 'mitterrand'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['compte', 'tenu', 'des'],[head, mod: -1, de_obj: -1]).  %% n=2
conll_simple_expansion(['eug�ne', 'forget'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean', 'peyrelevade'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['m.', 'michel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['fernand', 'corradetti'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['union', 'professionnelle', 'artisanale'],[head, mod: -1, mod: -2]).  %% n=2
conll_simple_expansion(['et', 'aussi'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['werner', 'sombart'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['bureau', 'international', 'du', 'travail'],[head, mod: -1, dep: -2, obj: -1]).  %% n=2
conll_simple_expansion(['jean', 'pr�tre'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['fabrice', 'th�obald'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean_-_charles', 'pellerin'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au_cours', 'desquelles'],[head, dep: -1]).  %% n=2
conll_simple_expansion(['au', 'c�t�', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['emiliano', 'zapata'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['edith', 'cresson'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['daniel', 'hoeffel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['pierre', 'guillen'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['henri', 'nallet'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['silver', 'moon'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['le', 'mieux'],[det: 1, head]).  %% n=2
conll_simple_expansion(['les', 'r�formateurs'],[det: 1, head]).  %% n=2
conll_simple_expansion(['sachant', 'que'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['au', 'cours', 'des'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['de', 'chez'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['ronald', 'reagan'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['chargeurs', 'sa'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['organisation', 'de', 'coop�ration', 'et', 'de', 'd�veloppement', '�conomiques'],[head, dep: -1, obj: -1, coord: -2, dep_coord: -1, obj: -1, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'milieu', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['dassault', 'aviation'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['caisse', 'des', 'd�p�ts', 'et', 'consignations'],[head, dep: -1, obj: -1, coord: -1, dep_coord: -1]).  %% n=2
conll_simple_expansion(['france', 'motors'],[head, dep: -1]).  %% n=2
conll_simple_expansion(['au', 'point', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['michel', 'leclerc'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['guy', 'azoulay'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['les', 'anglais'],[det: 1, head]).  %% n=2
conll_simple_expansion(['assurances', 'g�n�rales', 'de', 'france'],[head, mod: -1, dep: -2, obj: -1]).  %% n=2
conll_simple_expansion(['d''', 'o�'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['l''', '�quipe'],[det: 1, head]).  %% n=2
conll_simple_expansion(['anita', 'roddick'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'plus'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean_-_fran�ois', 'gavalda'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['christophe', 'delavenne'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['mamie', 'nova'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['thomson', 'consumer', 'electronics'],[head, dep: -1, dep: -2]).  %% n=2
conll_simple_expansion(['louis', 'mermaz'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['canard', 'encha�n�'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['daniel', 'vauvilliers'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['prix', 'nobel'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['mohamed', 'boudiaf'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['serge', 'bensimon'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['air', 'inter'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['�', 'tout', 'prix'],[head, det: 1, obj: -2]).  %% n=2
conll_simple_expansion(['xavier', 'grenet'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['helmut', 'kohl'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'milieu', 'des'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['martin', 'bouygues'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['d''', 'ici', '�'],[head, mod: -1, arg: -2]).  %% n=2
conll_simple_expansion(['chris', 'patten'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['parti', 'communiste'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['val�ry', 'giscard', 'd''', 'estaing'],[head, mod: -1, dep: -2, obj: -1]).  %% n=2
conll_simple_expansion(['d''', 'angleterre'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['de', 'loin'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['thierry', 'aulagnon'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['communaut�', 'des', 'etats', 'ind�pendants'],[head, dep: -1, obj: -1, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'bord', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['conseil', 'sup�rieur', 'de', 'l''', 'audiovisuel'],[head, mod: -1, dep: -2, det: 1, obj: -2]).  %% n=2
conll_simple_expansion(['fernando', 'collor', 'de', 'mello'],[head, mod: -1, det: -2, mod: -3]).  %% n=2
conll_simple_expansion(['alain', 'buhler'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['thierry', 'gandillot'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'camdessus'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'gilet'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['la', 'chambre'],[det: 1, head]).  %% n=2
conll_simple_expansion(['us', 'air'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['en', 'fin', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['bbc', 'world', 'service'],[head, dep: -1, dep: -2]).  %% n=2
conll_simple_expansion(['los', 'angeles'],[dep: 1, head]).  %% n=2
conll_simple_expansion(['british', 'gas'],[dep: 1, head]).  %% n=2
conll_simple_expansion(['frankfurter', 'allgemeine', 'zeitung'],[dep: 2, dep: 1, head]).  %% n=2
conll_simple_expansion(['luc', 'guyau'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['serge', 'tchuruk'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'gaillard'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['machine', 'arri�re'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'cours', 'de'],[head, obj: -1, dep: -1]).  %% n=2
conll_simple_expansion(['d''', 'autant'],[head, obj: -1]).  %% n=2
conll_simple_expansion(['place', 'de', 'paris'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['jean_-_pierre', 'soisson'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['jean_-_fran�ois', 'duhot'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['cerro', 'paranal'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['st�phane', 'k�lian'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['xx', 'si�cle'],[mod: 1, head]).  %% n=2
conll_simple_expansion(['boulevard', 'haussmann'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['michel', 'quesnot'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['brice', 'martin'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['maurice', 'strong'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['louis', 'fontvieille'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['gilles', 'laurent'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['cr�dit', 'national'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['r�publique', 'du', 'kazakhstan'],[head, dep: -1, obj: -1]).  %% n=2
conll_simple_expansion(['jacques', 'attali'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['adrien', 'zeller'],[head, mod: -1]).  %% n=2
conll_simple_expansion(['au', 'm�me', 'titre', 'que'],[head, mod: 1, obj: -2, dep: -1]).  %% n=2

