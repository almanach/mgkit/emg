/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: addons.tag 1516 2011-09-21 20:11:15Z clerger $
 * Copyright (C) 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  addons.tag -- extra TAG trees for emg
 *
 * ----------------------------------------------------------------
 * Description
 *   we had here trees that can not yet be easily described
 *   just using the metagrammar
 *
 *   Note that the trees described here can not be visualized
 *
 * ----------------------------------------------------------------
 */

:-include('header.tag').

tag_tree{ name => start,
	  family => ht{ cat => (-) },
	  tree => tree -start('$pos'(N),
			      { noop(( pos(0)
				     ;
				       postscan([ponct[':','�',';','.','�','(',')','!','?','!?','?!','!!!','_SENT_BOUND','"','_']])
				      ))
			      },
			      { N= 0
			      xor
			      'C'(_,lemma{ lex => ponct[':','�',';','.','�','(',')','!','?','!?','?!','!!!','_SENT_BOUND','"','_']}, M) , skip_lexical_closure(M,N)
			      }
			     )
	}
.

tag_tree{ name => end,
	  family => ht{ cat => (-) },
	  tree => tree -end('$skip',
			    '$pos'(N),
			    { noop((pos(end) ; scan(Scan))) },
			    { recorded('N'(N))
			    xor 'C'(N,lemma{ lex => Scan::ponct['�','"',')','_'] },_)
			    }
			   )
	}
.

tag_tree{ name => incise_start,
	  family => ht{ cat => (-) },
	  tree => auxtree top = X at -incise(
					     '$skip',
					     (	 { noop(
							( pos(0)
							; postscan([que,Scan])
							; postcat([coo])
							)
						       )
						 },
						 '$pos'(S),
						 { S = 0
						 xor 'C'(_,lemma{ cat => coo },S)
						 xor 'C'(_,lemma{ lex => que },S)
						 xor 'C'(_,lemma{ lex => Scan::ponct[':','�',';','.','�','(',')','!','?','!?','?!','!!!','_SENT_BOUND','"','_']}, S) 
						 }
					     ),
					     top = X::incise{ incise_kind => coma } at *incise,
%					     '$skip',
					     ( [',']
					     ; '$pos'(N),
					       { 'C'(N,lemma{ cat => ponctw, lex => (',') },_) }
					     )
					    )
	}
.


tag_tree{ name => incise,
	  family => ht{ cat => (-) },
	  tree => auxtree top = X at -incise(
%					     '$skip',
					     [','],
					     top = X::incise{ incise_kind => Kind::incise_kind[coma,comastrict] } at *incise,
					     (	 [','] ;
						 '$skip',
						 (   '$pos'(N),
						     { Kind=coma, recorded('N'(N))
						     xor   'C'(N,lemma{ lex => Lex,
									cat => cat[ponctw,poncts] },_),
						       ( Lex == (',') xor Kind = coma )
						     }
						 )
					     )
					    )
	}
.

/*
tag_tree{ name => incise_strict,
	  family => ht{ cat => (-) },
	  tree => auxtree top = X at -incise(
%%					     '$skip',
					     [','],
					     top = X::incise{ incise_kind => comastrict } at *incise,
%%					     '$skip',
					     ( [',']
					     ; '$pos'(N),
					       { 'C'(N,lemma{ cat => ponctw, lex => (',') },_) }
					     )
					    )
	}
.
*/

tag_tree{ name => par_incise,
	  family => ht{ cat => (-) },
	  tree => auxtree top=X at -incise(%['_EPSILON'] @*,
					   [ponct['(','[']],
					   top = X::incise{ %% position => post,
							    incise_kind => par }
					   at *incise,
					   %(['_EPSILON'] @*),
					   [ponct[')',']','_SMILEY']]
					  )
	}
.

tag_tree{ name => dash_incise,
	  family => ht{ cat => (-) },
	  tree => auxtree top=X at -incise(
					   %['_EPSILON'] @*,
					   [initponct[]],
					   %(['_EPSILON'] @*),
					   top = X::incise{ %% position => post,
							    incise_kind => dash
							  } at *incise,
					   %(['_EPSILON'] @*),
					   (   [','] ?,
					       [initponct[]] ;
					       '$skip',
					       '$pos'(N),
					       { recorded('N'(N))
					       xor 'C'(N,lemma{ cat => poncts },_)
					       }
					   )
					  )
	}
.


tag_tree{ name => unknown,
	  family => ht{},
	  tree => tree -unknown( [X] )
	}
.

tag_tree{ name => unknown_wponct,
	  family => ht{},
	  tree => tree -unknown( - <=> ponctw )
	}
.

tag_tree{ name => unknown_sponct,
	  family => ht{},
	  tree => tree -unknown( - <=> poncts )
	}
.

tag_tree{ name => unknown_adv,
	  family => ht{},
	  tree => tree -unknown( <=> adv )
	}
.

tag_tree{ name => unknown_csu,
	  family => ht{},
	  tree => tree -unknown( - <=> csu )
	}
.

tag_tree{ name => unknown_pri,
	  family => ht{},
	  tree => tree -unknown( - <=> pri )
	}
.

tag_tree{ name => unknown_prel,
	  family => ht{},
	  tree => tree -unknown( - <=> prel )
	}
.

tag_tree{ name => unknown_pro,
	  family => ht{},
	  tree => tree -unknown( - <=> pro )
	}
.

%% cat[ilimp,caimp,cln,cla,cld12,cld3,clg,cll,clr]

tag_tree{ name => unknown_cln,
	  family => ht{},
	  tree => tree -unknown( - <=> cln)
	}
.

tag_tree{ name => unknown_cla,
	  family => ht{},
	  tree => tree -unknown( - <=> cla)
	}
.

tag_tree{ name => unknown_cld,
	  family => ht{},
	  tree => tree -unknown( - <=> cld)
	}
.

tag_tree{ name => unknown_cll,
	  family => ht{},
	  tree => tree -unknown( - <=> cll)
	}
.

tag_tree{ name => unknown_clg,
	  family => ht{},
	  tree => tree -unknown( - <=> clg)
	}
.

tag_tree{ name => unknown_clr,
	  family => ht{},
	  tree => tree -unknown( - <=> clr)
	}
.

tag_tree{ name => unknown_clneg,
	  family => ht{},
	  tree => tree -unknown( - <=> clneg)
	}
.

tag_tree{ name => unknown_adj,
	  family => ht{},
	  tree => tree -unknown( <=> adj )
	}
.

tag_tree{ name => unknown_advneg,
	  family => ht{},
	  tree => tree -unknown( - <=> advneg )
	}
.

tag_tree{ name => unknown_prep,
	  family => ht{},
	  tree => tree -unknown( - <=> prep )
	}
.

tag_tree{ name => unknown_coo,
	  family => ht{},
	  tree => tree -unknown( - <=> coo )
	}
.

tag_tree{ name => unknown_v,
	  family => ht{},
	  tree => tree -unknown( - <=> v )
	}
.

tag_tree{ name => unknown_nc,
	  family => ht{},
	  tree => tree -unknown( - <=> nc )
	}
.


:-std_prolog noop/1.

noop(_).

:-std_prolog record_without_doublon/1.

follow_coord(N,L).
follow_coord_trace_infinitive(N,L).

tag_tree{ name => follow_coord,
	  family => ht{},
	  tree => tree strace( {
				noop([adv,advneg,cla,cld,clneg,clg,cll,clr,aux,v])
			       },
			       '$pos'(N),
			       {
				N > 0,
				 M is N-1,
%%				 format('recordX at ~w\n',[N]),
				 'C'(M,lemma{ cat => Cat, lex => Lex },N),
				 ( Cat = coo ; Lex = (',')),
%%				 format('record0 at ~w\n',[N]),
				 phrase(( [cat(cat[adv,advneg,cla,cld,clneg,clg,cll,clr])]@*,
					  [cat(cat[aux,v])]),N,R),
%%				 format('record1 at ~w\n',[R]),
				 L is R-1,
		%		 record_without_doublon(Rec::follow_coord(N,L)),
				follow_coord(N,L),
				 true
			       } )
	}
.


tag_tree{ name => follow_coord_aux,
	  family => ht{},
	  tree => auxtree
	top= 'Infl'{ mode => Mode::mode[~ [participle]],
		     person => Pers,
		     number => Num
		   
		   }
	at - 'Infl'(
		    '$pos'(N),
		    { ( %% 'C'(M,lemma{ cat => coo },N )
			%% too costly to try after each coma
			%% xor 'C'(M,lemma{ lex => (',') },N)
%%			format('search record ~w\n',[Rec]),
%			recorded(Rec::follow_coord(N,L0)),
			'$answers'(follow_coord(N,L0)),
%%			format('found record ~w\n',[Rec]),
			true
		      ),
		      'C'(L0,
			  Lemma0::lemma{ top => v{ mode => participle,
						   aux_req => AUX,
						   diathesis => Diathesis
						 }
				       },
			  R0),
		      %% format('Found lemma0 around ~w: ~w\n',[N,Lemma0]),
		      'D'(v,L1),
		      'C'(L1,
			  Lemma1::lemma{ top => v{ mode => participle,
						   aux_req => AUX,
						   diathesis => Diathesis
						 } },
			  R1),
		      R1 < N,
		      L1 > N - 20,
%%		      format('Found lemma1 at ~w: ~w\n',[L1,Lemma1]),
		      'D'(aux,L2),
		      'C'(L2,
			  Lemma2::lemma{
					top => aux{ form_aux => AUX,
						    mode => Mode2,
						    person => Pers,
						    number => Num
						  }
				       },
			  R2),
		      ( Mode2 = Mode xor Mode2 = participle ),
		      ( Mode2 = infinitive ->
			follow_coord_trace_infinitive(N,L0)
		      ;
			true
		      ),
		      R2 =< L1,
		      R2 > L1-3,
%%		      format('Found lemma2 at ~w: ~w\n',[L2,Lemma2]),
		      true
		    },
		    top = 'Infl'{ mode => participle }
		   at * 'Infl'
		   )
	}
.



tag_tree{ name => follow_coord_aux2,
	  family => ht{},
	  tree => auxtree
	top= 'V'{ mode => Mode::mode[~ [infinitive]],
		  person => Pers,
		  number => Num
		}
	at - 'V'(
		    '$pos'(N),
		    { ( %% 'C'(M,lemma{ cat => coo },N )
			%% too costly to try after each coma
			%% xor 'C'(M,lemma{ lex => (',') },N)
%%			format('search record ~w\n',[Rec]),
%%			recorded(Rec::follow_coord(N,L0)),
			'$answers'(follow_coord(N,L0)),
%%			format('found record ~w\n',[Rec]),
			true
		      ),
		      'C'(L0,
			  Lemma0::lemma{ top => v{ mode => Mode3 }
				       },
			  R0),
		      ( '$answers'(follow_coord_trace_infinitive(N,L0)),
			Mode3 = participle
		      ;
			Mode3 = infinitive
		      ),
%%		      format('Found lemma0 around ~w: ~w\n',[N,Lemma0]),
		      'D'(Cat::cat[v,aux],L1),
		      'C'(L1,
			   Lemma1::lemma{ cat => Cat,
					  top => Top },
			  R1),
		      ( Top = v{ mode => infinitive }
		      ;
			Top = aux{ mode => infinitive }
		      ),
		      R1 < N,
		      L1 > N - 20,
%%		      format('Found lemma1 at ~w: ~w\n',[L1,Lemma1]),
		      'D'(v,L2),
		      'C'(L2,
			  Lemma2::lemma{
					top => v{ mode => Mode2,
						  person => Pers,
						  number => Num
						},
					anchor => tag_anchor{ name => ht{ arg1 => arg{ kind => vcomp }}
							    }
				       },
			  R2),
		      ( Mode2 = Mode xor Mode2 = infinitive ),
		      R2 =< L1,
		      R2 > L1-3,
%%		      format('Found lemma2 at ~w: ~w\n',[L2,Lemma2]),
		      true
		    },
		    top = 'V'{ mode => infinitive }
		   at * 'V'
		   )
	}
.

%% This tree is only used in robust mode and does not check agreement
tag_tree{	name => 'robust_cnoun_leaf',
		family => _ht_221 :: ht{arg0 => _arg0 :: arg{extracted => (-), kind => (-), pcas => (-), real => (-)}, arg1 => _arg1 :: arg{extracted => (-), kind => (-), pcas => (-), real => (-)}, arg2 => _arg2 :: arg{extracted => (-), kind => (-), pcas => (-), real => (-)}, cat => nc},
		tree => tree id= 'N2'
			and bot= 'N2'{enum => (-), gender => _gender_226, hum => _hum_231, number => _number_228, person => _n_person_172, sat => (+), time => _time_233, wh => _wh_234}
			at 'N2'(
			        { recorded(robust) },
				id= det
				and top= det{def => _def_229, gender => __gender_226, number => __number_228, wh => _wh_234}
				at det,
				id= 'N'
				and top= 'N'{gender => _gender_226, number => _number_228, person => _n_person_172}
				and bot= 'N'{gender => _nc_gender_174, number => _nc_number_176, person => _nc_person_172}
				at 'N'(
					id= nc
					and top= nc{gender => _nc_gender_174, hum => _hum_231, number => _nc_number_176, person => _nc_person_172, time => _time_233}
					and bot= nc{person => 3}
					at <> nc,
					(id= 'Nc2'
					at - <=> nc)?
				),
				{ \+ _gender_226 = __gender_226 xor  \+ _number_228 = __number_228 },
				(id= np
				at - <=> np)?
			)
	}.
/*
%% this tree for epsilon that denote nominal chunks
tag_tree{ name => epsilon_chunk,
	  family => ht{ cat => epsilon },
	  tree => tree id='N2'
	          and bot= 'N2'{ enum => (-), sat => (+), wh => (-) }
	          at - 'N2'( bot= epsilon{ chunk_type => 'GN'} at <> epsilon )
	}
.
*/
