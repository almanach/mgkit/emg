/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: tag_generic.pl 1720 2012-04-16 21:38:52Z clerger $
 * Copyright (C) 2000, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_gneric.pl -- Generic Stuff for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-scanner(check_lexical).
:-skipper(skip_lexical).
	
:-parse_mode(token).

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-features( lemma, [lex,lemma,cat,top,anchor,truelex,lemmaid] ).

:-rec_prolog normalized_tag_lemma/5.
:-std_prolog anchor/7.
:-extensional tag_lexicon/4.

:-extensional tag_hypertag/2.

:-light_tabular
	verbose!lexical/5,
	verbose!struct/2,
	verbose!anchor/7,
	verbose!coanchor/5,
	verbose!adj/0,
	verbose!epsilon/4
	.

:-extensional autoload_check_hypertag/3.

:-std_prolog anchor_hypertag/2.
:-std_prolog check_extra_constraints/6.

:-require('format.pl').

:-op(  700, xfx, [?=]). % for default value
:-xcompiler(( X ?= V :- ( \+ var(X) xor X = V))). %% Setting a default value

%% return the finite set of all positions
:-extensional '$allpos'/1.

%% 'D'(Cat,CatPos) return all possible positions for Cat
:-extensional 'D'/2.

%% authorized ellipsis
:-extensional authorized_ellipsis/1.
	
anchor(Anchor::tag_anchor{ name => HyperTag,
			   coanchors => VarCoanchors,
			   equations => VarEquations
			 },
       Token,
       Cat,
       Left,
       Right,
       Top,
       Addr : [Lemma,TrueLex,LemmaId]
      ) :-
%	format('check anchor ~w ~w ~w\n',[Left,Right,Anchor]),
	( %fail,
	  Left = gen_pos(L1,L2),
	  number(L1), number(L2), L2 < L1,
	  Right = gen_pos(R1,R2) ->
%	  format('check anchor ~w ~w ~w\n',[Left,Right,Anchor]),
	  ( % ellipis
	    L1=R1,
	    authorized_ellipsis(Cat),
	    anchor(Anchor,_Token,Cat,L2,R2,Top,Addr : [Lemma,TrueLex,LemmaId]),
	    Token=ellipsis(_Token),
%	    Token=_Token,
%	    format('ellipsis ~w ~w ~w\n',[Left,Right,Token]),
	    true
	  ; % no ellipsis
	    anchor(_Anchor,_Token,Cat,L2,R2,_Top,_),
	    anchor(Anchor,Token,Cat,L1,R1,Top,Addr  : [Lemma,TrueLex,LemmaId])
	  )
	;
	  %%	Top = _Top,
	  %%	format( 'LOOKING ANCHOR left=~w ht=~w top=~w\n',[Left,HyperTag,Top] ),
	  recorded(
		   'C'( Left,
			Info::lemma{ lex=> _Token,
				     cat => Cat,
				     top => _Top,
				     lemma => Lemma,
				     lemmaid => LemmaId,
				     truelex => TrueLex,
				     anchor => tag_anchor{ name => HyperTag,
							   coanchors => Coanchors,
							   equations => VarEquations
							 }
				   },
			Right
		      ),
		   Addr
		  ),
	  ( Token = _Token xor Token = Lemma ),
	  (   cat_normalize(Cat,_Top),
	      %%	    format('Normalize ~w => ~w\n',[Cat,_Top]),
	      true
	  xor true),
	  Top = _Top,
	  %%	anchor_hypertag(HyperTag,Left),
	  anchor_hypertag(HyperTag,Token),
	  %%	anchor_hypertag(HyperTag,LemmaId),
	  %%	(   var(Lemma) xor check_all_coanchors(VarCoanchors,Coanchors)),
	  %%	format( 'ANCHOR ~w ~w ~w\n',[Left,Right,Info] ),
	  ( check_extra_constraints(Cat,Top,HyperTag,Coanchors,Left,Right) xor fail ),
	  true
	)
	.

%% authorized ellipsis
:-extensional coanchor_authorized_ellipsis/1.

:-std_prolog coanchor/5.

coanchor(Token,Cat,Left,Right,Top) :-
%%	Top = _Top,
	(  %fail,
	   Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1,
	   Right = gen_pos(R1,R2) ->

	   ( %% ellipsis
	     L1=R1,
	     coanchor_authorized_ellipsis(Cat),
	     coanchor(_Token,Cat,L2,R2,Top),
	     Token=ellipsis(_Token)
	   ;
	     %% no ellipsis
	     coanchor(_Token,Cat,L2,R2,_Top),
	     coanchor(Token,Cat,L1,R1,Top)
	   )
	;
	   'C'(Left,
	       Info::lemma{ lex => Token,
			    cat => Cat,
			    top => _Top },
	       Right
	      ),
	   (   cat_normalize(Cat,_Top) xor true),
	   Top = _Top,
	   %%	format( 'COANCHOR ~w ~w ~w\n',[Left,Right,Info] ),
	   true
	)
	.

:-rec_prolog cat_normalize/2.

:-rec_prolog check_all_coanchors/2.

check_all_coanchors([],[]).

check_all_coanchors([VarLemma|VL],[Lemma|L]) :-
	( Lemma == [] ->
	    (	VarLemma = [] xor true)
	;   var(VarLemma) ->
	    VarLemma = Lemma
	;   VarLemma = check_at_anchor(Left,Right) ->
	    check_coanchor_lemma(Lemma,Left,Right)
	;   
	    fail
	),
	check_all_coanchors(VL,L).

:-std_prolog check_coanchor/3.

check_coanchor(VarLemma,Left,Right) :-
	( var(VarLemma) ->
	    VarLemma = check_at_anchor(Left,Right)
	;   VarLemma == [] ->
	    true
	;   
	    check_coanchor_lemma(VarLemma,Left,Right)
	)
	.

:-std_prolog check_coanchor_in_anchor/2.

check_coanchor_in_anchor(VarLemma,VarLemma).

:-std_prolog check_coanchor_lemma/3.

check_coanchor_lemma(Lemma,Left,Right) :-
	domain(Token,Lemma),
	'C'(Left,lemma{ lex=>Token },Right)
	.

:-std_prolog skip_lexical/3.
:-finite_set(skipcat,[epsilon,sbound,meta]).
skip_lexical(Left,Token,Right) :-
%	format('Try epsilon ~w\n',[Left]),
	( 'C'(Left,lemma{ lex => Token, cat => skipcat[] }, Right )
	;
	  fail,			% not yet ready to activate
	  recorded(robust),
	  \+ recorded(is_covered(Left)),
	  ( Left < 4
	  xor XLeft is Left - 3,
	    recorded(is_covered(XLeft))
	  ),
	  'C'(Left,lemma{ lex => Token, cat => Cat }, Right ),
	  \+ Cat = skipcat[],
	  \+ domain(Cat,[ponctw,poncts])
	),
%	format('Found epsilon ~w ~w\n',[Left,Token]),
	true
	.

:-std_prolog skip_lexical_closure/2.
skip_lexical_closure(Left,Right) :-
	( Right = Left
	;
	  skip_lexical(Left,Token,Middle),
	  skip_lexical_closure(Middle,Right)
	).

verbose!epsilon(_Lex,Left,Right,[_Lemma,_Lexical,_LemmaId]) :-
	'C'(Left,lemma{lex => _Lex,
		       truelex => _Lexical,
		       cat => skipcat[],
		       lemma => _Lemma,
		       lemmaid => _LemmaId
		      }, Right),
	_Lemma ?= '_Uv',
	true
	.

/*
verbose!epsilon(_Lex,Left,Right,[_Lemma,_Lexical,_LemmaId]) :-
	recorded(robust),
	\+ recorded(is_covered(Left)),
	'C'(Left,lemma{lex => _Lex,
		       truelex => _Lexical,
%		       cat => skipcat[],
		       lemma => _Lemma,
		       lemmaid => _LemmaId
		      }, Right),
	_Lemma ?= '_Uv',
	true
	.
*/

:-std_prolog check_lexical/3.

check_lexical(Left,Token,Right) :-
%%	format('Try check lexical ~w ~w\n',[Left,Token]),
	'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat
		 },
	    Right),
	( var(Token) ->
	  Lex = Token
	; Token = cat(Cat) ->
	  true
	;  fail,		% not yet ready to activate
	  recorded(robust),
	  \+ recorded(is_covered(Left)),
	  Left > 0,
	  XLeft is Left - 1,
	  recorded(is_covered(XLeft)) ->
	  format('trying added empty coma ~w\n',[Left]),
	  Token = ',',
	  Right = Left
	;
	  (   Lex = Token
	  xor TrueLex = Token
	  xor \+ var(Lemma),
	      Token = Lemma
	  ),
	  %%	format('Checked lexical ~w ~w => ~w\n',[Left,Token,Right]),
	  %%	'C'(Left,lemma{ cat => Token },Right)
	  true
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!lexical([_Lexical],_Left,_Right,_Cat, [_Lemma,_TrueLex,LemmaId]) :-
	'C'(_Left,lemma{ lex => _Lex,
			 truelex => _TrueLex ,
			 lemma => _Lemma,
			 lemma => _LemmaId
		       }, _Right),
	( _Lex == _Lexical
	xor _TrueLex == _Lexical
	xor _Lemma == _Lexical ),
	_Lemma ?= '_Uv'
	.

/*
verbose!lexical([_Lexical :: ','],Left,_Left,_Cat,_Info) :-
	_Info = [_Lexical,_Lexical,_Lexical],
	recorded(robust),
	\+ recorded(is_covered(Left)),
	Left > 0,
	XLeft is Left - 1,
	recorded(is_covered(XLeft)),
	format('added empty coma ~w lexical=~w cat=~w info=~w\n',[Left,_Lexical,_Cat,_Info])
	.
*/

/*
verbose!lexical([L::(_;_)],_Left,_Right,_Cat, [_Lemma,_TrueLex]) :-
%%	format('Check verbose lexical ~w\n',[L]),
	check_verbose_lexical(L,_Left,_Right,_Cat,[_Lemma,_TrueLex]).
*/

:-std_prolog check_verbose_lexical/5.

check_verbose_lexical(L,_Left,_Right,_Cat,[_Lemma,_TrueLex]) :-
	( L = (A1;A2) ->
	  ( check_verbose_lexical(A1,_Left,_Right,_Cat,[_Lemma,_TrueLex])
	  ;  check_verbose_lexical(A2,_Left,_Right,_Cat,[_Lemma,_TrueLex])
	  )
	; L = [_Lexical],
	  'C'(_Left,lemma{ lex => _Lex, truelex => _TrueLex , lemma => _Lemma}, _Right),
	  ( _Lex == _Lexical
	  xor _TrueLex == _Lexical
	  xor _Lemma == _Lexical ),
	  _Lemma ?= '_Uv'
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!struct(_Tree_Name,_HyperTag) :-
%%	format('Recognized ~w\n',[_Tree_Name]),
	true
	.

%% Dummy predicate used to leave a trace in the forest
verbose!anchor(_Anchor,_Left,_Right,_Tree_Name,_Top, _Addr : [_Lemma,_Lex,_LemmaId], tag_anchor{ name => HT }) :-
% 	format('VERBOSE ANCHOR anchor=~w left=~w right=~w lemma=~w lex=~w tree=~w\n\tht=~w\n\ttop=~w\n',
% 	       [_Anchor,_Left,_Right,_Lemma,_Lex,_Tree_Name,HT,_Top]),
	( %fail,
	  _Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1,
	  _Right = gen_pos(R1,R2) ->
%	  format('verbose anchor left=~w right=~w anchor=~w tree=~w top=~w addr=~w ht=~w\n',
%		 [_Left,_Right,_Anchor,_Tree_Name,_Top,_Addr,HT]),
	  /*
	  ( L1=R1 ->
	    recorded('C'(L2,lemma{ top => __Top,
				   cat => _Cat,
				   truelex => _Lex,
				   lex => __Anchor,
				   lemma => _Lemma,
				   lemmaid => _LemmaId,
				   anchor => tag_anchor{ name => HT }
				 }, R2),
		     _Addr)
	  ;
	    recorded('C'(L1,lemma{ top => __Top,
				   cat => _Cat,
				   truelex => _Lex,
				   lex => __Anchor,
				   lemma => _Lemma,
				   lemmaid => _LemmaId,
				   anchor => tag_anchor{ name => HT }
				 }, R1),
		     _Addr)
	  ),
	  (_Anchor = __Anchor xor _Anchor = _Lemma ),
	  (   cat_normalize(_Cat,__Top) xor true ),
	  _Top = __Top,
	  _Lemma ?= '_Uv'
*/
	  true
	;
	  recorded('C'(_Left,lemma{ top => __Top,
				    cat => _Cat,
				    truelex => _Lex,
				    lex => __Anchor,
				    lemma => _Lemma,
				    lemmaid => _LemmaId,
				    anchor => tag_anchor{ name => HT }
				  }, _Right),
		   _Addr),
	  (_Anchor = __Anchor xor _Anchor = _Lemma ),
	  (   cat_normalize(_Cat,__Top) xor true ),
	  _Top = __Top,
	  _Lemma ?= '_Uv'
	)
	.

%% Dummy predicate used to leave a trace in the forest
verbose!coanchor(_Lex,_Left,_Right,_Top,[_Lemma,_TrueLex,_LemmaId]) :-
	( %fail,
	  _Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1,
	  _Right = gen_pos(R1,R2) ->
	  (L1=R1 -> _L=L2, _R=R2 ; _L = L1, _R=R1)
	;
	  _L=_Left,
	  _R=_Right
	),
	( _Lex  = ellipsis(_XLex) xor _Lex = _XLex),
	'C'(_L,
	    lemma{ top => __Top,
		   cat => _Cat,
		   lex => _XLex,
		   truelex => _TrueLex,
		   lemma => _Lemma,
		   lemmaid => _LemmaId
		 },
	    _R),
	(   cat_normalize(_Cat,__Top) xor true ),
	_Top = __Top,
	%%	format('VERBOSE COANCHOR ~w ~w ~w ~w\n',[_Left,_Lemma,_TrueLex,_Right]),
	_Lemma ?= '_Uv'
	.
	
%% Dummy predicate used to leave a trace in the forest
verbose!adj.

:-rec_prolog normalize_coanchors/1.

normalize_coanchors([]).
normalize_coanchors([VarCoanchors|VC]) :-
	(VarCoanchors = [] xor true ),
	normalize_coanchors(VC)
	.

:-std_prolog tag_family_load/7.

tag_family_load(HyperTag,Cat,Top,Token,Name,Left,Right) :-
%%	format('TAG FAMILY LOADER cat=~w left=~w ht=~w name=~w\n',[Cat,Left,HyperTag,Name]),
	(   %%format('** Hypertag = ~w\n',[Hypertag]),
	    'D'(Cat,DLeft),
	    domain(Left,DLeft),
	    'C'(Left,
		lemma{ cat => Cat,
		       top => _Top,
		       lex => Lex,
		       lemma => Lemma,
		       anchor => tag_anchor{ name => _HyperTag }
		     },
		Right),
	    (	cat_normalize(Cat,_Top) xor true),
	    %% Next line to handle support verbs (should find a more elegant way)
	    %% The trouble is that support verbs inherits their frame from arg1
	    %% but this inheritance is done after the autoloading phase (via clean_sentences)
	    %% April 2011: not sure it works (no ncpredobj in hypertags !)
	    \+ \+ ( 
		    Top = _Top,
		    (HyperTag = _HyperTag
		    xor autoload_check_hypertag(Cat,HyperTag,_HyperTag)),
		    (	  Lex = Token
		    xor (\+ var(Lemma), Lemma = Token)
		    )
		  ),
	    assert_anchor_points(Cat,HyperTag,Top,Name,Token),
%	    format('--> To be loaded cat=~w name=~w ~w ~w ~w\n',[Cat,Name,HyperTag,Left,Right]),
	    true
	)
	xor fail
	.


%:-std_prolog assert_anchor_points/5.

:-xcompiler
assert_anchor_points(Cat,HyperTag,Top,Name,Token) :-
	( recorded( XPoint::require_anchor(Name) )
	xor
	  record( XPoint ),
	  every((
		 'D'(Cat,DLeft),
		 domain(Left,DLeft),
		 \+  recorded( Point::anchor_point(Name,Left,Cat) ),
		 'C'(Left,
		      lemma{ cat => Cat,
			     top => Top,
			     lex => Lex,
			     lemma => Lemma,
			     anchor => tag_anchor{ name => HyperTag }
			 },
		      _
		     ),
		 ( Lex = Token xor (\+ var(Lemma), Lemma = Token)),
		 record( Point ),
%%		 format('anchor point ~w ~w\n',[Name,Left]),
		 true
		)
	       )
	)
	.
	
%:-std_prolog tag_check_coanchor/6.

:-xcompiler
tag_check_coanchor(Cat,Top,Token,Left,Right,Name) :-
%%	format('search coanchor ~w at ~w top=~w token=~w\n',[Cat,Left,Top,Token]),
	(   'D'(Cat,DLeft),
	    domain(Left,DLeft),
	    'C'(Left,
		X::lemma{ cat => Cat, lex => Lex, lemma => Lemma, lemmaid => LemmaId, top => _Top },
		Right),
	    (	cat_normalize(Cat,_Top) xor true),
	    \+ \+ ( Top = _Top,
		      (	  Lex = Token
		      xor (\+ var(LemmaId), LemmaId = Token)
		      xor (\+ var(Lemma), Lemma = Token)
		      )),
	    /* still unsafe
	    every((
		   domain(_Left,DLeft),
		   Left =< _Left, % Left is the smallest possible place for a coanchor
		   \+ recorded( Point::coanchor_point(Name,_Left) ),
		   record( Point )
		  )),
	    */
	    true
	)  xor fail
%%	format('checked coanchor ~w at ~w\n',[X,Left])
	.

%:-std_prolog tag_check_foot/6.

:-xcompiler
tag_check_foot(Cat,Top,Left,Right,Name,Mode) :-
%%	format('search foot ~w at left=~w right=~w\n',[Cat,Left,Right]),
	( 'D'(Cat,DLeft),
	  domain(Left,DLeft),
	  'C'(Left,X::lemma{ cat => Cat },Right),
	  every(( Mode == left,
		  domain(_Left,DLeft),
		  Left =< _Left, %% Left is the smallest possible place for a foot
		  \+ recorded( Point::foot_point(Name,_Left) ),
		  record( Point )
		))
	) xor fail
	%% format('checked foot ~w at ~w\n',[X,Left]),
	%% true
	.

%:-std_prolog tag_check_subst/5.

:-xcompiler
tag_check_subst(Cat,InCats,Left,Right,Name) :-
	( domain(InCat,InCats),
	  'D'(InCat,DLeft),
	  'C'(Left,lemma{ cat => InCat },Right),
	  /* still unsafe
	  every(( domain(_InCat,InCats),
		  'D'(_InCat,_DLeft),
		  domain(_Left,_DLeft),
		  \+ recorded( Point::subst_point(Name,_Left) ),
		  record( Point )
		))
	  */
	  true
	) xor fail
	.

%% defined in main
:-std_prolog tag_autoload_adj/7.
	  
:-extensional ok_tree/2.
:-extensional lctag/4.
%%:-light_tabular tag_filter/1.

:-std_prolog tag_filter_init/0.

:-extensional lctag_ok/1.
:-extensional lctag_map/3.
:-extensional lctag_map_reverse/3.
:-extensional ok_cat/3.
:-extensional loaded_tree/1.

:-std_prolog check_ok_cat/4.

check_ok_cat(Cat,Mode,Left,Right) :-
%	format('Check ok left=~w cat=~w mode=~w\n',[Left,Cat,Mode]),
	(  var(Left) ->
	  ( lctag_map(Cat,Mode,Tree),
	    ( lctag_ok(Tree)  xor check_ok_tree(Tree,Left))
	  xor fail
	  )
	;  %fail,
	   Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1
	->
	   ( Mode = subst ->
	     check_ok_cat(Cat,Mode,L2,Right)
%	     check_ok_cat(Cat,Mode,L1,Right)
	   ;
	     check_ok_cat(Cat,Mode,L1,Right)
	   ),
	   true
	; ok_cat(Cat,Mode,Left) ->
	   %% ok_cat are installed by tag_filter, or by the following case
%%	   control_continuations(Cat,Mode,Left),
	   true
	; lctag_map(Cat,Mode,Tree),
	  lctag_ok(Tree)
	  ->
	   %% Tree has no lctag constraints
	   %% il may be started anywhere
	   record(ok_cat(Cat,Mode,_)),
%%	   control_continuations(Cat,Mode,Left),
	   true
	;
	   fail
	),
%	format('cat selected left=~w cat=~w mode=~w right=~w\n',[Left,Cat,Mode,Right] ),
	true
	.

:-std_prolog control_continuations/3.

control_continuations(Cat,Mode,Left) :-
	( recorded( control_cont(Left,Cat,Mode,M) ) ->
	  mutable_inc(M,V),
	  V < 100
	; mutable(M,1),
	  record( control_cont(Left,Cat,Mode,M) )
	)
	.

:-std_prolog check_ok_tree/2.

check_ok_tree(Name,Left) :-
%	format('try check at ~w for ~w\n',[Left,Name]),
	( var(Left) ->
	  ( ok_tree(Name,_Left) xor fail )
	; %fail,
	  Left = gen_pos(L1,L2), number(L1), number(L2), L2 < L1 ->
	  (ok_tree(Name,L1) xor ok_tree(Name,L2)),
	  true
	; ok_tree(Name,Left),
	  true
	),
%	format('tree selected left=~w tree=~w\n',[Left,Name] ),
	true
	.

:-extensional
	require_anchor/1,
	anchor_point/3,
	coanchor_point/2,
	foot_point/2,
	subst_point/2,
	added_correction/0
	.

tag_filter_init :-
	%% remove some anchoring points that were found at autoload time
	%% but are no longer valid after clean_sentence
	every(( argv(Argv),
		\+ domain('-no_lctag',Argv),
		K::anchor_point(Name,Left,Cat),
		\+ 'C'(Left,lemma{ cat => Cat },_),
		erase( K )
	      ))
	.

tag_filter(Left) :-
	argv(Argv),
	domain('-no_lctag',Argv),
	recorded('N'(Left)),
	lctag(_,_,Name,_),
	\+ recorded( block_tree(Name,Left) ),
	\+ recorded(OK::ok_tree(Name,Left)),
	record(OK),
	every(( lctag_map_reverse(Name,_Cat,_Mode),
		\+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Left)),
		record(OK_CAT)
	      )),
	true
	.

tag_filter(Left) :-
	argv(Argv),
	domain('-no_lctag',Argv),
	'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat
		 },
	    Right),
	lctag(_,_,Name,_),
	\+ recorded( block_tree(Name,Left) ),
	\+ recorded(OK::ok_tree(Name,Left)),
	record(OK),
	every(( lctag_map_reverse(Name,_Cat,_Mode),
		\+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Left)),
		record(OK_CAT)
	      )),
	true
	.

tag_filter(Left) :-
%%	format('Try LC selected ~w ~w\n',[Left,Name]),
	argv(Argv),
	\+ domain('-no_lctag',Argv),
	'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat,
		   top => Top
		 },
	    Right),
	( cat_normalize(Cat,Top) xor true ),
	%% check basic trees first
	( (
	   lctag(cat,Cat:Top,Name,Trees)
%	   Kind = cat
	  ; lctag(scan,Lex,Name,Trees)
%	   Kind = scan
	  ; Lex \== TrueLex,
	   lctag(scan,TrueLex,Name,Trees)
%	   Kind = scan
	  ; lctag(scan,Lemma,Name,Trees)
%	   Kind = scan
	  ; lctag(pos,Left,Name,Trees)
%	   Kind = pos
	  ; lctag(special,special,Name,Trees),
	   special_lctag(Name,Left),
%	   Kind = special,
	   true
	  ; Cat = skipcat[],
	    '$answers'(tag_filter(Right)),
	    ok_tree(Name,Right),
	    Kind = skip
	  ),
	  \+ recorded( block_tree(Name,Left) ),
	  \+ recorded(OK::ok_tree(Name,Left)),
%%	    format('potential ok_tree left=~w tree=~w\n',[Left,Name]),
	  ( Kind == skip
	  xor
	  (  ( Trees = [] ->
	       check_close_anchor_point(Name,Left)
	     ;
%	       format('try first lctag left=~w trees=~w name=~w\n',[Left,Trees,Name]),
	       check_first_lctag_tree(Trees,Left),
%	       format('ok first lctag left=~w trees=~w name=~w\n',[Left,Trees,Name]),
	       check_anchor_point(Name,Left)
	     ),
%%	    check_coanchor_point(Name,Left),
	    check_foot_point(Name,Left),
%%	    check_subst_point(Name,Left),
	    true
	  )
	  xor fail
	  ),
	  record(OK),
	  every(( lctag_map_reverse(Name,_Cat,_Mode),
		  \+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Left)),
		  record(OK_CAT)
		)),
%%	    format('ok_tree left=~w kind=~w cat=~w lex=~w tree=~w\n',[Left,Kind,Cat,Lex,Name]),
	  fail
	%%	  format('ok_tree left=~w tree=~w\n',[Left,Name]),
	;
	  %%	tabulated_tag_filter(Name,Left),
	  %%	format('LC selected ~w ~w\n',[Left,Name]),
	  true
	)
	.

tag_filter(Right) :-
%%	format('Try LC selected ~w ~w\n',[Left,Name]),
	argv(Argv),
	\+ domain('-no_lctag',Argv),
	'C'(Left,
	    lemma{ lex => Lex,
		   truelex => TrueLex,
		   lemma => Lemma,
		   cat => Cat,
		   top => Top
		 },
	    Right),
%	( cat_normalize(Cat,Top) xor true ),
	( 
	  ( lctag(postscan,Lex,Name,Trees)
	  ; Lex \== TrueLex,
	    lctag(postscan,TrueLex,Name,Trees)
	  ; lctag(postcat,Cat:Top,Name,Trees)
	  ; lctag(pos,end,Name,Trees),
	    recorded('N'(Right))
	  ),
	  \+ recorded( block_tree(Name,Left) ),
	  \+ recorded(OK::ok_tree(Name,Right)),
	  (  ( Trees = [] ->
	       check_close_anchor_point(Name,Right)
	     ;
	       check_first_lctag_tree(Trees,Right),
	       check_anchor_point(Name,Right)
	     ),
%%	    check_coanchor_point(Name,Right),
	    check_foot_point(Name,Right),
%%	    check_subst_point(Name,Right),
	    true
	  xor fail ),
	  record(OK),
	  every(( lctag_map_reverse(Name,_Cat,_Mode),
		  \+ recorded( OK_CAT::ok_cat(_Cat,_Mode,Right)),
		  record(OK_CAT)
		)),
	  fail
	;
	  true
	)
	.

%%:-std_prolog check_first_lctag_tree/1.

:-xcompiler((

check_first_lctag_tree(Trees,Left) :-
	( domain(Name,Trees),
	  loaded_tree(Name),
	  check_close_anchor_point(Name,Left)
	->
	  true
	;
	  fail
	)

	))
	.
	
:-light_tabular special_lctag/2.
:-mode(special_lctag/2,+(+,-)).

%:-std_prolog check_anchor_point/2.

:-xcompiler((
check_anchor_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ require_anchor(Name) ->
	  true
	; anchor_point(Name,_Left,Cat),
	  Left =< _Left ->
	  true
	;
	  fail
	)
	))
	.

:-extensional distance_constraint/2.

:-xcompiler((
check_close_anchor_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ require_anchor(Name) ->
	  true
	; anchor_point(Name,_Left,Cat),
	  Left =< _Left ->
	  true
	;
	  fail
	)
	
	))
	.



:-xcompiler((
check_coanchor_point(Name,Left) :- % still unsafe
%%	recorded(loaded_tree(Name)),
	( \+ coanchor_point(Name,_) ->
	  true
	; coanchor_point(Name,_Left),
	  Left =< _Left   ->
	  true
	;
	  fail
	)
	))
	.

:-xcompiler((
check_foot_point(Name,Left) :-
%%	recorded(loaded_tree(Name)),
	( \+ foot_point(Name,_) ->
	  true
	; foot_point(Name,_Left),
	  Left =< _Left,
%%	  format('foot point left=~w _left=~w name=~w\n',[Left,_Left,Name]),
	  %% foot point in left tig tree comes last (after any anchor point)
	  ( \+ require_anchor(Name)
	  xor
	  anchor_point(Name,_LeftAnchor,_),
	    %%	    format('anchor+foot point left=~w _left=~w _leftanchor=~w name=~w\n',[Left,_Left,_LeftAnchor,Name]),
	    _LeftAnchor < _Left,
	    Left =< _LeftAnchor 
	  ),
	  true
	->
	  true
	;
	  fail
	)
	))
	.

:-xcompiler((
check_subst_point(Name,Left) :-	% still unsafe
%%	recorded(loaded_tree(Name)),
	( \+ subst_point(Name,_) ->
	  true
	; subst_point(Name,_Left),
	  Left =< _Left   ->
	  true
	;
	  fail
	)
	))
	.

correction_anchor(_).

register_correction(Cat,Left,Right,Token,Info,Top,_Top,TLex,Lemma) :-
	( \+ added_correction,
	  record( added_correction ),
	  fail
	; ( Left = (Left1:Left2) xor Left1 = Left, Left2=Left),
	  record( 'C'(Left1,
		      lemma{ top => Top,
			     lex => Token,
			     cat => Cat,
			     truelex => TLex,
			     lemma => Lemma,
			     anchor => Info
			   },
		      Right
		     )
		),
	  every((
		 continuation_at(_Left,Addr),
		 _Left =< Left1,
		 _Left > Left2 - 3,
		 '$reschedule'(Addr)
		))
	)
	.

:-light_tabular continuation_at/2.
:-mode(continuation_at/2,+(-,-)).

continuation_at(Left,Addr) :-
	recorded(O,Addr),
%%	format('found item to rechedule ~w\n',[O]),
	domain(O,
	       ['*RITEM*'(Call,Ret),
		'*CITEM*'(Call,Call),
		'*SACITEM*'(Call)
	       ]),
	( item_term( '*RITEM*'(Call,Ret), T (_Left,_Right) )
	;  item_term( '*RITEM*'(Call,Ret), T1 (_Left,_Left) * T2 (_Left,Right) )
	),
	( O = '*CITEM*'(Call,Call) -> Left = _Left
	; O = '*SACITEM*'(Call) -> Left = _Left
	; O = '*RITEM*'(Call,Ret) -> Left = _Right
	)
	.


