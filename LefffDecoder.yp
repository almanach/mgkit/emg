%{
##  use Data::Dumper;
%}

%strict
%token NUM IDENT LIDENT MACRO OP
%start entries
%syntactic token ',' ';' '|'
%tree

%%

entries : fs ';'          {  $_[1] } /* macrodef */
;

fs : '[' fv <* ','> ']'   { TAG::Feature->new( { f => { map( @$_, @{$_[2]->{children}}) } } ) }
;

fv : 
  IDENT.x OP value.v    {  [main::features(main::normalize_feature($x) => $v ) ] }
  | MACRO.m             { main::macro_expand($m) }
;

value : fs.v NUM? 
      {  my $val = [$v];  @{$_[2]->{children}} ? [TAG::Var->new({name => $_[2]->{children}[0], value => $val }) ] : $val }
  | atom <+ '|'>       { $_[1]->{children} }
  | syntax             { [ $_[1] ] }
;

atom : '+'       { TAG::Plus->new() }
     | '-'       { TAG::Minus->new() }
     | IDENT     { TAG::Val->new( { text => main::normalize_value($_[1]) } ) }
     | NUM     { TAG::Val->new( { text => $_[1] } ) }
;

syntax : '"' lemma subcat xsubcat '"'  { main::syntax($_[0],$_[2],$_[3]) }
;

lemma : {$_[0]->{lemma} = 1} LIDENT.lemma {undef $_[0]->{lemma}; $lemma}
;

subcat :  '<' arg <+ ','> '>'    { $_[2]->{children} }
       |  { [] } # EMPTY     
;

arg : argfun ':' realarg { { kind => $_ [1], real => $_[3] } }
    | realarg            { { real => $_[1] } }
;

realarg : atom <+ '|'>           { $_[1]->{children} }
  | '(' atom <+ '|'> ')'     {  [@{$_[2]->{children}},TAG::Minus->new()] }
;

argfun : IDENT        { $_[1] }
;

xsubcat : argfun  { { kind => $_[1] }}
        | { [] } # EMPTY
;

%%

sub make_lexer {
  my $input = shift;

  sub  {
     my($parser)=shift;
  
     for ($$input) {
       m{\G\s*}gc;
    if ($parser->{lemma} and m{\G((?:[^"<]|\\["<\\])+)}gc) {
       my $v = $1;
       $v =~ s/\\(.)/$1/og;
##       print "%% LEMMA $v\n";
       return ('LIDENT',$v);
    }
    m{\G(\@+[[:alnum:]�������������������?_-]+)}gc and return ('MACRO',$1);
    m{\G(\d+)(?!\w)}gc and return('NUM',$1);
    m{\G(==|=c(?=\s+)|=\?|=)}gc and return ('OP',$1);
    if (m{\G((?:[[:alnum:]��������������������������������������_'-]|\\["\\])+)}gc) {
      my $v = $1;
      $v =~ s/\\(.)/$1/og;
##      print "%% IDENT $v\n";
      return('IDENT',$v);
    }
    m{\G(\%[^\s;|]+)}gc and return ('INFO',$1);
    m{\G(.)}gcc and return($1,$1);
    }
  }
}

sub Run {
  my ($self)=shift;
  my $input = shift or die "no input given\n";
  $self->YYParse( yylex => make_lexer(\$input), 
		  yyerror => sub {
		    my $parser = shift;
		    my $token = $parser->YYCurval;
                    die  "Syntax error near '$token' in $input\n";
		  },
##		  yydebug => 0xF
		);
}

sub _Error {
  my $parser = shift;
  my $token = $parser->YYCurval;
  die  "Syntax error near $token.\n";
}
